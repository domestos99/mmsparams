package cz.uhk.bak.mmsparams.pdu;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.NotifyRespInd;
import com.google.android.mms.pdu_alt.PduHeaders;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cz.uhk.bak.mmsparams.jobs.PduByteHelper;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;


@RunWith(PowerMockRunner.class)
@PrepareForTest(TextUtils.class)
public class PduDRTest
{

    Context context;

    @Before
    public void setUp() throws Exception
    {
        context = mock(Context.class);

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(any(CharSequence.class))).thenAnswer(new Answer<Boolean>()
        {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable
            {
                CharSequence a = (CharSequence) invocation.getArguments()[0];
                return !(a != null && a.length() > 0);
            }
        });
    }


    @Test
    public void test2() throws InvalidHeaderValueException
    {
        final MySendReq send = new MySendReq();


        send.setDrmContent(PduHeaders.VALUE_YES);
        send.setAdaptationAllowed(PduHeaders.VALUE_YES);

        final byte[] bytes = PduByteHelper.getPduBytes(context, send);


        final MySendReq sr = PduByteHelper.getPdu(bytes);

        assertEquals(sr.getDrmContent(), PduHeaders.VALUE_YES);
        assertEquals(sr.getAdaptationAllowed(), PduHeaders.VALUE_YES);


    }

    @Test
    public void test1() throws InvalidHeaderValueException
    {
        byte[] transactionId = new byte[]{
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        };

        NotifyRespInd notifyResponse = new NotifyRespInd(PduHeaders.CURRENT_MMS_VERSION,
                transactionId,
                PduHeaders.STATUS_RETRIEVED);


        notifyResponse.setReportAllowed(PduHeaders.VALUE_NO);
        assertEquals(notifyResponse.getReportAllowed(), PduHeaders.VALUE_NO);


        notifyResponse.setReportAllowed(PduHeaders.VALUE_YES);
        assertEquals(notifyResponse.getReportAllowed(), PduHeaders.VALUE_YES);


    }

}
