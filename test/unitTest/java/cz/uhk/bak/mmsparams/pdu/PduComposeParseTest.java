//package cz.uhk.bak.mmsparams.pdu;
//
//import android.content.Context;
//import android.text.TextUtils;
//import cz.uhk.bak.mmsparams.log.LogFB;
//import android.webkit.MimeTypeMap;
//
//import com.android.mms.dom.smil.parser.SmilXmlSerializer;
//import com.google.android.mms.ContentType;
//import com.google.android.mms.InvalidHeaderValueException;
//import com.google.android.mms.pdu_alt.CharacterSets;
//import com.google.android.mms.pdu_alt.EncodedStringValue;
//import com.google.android.mms.pdu_alt.MyPduComposer;
//import com.google.android.mms.pdu_alt.MyPduParser;
//import com.google.android.mms.pdu_alt.MySendReq;
//import com.google.android.mms.pdu_alt.PduBody;
//import com.google.android.mms.pdu_alt.PduHeaders;
//import com.google.android.mms.pdu_alt.PduPart;
//import com.google.android.mms.smil.SmilHelper;
//import com.klinker.android.send_message.Utils;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.powermock.modules.junit4.PowerMockRunner;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.util.List;
//
//import cz.uhk.bak.mmsparams.BaseUnitTest;
//import cz.uhk.bak.mmsparams.attachments.Attachment;
//import cz.uhk.bak.mmsparams.crypto.MasterSecret;
//import cz.uhk.bak.mmsparams.database.model.MmsSetting;
//import cz.uhk.bak.mmsparams.mms.MediaConstraints;
//import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
//import cz.uhk.bak.mmsparams.mms.PartAuthority;
//import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;
//import cz.uhk.bak.mmsparams.util.Util;
//
//import static org.mockito.Mockito.mock;
//
//
//@RunWith(PowerMockRunner.class)
//public class PduComposeParseTest extends BaseUnitTest
//{
//
//    @Test
//    public void Test1()
//    {
//        Context context = mock(Context.class);
//
//        MySendReq send = constructSendPdu();
//f
//
//        byte[] bytes =  new MyPduComposer(context, send).make();
//
//
//        MySendReq sr = (MySendReq)new MyPduParser(bytes).parse();
//
//
//    }
//
//
//    private MySendReq constructSendPdu()
//    {
//        MySendReq req = new MySendReq();
//
//
//        req.addTo(new EncodedStringValue("+420123456789"));
//
//
//        req.setDate(System.currentTimeMillis() / 1000);
//
//        PduBody body = new PduBody();
//        int size = 0;
//
//
//        PduPart part = new PduPart();
//        String name = String.valueOf(System.currentTimeMillis());
//        part.setData(Util.toUtf8Bytes("xxxx"));
//        part.setCharset(CharacterSets.UTF_8);
//        part.setContentType(ContentType.TEXT_PLAIN.getBytes());
//        part.setContentId(name.getBytes());
//        part.setContentLocation((name + ".txt").getBytes());
//        part.setName((name + ".txt").getBytes());
//
//        body.addPart(part);
//        size += getPartSize(part);
//
//
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(body), out);
//        PduPart smilPart = new PduPart();
//        smilPart.setContentId("smil".getBytes());
//        smilPart.setContentLocation("smil.xml".getBytes());
//        smilPart.setContentType(ContentType.APP_SMIL.getBytes());
//        smilPart.setData(out.toByteArray());
//        body.addPart(0, smilPart);
//
//        req.setBody(body);
//        req.setMessageSize(size);
//
//        req.setMessageClass(PduHeaders.MESSAGE_CLASS_ADVERTISEMENT_STR.getBytes());
//        req.setExpiry(7 * 24 * 60 * 60);
////        req.setMessageClass(PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes());
////        req.setExpiry(7 * 24 * 60 * 60);
//
//        try
//        {
//            req.setPriority(PduHeaders.PRIORITY_HIGH);
//            req.setDeliveryReport(PduHeaders.VALUE_YES);
//            req.setReadReport(PduHeaders.VALUE_YES);
//
//            req.setDeliveryTime(System.currentTimeMillis() / 1000);
//        }
//        catch (InvalidHeaderValueException e)
//        {
//        }
//
//        return req;
//    }
//
//    private long getPartSize(PduPart part)
//    {
//        return part.getName().length + part.getContentLocation().length +
//                part.getContentType().length + part.getData().length +
//                part.getContentId().length;
//    }
//
//}
