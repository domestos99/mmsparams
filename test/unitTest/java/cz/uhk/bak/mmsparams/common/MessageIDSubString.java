package cz.uhk.bak.mmsparams.common;

import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;

public class MessageIDSubString
{


    @Test
    public void Test1()
    {
        String s = "fds1f3ds13fsd13fsda@mmsc1";

        String a = magic(s);

        assertNotNull(a);
        assertNotSame("fds1f3ds13fsd13fsda", a);


    }

    private String magic(String s)
    {
        int i = s.indexOf('@');
        return s.substring(0, i);
    }


}
