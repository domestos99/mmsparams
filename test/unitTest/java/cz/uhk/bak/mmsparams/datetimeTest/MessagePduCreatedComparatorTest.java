//package cz.uhk.bak.mmsparams.datetimeTest;
//
//import com.google.android.mms.pdu_alt.PduHeaders;
//
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.Collections;
//
//import cz.uhk.bak.mmsparams.database.model.MessagePdu;
//
//import static junit.framework.Assert.assertEquals;
//
//public class MessagePduCreatedComparatorTest
//{
//
//    @Test
//    public void sortTest() throws InterruptedException
//    {
//        ArrayList<MessagePdu> pdus = new ArrayList<>();
//
//        MessagePdu one = new MessagePdu(1, 1, PduHeaders.MESSAGE_TYPE_SEND_REQ, null);
//        MessagePdu two = new MessagePdu(2, 1, PduHeaders.MESSAGE_TYPE_SEND_REQ, null);
//
//        pdus.add(one);
//        pdus.add(two);
//
//
//        pdus.get(1).setCreatedNow();
//        Thread.sleep(2000);
//        pdus.get(0).setCreatedNow();
//
//
//        Collections.sort(pdus, new MessagePdu.MessagePduCreatedComparator());
//
//
//        assertEquals(pdus.get(0), two);
//        assertEquals(pdus.get(1), one);
//    }
//
//
//    @Test
//    public void sort2Test() throws InterruptedException
//    {
//        ArrayList<MessagePdu> pdus = new ArrayList<>();
//
//        MessagePdu one = new MessagePdu(1, 1, PduHeaders.MESSAGE_TYPE_SEND_REQ, null);
//        MessagePdu two = new MessagePdu(2, 1, PduHeaders.MESSAGE_TYPE_SEND_REQ, null);
//
//        pdus.add(one);
//        pdus.add(two);
//
//
//        pdus.get(0).setCreatedNow();
//        Thread.sleep(2000);
//        pdus.get(1).setCreatedNow();
//
//
//        Collections.sort(pdus, new MessagePdu.MessagePduCreatedComparator());
//
//
//        assertEquals(pdus.get(0), one);
//        assertEquals(pdus.get(1), two);
//    }
//
//    @Test
//    public void sort3Test() throws InterruptedException
//    {
//        ArrayList<MessagePdu> pdus = new ArrayList<>();
//
//        MessagePdu one = new MessagePdu(1, 1, PduHeaders.MESSAGE_TYPE_SEND_REQ, null);
//        MessagePdu two = new MessagePdu(2, 1, PduHeaders.MESSAGE_TYPE_SEND_REQ, null);
//
//        pdus.add(one);
//        pdus.add(two);
//
//
//        pdus.get(0).setCreatedNow();
//        pdus.get(1).setCreatedNow();
//
//
//        Collections.sort(pdus, new MessagePdu.MessagePduCreatedComparator());
//
//
//        assertEquals(pdus.get(0), one);
//        assertEquals(pdus.get(1), two);
//    }
//}
