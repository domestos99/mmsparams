package cz.uhk.bak.mmsparams.datetimeTest;

import org.junit.Test;

import cz.uhk.bak.mmsparams.components.relativeTimePicker.DateTimePair;

import static junit.framework.Assert.assertEquals;

public class DateTimePairTest
{

    @Test
    public void testDateTimePairTest() throws Exception
    {
        for (long i = 0; i < Integer.MAX_VALUE; i = (i + 1) * 2)
        {
            DateTimePair aa = new DateTimePair(i);
            assertEquals(i, aa.getTotalSeconds());
        }
    }

    @Test
    public void testDateTimePairTest2() throws Exception
    {

        DateTimePair aa = new DateTimePair(1000000);
        assertEquals(11, aa.getDay());
        assertEquals(13, aa.getHour());
        assertEquals(46, aa.getMinute());
        assertEquals(40, aa.getSecond());

    }

    @Test
    public void testDateTimePairTest3() throws Exception
    {

        DateTimePair aa = new DateTimePair(11, 13, 46, 40);
        assertEquals(1000000, aa.getTotalSeconds());

    }
}
