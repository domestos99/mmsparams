package cz.uhk.bak.mmsparams.json;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.database.model.MmsProfileBLO;
import cz.uhk.bak.mmsparams.database.model.MmsProfilesFactory;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.database.model.MmsTemplateBLO;
import cz.uhk.bak.mmsparams.database.model.MmsTemplateDetailGroup;
import cz.uhk.bak.mmsparams.util.JsonUtils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;


public class JsonTest
{
    Context context;

    @Before
    public void Init()
    {
        mmsProfile = MmsProfilesFactory.getDefault();

        context = mock(Context.class);

    }

    MmsProfile mmsProfile;


    @Test
    public void JsonSerialize1Test() throws IOException
    {
        String json = JsonUtils.toJson(mmsProfile);
        assertNotNull(json);
        assertNotSame("", json);

    }

    @Test
    public void JsonDeserialize1Test() throws IOException
    {
        String json = JsonUtils.toJson(mmsProfile);

        MmsProfile obj = JsonUtils.fromJson(json, MmsProfile.class);

        assertNotNull(obj);

        assertEquals(mmsProfile.getID(), obj.getID());
        assertEquals(mmsProfile.getName(), obj.getName());
        assertEquals(mmsProfile.getMessage_class(), obj.getMessage_class());
        assertEquals(mmsProfile.getPriority(), obj.getPriority());
        assertEquals(mmsProfile.getExpiryTime(), obj.getExpiryTime());
        assertEquals(mmsProfile.getDeliveryTime(), obj.getDeliveryTime());
        assertEquals(mmsProfile.getSenderVisibility(), obj.getSenderVisibility());
        assertEquals(mmsProfile.getIsVirtual(), obj.getIsVirtual());
        assertEquals(mmsProfile.isDeliveryReport(), obj.isDeliveryReport());
        assertEquals(mmsProfile.isReadReport(), obj.isReadReport());
    }


    @Test
    public void MmsSettingBLOTest1()
    {
        MmsProfileBLO blo = MmsProfileBLO.getMmsBlo(mmsProfile);
        String json = MmsProfileBLO.getJson(blo);

        assertNotNull(json);
        assertNotSame("", json);

        MmsProfileBLO blo2 = MmsProfileBLO.getFromJson(json);
        assertNotNull(blo2);


        MmsProfile obj = MmsProfileBLO.getMmsSetting(blo2);


        assertEquals(mmsProfile.getID(), obj.getID());
        assertEquals(mmsProfile.getName(), obj.getName());
        assertEquals(mmsProfile.getMessage_class(), obj.getMessage_class());
        assertEquals(mmsProfile.getPriority(), obj.getPriority());
        assertEquals(mmsProfile.getExpiryTime(), obj.getExpiryTime());
        assertEquals(mmsProfile.getDeliveryTime(), obj.getDeliveryTime());
        assertEquals(mmsProfile.getSenderVisibility(), obj.getSenderVisibility());
        assertEquals(mmsProfile.getIsVirtual(), obj.getIsVirtual());
        assertEquals(mmsProfile.isDeliveryReport(), obj.isDeliveryReport());
        assertEquals(mmsProfile.isReadReport(), obj.isReadReport());


    }


    @Test
    public void Testxxx()
    {
        MmsProfile ms1 = MmsProfilesFactory.getDefault();


        MmsProfile ms2 = MmsProfilesFactory.getDefault();


        MmsProfileBLO blo = MmsProfileBLO.getMmsBlo(ms1);
        String json = MmsProfileBLO.getJson(blo);


        MmsProfileBLO blo2 = MmsProfileBLO.getMmsBlo(ms2);
        String json2 = MmsProfileBLO.getJson(blo);


        assertEquals(json2, json);


    }

    @Test
    public void TestxxxXX() throws IOException
    {

        MmsTemplate tm = new MmsTemplate();

        tm.setCreatedDT(123456);
        tm.setUpdatedDT(97852);

        tm.setMmsTemplateDetailGroup(new MmsTemplateDetailGroup("to", "jkdf", "kjsdfk", "kjfsdklsdů, ", "kdjkldaf"));


        String json1 = JsonUtils.toJson(MmsTemplateBLO.getMmsBlo(tm));

        MmsTemplateBLO blo = JsonUtils.fromJson(json1, MmsTemplateBLO.class);

        MmsTemplate aaa = MmsTemplateBLO.getMmsSetting(blo);

        assertTrue(MmsTemplate.isEqual(aaa, tm));


    }

    @Test
    public void TestxxxXXadfdsdfa() throws IOException
    {

        MmsTemplate tm = new MmsTemplate();

        tm.setCreatedDT(123456);
        tm.setUpdatedDT(97852);

        tm.setMmsTemplateDetailGroup(new MmsTemplateDetailGroup("to", "jkdf", "kjsdfk", "kjfsdklsdů, ", "kdjkldaf"));


        MmsTemplate aaa = new MmsTemplate(tm);

        assertTrue(MmsTemplate.isEqual(aaa, tm));


        aaa.setCreatedDT(6515165);

        assertFalse(MmsTemplate.isEqual(aaa, tm));

    }
}
