package cz.uhk.bak.mmsparams.util;

import android.support.annotation.NonNull;

import org.junit.Test;

import java.io.IOException;
import java.util.Random;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;

public class BytesUtilTest
{
    @Test
    public void ByteUtilTestString() throws IOException, ClassNotFoundException
    {
        final String totoJeMujString = "totJe muj String";
        String s = (String) doRound(totoJeMujString);

        assertNotNull(s);
        assertEquals(totoJeMujString, s);
    }

    @Test
    public void ByteUtilTestInt() throws IOException, ClassNotFoundException
    {
        final int totoJeMujString = new Random().nextInt(999);
        int s = (int) doRound(totoJeMujString);

        assertNotNull(s);
        assertEquals(totoJeMujString, s);
    }

    @Test
    public void ByteUtilTestBoolean() throws IOException, ClassNotFoundException
    {
        final boolean totoJeMujString = new Random().nextBoolean();
        boolean s = (boolean) doRound(totoJeMujString);

        assertNotNull(s);
        assertEquals(totoJeMujString, s);
    }


    @NonNull
    private Object doRound(Object obj) throws IOException, ClassNotFoundException
    {
        byte[] bytes = BytesUtil.toByteArray(obj);
        assertNotNull(bytes);
        assertNotSame(0, bytes.length);
        Object o = BytesUtil.toObject(bytes);
        assertNotNull(o);
        return o;
    }


}
