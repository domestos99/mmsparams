package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import cz.uhk.bak.mmsparams.adapters.MmsProfilesListAdapter;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsProfilesDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.database.model.MmsProfilesFactory;
import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.ActivityResultHelper;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsProfilesListFragment extends BrowseBaseFragment<MmsProfile>
{
    ListView listView;
    MmsProfilesListAdapter adapter;

    private static final int PICK_GLOBAL = 6;

    private android.support.design.widget.FloatingActionButton fab;
    public static final String TAG = MmsProfilesListFragment.class.getSimpleName();

    @Override
    protected void afterBaseInit()
    {
        adapter = new MmsProfilesListAdapter(getApplicationContext(), data);
        listView.setAdapter(adapter);

        super.afterBaseInit();
    }

    @Override
    protected ArrayAdapter getAdapter()
    {
        return adapter;
    }

    @Override
    protected ListView getListView()
    {
        return listView;
    }

    @Override
    protected List<MmsProfile> loadData()
    {
        MmsProfilesDatabase db = DatabaseFactory.getMmsProfileDatabase(getApplicationContext());
        // return db.getAll(" where (" + MmsSetting.MetaData.IS_VIRTUAL + "=0) ");
        List<MmsProfile> d = db.getAll();

        MmsProfile defaultt = MmsProfilesFactory.getFromPref(getContext());
        d.add(0, defaultt);

        return d;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected void setSensitive(int mode)
    {
        super.setSensitive(mode);

        if (mode != -1)
            fab.setVisibility(View.INVISIBLE);

    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_profiles_browse_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        listView = ViewUtil.findById(view, R.id.list);
        fab = ViewUtil.findById(view, R.id.fab);

        setHasOptionsMenu(true);
    }

    @Override
    public void onResume()
    {
        if (allowOpenDetail())
        {
            getActivity().setTitle(R.string.text_menu_mms_settings_browse);
        }
        else
        {
            getActivity().setTitle(R.string.pick_settings);
        }
        super.onResume();
    }


    @Override
    protected void setupListeners()
    {
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                createNew(MmsProfilesDetailFragment.class);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                MmsProfile obj = (MmsProfile) parent.getItemAtPosition(position);

                if (allowOpenDetail())
                {
                    if (position == 0)
                    {
                        handleDefaultMmsSettingClick();
                    }
                    else
                    {
                        // This Activity was called by startActivity
                        openDetail(MmsProfilesDetailFragment.class, obj.getID());
                    }
                }
                else
                {
                    //This Activity was called by startActivityForResult
                    returnResult(obj);
                }

            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        if (super.getMode() < 0)
        {
            inflater.inflate(R.menu.menu_global_mms_setting, menu);
            //inflater.inflate(R.menu.menu_default_mms_setting, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {
            case R.id.menu_default_mms_setting:
                handleDefaultMmsSettingClick();
                return true;

            case R.id.menu_pick_global:
                handlePickGlobal();
                return true;

            case R.id.menu_info:
                showInfo();
                return true;

        }
        return false;
    }

    private void showInfo()
    {
        Toast.makeText(getContext(), R.string.MmsSettings_list_info, Toast.LENGTH_SHORT).show();
    }

    private void handlePickGlobal()
    {
        MmsProfilesListActivity.pickSettings(getActivity(), PICK_GLOBAL);
    }

    @Override
    public void onActivityResult(final int reqCode, int resultCode, Intent data)
    {
        LogFB.w(TAG, "onActivityResult called: " + reqCode + ", " + resultCode + " , " + data);
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode)
        {
            case PICK_GLOBAL:
                handlePickGlobalProfile(data);
                break;
        }
    }


    private void handlePickGlobalProfile(@Nullable Intent intent)
    {
        try
        {
            MmsProfile ms = ActivityResultHelper.handleMmsSettingsPicked(intent);

            TextSecurePreferences.setMmsSettingProfileObj(getContext(), ms);
            adapter = new MmsProfilesListAdapter(getApplicationContext(), super.data);
            listView.setAdapter(adapter);
            super.refreshData();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void handleDefaultMmsSettingClick()
    {
        openDetail(MmsProfilesDetailFragment.class, -1, MmsProfilesDetailFragment.DEFAULT_MMS_SETTINGS_MODE);
    }


    @Override
    public boolean handleOnClosing()
    {
        BaseFragment bf = super.getActiveFragment();

        if (bf == null)
            return super.handleOnClosing();
        else
        {
            return bf.handleOnClosing();
        }
    }

    private OnFragmentInteractionListener mListener;


    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


}
