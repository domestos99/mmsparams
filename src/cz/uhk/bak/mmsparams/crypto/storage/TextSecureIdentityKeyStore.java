package cz.uhk.bak.mmsparams.crypto.storage;

import android.content.Context;

import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.state.IdentityKeyStore;
import org.whispersystems.libsignal.util.guava.Optional;

import java.util.concurrent.TimeUnit;

import cz.uhk.bak.mmsparams.crypto.IdentityKeyUtil;
import cz.uhk.bak.mmsparams.crypto.SessionUtil;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.IdentityDatabase;
import cz.uhk.bak.mmsparams.database.IdentityDatabase.VerifiedStatus;
import cz.uhk.bak.mmsparams.database.identity.IdentityRecord;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.util.IdentityUtil;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class TextSecureIdentityKeyStore implements IdentityKeyStore
{

    private static final int TIMESTAMP_THRESHOLD_SECONDS = 5;

    private static final String TAG = TextSecureIdentityKeyStore.class.getSimpleName();
    private static final Object LOCK = new Object();

    private final Context context;

    public TextSecureIdentityKeyStore(Context context)
    {
        this.context = context;
    }

    @Override
    public IdentityKeyPair getIdentityKeyPair()
    {
        return IdentityKeyUtil.getIdentityKeyPair(context);
    }

    @Override
    public int getLocalRegistrationId()
    {
        return TextSecurePreferences.getLocalRegistrationId(context);
    }

    public boolean saveIdentity(SignalProtocolAddress address, IdentityKey identityKey, boolean nonBlockingApproval)
    {
        synchronized (LOCK)
        {
            IdentityDatabase identityDatabase = DatabaseFactory.getIdentityDatabase(context);
            Recipients recipients = RecipientFactory.getRecipientsFromString(context, address.getName(), true);
            long recipientId = recipients.getPrimaryRecipient().getRecipientId();
            Optional<IdentityRecord> identityRecord = identityDatabase.getIdentity(recipientId);

            if (!identityRecord.isPresent())
            {
                LogFB.w(TAG, "Saving new identity...");
                identityDatabase.saveIdentity(recipientId, identityKey, VerifiedStatus.DEFAULT, true, System.currentTimeMillis(), nonBlockingApproval);
                return false;
            }

            if (!identityRecord.get().getIdentityKey().equals(identityKey))
            {
                LogFB.w(TAG, "Replacing existing identity...");
                VerifiedStatus verifiedStatus;

                if (identityRecord.get().getVerifiedStatus() == VerifiedStatus.VERIFIED ||
                        identityRecord.get().getVerifiedStatus() == VerifiedStatus.UNVERIFIED)
                {
                    verifiedStatus = VerifiedStatus.UNVERIFIED;
                }
                else
                {
                    verifiedStatus = VerifiedStatus.DEFAULT;
                }

                identityDatabase.saveIdentity(recipientId, identityKey, verifiedStatus, false, System.currentTimeMillis(), nonBlockingApproval);
                IdentityUtil.markIdentityUpdate(context, recipients.getPrimaryRecipient());
                SessionUtil.archiveSiblingSessions(context, address);
                return true;
            }

            if (isNonBlockingApprovalRequired(identityRecord.get()))
            {
                LogFB.w(TAG, "Setting approval status...");
                identityDatabase.setApproval(recipientId, nonBlockingApproval);
                return false;
            }

            return false;
        }
    }

    @Override
    public boolean saveIdentity(SignalProtocolAddress address, IdentityKey identityKey)
    {
        return saveIdentity(address, identityKey, false);
    }

    @Override
    public boolean isTrustedIdentity(SignalProtocolAddress address, IdentityKey identityKey, Direction direction)
    {
        synchronized (LOCK)
        {
            IdentityDatabase identityDatabase = DatabaseFactory.getIdentityDatabase(context);
            long recipientId = RecipientFactory.getRecipientsFromString(context, address.getName(), true).getPrimaryRecipient().getRecipientId();
            String ourNumber = TextSecurePreferences.getLocalNumber(context);
            long ourRecipientId = RecipientFactory.getRecipientsFromString(context, ourNumber, true).getPrimaryRecipient().getRecipientId();

            if (ourRecipientId == recipientId || ourNumber.equals(address.getName()))
            {
                return identityKey.equals(IdentityKeyUtil.getIdentityKey(context));
            }

            switch (direction)
            {
                case SENDING:
                    return isTrustedForSending(identityKey, identityDatabase.getIdentity(recipientId));
                case RECEIVING:
                    return true;
                default:
                    throw new AssertionError("Unknown direction: " + direction);
            }
        }
    }

    private boolean isTrustedForSending(IdentityKey identityKey, Optional<IdentityRecord> identityRecord)
    {
        if (!identityRecord.isPresent())
        {
            LogFB.w(TAG, "Nothing here, returning true...");
            return true;
        }

        if (!identityKey.equals(identityRecord.get().getIdentityKey()))
        {
            LogFB.w(TAG, "Identity keys don't match...");
            return false;
        }

        if (identityRecord.get().getVerifiedStatus() == VerifiedStatus.UNVERIFIED)
        {
            LogFB.w(TAG, "Needs unverified approval!");
            return false;
        }

        if (isNonBlockingApprovalRequired(identityRecord.get()))
        {
            LogFB.w(TAG, "Needs non-blocking approval!");
            return false;
        }

        return true;
    }

    private boolean isNonBlockingApprovalRequired(IdentityRecord identityRecord)
    {
        return !identityRecord.isFirstUse() &&
                System.currentTimeMillis() - identityRecord.getTimestamp() < TimeUnit.SECONDS.toMillis(TIMESTAMP_THRESHOLD_SECONDS) &&
                !identityRecord.isApprovedNonBlocking();
    }
}
