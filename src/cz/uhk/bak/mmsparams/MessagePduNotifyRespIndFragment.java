package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.NotifyRespInd;
import com.google.android.mms.pdu_alt.PduBody;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduNotifyRespIndFragment extends MessagePduBaseFragment
{
    private int paramID;
    private NotifyRespInd notifyRespInd;
    private MessagePdu messagePdu;

    private TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMmsReportAllowed, tvMmsStatus, tvMmsTransactionID, tvPduCreated;
    public static final String TAG = MessagePduNotifyRespIndFragment.class.getSimpleName();

    public static MessagePduNotifyRespIndFragment newInstance(int paramID)
    {
        MessagePduNotifyRespIndFragment fragment = new MessagePduNotifyRespIndFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        notifyRespInd = (NotifyRespInd) messagePdu.getGenericPdu();
    }

    private void fillControls()
    {

        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getString(notifyRespInd.getFrom()));
            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(notifyRespInd.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(notifyRespInd.getMmsVersion()));

            tvMmsReportAllowed.setText(PduHeaderConvertor.getValueString(notifyRespInd.getReportAllowed()));
            tvMmsStatus.setText(PduHeaderConvertor.getStatusString(notifyRespInd.getStatus()));
            tvMmsTransactionID.setText(getString(notifyRespInd.getTransactionId()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }

    private String getBody(PduBody body)
    {

        return "";
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_notify_resp_ind_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);
        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);

        tvMmsReportAllowed = ViewUtil.findById(view, R.id.tvMmsReportAllowed);
        tvMmsStatus = ViewUtil.findById(view, R.id.tvMmsStatus);
        tvMmsTransactionID = ViewUtil.findById(view, R.id.tvMmsTransactionID);

        fillControls();
    }

    @Override
    protected void setupListeners()
    {

    }
}
