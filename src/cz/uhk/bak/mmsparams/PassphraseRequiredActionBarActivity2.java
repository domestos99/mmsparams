package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;

public abstract class PassphraseRequiredActionBarActivity2 extends PassphraseRequiredActionBarActivity
{
    private MasterSecret masterSecret;

    @Override
    protected void onCreate(Bundle savedInstanceState, @NonNull MasterSecret masterSecret)
    {
        super.onCreate(savedInstanceState, masterSecret);
        this.masterSecret = masterSecret;

        setContentView(GetLayoutResource());

        baseInit();
        afterBaseInit();
    }

    protected void baseInit()
    {
        initControls();
        setupListeners();
    }

    protected void afterBaseInit()
    {
    }

    protected abstract
    @LayoutRes
    int GetLayoutResource();

    protected abstract void initControls();

    protected abstract void setupListeners();

    protected MasterSecret getMasterSecret()
    {
        return masterSecret;
    }
}
