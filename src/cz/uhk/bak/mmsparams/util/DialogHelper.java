package cz.uhk.bak.mmsparams.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class DialogHelper
{
    public interface OnOkListener
    {
        void OnOk();
    }

    public interface OnCancelListener
    {
        void OnCancel();
    }

    public static void createAndShowDeleteAlertDialog(final Context context, final OnOkListener onOkListener, final OnCancelListener onCancelListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this record?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                if (onOkListener != null)
                    onOkListener.OnOk();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                if (onCancelListener != null)
                    onCancelListener.OnCancel();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public static void createAndShowSaveAlertDialog(final Context context, final OnOkListener onOkListener, final OnCancelListener onCancelListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Save?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                if (onOkListener != null)
                    onOkListener.OnOk();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                if (onCancelListener != null)
                    onCancelListener.OnCancel();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
