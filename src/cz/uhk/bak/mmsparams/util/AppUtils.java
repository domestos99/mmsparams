package cz.uhk.bak.mmsparams.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;

public class AppUtils
{
    public static void makeDefaultSmsApp(Activity activity)
    {
        handleMakeDefaultSms(activity);
    }

    private static final int SMS_DEFAULT = 10;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static void handleMakeDefaultSms(Activity activity)
    {
        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, activity.getPackageName());
        activity.startActivityForResult(intent, SMS_DEFAULT);
    }
}
