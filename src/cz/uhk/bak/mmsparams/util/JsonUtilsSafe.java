package cz.uhk.bak.mmsparams.util;

import java.io.InputStream;
import java.io.Reader;

import cz.uhk.bak.mmsparams.log.LogFB;

public class JsonUtilsSafe
{
    public static final String TAG = JsonUtilsSafe.class.getSimpleName();


    public static <T> T fromJson(byte[] serialized, Class<T> clazz)
    {
        try
        {
            return fromJson(new String(serialized), clazz);
        }
        catch (Exception e)
        {
            LogFB.e(TAG, "fromJson", e);
            return null;
        }
    }

    public static <T> T fromJson(String serialized, Class<T> clazz)
    {
        try
        {
            return JsonUtils.fromJson(serialized, clazz);
        }
        catch (Exception e)
        {
            LogFB.e(TAG, "fromJson", e);
            return null;
        }
    }

    public static <T> T fromJson(InputStream serialized, Class<T> clazz)
    {
        try
        {
            return JsonUtils.fromJson(serialized, clazz);
        }
        catch (Exception e)
        {
            LogFB.e(TAG, "fromJson", e);
            return null;
        }
    }

    public static <T> T fromJson(Reader serialized, Class<T> clazz)
    {
        try
        {
            return JsonUtils.fromJson(serialized, clazz);
        }
        catch (Exception e)
        {
            LogFB.e(TAG, "fromJson", e);
            return null;
        }
    }

    public static String toJson(Object object)
    {
        try
        {
            return JsonUtils.toJson(object);
        }
        catch (Exception e)
        {
            LogFB.e(TAG, "toJson", e);
            return null;
        }
    }

}
