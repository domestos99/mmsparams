package cz.uhk.bak.mmsparams.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.style.ClickableSpan;
import android.view.View;

import org.whispersystems.libsignal.IdentityKey;

import cz.uhk.bak.mmsparams.database.documents.IdentityKeyMismatch;

public class VerifySpan extends ClickableSpan
{

    private final Context context;
    private final long recipientId;
    private final IdentityKey identityKey;

    public VerifySpan(@NonNull Context context, @NonNull IdentityKeyMismatch mismatch)
    {
        this.context = context;
        this.recipientId = mismatch.getRecipientId();
        this.identityKey = mismatch.getIdentityKey();
    }

    public VerifySpan(@NonNull Context context, long recipientId, @NonNull IdentityKey identityKey)
    {
        this.context = context;
        this.recipientId = recipientId;
        this.identityKey = identityKey;
    }

    @Override
    public void onClick(View widget)
    {
//        Intent intent = new Intent(context, VerifyIdentityActivity.class);
//        intent.putExtra(VerifyIdentityActivity.RECIPIENT_ID_EXTRA, recipientId);
//        intent.putExtra(VerifyIdentityActivity.IDENTITY_EXTRA, new IdentityKeyParcelable(identityKey));
//        intent.putExtra(VerifyIdentityActivity.VERIFIED_EXTRA, false);
//        context.startActivity(intent);
    }
}
