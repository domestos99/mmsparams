package cz.uhk.bak.mmsparams.util;

import com.google.android.mms.pdu_alt.CharacterSets;

public class CharSetHelper
{

    public static String getCharSetName(int value)
    {
        String ch = "";
        switch (value)
        {
            case CharacterSets.ANY_CHARSET:
                ch = "ANY_CHARSET";
                break;
            case CharacterSets.US_ASCII:
                ch = "US_ASCII";
                break;
            case CharacterSets.ISO_8859_1:
                ch = "ISO_8859_1";
                break;
            case CharacterSets.ISO_8859_2:
                ch = "ISO_8859_2";
                break;
            case CharacterSets.ISO_8859_3:
                ch = "ISO_8859_3";
                break;
            case CharacterSets.ISO_8859_4:
                ch = "ISO_8859_4";
                break;
            case CharacterSets.ISO_8859_5:
                ch = "ISO_8859_5";
                break;
            case CharacterSets.ISO_8859_6:
                ch = "ISO_8859_6";
                break;
            case CharacterSets.ISO_8859_7:
                ch = "ISO_8859_7";
                break;
            case CharacterSets.ISO_8859_8:
                ch = "ISO_8859_8";
                break;
            case CharacterSets.ISO_8859_9:
                ch = "ISO_8859_9";
                break;
            case CharacterSets.SHIFT_JIS:
                ch = "SHIFT_JIS";
                break;
            case CharacterSets.UTF_8:
                ch = "UTF_8";
                break;
            case CharacterSets.BIG5:
                ch = "BIG5";
                break;
            case CharacterSets.UCS2:
                ch = "UCS2";
                break;
            case CharacterSets.UTF_16:
                ch = "UTF_16";
                break;
        }
        return ch + " (" + String.valueOf(value) + ")";
    }


}
