package cz.uhk.bak.mmsparams.util;

import android.os.Build;

public class AndroidVersionUtil
{
    public static boolean isHigherThanLollipop()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isHigherThanLollipopMR1()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    public static boolean isHigherThanMarshmallow()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isLowerThanKitKat()
    {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT;
    }


}
