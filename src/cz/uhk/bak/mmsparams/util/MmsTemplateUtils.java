package cz.uhk.bak.mmsparams.util;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.crypto.MasterCipher;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.DraftDatabase;
import cz.uhk.bak.mmsparams.database.MmsTemplateDatabase;
import cz.uhk.bak.mmsparams.database.ThreadDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.database.model.MmsTemplateDetailGroup;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.mms.PartAuthority;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.mms.SlideDeck;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.util.dualsim.SubscriptionInfoCompat;
import cz.uhk.bak.mmsparams.util.dualsim.SubscriptionManagerCompat;

public class MmsTemplateUtils
{
    private static final String TAG = MmsTemplateUtils.class.getSimpleName();

    public static boolean hasRecipients(MmsTemplateDetailGroup mmsTemplateDetailGroup)
    {
        List<String> l = getRecipientNumbers(mmsTemplateDetailGroup);

        return l.size() != 0;
    }

    private static List<String> getRecipientNumbers(final MmsTemplateDetailGroup group)
    {
        List<String> l = new ArrayList<>();

        l.addAll(getNumbers(group.getTo()));
        l.addAll(getNumbers(group.getBcc()));
        l.addAll(getNumbers(group.getCc()));

        return l;
    }

    public static List<String> getNumbers(String to)
    {
        List<String> result = new ArrayList<>();
        if (!StringUtil.isEmptyOrNull(to))
        {
            to = to.trim();

            String[] tos = to.split(";");

            for (String t : tos)
            {
                if (!StringUtil.isEmptyOrNull(t))
                {
                    String trm = t.trim();
                    if (!StringUtil.isEmptyOrNull(trm))
                    {
                        result.add(trm);
                    }
                }
            }
        }
        return result;
    }

    public static OutgoingMediaMessage createMessage(Context context, MasterSecret masterSecret, @NonNull final MmsTemplate mt)
    {
        MmsProfile ms = mt.getMmsSettingsObj();


        Recipients recipients = RecipientFactory.getRecipientsFromStrings(context, getRecipientNumbers(mt.getMmsTemplateDetailGroup()), false);

        DraftDatabase db = DatabaseFactory.getDraftDatabase(context);
        List<DraftDatabase.Draft> drafts = db.getDrafts(new MasterCipher(masterSecret), MmsTemplateDatabase.getDraftMmsTemplateID(mt.getID()));
        List<Slide> slides = getSlides(context, masterSecret, drafts);

        long expiresIn = recipients.getExpireMessages() * 1000;


        SubscriptionManagerCompat subscriptionManager = new SubscriptionManagerCompat(context);
        List<SubscriptionInfoCompat> subscriptions = subscriptionManager.getActiveSubscriptionInfoList();

        int subsID = -1;
        if (subscriptions.size() > 0)
        {
            SubscriptionInfoCompat s = subscriptions.get(0);
            subsID = s.getSubscriptionId();
        }

        OutgoingMediaMessage outgoingMessage = new OutgoingMediaMessage(recipients,
                SlideUtils.buildSlideDeck(slides),

                mt.getMmsTemplateDetailGroup().getBody(),
                System.currentTimeMillis(),
                subsID,
                expiresIn,
                ThreadDatabase.DistributionTypes.CONVERSATION);


        return outgoingMessage;
    }


    private static List<Attachment> getAttachments(final Context context, final MasterSecret masterSecret, final int mmsTemplateID)
    {
        DraftDatabase db = DatabaseFactory.getDraftDatabase(context);
        List<DraftDatabase.Draft> drafts = db.getDrafts(new MasterCipher(masterSecret), MmsTemplateDatabase.getDraftMmsTemplateID(mmsTemplateID));


        List<Slide> slides = getSlides(context, masterSecret, drafts);

        SlideDeck sd = SlideUtils.buildSlideDeck(slides);
        return sd.asAttachments();
    }

    @NonNull
    private static List<Slide> getSlides(Context context, MasterSecret masterSecret, List<DraftDatabase.Draft> drafts)
    {
        List<Slide> slides = new ArrayList<>();

        for (DraftDatabase.Draft draft : drafts)
        {
            try
            {
                if (draft.getType().equals(DraftDatabase.Draft.TEXT))
                {
                    // composeText.setText(draft.getValue());
                }
                else if (draft.getType().equals(DraftDatabase.Draft.LOCATION))
                {

                }
                else if (draft.getType().equals(DraftDatabase.Draft.IMAGE))
                {
                    slides.add(createAttachment(context, masterSecret, Uri.parse(draft.getValue()), MediaType.IMAGE)); // setMedia(Uri.parse(draft.getValue()), MediaType.IMAGE);
                }
                else if (draft.getType().equals(DraftDatabase.Draft.AUDIO))
                {
                    slides.add(createAttachment(context, masterSecret, Uri.parse(draft.getValue()), MediaType.AUDIO)); //  setMedia(Uri.parse(draft.getValue()), MediaType.AUDIO);
                }
                else if (draft.getType().equals(DraftDatabase.Draft.VIDEO))
                {
                    slides.add(createAttachment(context, masterSecret, Uri.parse(draft.getValue()), MediaType.VIDEO)); //  setMedia(Uri.parse(draft.getValue()), MediaType.VIDEO);
                }
            }
            catch (Exception e)
            {
                LogFB.w(TAG, e);
            }
        }
        return slides;
    }

    private static Slide createAttachment(final Context context, final MasterSecret masterSecret, final Uri uri, final MediaType mediaType)
    {
        Slide sl;
        try
        {
            if (PartAuthority.isLocalUri(uri))
            {
                return SlideUtils.getManuallyCalculatedSlideInfo(context, masterSecret, uri, mediaType);
            }
            else
            {
                Slide result = SlideUtils.getContentResolverSlideInfo(context, uri, mediaType);

                if (result == null)
                    return SlideUtils.getManuallyCalculatedSlideInfo(context, masterSecret, uri, mediaType);
                else
                    return result;
            }
        }
        catch (IOException e)
        {
            LogFB.w(TAG, e);
            return null;
        }
    }


}
