package cz.uhk.bak.mmsparams.util;

import android.widget.RadioButton;

import com.google.android.mms.pdu_alt.PduHeaders;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PduHeaderConvertor
{

    public static final String ATTACH_SIZE_300 = "300";
    public static final String ATTACH_SIZE_UNLIMITED = "UNLIMITED";

    public static int convertValue(boolean val)
    {
        return val ? PduHeaders.VALUE_YES : PduHeaders.VALUE_NO;
    }

//    public static boolean convertValue(int val)
//    {
//        if (val == PduHeaders.VALUE_YES)
//            return true;
//        return false;
//    }

    public static String getPriorityString(int priority)
    {
        switch (priority)
        {
            case PduHeaders.PRIORITY_LOW:
                return "LOW";
            case PduHeaders.PRIORITY_NORMAL:
                return "NORMAL";
            case PduHeaders.PRIORITY_HIGH:
                return "HIGH";
            default:
                return String.valueOf(priority);
        }
    }

    public static String getMessageTypeString(int type)
    {
        switch (type)
        {
            case PduHeaders.MESSAGE_TYPE_SEND_REQ:
                return "SEND_REQ";
            case PduHeaders.MESSAGE_TYPE_SEND_CONF:
                return "SEND_CONF";
            case PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND:
                return "NOTIFICATION_IND";
            case PduHeaders.MESSAGE_TYPE_NOTIFYRESP_IND:
                return "NOTIFYRESP_IND";
            case PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF:
                return "RETRIEVE_CONF";
            case PduHeaders.MESSAGE_TYPE_ACKNOWLEDGE_IND:
                return "ACKNOWLEDGE_IND";
            case PduHeaders.MESSAGE_TYPE_DELIVERY_IND:
                return "DELIVERY_IND";
            case PduHeaders.MESSAGE_TYPE_READ_REC_IND:
                return "READ_REC_IND";
            case PduHeaders.MESSAGE_TYPE_READ_ORIG_IND:
                return "READ_ORIG_IND";
            case PduHeaders.MESSAGE_TYPE_FORWARD_REQ:
                return "FORWARD_REQ";
            case PduHeaders.MESSAGE_TYPE_FORWARD_CONF:
                return "FORWARD_CONF";
            case PduHeaders.MESSAGE_TYPE_MBOX_STORE_REQ:
                return "MBOX_STORE_REQ";
            case PduHeaders.MESSAGE_TYPE_MBOX_STORE_CONF:
                return "MBOX_STORE_CONF";
            case PduHeaders.MESSAGE_TYPE_MBOX_VIEW_REQ:
                return "MBOX_VIEW_REQ";
            case PduHeaders.MESSAGE_TYPE_MBOX_VIEW_CONF:
                return "MBOX_VIEW_CONF";
            case PduHeaders.MESSAGE_TYPE_MBOX_UPLOAD_REQ:
                return "MBOX_UPLOAD_REQ";
            case PduHeaders.MESSAGE_TYPE_MBOX_UPLOAD_CONF:
                return "MBOX_UPLOAD_CONF";
            case PduHeaders.MESSAGE_TYPE_MBOX_DELETE_REQ:
                return "MBOX_DELETE_REQ";
            case PduHeaders.MESSAGE_TYPE_MBOX_DELETE_CONF:
                return "MBOX_DELETE_CONF";
            case PduHeaders.MESSAGE_TYPE_MBOX_DESCR:
                return "MBOX_DESCR";
            case PduHeaders.MESSAGE_TYPE_DELETE_REQ:
                return "DELETE_REQ";
            case PduHeaders.MESSAGE_TYPE_DELETE_CONF:
                return "DELETE_CONF";
            case PduHeaders.MESSAGE_TYPE_CANCEL_REQ:
                return "CANCEL_REQ";
            case PduHeaders.MESSAGE_TYPE_CANCEL_CONF:
                return "CANCEL_CONF";
            default:
                return String.valueOf(type);
        }

    }

    public static String getMessageClassString(int cls)
    {
        switch (cls)
        {
            case PduHeaders.MESSAGE_CLASS_PERSONAL:
                return PduHeaders.MESSAGE_CLASS_PERSONAL_STR;
            case PduHeaders.MESSAGE_CLASS_ADVERTISEMENT:
                return PduHeaders.MESSAGE_CLASS_ADVERTISEMENT_STR;
            case PduHeaders.MESSAGE_CLASS_INFORMATIONAL:
                return PduHeaders.MESSAGE_CLASS_INFORMATIONAL_STR;
            case PduHeaders.MESSAGE_CLASS_AUTO:
                return PduHeaders.MESSAGE_CLASS_AUTO_STR;

            default:
                return String.valueOf(cls);
        }
    }

    public static String getMessageClassString(byte[] cls)
    {
        String value = new String(cls);
        return value;
    }


    public static byte[] convertMessageClass(int msgCls)
    {
        switch (msgCls)
        {
            case PduHeaders.MESSAGE_CLASS_PERSONAL:
                return PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes();

            case PduHeaders.MESSAGE_CLASS_ADVERTISEMENT:
                return PduHeaders.MESSAGE_CLASS_ADVERTISEMENT_STR.getBytes();

            case PduHeaders.MESSAGE_CLASS_INFORMATIONAL:
                return PduHeaders.MESSAGE_CLASS_INFORMATIONAL_STR.getBytes();

            case PduHeaders.MESSAGE_CLASS_AUTO:
                return PduHeaders.MESSAGE_CLASS_AUTO_STR.getBytes();
        }
        return null;
    }


    public static String getValueString(int value)
    {
        if (value == PduHeaders.VALUE_YES)
            return String.valueOf(true);
        if (value == PduHeaders.VALUE_NO)
            return String.valueOf(false);

        return "...";
    }

    public static String getResponseStatusString(int responseStatus)
    {
        switch (responseStatus)
        {
            case PduHeaders.RESPONSE_STATUS_OK:
                return "OK";
            case PduHeaders.RESPONSE_STATUS_ERROR_UNSPECIFIED:
                return "ERROR_UNSPECIFIED";
            case PduHeaders.RESPONSE_STATUS_ERROR_SERVICE_DENIED:
                return "ERROR_SERVICE_DENIED";
            case PduHeaders.RESPONSE_STATUS_ERROR_MESSAGE_FORMAT_CORRUPT:
                return "ERROR_MESSAGE_FORMAT_CORRUPT";
            case PduHeaders.RESPONSE_STATUS_ERROR_SENDING_ADDRESS_UNRESOLVED:
                return "ERROR_SENDING_ADDRESS_UNRESOLVED";
            case PduHeaders.RESPONSE_STATUS_ERROR_MESSAGE_NOT_FOUND:
                return "ERROR_MESSAGE_NOT_FOUND";
            case PduHeaders.RESPONSE_STATUS_ERROR_NETWORK_PROBLEM:
                return "ERROR_NETWORK_PROBLEM";
            case PduHeaders.RESPONSE_STATUS_ERROR_CONTENT_NOT_ACCEPTED:
                return "ERROR_CONTENT_NOT_ACCEPTED";
            case PduHeaders.RESPONSE_STATUS_ERROR_UNSUPPORTED_MESSAGE:
                return "ERROR_UNSUPPORTED_MESSAGE";
            case PduHeaders.RESPONSE_STATUS_ERROR_TRANSIENT_FAILURE:
                return "ERROR_TRANSIENT_FAILURE";
            case PduHeaders.RESPONSE_STATUS_ERROR_TRANSIENT_SENDNG_ADDRESS_UNRESOLVED:
                return "ERROR_TRANSIENT_SENDNG_ADDRESS_UNRESOLVED";
            case PduHeaders.RESPONSE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND:
                return "ERROR_TRANSIENT_MESSAGE_NOT_FOUND";
            case PduHeaders.RESPONSE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM:
                return "ERROR_TRANSIENT_NETWORK_PROBLEM";
            case PduHeaders.RESPONSE_STATUS_ERROR_TRANSIENT_PARTIAL_SUCCESS:
                return "ERROR_TRANSIENT_PARTIAL_SUCCESS";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_FAILURE:
                return "ERROR_PERMANENT_FAILURE";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_SERVICE_DENIED:
                return "ERROR_PERMANENT_SERVICE_DENIED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT:
                return "ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_SENDING_ADDRESS_UNRESOLVED:
                return "ERROR_PERMANENT_SENDING_ADDRESS_UNRESOLVED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND:
                return "ERROR_PERMANENT_MESSAGE_NOT_FOUND";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_CONTENT_NOT_ACCEPTED:
                return "ERROR_PERMANENT_CONTENT_NOT_ACCEPTED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_LIMITATIONS_NOT_MET:
                return "ERROR_PERMANENT_REPLY_CHARGING_LIMITATIONS_NOT_MET";
//            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_REQUEST_NOT_ACCEPTED:
//                return "RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_REQUEST_NOT_ACCEPTED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_FORWARDING_DENIED:
                return "ERROR_PERMANENT_REPLY_CHARGING_FORWARDING_DENIED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_NOT_SUPPORTED:
                return "ERROR_PERMANENT_REPLY_CHARGING_NOT_SUPPORTED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_ADDRESS_HIDING_NOT_SUPPORTED:
                return "ERROR_PERMANENT_ADDRESS_HIDING_NOT_SUPPORTED";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_LACK_OF_PREPAID:
                return "ERROR_PERMANENT_LACK_OF_PREPAID";
            case PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_END:
                return "ERROR_PERMANENT_END";

            default:
                return String.valueOf(responseStatus);
        }
    }


    public static String getMmsVersionString(int mmsVersion)
    {
        String value = "";
        switch (mmsVersion)
        {
            case PduHeaders.MMS_VERSION_1_3:
                value = "MMS_VERSION_1_3";
                break;
            case PduHeaders.MMS_VERSION_1_2:
                value = "MMS_VERSION_1_2";
                break;
            case PduHeaders.MMS_VERSION_1_1:
                value = "MMS_VERSION_1_1";
                break;
            case PduHeaders.MMS_VERSION_1_0:
                value = "MMS_VERSION_1_0";
                break;
        }

        // return "(" + String.valueOf(mmsVersion) + ") " + value;
        return value;
    }

    public static String getDateStringMilis(long miliSeconds)
    {
        if (miliSeconds < 0)
            return String.valueOf(miliSeconds);

        Date dt = new Date(miliSeconds);

        return new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(dt);
    }

    public static String getDateString(long seconds)
    {
        if (seconds < 0)
            return String.valueOf(seconds);

        return getDateStringMilis(seconds * 1000);
    }

    public static void setPriority(int priority, RadioButton rbPrioLow, RadioButton rbPrioNormal, RadioButton rbPrioHigh)
    {
        switch (priority)
        {
            case PduHeaders.PRIORITY_LOW:
                rbPrioLow.setChecked(true);
                break;
            case PduHeaders.PRIORITY_NORMAL:
                rbPrioNormal.setChecked(true);
                break;
            case PduHeaders.PRIORITY_HIGH:
                rbPrioHigh.setChecked(true);
                break;
        }
    }

    public static void setMessageCls(int message_class, RadioButton rbMsgClsPersonal, RadioButton rbMsgClsAdvert, RadioButton rbMsgClsInforma, RadioButton rbMsgClsAuto)
    {
        switch (message_class)
        {
            case PduHeaders.MESSAGE_CLASS_PERSONAL:
                rbMsgClsPersonal.setChecked(true);
                break;
            case PduHeaders.MESSAGE_CLASS_ADVERTISEMENT:
                rbMsgClsAdvert.setChecked(true);
                break;
            case PduHeaders.MESSAGE_CLASS_INFORMATIONAL:
                rbMsgClsInforma.setChecked(true);
                break;
            case PduHeaders.MESSAGE_CLASS_AUTO:
                rbMsgClsAuto.setChecked(true);
                break;
        }
    }

    public static int resolvePriority(RadioButton rbPrioLow, RadioButton rbPrioNormal, RadioButton rbPrioHigh)
    {
        if (rbPrioLow.isChecked())
            return PduHeaders.PRIORITY_LOW;
        if (rbPrioNormal.isChecked())
            return PduHeaders.PRIORITY_NORMAL;
        if (rbPrioHigh.isChecked())
            return PduHeaders.PRIORITY_HIGH;
        return PduHeaders.PRIORITY_NORMAL;
    }

    public static int resolveMessageCls(RadioButton rbMsgClsPersonal, RadioButton rbMsgClsAdvert, RadioButton rbMsgClsInforma, RadioButton rbMsgClsAuto)
    {
        if (rbMsgClsPersonal.isChecked())
            return PduHeaders.MESSAGE_CLASS_PERSONAL;
        else if (rbMsgClsAdvert.isChecked())
            return PduHeaders.MESSAGE_CLASS_ADVERTISEMENT;
        else if (rbMsgClsInforma.isChecked())
            return PduHeaders.MESSAGE_CLASS_INFORMATIONAL;
        else if (rbMsgClsAuto.isChecked())
            return PduHeaders.MESSAGE_CLASS_AUTO;

        return PduHeaders.MESSAGE_CLASS_PERSONAL;
    }

    public static String getStatusString(int status)
    {
        switch (status)
        {
            case PduHeaders.STATUS_EXPIRED:
                return "EXPIRED";
            case PduHeaders.STATUS_RETRIEVED:
                return "RETRIEVED";
            case PduHeaders.STATUS_REJECTED:
                return "REJECTED";
            case PduHeaders.STATUS_DEFERRED:
                return "DEFERRED";
            case PduHeaders.STATUS_UNRECOGNIZED:
                return "UNRECOGNIZED";
            case PduHeaders.STATUS_INDETERMINATE:
                return "INDETERMINATE";
            case PduHeaders.STATUS_FORWARDED:
                return "FORWARDED";
            case PduHeaders.STATUS_UNREACHABLE:
                return "UNREACHABLE";
            default:
                return String.valueOf(status);
        }
    }

    public static String getReadStatusString(int readStatus)
    {
        switch (readStatus)
        {
            case PduHeaders.READ_STATUS_READ:
                return "READ";
            case PduHeaders.READ_STATUS__DELETED_WITHOUT_BEING_READ:
                return "DELETED_WITHOUT_BEING_READ";
            default:
                return String.valueOf(readStatus);
        }

    }

    public static String getRetrieveStatusString(int retrieveStatus)
    {
        switch (retrieveStatus)
        {

            case PduHeaders.RETRIEVE_STATUS_OK:
                return "OK";
            case PduHeaders.RETRIEVE_STATUS_ERROR_TRANSIENT_FAILURE:
                return "ERROR_TRANSIENT_FAILURE";
            case PduHeaders.RETRIEVE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND:
                return "ERROR_TRANSIENT_MESSAGE_NOT_FOUND";
            case PduHeaders.RETRIEVE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM:
                return "ERROR_TRANSIENT_NETWORK_PROBLEM";
            case PduHeaders.RETRIEVE_STATUS_ERROR_PERMANENT_FAILURE:
                return "ERROR_PERMANENT_FAILURE";
            case PduHeaders.RETRIEVE_STATUS_ERROR_PERMANENT_SERVICE_DENIED:
                return "ERROR_PERMANENT_SERVICE_DENIED";
            case PduHeaders.RETRIEVE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND:
                return "ERROR_PERMANENT_MESSAGE_NOT_FOUND";
            case PduHeaders.RETRIEVE_STATUS_ERROR_PERMANENT_CONTENT_UNSUPPORTED:
                return "ERROR_PERMANENT_CONTENT_UNSUPPORTED";
            case PduHeaders.RETRIEVE_STATUS_ERROR_END:
                return "ERROR_END";

            default:
                return String.valueOf(retrieveStatus);
        }

    }

    public static boolean isSelectPriorityEqual(int priority, RadioButton rbPrioLow, RadioButton rbPrioNormal, RadioButton rbPrioHigh)
    {
        switch (priority)
        {
            case PduHeaders.PRIORITY_LOW:
                return rbPrioLow.isChecked();

            case PduHeaders.PRIORITY_NORMAL:
                return rbPrioNormal.isChecked();

            case PduHeaders.PRIORITY_HIGH:
                return rbPrioHigh.isChecked();
        }
        return false;

    }

    public static boolean isSelectMsgClassEqual(int message_class, RadioButton rbMsgClsPersonal, RadioButton rbMsgClsAdvert, RadioButton rbMsgClsInforma, RadioButton rbMsgClsAuto)
    {
        switch (message_class)
        {
            case PduHeaders.MESSAGE_CLASS_PERSONAL:
                return rbMsgClsPersonal.isChecked();

            case PduHeaders.MESSAGE_CLASS_ADVERTISEMENT:
                return rbMsgClsAdvert.isChecked();

            case PduHeaders.MESSAGE_CLASS_INFORMATIONAL:
                return rbMsgClsInforma.isChecked();

            case PduHeaders.MESSAGE_CLASS_AUTO:
                return rbMsgClsAuto.isChecked();
        }
        return false;
    }

    public static String getSenderVisibility(int senderIsVisible)
    {
        switch (senderIsVisible)
        {
            case 0:
                return "false";
            case 1:
                return "true";
            default:
                return "...";
        }
    }

    public static String getStoreStatusString(int storeStatus)
    {
        String result = "";
        switch (storeStatus)
        {
            case PduHeaders.STORE_STATUS_SUCCESS:
                result = "STORE_STATUS_SUCCESS";
                break;

            case PduHeaders.STORE_STATUS_ERROR_TRANSIENT_FAILURE:
                result = "ERROR_TRANSIENT_FAILURE";
                break;
            case PduHeaders.STORE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM:
                result = "ERROR_TRANSIENT_NETWORK_PROBLEM";
                break;
            case PduHeaders.STORE_STATUS_ERROR_PERMANENT_FAILURE:
                result = "ERROR_PERMANENT_FAILURE";
                break;
            case PduHeaders.STORE_STATUS_ERROR_PERMANENT_SERVICE_DENIED:
                result = "ERROR_PERMANENT_SERVICE_DENIED";
                break;
            case PduHeaders.STORE_STATUS_ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT:
                result = "ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT";
                break;
            case PduHeaders.STORE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND:
                result = "ERROR_PERMANENT_MESSAGE_NOT_FOUND";
                break;
            case PduHeaders.STORE_STATUS_ERROR_PERMANENT_MMBOX_FULL:
                result = "ERROR_PERMANENT_MMBOX_FULL";
                break;
            case PduHeaders.STORE_STATUS_ERROR_END:
                result = "ERROR_END";
                break;
        }

        result = "(" + storeStatus + ") " + result;

        return result;
    }


    public static String resolveMaxAttachSize(RadioButton rbMaxAttachSize300, RadioButton rbMaxAttachSizeUnlimited)
    {
        if (rbMaxAttachSize300.isChecked())
            return ATTACH_SIZE_300;
        else if (rbMaxAttachSizeUnlimited.isChecked())
            return ATTACH_SIZE_UNLIMITED;

        return null;
    }
}
