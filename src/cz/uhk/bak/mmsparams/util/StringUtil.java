package cz.uhk.bak.mmsparams.util;

import java.util.List;

public class StringUtil
{

    public static boolean isEmptyOrNull(final String s)
    {
        if (s == null || s.length() == 0)
            return true;
        return false;
    }


    public static String trim(final String s, final char ch)
    {
        if (isEmptyOrNull(s))
            return s;

        String result = s;

        char start = s.charAt(0);
        if (ch == start)
        {
            result = s.substring(1, s.length() - 1);
        }

        char end = result.charAt(result.length() - 1);


        if (ch == end)
        {
            result = result.substring(0, result.length() - 1);
        }

        return result;
    }


    public static String join(List<String> list, char c)
    {
        if (list == null || list.size() == 0)
            return null;

        String result = "";

        for (String s : list)
        {
            if (StringUtil.isEmptyOrNull(s))
                continue;

            result = result + s + c;
        }
        result = trim(result, c);

        return result;

    }

    public static String removeSpaces(final String s)
    {
        String result = s.replaceAll("\\s+", "").replace(" ", "").trim();
        return result;

    }


}
