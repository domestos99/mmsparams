package cz.uhk.bak.mmsparams.util;

import android.content.Intent;
import android.support.annotation.Nullable;

import cz.uhk.bak.mmsparams.log.LogFB;

public class ActivityResultHelper
{
    @Nullable
    public static <T> T handleMmsSettingsPicked(final Intent data)
    {
        try
        {
            if (data == null || data.getExtras() == null)
                return null;

            Object o = data.getExtras().get("result");
            return (T) o;
        }
        catch (Exception e)
        {
            LogFB.e("ActivityResultHelper", "handleMmsSettingsPicked", e);
            return null;
        }
    }
}
