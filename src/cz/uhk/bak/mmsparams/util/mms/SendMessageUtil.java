package cz.uhk.bak.mmsparams.util.mms;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import org.whispersystems.libsignal.InvalidMessageException;

import cz.uhk.bak.mmsparams.ConversationFragment;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.components.ComposeText;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.SmsDatabase;
import cz.uhk.bak.mmsparams.mms.AttachmentManager2;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.mms.OutgoingSecureMediaMessage;
import cz.uhk.bak.mmsparams.mms.SlideDeck;
import cz.uhk.bak.mmsparams.notifications.MessageNotifier;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.sms.MessageSender;
import cz.uhk.bak.mmsparams.util.concurrent.ListenableFuture;
import cz.uhk.bak.mmsparams.util.concurrent.SettableFuture;

public class SendMessageUtil
{
    final Context context;
    Recipients recipients;
    ConversationFragment fragment;
    boolean isSecureText;
    AttachmentManager2 attachmentManager;
    ComposeText composeText;
    int distributionType;
    MasterSecret masterSecret;
    long threadId;
    Activity activity;


    public SendMessageUtil(Context context, Recipients recipients, ConversationFragment fragment,
                           boolean isSecureText, AttachmentManager2 attachmentManager,
                           ComposeText composeText,
                           int distributionType, MasterSecret masterSecret,
                           long threadId, Activity activity)
    {
        this.context = context;
        this.recipients = recipients;
        this.fragment = fragment;
        this.isSecureText = isSecureText;
        this.attachmentManager = attachmentManager;
        this.composeText = composeText;
        this.distributionType = distributionType;
        this.masterSecret = masterSecret;
        this.threadId = threadId;
        this.activity = activity;
    }

    private void sendMediaMessage(final boolean forceSms, final long expiresIn, final int subscriptionId)
            throws InvalidMessageException
    {
        sendMediaMessage(forceSms, getMessage(), attachmentManager.buildSlideDeck(), expiresIn, subscriptionId);
    }


    private ListenableFuture<Void> sendMediaMessage(final boolean forceSms, String body, SlideDeck slideDeck, final long expiresIn, final int subscriptionId)
            throws InvalidMessageException
    {
        final SettableFuture<Void> future = new SettableFuture<>();

        OutgoingMediaMessage outgoingMessage = new OutgoingMediaMessage(recipients,
                slideDeck,
                body,
                System.currentTimeMillis(),
                subscriptionId,
                expiresIn,
                distributionType);

        if (isSecureText && !forceSms)
        {
            outgoingMessage = new OutgoingSecureMediaMessage(outgoingMessage);
        }

        attachmentManager.clear(false);
        composeText.setText("");
        final long id = fragment.stageOutgoingMessage(outgoingMessage);

        new AsyncTask<OutgoingMediaMessage, Void, Long>()
        {
            @Override
            protected Long doInBackground(OutgoingMediaMessage... messages)
            {
                return MessageSender.send(context, masterSecret, messages[0], threadId, forceSms, new SmsDatabase.InsertListener()
                {
                    @Override
                    public void onComplete()
                    {
                        fragment.releaseOutgoingMessage(id);
                    }
                });
            }

            @Override
            protected void onPostExecute(Long result)
            {
                sendComplete(result);
                future.set(null);
            }
        }.execute(outgoingMessage);

        return future;
    }

    protected void sendComplete(long threadId)
    {
        boolean refreshFragment = (threadId != this.threadId);
        this.threadId = threadId;

        if (fragment == null || !fragment.isVisible() || activity.isFinishing())
        {
            return;
        }

        fragment.setLastSeen(0);

        if (refreshFragment)
        {
            fragment.reload(recipients, threadId);
            MessageNotifier.setVisibleThread(threadId);
        }

        fragment.scrollToBottom();
        attachmentManager.cleanup();
    }

    private String getMessage() throws InvalidMessageException
    {
        String rawText = composeText.getTextTrimmed();

        if (rawText.length() < 1 && !attachmentManager.isAttachmentPresent())
            throw new InvalidMessageException(context.getString(R.string.ConversationActivity_message_is_empty_exclamation));

        return rawText;
    }


}
