package cz.uhk.bak.mmsparams.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.List;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.MediaConstraints;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.PartAuthority;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.mms.SlideDeck;

public class SlideUtils
{
    private static final String TAG = SlideUtils.class.getSimpleName();

    public static
    @NonNull
    Slide getManuallyCalculatedSlideInfo(Context context, MasterSecret masterSecret, Uri uri, @NonNull final MediaType mediaType) throws IOException
    {
        long start = System.currentTimeMillis();
        Long mediaSize = null;
        String fileName = null;
        String mimeType = null;

        if (PartAuthority.isLocalUri(uri))
        {
            mediaSize = PartAuthority.getAttachmentSize(context, masterSecret, uri);
            fileName = PartAuthority.getAttachmentFileName(context, masterSecret, uri);
            mimeType = PartAuthority.getAttachmentContentType(context, masterSecret, uri);
        }

        if (mediaSize == null)
        {
            mediaSize = MediaUtil.getMediaSize(context, masterSecret, uri);
        }

        LogFB.w(TAG, "local slide with size " + mediaSize + " took " + (System.currentTimeMillis() - start) + "ms");
        return mediaType.createSlide(context, uri, fileName, mimeType, mediaSize);
    }


    public static
    @Nullable
    Slide getContentResolverSlideInfo(Context context, Uri uri, @NonNull final MediaType mediaType)
    {
        Cursor cursor = null;
        long start = System.currentTimeMillis();

        try
        {
            cursor = context.getContentResolver().query(uri, null, null, null, null);

            if (cursor != null && cursor.moveToFirst())
            {
                String fileName = cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));
                long fileSize = cursor.getLong(cursor.getColumnIndexOrThrow(OpenableColumns.SIZE));
                String mimeType = context.getContentResolver().getType(uri);

                LogFB.w(TAG, "remote slide with size " + fileSize + " took " + (System.currentTimeMillis() - start) + "ms");
                return mediaType.createSlide(context, uri, fileName, mimeType, fileSize);
            }
        }
        finally
        {
            if (cursor != null) cursor.close();
        }

        return null;
    }


    public static boolean areConstraintsSatisfied(final @NonNull Context context,
                                                  final @NonNull MasterSecret masterSecret,
                                                  final @Nullable Slide slide,
                                                  final @NonNull MediaConstraints constraints)
    {
        return slide == null ||
                constraints.isSatisfied(context, masterSecret, slide.asAttachment()) ||
                constraints.canResize(slide.asAttachment());
    }


    public static
    @NonNull
    SlideDeck buildSlideDeck(final List<Slide> data)
    {
        SlideDeck deck = new SlideDeck();

        if (data == null)
            return deck;

        for (Slide s : data)
        {
            if (s != null)
                deck.addSlide(s);
        }
        return deck;
    }


}
