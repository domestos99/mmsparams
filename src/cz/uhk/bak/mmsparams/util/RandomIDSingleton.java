package cz.uhk.bak.mmsparams.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomIDSingleton
{
    public static final String TAG = RandomIDSingleton.class.getSimpleName();
    private static RandomIDSingleton instance;

    private final Random random;
    private final List<Integer> ids;

    public static RandomIDSingleton getInstance()
    {
        if (instance == null)
        {
            instance = new RandomIDSingleton();
        }
        return instance;
    }

    private RandomIDSingleton()
    {
        random = new Random();
        ids = new ArrayList<>();
    }


    public int getNextInt()
    {
        int id = 0;
        do
        {
            id = random.nextInt();
        }
        while (ids.contains(id));

        ids.add(id);
        return id;
    }

    public int getNextInt(int max)
    {
        int id = 0;
        do
        {
            id = random.nextInt(max);
        }
        while (ids.contains(id));

        ids.add(id);
        return id;
    }


    public int getNextMinusID()
    {
        int id = getNextInt(97);
        id++;
        id++;
        return (-id);
    }
}
