package cz.uhk.bak.mmsparams.interfaces;

import android.net.Uri;

public interface OnFragmentInteractionListener
{
    void onFragmentInteraction(Uri uri);
}
