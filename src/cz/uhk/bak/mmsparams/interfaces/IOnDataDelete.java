package cz.uhk.bak.mmsparams.interfaces;

public interface IOnDataDelete<T>
{
    void onDataDelete(T item);
}
