package cz.uhk.bak.mmsparams.interfaces;

public interface INotifyDataSetChanged
{
    void notifyDataSetChanged();
}
