package cz.uhk.bak.mmsparams.components.relativeTimePicker;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import cz.uhk.bak.mmsparams.R;


public class SlideDateTimeDialogFragment extends DialogFragment
{
    public static final String TAG_SLIDE_DATE_TIME_DIALOG_FRAGMENT = "tagSlideDateTimeDialogFragment";
    public static final String THEME = "theme";
    public static final String INDICATOR_COLOR = "indicatorColor";
    public static final String INITIAL_DATE = "initialDate";
    public static final String DEFAULT_DATE = "defaultDate";


    private static SlideRelativeTimePickerListener mListener;
    private CustomRelativeTimePicker customRelativeTimePicker;

    private Context mContext;
    private View mButtonHorizontalDivider;
    private View mButtonVerticalDivider;
    private Button mOkButton;
    private Button mCancelButton;

    private DateTimePair mInitialDate, mDefaultDate;

    private int mTheme;
    private int mIndicatorColor;


    public SlideDateTimeDialogFragment()
    {
        // Required empty public constructor
    }

    public static SlideDateTimeDialogFragment newInstance(SlideRelativeTimePickerListener listener, DateTimePair dateTimePair, DateTimePair defaultDateTime, int theme, int indicatorColor)
    {
        mListener = listener;

        // Create a new instance of SlideDateTimeDialogFragment
        SlideDateTimeDialogFragment dialogFragment = new SlideDateTimeDialogFragment();

        // Store the arguments and attach the bundle to the fragment
        Bundle bundle = new Bundle();
        bundle.putSerializable(INITIAL_DATE, dateTimePair);
        bundle.putSerializable(DEFAULT_DATE, defaultDateTime);
        bundle.putInt(THEME, theme);
        bundle.putInt(INDICATOR_COLOR, indicatorColor);
        dialogFragment.setArguments(bundle);

        // Return the fragment with its bundle
        return dialogFragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        unpackBundle();


        switch (mTheme)
        {
            case SlideRelativeTimePicker.HOLO_DARK:
                setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog_NoActionBar);
                break;
            case SlideRelativeTimePicker.HOLO_LIGHT:
                setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                break;
            default:  // if no theme was specified, default to holo light
                setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.slide_relative_time_picker, container);

        setupViews(view);
        customizeViews();

        initButtons();

        return view;
    }

    @Override
    public void onDestroyView()
    {
        // Workaround for a bug in the compatibility library where calling
        // setRetainInstance(true) does not retain the instance across
        // orientation changes.
        if (getDialog() != null && getRetainInstance())
        {
            getDialog().setDismissMessage(null);
        }

        super.onDestroyView();
    }

    private void unpackBundle()
    {
        Bundle args = getArguments();

        mInitialDate = (DateTimePair) args.getSerializable(INITIAL_DATE);
        mDefaultDate = (DateTimePair) args.getSerializable(DEFAULT_DATE);
        mTheme = args.getInt(THEME);
        mIndicatorColor = args.getInt(INDICATOR_COLOR);
    }

    private void setupViews(View v)
    {
        customRelativeTimePicker = (CustomRelativeTimePicker) v.findViewById(R.id.customRelativeTimePicker);
        mButtonHorizontalDivider = v.findViewById(R.id.buttonHorizontalDivider);
        mButtonVerticalDivider = v.findViewById(R.id.buttonVerticalDivider);
        mOkButton = (Button) v.findViewById(R.id.okButton);
        mCancelButton = (Button) v.findViewById(R.id.cancelButton);

        customRelativeTimePicker.init(mInitialDate, mDefaultDate);
    }

    private void customizeViews()
    {
        int lineColor = mTheme == SlideRelativeTimePicker.HOLO_DARK ?
                getResources().getColor(R.color.gray_holo_dark) :
                getResources().getColor(R.color.gray_holo_light);

        // Set the colors of the horizontal and vertical lines for the
        // bottom buttons depending on the theme.
        switch (mTheme)
        {
            case SlideRelativeTimePicker.HOLO_LIGHT:
            case SlideRelativeTimePicker.HOLO_DARK:
                mButtonHorizontalDivider.setBackgroundColor(lineColor);
                mButtonVerticalDivider.setBackgroundColor(lineColor);
                break;

            default:  // if no theme was specified, default to holo light
                mButtonHorizontalDivider.setBackgroundColor(getResources().getColor(R.color.gray_holo_light));
                mButtonVerticalDivider.setBackgroundColor(getResources().getColor(R.color.gray_holo_light));
        }

    }

    private void initButtons()
    {
        mOkButton.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                if (mListener == null)
                {
                    throw new NullPointerException(
                            "Listener no longer exists for mOkButton");
                }

                mListener.onDateTimeSet(customRelativeTimePicker.getDateTime());

                dismiss();
            }
        });

        mCancelButton.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                if (mListener == null)
                {
                    throw new NullPointerException(
                            "Listener no longer exists for mCancelButton");
                }

                mListener.onDateTimeCancel();

                dismiss();
            }
        });
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {
        super.onCancel(dialog);

        if (mListener == null)
        {
            throw new NullPointerException(
                    "Listener no longer exists in onCancel()");
        }

        mListener.onDateTimeCancel();
    }
}
