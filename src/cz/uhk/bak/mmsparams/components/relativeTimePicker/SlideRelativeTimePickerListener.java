package cz.uhk.bak.mmsparams.components.relativeTimePicker;

public abstract class SlideRelativeTimePickerListener
{
    public abstract void onDateTimeSet(DateTimePair dateTimePair);

    public void onDateTimeCancel()
    {

    }
}
