package cz.uhk.bak.mmsparams.components.relativeTimePicker;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class CustomRelativeTimePicker extends FrameLayout
{
    private static final String TAG = CustomRelativeTimePicker.class.getSimpleName();

    private NumberPicker npDay, npHour, npMinute, npSecond;
    private DateTimePair dateTimePair, defaultDateTime;
    private ImageView imgUndo;
    private TextView lInfo;


    public CustomRelativeTimePicker(@NonNull Context context)
    {
        super(context);
        init(context);
    }

    public CustomRelativeTimePicker(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public CustomRelativeTimePicker(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CustomRelativeTimePicker(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(context);
    }

    private void init(@NonNull Context context)
    {
        inflate(context, R.layout.control_relative_time_picker, this);

        initControls();
        setupControls();
        setupListeners();
    }

    private void setupControls()
    {
        npDay.setMinValue(0);
        npDay.setMaxValue(100);

        npHour.setMinValue(0);
        npHour.setMaxValue(23);

        npMinute.setMinValue(0);
        npMinute.setMaxValue(59);

        npSecond.setMinValue(0);
        npSecond.setMaxValue(59);
    }

    private void setupListeners()
    {
        NumberPicker.OnValueChangeListener l = new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
                if (picker == npDay)
                    dateTimePair = dateTimePair.setDay(newVal);
                else if (picker == npHour)
                    dateTimePair = dateTimePair.setHour(newVal);
                else if (picker == npMinute)
                    dateTimePair = dateTimePair.setMinute(newVal);
                else if (picker == npSecond)
                    dateTimePair = dateTimePair.setSecond(newVal);

                updateInfoText(dateTimePair);
            }
        };

        npDay.setOnValueChangedListener(l);
        npHour.setOnValueChangedListener(l);
        npMinute.setOnValueChangedListener(l);
        npSecond.setOnValueChangedListener(l);

        imgUndo.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                init(defaultDateTime, defaultDateTime);
            }
        });
    }

    private void updateInfoText(DateTimePair dtp)
    {
        lInfo.setText(dtp.getString());
    }

    private void initControls()
    {
        npDay = (NumberPicker) findViewById(R.id.npDay);
        npHour = (NumberPicker) findViewById(R.id.npHour);
        npMinute = (NumberPicker) findViewById(R.id.npMinute);
        npSecond = (NumberPicker) findViewById(R.id.npSecond);
        lInfo = (TextView) findViewById(R.id.lInfo);
        imgUndo = ViewUtil.findById(this, R.id.imgUndo);
    }

//
//    public void setDescendantFocusability(int focusBlockDescendants)
//    {
//        npDay.setDescendantFocusability(focusBlockDescendants);
//        npHour.setDescendantFocusability(focusBlockDescendants);
//        npMinute.setDescendantFocusability(focusBlockDescendants);
//        npSecond.setDescendantFocusability(focusBlockDescendants);
//    }

    public void init(final DateTimePair dateValue, final DateTimePair defaultDateTime)
    {
        if (dateValue == null)
            this.dateTimePair = DateTimePair.getDefaultMin();
        else
            this.dateTimePair = dateValue;


        if (defaultDateTime == null)
            this.defaultDateTime = dateTimePair;
        else
            this.defaultDateTime = defaultDateTime;


        updateControls(this.dateTimePair);

        updateInfoText(this.dateTimePair);
    }

    private void updateControls(DateTimePair dateTimePair)
    {
        npDay.setValue(dateTimePair.getDay());
        npHour.setValue(dateTimePair.getHour());
        npMinute.setValue(dateTimePair.getMinute());
        npSecond.setValue(dateTimePair.getSecond());
    }

    public DateTimePair getDateTime()
    {
        return this.dateTimePair;
    }
}
