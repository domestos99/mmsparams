//package cz.uhk.bak.mmsparams.components.relativeTimePicker;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.ContextThemeWrapper;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.DatePicker;
//
//import cz.uhk.bak.mmsparams.R;
//
//import static cz.uhk.bak.mmsparams.components.relativeTimePicker.SlideDateTimeDialogFragment.INITIAL_DATE;
//
//public class RelativeTimePickerFragment extends Fragment
//{
//    public interface DateTimeChangedListener
//    {
//        void onDateTimeChanged(DateTimePair dateTimePair);
//    }
//
//    private RelativeTimePickerFragment.DateTimeChangedListener mCallback;
//
//    private CustomRelativeTimePicker mDatePicker;
//
//    public RelativeTimePickerFragment()
//    {
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//
//        try
//        {
//            mCallback = (RelativeTimePickerFragment.DateTimeChangedListener) getTargetFragment();
//        }
//        catch (ClassCastException e)
//        {
//            throw new ClassCastException("Calling fragment must implement " +
//                    "DateFragment.DateChangedListener interface");
//        }
//    }
//
//    public static final RelativeTimePickerFragment newInstance(int theme, DateTimePair dt)
//    {
//        RelativeTimePickerFragment f = new RelativeTimePickerFragment();
//
//        Bundle b = new Bundle();
//        b.putInt("theme", theme);
//        b.putSerializable(INITIAL_DATE, dt);
//        f.setArguments(b);
//
//        return f;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState)
//    {
//        int theme = getArguments().getInt("theme");
//        DateTimePair dateValue = (DateTimePair) getArguments().getSerializable(INITIAL_DATE);
//
//        // Unless we inflate using a cloned inflater with a Holo theme,
//        // on Lollipop devices the DatePicker will be the new-style
//        // DatePicker, which is not what we want. So we will
//        // clone the inflater that we're given but with our specified
//        // theme, then inflate the layout with this new inflater.
//
//        Context contextThemeWrapper = new ContextThemeWrapper(
//                getActivity(),
//                theme == SlideRelativeTimePicker.HOLO_DARK ?
//                        android.R.style.Theme_Holo :
//                        android.R.style.Theme_Holo_Light);
//
//        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
//
//        View v = localInflater.inflate(R.layout.fragment_date_time, container, false);
//
//        mDatePicker = (CustomRelativeTimePicker) v.findViewById(R.id.dateTimePicker);
//
//        // block keyboard popping up on touch
//        mDatePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
//        mDatePicker.init(
//                dateValue,
//                new CustomRelativeTimePicker.OnDateTimeChangedListener()
//                {
//                    @Override
//                    public void onDateChanged(DateTimePair dateTimePair)
//                    {
//                        mCallback.onDateTimeChanged(dateTimePair);
//                    }
//                });
//
//
//        return v;
//    }
//
//
//}
