package cz.uhk.bak.mmsparams.components.relativeTimePicker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class SlideRelativeTimePicker
{
    public static final int HOLO_DARK = 1;
    public static final int HOLO_LIGHT = 2;

    private FragmentManager mFragmentManager;
    private SlideRelativeTimePickerListener mListener;
    private DateTimePair mInitialDate;
    private DateTimePair mDefaultDate;
    private int mTheme;
    private int mIndicatorColor;

    public SlideRelativeTimePicker(FragmentManager fm)
    {
        // See if there are any DialogFragments from the FragmentManager
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag(SlideDateTimeDialogFragment.TAG_SLIDE_DATE_TIME_DIALOG_FRAGMENT);

        // Remove if found
        if (prev != null)
        {
            ft.remove(prev);
            ft.commit();
        }

        mFragmentManager = fm;
    }

    public void setListener(SlideRelativeTimePickerListener listener)
    {
        mListener = listener;
    }

    public void setInitialDate(DateTimePair initialDate)
    {
        mInitialDate = initialDate;
    }

    public void setTheme(int theme)
    {
        mTheme = theme;
    }

    public void setIndicatorColor(int indicatorColor)
    {
        mIndicatorColor = indicatorColor;
    }

    public void setDefaultDate(DateTimePair mDefaultDate)
    {
        this.mDefaultDate = mDefaultDate;
    }

    public void show()
    {
        if (mListener == null)
        {
            throw new NullPointerException(
                    "Attempting to bind null listener to SlideDateTimePicker");
        }

        if (mInitialDate == null)
        {
            setInitialDate(DateTimePair.getDefaultMin());
        }

        if (mDefaultDate == null)
        {
            mDefaultDate = mInitialDate;
        }

        SlideDateTimeDialogFragment dialogFragment =
                SlideDateTimeDialogFragment.newInstance(
                        mListener,
                        mInitialDate,
                        mDefaultDate,
                        mTheme,
                        mIndicatorColor);

        dialogFragment.show(mFragmentManager,
                SlideDateTimeDialogFragment.TAG_SLIDE_DATE_TIME_DIALOG_FRAGMENT);
    }

    public static class Builder
    {
        // Required
        private FragmentManager fm;
        private SlideRelativeTimePickerListener listener;

        // Optional
        private DateTimePair initialDate;
        private DateTimePair defaultDate;
        private int theme;
        private int indicatorColor;

        public Builder(FragmentManager fm)
        {
            this.fm = fm;
        }

        public Builder setListener(SlideRelativeTimePickerListener listener)
        {
            this.listener = listener;
            return this;
        }

        public Builder setInitialDate(DateTimePair initialDate)
        {
            this.initialDate = initialDate;
            return this;
        }

        public Builder setDefaultDate(DateTimePair defaultDate)
        {
            this.defaultDate = defaultDate;
            return this;
        }

        public Builder setTheme(int theme)
        {
            this.theme = theme;
            return this;
        }


        public Builder setIndicatorColor(int indicatorColor)
        {
            this.indicatorColor = indicatorColor;
            return this;
        }

        public SlideRelativeTimePicker build()
        {
            SlideRelativeTimePicker picker = new SlideRelativeTimePicker(fm);
            picker.setListener(listener);
            picker.setInitialDate(initialDate);
            picker.setTheme(theme);
            picker.setIndicatorColor(indicatorColor);
            picker.setDefaultDate(defaultDate);

            return picker;
        }


    }
}
