package cz.uhk.bak.mmsparams.components.relativeTimePicker;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class DateTimePair implements Serializable
{
    private final int day, hour, minute, second;

    public DateTimePair(long seconds)
    {
        day = (int) TimeUnit.SECONDS.toDays(seconds);
        hour = (int) (TimeUnit.SECONDS.toHours(seconds) - (day * 24));
        minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60));
        second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60));

        // day = (int) (seconds % (24*60*60));
        // seconds  = seconds - (day * (24*60*60));

    }

    public DateTimePair(DateTimePair dateTimePair)
    {
        this.day = dateTimePair.day;
        this.hour = dateTimePair.hour;
        this.minute = dateTimePair.minute;
        this.second = dateTimePair.second;
    }

    public DateTimePair(int day, int hour, int minute, int second)
    {
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public static DateTimePair getDefaultMin()
    {
        return new DateTimePair(0, 0, 0, 0);
    }

    public static DateTimePair getDefaultMax()
    {
        return new DateTimePair(100, 24, 60, 60);
    }

    public int getDay()
    {
        return day;
    }

    public int getHour()
    {
        return hour;
    }

    public int getMinute()
    {
        return minute;
    }

    public int getSecond()
    {
        return second;
    }

    public String getString()
    {
        DecimalFormat df = new DecimalFormat("00");

        return getDay() + "d" + " " + df.format(getHour()) + ":" + df.format(getMinute()) + ":" + df.format(getSecond()) + " (" + getTotalSeconds() + "s)";
    }

    public long getTotalSeconds()
    {
        return getSecond() + 60 * getMinute() + 60 * 60 * getHour() + 60 * 60 * 24 * getDay();
    }

    @Override
    public String toString()
    {
        return "DateTimePair{" +
                "day=" + day +
                ", hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                '}';
    }

    public DateTimePair setDay(int newVal)
    {
        DateTimePair dtp = new DateTimePair(newVal, this.hour, this.minute, this.second);
        return dtp;
    }

    public DateTimePair setHour(int newVal)
    {
        DateTimePair dtp = new DateTimePair(this.day, newVal, this.minute, this.second);
        return dtp;
    }

    public DateTimePair setMinute(int newVal)
    {
        DateTimePair dtp = new DateTimePair(this.day, this.hour, newVal, this.second);
        return dtp;
    }

    public DateTimePair setSecond(int newVal)
    {
        DateTimePair dtp = new DateTimePair(this.day, this.hour, this.minute, newVal);
        return dtp;
    }
}
