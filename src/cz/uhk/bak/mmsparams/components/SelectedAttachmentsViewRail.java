package cz.uhk.bak.mmsparams.components;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.adapters.SelectedAttachmentAdapter;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class SelectedAttachmentsViewRail extends FrameLayout
{
    @NonNull
    private final RecyclerView recyclerView;

    SelectedAttachmentAdapter adapter;

    public SelectedAttachmentsViewRail(Context context)
    {

        this(context, null);
    }

    public SelectedAttachmentsViewRail(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public SelectedAttachmentsViewRail(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        inflate(context, R.layout.selected_attachments_view_rail, this);

        this.recyclerView = ViewUtil.findById(this, R.id.attachment_list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    public void setData(MasterSecret masterSecret, List<Slide> data)
    {
        adapter = new SelectedAttachmentAdapter(getContext(), masterSecret, data);
        this.recyclerView.setAdapter(adapter);
    }

    public void notifyDataSetChanged()
    {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }
}
