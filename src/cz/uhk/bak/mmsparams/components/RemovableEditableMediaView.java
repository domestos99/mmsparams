package cz.uhk.bak.mmsparams.components;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import cz.uhk.bak.mmsparams.R;

public class RemovableEditableMediaView extends FrameLayout
{

    private final
    @NonNull
    ImageView remove;
    private final
    @NonNull
    ImageView edit;

    private final int removeSize;
    private final int editSize;

    private FrameType frameType;
    private
    @Nullable
    View current;

    public RemovableEditableMediaView(Context context)
    {
        this(context, null);
    }

    public RemovableEditableMediaView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public RemovableEditableMediaView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        this.remove = (ImageView) LayoutInflater.from(context).inflate(R.layout.media_view_remove_button, this, false);
        this.edit = (ImageView) LayoutInflater.from(context).inflate(R.layout.media_view_edit_button, this, false);

        this.removeSize = getResources().getDimensionPixelSize(R.dimen.media_bubble_remove_button_size);
        this.editSize = getResources().getDimensionPixelSize(R.dimen.media_bubble_edit_button_size);

        this.remove.setVisibility(View.GONE);
        this.edit.setVisibility(View.GONE);
    }

    @Override
    public void onFinishInflate()
    {
        super.onFinishInflate();
        this.addView(remove);
        this.addView(edit);
    }

    public enum FrameType
    {
        MEDIA
    }

    public void display(@Nullable View view, boolean editable, boolean removeable, FrameType frameType)
    {
        this.frameType = frameType;

        edit.setVisibility(editable ? View.VISIBLE : View.GONE);

        if (view == current) return;
        if (current != null) current.setVisibility(View.GONE);

        if (view != null)
        {
            view.setPadding(0, removeSize / 2, removeSize / 2, 0);
            edit.setPadding(0, 0, removeSize / 2, 0);

            view.setVisibility(View.VISIBLE);
            remove.setVisibility(View.VISIBLE);
        }
        else
        {
            remove.setVisibility(View.GONE);
            edit.setVisibility(View.GONE);
        }

        if (!removeable)
        {
            remove.setVisibility(View.GONE);
        }

        current = view;
    }

    public void display(@Nullable View view, boolean editable, FrameType frameType)
    {
        display(view, editable, true, frameType);
    }

    public void setRemoveClickListener(View.OnClickListener listener)
    {
        this.remove.setOnClickListener(listener);
    }

    OnEditButtonClick onEditButtonClick;

    public void setEditClickListener(OnEditButtonClick listener)
    {
        this.onEditButtonClick = listener;
        this.edit.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onEditButtonClick.OnClick(v, frameType);
            }
        });
    }

    public interface OnEditButtonClick
    {
        void OnClick(View v, FrameType frameType);
    }

}
