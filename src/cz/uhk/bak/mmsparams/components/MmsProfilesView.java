package cz.uhk.bak.mmsparams.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;

public class MmsProfilesView extends FrameLayout
{
    private static final String TAG = ThumbnailView.class.getSimpleName();


    private TextView tvName, tvInfo;

    public MmsProfilesView(Context context)
    {
        this(context, null);
    }

    public MmsProfilesView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public MmsProfilesView(final Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);

        inflate(context, R.layout.mms_profile_view, this);


        this.tvName = (TextView) findViewById(R.id.name);
        this.tvInfo = (TextView) findViewById(R.id.info);


    }

    MmsProfile mmsProfile;


    public void display(MmsProfile mmsProfile)
    {
        this.mmsProfile = mmsProfile;

        tvName.setText(mmsProfile.getName());
        tvInfo.setText(mmsProfile.getInfo());
    }


}
