package cz.uhk.bak.mmsparams.components.RecipientPicker;

import android.content.Context;

import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;

public class RecipientPickerItem
{
    private final Context context;

    public RecipientPickerItem(Context context, String number)
    {
        this.context = context;

        setNumber(number);
    }

    private String number;
    private String name;

    public void setNumber(String number)
    {
        this.number = number;

        Recipients r = RecipientFactory.getRecipientsFromStrings(context, number, true);

        if (r == null || r.getPrimaryRecipient() == null)
        {
            name = null;
        }
        else
        {
            name = r.getPrimaryRecipient().getName();
        }
    }


    public String getName()
    {
        return name;
    }

    public String getNumber()
    {
        return number;
    }
}

