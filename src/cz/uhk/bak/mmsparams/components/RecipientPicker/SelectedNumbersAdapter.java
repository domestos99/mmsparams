package cz.uhk.bak.mmsparams.components.RecipientPicker;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.interfaces.INotifyDataSetChanged;
import cz.uhk.bak.mmsparams.interfaces.IOnDataDelete;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class SelectedNumbersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final List<RecipientPickerItem> data;
    private final Context context;
    SelectedNumbersAdapter self;

    private static final int row_layout_ID = R.layout.selected_attachments_view_item;


    public SelectedNumbersAdapter(Context context, List<RecipientPickerItem> data)
    {
        this.data = data;
        this.context = context;
        self = this;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(row_layout_ID, parent, false);

        return new SelectedNumbersAdapter.ContactViewHolder(context, itemView,
                new INotifyDataSetChanged()
                {
                    @Override
                    public void notifyDataSetChanged()
                    {
                        self.notifyDataSetChanged();
                    }
                },
                new IOnDataDelete<RecipientPickerItem>()
                {
                    @Override
                    public void onDataDelete(RecipientPickerItem item)
                    {
                        data.remove(item);
                    }
                });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        ContactViewHolder hld = (ContactViewHolder) holder;
        RecipientPickerItem i = data.get(position);
        hld.setupData(i);
    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }


    static class ContactViewHolder extends RecyclerView.ViewHolder
    {
        private final Context context;
        INotifyDataSetChanged iNotifyDataSetChanged;
        IOnDataDelete<RecipientPickerItem> iOnDataDelete;
        View view;

        TextView tvNumber, tvRecipientName;
        ImageView btnDelete;

        public ContactViewHolder(Context context, View itemView,
                                 INotifyDataSetChanged iNotifyDataSetChanged,
                                 IOnDataDelete<RecipientPickerItem> iOnDataDelete)
        {
            super(itemView);

            this.view = itemView;
            this.context = context;
            this.iNotifyDataSetChanged = iNotifyDataSetChanged;
            this.iOnDataDelete = iOnDataDelete;

            initControls(view);
        }

        private void initControls(View view)
        {
            tvNumber = ViewUtil.findById(view, R.id.tvNumber);
            tvRecipientName = ViewUtil.findById(view, R.id.tvRecipientName);
            btnDelete = ViewUtil.findById(view, R.id.btnDelete);


        }

        public void setupData(final RecipientPickerItem obj)
        {
            btnDelete.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    iOnDataDelete.onDataDelete(obj);
                    iNotifyDataSetChanged.notifyDataSetChanged();
                }
            });

            tvNumber.setText(obj.getNumber());
            tvRecipientName.setText(obj.getName());
        }


    }


}
