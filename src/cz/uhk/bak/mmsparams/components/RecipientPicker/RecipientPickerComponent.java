package cz.uhk.bak.mmsparams.components.RecipientPicker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.util.StringUtil;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class RecipientPickerComponent extends FrameLayout
{
    private static final String TAG = RecipientPickerComponent.class.getSimpleName();

    TextView tvTitle;
    ImageView btnPickContact, btnRemoveContact;
    // private String number;

    private List<RecipientPickerItem> numbers;

    private IRecipientPickInitListener iRecipientPickInitListener;
    SelectedNumbersViewRail selected_numbers_view_rail;


    public RecipientPickerComponent(@NonNull Context context)
    {
        super(context);
        init(context);
    }

    public RecipientPickerComponent(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public RecipientPickerComponent(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public RecipientPickerComponent(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(@NonNull Context context)
    {
        inflate(context, R.layout.control_recipient_picker, this);

        numbers = new ArrayList<>();

        initControls();
        setupListeners();
    }

    public void setListeners(IRecipientPickInitListener listeners)
    {
        this.iRecipientPickInitListener = listeners;
    }

    private void initControls()
    {
        tvTitle = ViewUtil.findById(this, R.id.tvTitle);


        btnPickContact = ViewUtil.findById(this, R.id.btnPickContact);
        btnRemoveContact = ViewUtil.findById(this, R.id.btnRemoveContact);
        selected_numbers_view_rail = ViewUtil.findById(this, R.id.selected_numbers_view_rail);
    }

    private void setupListeners()
    {
        btnPickContact.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (iRecipientPickInitListener != null)
                    iRecipientPickInitListener.OnRecipientPickInit();
            }
        });

        btnRemoveContact.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                numbers.clear();
                handleNumberChanged();
            }
        });

        selected_numbers_view_rail.setData(numbers);
    }

    public void setTitle(String title)
    {
        if (tvTitle == null)
            return;

        tvTitle.setText(title);
    }


    public void addNumber(String number)
    {
        if (StringUtil.isEmptyOrNull(number))
            return;

        this.numbers.add(new RecipientPickerItem(getContext(), number));
        handleNumberChanged();
    }

    private void handleNumberChanged()
    {
        selected_numbers_view_rail.notifyDataSetChanged();
    }

    public List<String> getNumbers()
    {
        List<String> result = new ArrayList<>();

        for (RecipientPickerItem i : numbers)
        {
            String n = i.getNumber();
            if (!StringUtil.isEmptyOrNull(n))
                result.add(n);
        }

        return result;
    }

    public String getNumberCSV()
    {
        List<String> l = getNumbers();

        String r = StringUtil.join(l, ';');

        return r;
    }

    public void addNumberCSV(String to)
    {
        numbers.clear();

        if (StringUtil.isEmptyOrNull(to))
            return;

        to = to.trim();
        String[] tos = to.split(";");

        for (String s : tos)
        {
            if (StringUtil.isEmptyOrNull(s))
                continue;

            addNumber(s.trim());
        }
    }


    public interface IRecipientPickInitListener
    {
        void OnRecipientPickInit();
    }


}
