//package cz.uhk.bak.mmsparams;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import cz.uhk.bak.mmsparams.log.LogFB;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ListView;
//import android.widget.Toast;
//
//import com.google.android.gms.location.places.ui.PlacePicker;
//
//import org.whispersystems.libsignal.InvalidMessageException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import cz.uhk.bak.mmsparams.adapters.MmsAttachmentAdapter;
//import cz.uhk.bak.mmsparams.components.AttachmentTypeSelector;
//import cz.uhk.bak.mmsparams.components.ComposeText;
//import cz.uhk.bak.mmsparams.components.location.SignalPlace;
//import cz.uhk.bak.mmsparams.database.DatabaseFactory;
//import cz.uhk.bak.mmsparams.database.SmsDatabase;
//import cz.uhk.bak.mmsparams.database.ThreadDatabase;
//import cz.uhk.bak.mmsparams.database.model.MessageRecord;
//import cz.uhk.bak.mmsparams.database.model.MmsSetting;
//import cz.uhk.bak.mmsparams.mms.AttachmentManager;
//import cz.uhk.bak.mmsparams.mms.MediaConstraints;
//import cz.uhk.bak.mmsparams.mms.MediaType;
//import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
//import cz.uhk.bak.mmsparams.mms.PushMediaConstraints;
//import cz.uhk.bak.mmsparams.mms.Slide;
//import cz.uhk.bak.mmsparams.mms.SlideDeck;
//import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
//import cz.uhk.bak.mmsparams.recipients.Recipients;
//import cz.uhk.bak.mmsparams.scribbles.ScribbleActivity;
//import cz.uhk.bak.mmsparams.sms.MessageSender;
//import cz.uhk.bak.mmsparams.util.MediaUtil;
//import cz.uhk.bak.mmsparams.util.ViewUtil;
//import cz.uhk.bak.mmsparams.util.concurrent.ListenableFuture;
//import cz.uhk.bak.mmsparams.util.concurrent.SettableFuture;
//
//public class ConversationSendMmsLong extends PassphraseRequiredActionBarActivity2 implements AttachmentManager.AttachmentListener
//{
//    public static final String THREAD_ID = "param_thread_id";
//    private static final String TAG = ConversationSendMmsLong.class.getSimpleName();
//    private static final int PICK_GALLERY = 1;
//    private static final int PICK_DOCUMENT = 2;
//    private static final int PICK_AUDIO = 3;
//    private static final int PICK_CONTACT_INFO = 4;
//    private static final int GROUP_EDIT = 5;
//    private static final int TAKE_PHOTO = 6;
//    private static final int ADD_CONTACT = 7;
//    private static final int PICK_LOCATION = 8;
//    private static final int PICK_GIF = 9;
//    private static final int SMS_DEFAULT = 10;
//    private static final int PICK_SETTINGS = 11;
//    private static final int PICK_APN = 12;
//
//
//    private ConversationTitleView ctvRecipientInfo;
//
//    @Override
//    protected int GetLayoutResource()
//    {
//        return R.layout.send_mms_long_activity;
//    }
//
//    private long threadID;
//    private Recipients recipients;
//
//    private Button btnPickSettings;
//
//    private MmsSetting mmsSetting;
//
//    private MmsSettingsDetailControls mmsSettingsDetailControls;
//
//
//
//    private Button btnSendMms;
//    protected ComposeText composeText;
//
//    private int distributionType;
//
//    private ListView lvFiles;
//    private MmsAttachmentAdapter adapter;
//
//    private List<AttachmentManager> mediaData = new ArrayList<>();
//    private Button btnPickMedia;
//
//
//    @Override
//    protected void initControls()
//    {
//        threadID = getIntent().getLongExtra(THREAD_ID, -1);
//
//        if (threadID == -1)
//        {
//            Exception ex = new Exception("Thread is -1");
//            LogFB.e(TAG, "Thead_ID is -1", ex);
//        }
//
//
//        lvFiles = (ListView)findViewById(R.id.lvFiles);
//
//        btnPickMedia = (Button)findViewById(R.id.btnPickMedia);
//
//        ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(getApplicationContext());
//        recipients = threadDatabase.getRecipientsForThreadId(threadID);
//
//
//        Button btnSave = (Button) findViewById(R.id.btnSave);
//        btnSave.setVisibility(View.INVISIBLE);
//
//        Button btnDelete = (Button) findViewById(R.id.btnDelete);
//        btnDelete.setVisibility(View.INVISIBLE);
//
//
//        ctvRecipientInfo = (ConversationTitleView) findViewById(R.id.ctvRecipientInfo);
//
//        ctvRecipientInfo.setTitle(recipients);
//        ctvRecipientInfo.setColor(Color.BLACK);
//
//
//        btnPickSettings = (Button) findViewById(R.id.btnPickSettings);
//
//        mmsSettingsDetailControls = new MmsSettingsDetailControls(getApplicationContext(), findViewById(android.R.id.content).getRootView());
//        setMmsSetting(MmsSetting.MmsSettingFactory.getDefault());
//
//        btnSendMms = (Button) findViewById(R.id.btnSendMms);
//
//        attachmentManager = new AttachmentManager(this, this, getMasterSecret());
//
//        composeText = ViewUtil.findById(this, R.id.embedded_text_editor);
//
//        distributionType = getIntent().getIntExtra(ConversationActivity.DISTRIBUTION_TYPE_EXTRA, ThreadDatabase.DistributionTypes.DEFAULT);
//    }
//
//    @Override
//    protected void setupListeners()
//    {
//        btnPickSettings.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                handlePickSettings();
//            }
//        });
//
//        btnSendMms.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                sendMessage();
//            }
//        });
//
//        btnPickMedia.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                handlePickMedia();
//            }
//        });
//    }
//
//    private void handlePickMedia()
//    {
//
//    }
//
//    private AttachmentTypeSelector attachmentTypeSelector;
//    private void handleAddAttachment()
//    {
////        if (this.isMmsEnabled || isSecureText)
////        {
//            if (attachmentTypeSelector == null)
//            {
//                attachmentTypeSelector = new AttachmentTypeSelector(this, getSupportLoaderManager(), new AttachmentTypeListener());
//            }
//            attachmentTypeSelector.show(btnPickMedia);
////        }
////        else
////        {
////            handleManualMmsRequired();
////        }
//    }
//
//    private void addAttachment(int type)
//    {
//        LogFB.w("ComposeMessageActivity", "Selected: " + type);
//        switch (type)
//        {
//            case AttachmentTypeSelector.ADD_GALLERY:
//                AttachmentManager.selectGallery(this, PICK_GALLERY);
//                break;
//            case AttachmentTypeSelector.ADD_DOCUMENT:
//                AttachmentManager.selectDocument(this, PICK_DOCUMENT);
//                break;
//            case AttachmentTypeSelector.ADD_SOUND:
//                AttachmentManager.selectAudio(this, PICK_AUDIO);
//                break;
//            case AttachmentTypeSelector.ADD_CONTACT_INFO:
//                AttachmentManager.selectContactInfo(this, PICK_CONTACT_INFO);
//                break;
//            case AttachmentTypeSelector.ADD_LOCATION:
//                AttachmentManager.selectLocation(this, PICK_LOCATION);
//                break;
//            case AttachmentTypeSelector.TAKE_PHOTO:
//                attachmentManager.capturePhoto(this, TAKE_PHOTO);
//                break;
//            case AttachmentTypeSelector.ADD_GIF:
//                AttachmentManager.selectGif(this, PICK_GIF, !false);
//                break;
//
//            case AttachmentTypeSelector.PICK_SETTINGS:
//                AttachmentManager.pickSettings(this, this.PICK_SETTINGS);
//                break;
//
//        }
//    }
//
//    private class AttachmentTypeListener implements AttachmentTypeSelector.AttachmentClickedListener
//    {
//        @Override
//        public void onClick(int type)
//        {
//
//            addAttachment(type);
//        }
//
//        @Override
//        public void onQuickAttachment(Uri uri)
//        {
//            Intent intent = new Intent();
//            intent.setData(uri);
//
//            onActivityResult(PICK_GALLERY, RESULT_OK, intent);
//        }
//    }
//
//    @Override
//    protected void afterBaseInit()
//    {
//        super.afterBaseInit();
//
//
//        super.afterBaseInit();
//        adapter = new MmsAttachmentAdapter(getApplicationContext(), mediaData);
//        lvFiles.setAdapter(adapter);
//
//    }
//
//    private void sendMessage()
//    {
//        // is edited - mark as new + virtual
//        MmsSetting mmsSetting = mmsSettingsDetailControls.handleChangeAndSave(getMmsSetting());
//        setMmsSetting(mmsSetting);
//
//
//        boolean forceSms = false; //sendButton.isManualSelection() && sendButton.getSelectedTransport().isSms();
//        int subscriptionId = -1; //sendButton.getSelectedTransport().getSimSubscriptionId().or(-1);
//        long expiresIn = recipients.getExpireMessages() * 1000;
//
//
////        LogFB.w(TAG, "isManual Selection: " + sendButton.isManualSelection());
//        LogFB.w(TAG, "forceSms: " + forceSms);
//
//
//        try
//        {
//            sendMediaMessage(forceSms, expiresIn, subscriptionId);
//        } catch (InvalidMessageException ex)
//        {
//            Toast.makeText(getApplicationContext(), R.string.ConversationActivity_message_is_empty_exclamation,
//                    Toast.LENGTH_SHORT).show();
//            LogFB.w(TAG, ex);
//        }
//    }
//
//
//    private AttachmentManager attachmentManager;
//
//
//    private void sendMediaMessage(final boolean forceSms, final long expiresIn, final int subscriptionId)
//            throws InvalidMessageException
//    {
//        sendMediaMessage(forceSms, getMessage(), attachmentManager.buildSlideDeck(), mmsSetting.getID(),apnExtId. expiresIn, subscriptionId);
//    }
//
//    private ListenableFuture<Void> sendMediaMessage(final boolean forceSms, String body, SlideDeck slideDeck, int mmsSettingId, int apnExtId, final long expiresIn, final int subscriptionId)
//            throws InvalidMessageException
//    {
//        final SettableFuture<Void> future = new SettableFuture<>();
//        final Context context = getApplicationContext();
//        OutgoingMediaMessage outgoingMessage = new OutgoingMediaMessage(recipients,
//                slideDeck,
//                mmsSettingId,
//                apnExtId,
//                body,
//                System.currentTimeMillis(),
//                subscriptionId,
//                expiresIn,
//                distributionType);
//
////        if (isSecureText && !forceSms)
////        {
////            outgoingMessage = new OutgoingSecureMediaMessage(outgoingMessage);
////        }
//
//        attachmentManager.clear(false);
//        composeText.setText("");
//        final long id = stageOutgoingMessage(outgoingMessage);
//
//        new AsyncTask<OutgoingMediaMessage, Void, Long>()
//        {
//            @Override
//            protected Long doInBackground(OutgoingMediaMessage... messages)
//            {
//                return MessageSender.send(context, getMasterSecret(), messages[0], threadID, forceSms, new SmsDatabase.InsertListener()
//                {
//                    @Override
//                    public void onComplete()
//                    {
//                        releaseOutgoingMessage(id);
//                    }
//                });
//            }
//
//            @Override
//            protected void onPostExecute(Long result)
//            {
//                sendComplete(result);
//                future.set(null);
//            }
//        }.execute(outgoingMessage);
//
//        return future;
//    }
//
//    private void sendComplete(Long threadID)
//    {
//        finish();
//    }
//
//    public long stageOutgoingMessage(OutgoingMediaMessage message)
//    {
//        MessageRecord messageRecord = DatabaseFactory.getMmsDatabase(getApplicationContext()).readerFor(message, threadID).getCurrent();
//
////        if (getListAdapter() != null)
////        {
////            getListAdapter().addFastRecord(messageRecord);
////        }
//
//        return messageRecord.getId();
//    }
//
//    public void releaseOutgoingMessage(long id)
//    {
////        if (getListAdapter() != null)
////        {
////            getListAdapter().releaseFastRecord(id);
////        }
//    }
//
//    private String getMessage() throws InvalidMessageException
//    {
//        String rawText = composeText.getTextTrimmed();
//
//        if (rawText.length() < 1 && !attachmentManager.isAttachmentPresent())
//            throw new InvalidMessageException(getString(R.string.ConversationActivity_message_is_empty_exclamation));
//
//        return rawText;
//    }
//
//
//    private void handlePickSettings()
//    {
//        AttachmentManager.pickSettings(this, this.PICK_SETTINGS);
//    }
//
//    @Override
//    protected void onActivityResult(final int reqCode, int resultCode, Intent data)
//    {
//        LogFB.w(TAG, "onActivityResult called: " + reqCode + ", " + resultCode + " , " + data);
//        super.onActivityResult(reqCode, resultCode, data);
//
//        switch (reqCode)
//        {
//            case PICK_GALLERY:
//                MediaType mediaType;
//
//                String mimeType = MediaUtil.getMimeType(this, data.getData());
//
//                if (MediaUtil.isGif(mimeType)) mediaType = MediaType.GIF;
//                else if (MediaUtil.isVideo(mimeType)) mediaType = MediaType.VIDEO;
//                else mediaType = MediaType.IMAGE;
//
//                setMedia(data.getData(), mediaType);
//                break;
//            case PICK_DOCUMENT:
//                setMedia(data.getData(), MediaType.DOCUMENT);
//                break;
//            case PICK_AUDIO:
//                setMedia(data.getData(), MediaType.AUDIO);
//                break;
////            case PICK_CONTACT_INFO:
////                addAttachmentContactInfo(data.getData());
////                break;
////            case GROUP_EDIT:
////                recipients = RecipientFactory.getRecipientsForIds(this, data.getLongArrayExtra(GroupCreateActivity.GROUP_RECIPIENT_EXTRA), true);
////                recipients.addListener(this);
////                titleView.setTitle(recipients);
////                setBlockedUserState(recipients, isSecureText, isDefaultSms);
////                supportInvalidateOptionsMenu();
////                break;
//            case TAKE_PHOTO:
//                if (attachmentManager.getCaptureUri() != null)
//                {
//                    setMedia(attachmentManager.getCaptureUri(), MediaType.IMAGE);
//                }
//                break;
////            case ADD_CONTACT:
////                recipients = RecipientFactory.getRecipientsForIds(getApplicationContext(), recipients.getIds(), true);
////                recipients.addListener(this);
////                fragment.reloadList();
////                break;
//            case PICK_LOCATION:
//                SignalPlace place = new SignalPlace(PlacePicker.getPlace(data, this));
//                attachmentManager.setLocation(getMasterSecret(), place, getCurrentMediaConstraints());
//                break;
//            case PICK_GIF:
//                setMedia(data.getData(), MediaType.GIF);
//                break;
//            case ScribbleActivity.SCRIBBLE_REQUEST_CODE:
//                setMedia(data.getData(), MediaType.IMAGE);
//                break;
////            case SMS_DEFAULT:
////                initializeSecurity(isSecureText, isDefaultSms);
////                break;
//
//
//
//            case PICK_SETTINGS:
//                setSettings(data);
//                break;
//        }
//
//
//        adapter.notifyDataSetChanged();
//    }
//
//    private void setMedia(@Nullable Uri uri, @NonNull MediaType mediaType)
//    {
//        if (uri == null)
//            return;
//
//        AttachmentManager am = new AttachmentManager(this, this, getMasterSecret());
//
//        am.setMedia(super.getMasterSecret(),  uri, mediaType, getCurrentMediaConstraints());
//
//        mediaData.add(am);
//
//    }
//
//    private MediaConstraints getCurrentMediaConstraints()
//    {
//        return new PushMediaConstraints();
////                sendButton.getSelectedTransport().getType() == TransportOption.Type.TEXTSECURE
////                ? MediaConstraints.getPushMediaConstraints()
////                : MediaConstraints.getMmsMediaConstraints(sendButton.getSelectedTransport().getSimSubscriptionId().or(-1));
//    }
//
//    public void setSettings(@Nullable Intent data)
//    {
//        try
//        {
//            Object o = data.getExtras().get("result");
//            if (o == null || !(o instanceof MmsSetting))
//                return;
//
//            setMmsSetting((MmsSetting) o);
//
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//
//    public MmsSetting getMmsSetting()
//    {
//        if (mmsSetting == null)
//            this.mmsSetting = MmsSetting.MmsSettingFactory.getDefault();
//        return mmsSetting;
//    }
//
//    public void setMmsSetting(MmsSetting mmsSetting)
//    {
//        this.mmsSetting = mmsSetting;
//        mmsSettingsDetailControls.fillControls(mmsSetting);
//    }
//
//
//    @Override
//    public void onAttachmentChanged()
//    {
////        handleSecurityChange(isSecureText, isDefaultSms);
////        updateToggleButtonState();
//    }
//
//}
