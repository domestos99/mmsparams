package cz.uhk.bak.mmsparams.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;

import java.util.LinkedList;
import java.util.List;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagingDatabase.ExpirationInfo;
import cz.uhk.bak.mmsparams.database.MessagingDatabase.MarkedMessageInfo;
import cz.uhk.bak.mmsparams.database.MessagingDatabase.SyncMessageId;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.service.ExpiringMessageManager;

public class MarkReadReceiver extends MasterSecretBroadcastReceiver
{

    private static final String TAG = MarkReadReceiver.class.getSimpleName();
    public static final String CLEAR_ACTION = "cz.uhk.bak.mmsparams.notifications.CLEAR";
    public static final String THREAD_IDS_EXTRA = "thread_ids";
    public static final String NOTIFICATION_ID_EXTRA = "notification_id";

    @Override
    protected void onReceive(final Context context, Intent intent, @Nullable final MasterSecret masterSecret)
    {
        if (!CLEAR_ACTION.equals(intent.getAction()))
            return;

        final long[] threadIds = intent.getLongArrayExtra(THREAD_IDS_EXTRA);

        if (threadIds != null)
        {
            NotificationManagerCompat.from(context).cancel(intent.getIntExtra(NOTIFICATION_ID_EXTRA, -1));

            new AsyncTask<Void, Void, Void>()
            {
                @Override
                protected Void doInBackground(Void... params)
                {
                    List<MarkedMessageInfo> messageIdsCollection = new LinkedList<>();

                    for (long threadId : threadIds)
                    {
                        LogFB.w(TAG, "Marking as read: " + threadId);
                        List<MarkedMessageInfo> messageIds = DatabaseFactory.getThreadDatabase(context).setRead(threadId, true);
                        messageIdsCollection.addAll(messageIds);
                    }

                    process(context, messageIdsCollection);

                    MessageNotifier.updateNotification(context, masterSecret);

                    return null;
                }
            }.execute();
        }
    }

    public static void process(@NonNull Context context, @NonNull List<MarkedMessageInfo> markedReadMessages)
    {
        if (markedReadMessages.isEmpty()) return;

        List<SyncMessageId> syncMessageIds = new LinkedList<>();

        for (MarkedMessageInfo messageInfo : markedReadMessages)
        {
            scheduleDeletion(context, messageInfo.getExpirationInfo());
            syncMessageIds.add(messageInfo.getSyncMessageId());
        }

//        ApplicationContext.getInstance(context)
//                .getJobManager()
//                .add(new MultiDeviceReadUpdateJob(context, syncMessageIds));
    }

    private static void scheduleDeletion(Context context, ExpirationInfo expirationInfo)
    {
        if (expirationInfo.getExpiresIn() > 0 && expirationInfo.getExpireStarted() <= 0)
        {
            ExpiringMessageManager expirationManager = ApplicationContext.getInstance(context).getExpiringMessageManager();

            if (expirationInfo.isMms())
                DatabaseFactory.getMmsDatabase(context).markExpireStarted(expirationInfo.getId());
            else DatabaseFactory.getSmsDatabase(context).markExpireStarted(expirationInfo.getId());

            expirationManager.scheduleDeletion(expirationInfo.getId(), expirationInfo.isMms(), expirationInfo.getExpiresIn());
        }
    }
}
