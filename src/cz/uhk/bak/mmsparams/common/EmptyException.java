package cz.uhk.bak.mmsparams.common;

public class EmptyException extends Throwable
{
    public EmptyException()
    {
    }

    public EmptyException(String message)
    {
        super(message);
    }
}
