package cz.uhk.bak.mmsparams.jobs;

import android.app.Activity;
import android.content.Context;
import android.telephony.SmsManager;

import org.whispersystems.jobqueue.JobParameters;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.EncryptingSmsDatabase;
import cz.uhk.bak.mmsparams.database.NoSuchMessageException;
import cz.uhk.bak.mmsparams.database.model.SmsMessageRecord;
import cz.uhk.bak.mmsparams.jobs.requirements.MasterSecretRequirement;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.notifications.MessageNotifier;
import cz.uhk.bak.mmsparams.service.SmsDeliveryListener;

public class SmsSentJob extends MasterSecretJob
{

    private static final String TAG = SmsSentJob.class.getSimpleName();

    private final long messageId;
    private final String action;
    private final int result;

    public SmsSentJob(Context context, long messageId, String action, int result)
    {
        super(context, JobParameters.newBuilder()
                .withPersistence()
                .withRequirement(new MasterSecretRequirement(context))
                .create());

        this.messageId = messageId;
        this.action = action;
        this.result = result;
    }

    @Override
    public void onAdded()
    {

    }

    @Override
    public void onRun(MasterSecret masterSecret)
    {
        LogFB.w(TAG, "Got SMS callback: " + action + " , " + result);

        switch (action)
        {
            case SmsDeliveryListener.SENT_SMS_ACTION:
                handleSentResult(masterSecret, messageId, result);
                break;
            case SmsDeliveryListener.DELIVERED_SMS_ACTION:
                handleDeliveredResult(messageId, result);
                break;
        }
    }

    @Override
    public boolean onShouldRetryThrowable(Exception throwable)
    {
        return false;
    }

    @Override
    public void onCanceled()
    {

    }

    private void handleDeliveredResult(long messageId, int result)
    {
        DatabaseFactory.getEncryptingSmsDatabase(context).markStatus(messageId, result);
    }

    private void handleSentResult(MasterSecret masterSecret, long messageId, int result)
    {
        try
        {
            EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
            SmsMessageRecord record = database.getMessage(masterSecret, messageId);

            switch (result)
            {
                case Activity.RESULT_OK:
                    database.markAsSent(messageId, false);
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    LogFB.w(TAG, "Service connectivity problem, requeuing...");
                    ApplicationContext.getInstance(context)
                            .getJobManager()
                            .add(new SmsSendJob(context, messageId, record.getIndividualRecipient().getNumber()));
                    break;
                default:
                    database.markAsSentFailed(messageId);
                    MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipients(), record.getThreadId());
            }
        }
        catch (NoSuchMessageException e)
        {
            LogFB.w(TAG, e);
        }
    }
}
