package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduHeaders;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.jobqueue.requirements.NetworkRequirement;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.MmsTemplateDatabase;
import cz.uhk.bak.mmsparams.database.NoSuchMessageException;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.database.model.MmsTemplateDetailGroup;
import cz.uhk.bak.mmsparams.jobs.requirements.MasterSecretRequirement;
import cz.uhk.bak.mmsparams.log.DBLog;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.CompatMmsConnection;
import cz.uhk.bak.mmsparams.mms.MmsException;
import cz.uhk.bak.mmsparams.mms.MmsSendResult;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.notifications.MessageNotifier;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.transport.InsecureFallbackApprovalException;
import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;
import cz.uhk.bak.mmsparams.util.Hex;
import cz.uhk.bak.mmsparams.util.MmsTemplateUtils;
import cz.uhk.bak.mmsparams.util.NumberUtil;
import cz.uhk.bak.mmsparams.util.StringUtil;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class MmsTemplateSendJob extends SendJob
{
    private static final String TAG = MmsTemplateSendJob.class.getSimpleName();

    private final int templateID;
    private final long messageId;

    public MmsTemplateSendJob(Context context, long messageId, int templateID)
    {
        super(context, JobParameters.newBuilder()
                .withGroupId("mms-operation")
                .withRequirement(new NetworkRequirement(context))
                .withRequirement(new MasterSecretRequirement(context))
                .withPersistence()
                .withRetryCount(1)
                .create());


        this.templateID = templateID;
        this.messageId = messageId;
    }

    @Override
    public void onAdded()
    {

    }

    @Override
    public void onSend(MasterSecret masterSecret) throws MmsException, NoSuchMessageException, IOException
    {
        final MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);

        try
        {
            MmsDatabase mmsDatabase = DatabaseFactory.getMmsDatabase(context);
            OutgoingMediaMessage message = mmsDatabase.getOutgoingMessage(masterSecret, messageId);

            MmsTemplateDatabase database = DatabaseFactory.getMmsTemplateDatabase(context);
            MmsTemplate mt = database.getById(templateID);

            if (mt == null)
            {
                DBLog.logToDB(context, "MmsTemplate ID not found: " + templateID, TAG, null);
                return;
            }

            // nacteni profilu
            MmsProfile settings = TextSecurePreferences.getMmsSettingProfileObj(context);

            try
            {
                MySendReq pdu = PduByteHelper.constructSendPdu(context, masterSecret, message, settings, false);

                pdu = addParamsFromTeplate(pdu, mt);

                validateDestinations(pdu);

                final byte[] pduBytes = getPduBytes(pdu);


                MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);

                PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_SEND_REQ, pduBytes);

//            if(true)
//                return;

                mmsErrorLogger.setMessageID(messageId);
                // Send and get confirmation
                final byte[] sendConf = new CompatMmsConnection(context, mmsErrorLogger).send(pduBytes, message.getSubscriptionId());

                MySendConf sendConfObj = PduByteHelper.getPdu(sendConf);  //(SendConf) new MyPduParser(sendConf).parse();

                // Save PDU data of confirmation
                PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_SEND_CONF, sendConf);


                final MmsSendResult result = getSendResult(sendConfObj, pdu);

                mmsErrorLogger.setMessageID(messageId);
                mmsDatabase.markAsSent(messageId, false);
                markAttachmentsUploaded(messageId, message.getAttachments());
                mmsErrorLogger.setMessageID(messageId);
            }
            catch (UndeliverableMessageException | IOException e)
            {
                LogFB.w(TAG, e);
                DBLog.logToDB(context, TAG, e);
                mmsDatabase.markAsSentFailed(messageId);
                notifyMediaMessageDeliveryFailed(context, messageId);

                mmsErrorLogger.log(messageId, TAG, e);
            }
            catch (InsecureFallbackApprovalException e)
            {
                LogFB.w(TAG, e);
                DBLog.logToDB(context, TAG, e);
                mmsDatabase.markAsPendingInsecureSmsFallback(messageId);
                notifyMediaMessageDeliveryFailed(context, messageId);

                mmsErrorLogger.log(messageId, TAG, e);
            }
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "OnSend - whole t-c", TAG, ex);
            mmsErrorLogger.log(messageId, TAG, ex);
            onCanceled();
            return;
        }
    }

    private MySendReq addParamsFromTeplate(final MySendReq pdu, final MmsTemplate mt)
    {
        MmsTemplateDetailGroup gr = mt.getMmsTemplateDetailGroup();

        if (gr == null)
            return pdu;


        List<String> to = MmsTemplateUtils.getNumbers(gr.getTo());
        List<String> bcc = MmsTemplateUtils.getNumbers(gr.getBcc());
        List<String> cc = MmsTemplateUtils.getNumbers(gr.getCc());

        for (String t : to)
        {
            if (!StringUtil.isEmptyOrNull(t))
                pdu.addTo(new EncodedStringValue(StringUtil.removeSpaces(t)));
        }

        for (String t : bcc)
        {
            if (!StringUtil.isEmptyOrNull(t))
                pdu.addBcc(new EncodedStringValue(StringUtil.removeSpaces(t)));
        }
        for (String t : cc)
        {
            if (!StringUtil.isEmptyOrNull(t))
                pdu.addCc(new EncodedStringValue(StringUtil.removeSpaces(t)));
        }


        if (!StringUtil.isEmptyOrNull(gr.getSubject()))
        {
            pdu.setSubject(new EncodedStringValue(gr.getSubject()));
        }

        return pdu;
    }


    @Override
    public boolean onShouldRetryThrowable(Exception exception)
    {
        return false;
    }

    @Override
    public void onCanceled()
    {
        DatabaseFactory.getMmsDatabase(context).markAsSentFailed(messageId);
        notifyMediaMessageDeliveryFailed(context, messageId);
    }

    private byte[] getPduBytes(GenericPdu message)
            throws IOException, UndeliverableMessageException, InsecureFallbackApprovalException
    {
        byte[] pduBytes = PduByteHelper.getPduBytes(context, message);

        if (pduBytes == null)
        {
            throw new UndeliverableMessageException("PDU composition failed, null payload");
        }

        return pduBytes;
    }

    private MmsSendResult getSendResult(MySendConf conf, MySendReq message)
            throws UndeliverableMessageException
    {
        if (conf == null)
        {
            throw new UndeliverableMessageException("No M-Send.conf received in response to send.");
        }
        else if (conf.getResponseStatus() != PduHeaders.RESPONSE_STATUS_OK)
        {
            throw new UndeliverableMessageException("Got bad response: " + conf.getResponseStatus());
        }
        else if (isInconsistentResponse(message, conf))
        {
            throw new UndeliverableMessageException("Mismatched response!");
        }
        else
        {
            return new MmsSendResult(conf.getMessageId(), conf.getResponseStatus());
        }
    }

    private boolean isInconsistentResponse(MySendReq message, MySendConf response)
    {
        LogFB.w(TAG, "Comparing: " + Hex.toString(message.getTransactionId()));
        LogFB.w(TAG, "With:      " + Hex.toString(response.getTransactionId()));
        return !Arrays.equals(message.getTransactionId(), response.getTransactionId());
    }

    private void validateDestinations(EncodedStringValue[] destinations) throws UndeliverableMessageException
    {
        if (destinations == null)
            return;

        for (EncodedStringValue destination : destinations)
        {
            if (destination == null || !NumberUtil.isValidSmsOrEmail(destination.getString()))
            {
                throw new UndeliverableMessageException("Invalid destination: " +
                        (destination == null ? null : destination.getString()));
            }
        }
    }

    private void validateDestinations(MySendReq message) throws UndeliverableMessageException
    {
        validateDestinations(message.getTo());
        validateDestinations(message.getCc());
        validateDestinations(message.getBcc());

        if (message.getTo() == null && message.getCc() == null && message.getBcc() == null)
        {
            throw new UndeliverableMessageException("No to, cc, or bcc specified!");
        }
    }


    private void notifyMediaMessageDeliveryFailed(Context context, long messageId)
    {
        long threadId = DatabaseFactory.getMmsDatabase(context).getThreadIdForMessage(messageId);
        Recipients recipients = DatabaseFactory.getThreadDatabase(context).getRecipientsForThreadId(threadId);

        if (recipients != null)
        {
            MessageNotifier.notifyMessageDeliveryFailed(context, recipients, threadId);
        }
    }
}
