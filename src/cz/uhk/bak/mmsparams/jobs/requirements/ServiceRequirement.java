package cz.uhk.bak.mmsparams.jobs.requirements;

import android.content.Context;

import org.whispersystems.jobqueue.dependencies.ContextDependent;
import org.whispersystems.jobqueue.requirements.Requirement;

import cz.uhk.bak.mmsparams.sms.TelephonyServiceState;

public class ServiceRequirement implements Requirement, ContextDependent
{

    private static final String TAG = ServiceRequirement.class.getSimpleName();

    private transient Context context;

    public ServiceRequirement(Context context)
    {
        this.context = context;
    }

    @Override
    public void setContext(Context context)
    {
        this.context = context;
    }

    @Override
    public boolean isPresent()
    {
        TelephonyServiceState telephonyServiceState = new TelephonyServiceState();
        return telephonyServiceState.isConnected(context);
    }
}
