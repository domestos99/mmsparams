package cz.uhk.bak.mmsparams.jobs.requirements;

import android.content.Context;

import org.whispersystems.jobqueue.dependencies.ContextDependent;
import org.whispersystems.jobqueue.requirements.Requirement;

import cz.uhk.bak.mmsparams.service.KeyCachingService;

public class MasterSecretRequirement implements Requirement, ContextDependent
{

    private transient Context context;

    public MasterSecretRequirement(Context context)
    {
        this.context = context;
    }

    @Override
    public boolean isPresent()
    {
        return KeyCachingService.getMasterSecret(context) != null;
    }

    @Override
    public void setContext(Context context)
    {
        this.context = context;
    }
}
