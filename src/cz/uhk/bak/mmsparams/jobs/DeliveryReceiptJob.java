package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.jobqueue.requirements.NetworkRequirement;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

import java.io.IOException;

import javax.inject.Inject;

import cz.uhk.bak.mmsparams.dependencies.InjectableType;
import cz.uhk.bak.mmsparams.log.LogFB;

import static cz.uhk.bak.mmsparams.dependencies.SignalCommunicationModule.SignalMessageSenderFactory;

public class DeliveryReceiptJob extends ContextJob implements InjectableType
{

    private static final long serialVersionUID = 1L;

    private static final String TAG = DeliveryReceiptJob.class.getSimpleName();

    @Inject
    transient SignalMessageSenderFactory messageSenderFactory;

    private final String destination;
    private final long timestamp;
    private final String relay;

    public DeliveryReceiptJob(Context context, String destination, long timestamp, String relay)
    {
        super(context, JobParameters.newBuilder()
                .withRequirement(new NetworkRequirement(context))
                .withPersistence()
                .withRetryCount(50)
                .create());

        this.destination = destination;
        this.timestamp = timestamp;
        this.relay = relay;
    }

    @Override
    public void onAdded()
    {
    }

    @Override
    public void onRun() throws IOException
    {
        LogFB.w("DeliveryReceiptJob", "Sending delivery receipt...");
        SignalServiceMessageSender messageSender = messageSenderFactory.create();
        SignalServiceAddress textSecureAddress = new SignalServiceAddress(destination, Optional.fromNullable(relay));

        messageSender.sendDeliveryReceipt(textSecureAddress, timestamp);
    }

    @Override
    public void onCanceled()
    {
        LogFB.w(TAG, "Failed to send receipt after retry exhausted!");
    }

    @Override
    public boolean onShouldRetry(Exception exception)
    {
        LogFB.w(TAG, exception);
        if (exception instanceof NonSuccessfulResponseCodeException) return false;
        if (exception instanceof PushNetworkException) return true;

        return false;
    }
}
