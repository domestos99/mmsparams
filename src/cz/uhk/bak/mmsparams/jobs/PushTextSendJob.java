package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;
import org.whispersystems.signalservice.api.util.InvalidNumberException;

import java.io.IOException;

import javax.inject.Inject;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.EncryptingSmsDatabase;
import cz.uhk.bak.mmsparams.database.NoSuchMessageException;
import cz.uhk.bak.mmsparams.database.model.SmsMessageRecord;
import cz.uhk.bak.mmsparams.dependencies.InjectableType;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.notifications.MessageNotifier;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.service.ExpiringMessageManager;
import cz.uhk.bak.mmsparams.transport.InsecureFallbackApprovalException;
import cz.uhk.bak.mmsparams.transport.RetryLaterException;

import static cz.uhk.bak.mmsparams.dependencies.SignalCommunicationModule.SignalMessageSenderFactory;

public class PushTextSendJob extends PushSendJob implements InjectableType
{

    private static final long serialVersionUID = 1L;

    private static final String TAG = PushTextSendJob.class.getSimpleName();

    @Inject
    transient SignalMessageSenderFactory messageSenderFactory;

    private final long messageId;

    public PushTextSendJob(Context context, long messageId, String destination)
    {
        super(context, constructParameters(context, destination));
        this.messageId = messageId;
    }

    @Override
    public void onAdded()
    {
    }

    @Override
    public void onPushSend(MasterSecret masterSecret) throws NoSuchMessageException, RetryLaterException
    {
        ExpiringMessageManager expirationManager = ApplicationContext.getInstance(context).getExpiringMessageManager();
        EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
        SmsMessageRecord record = database.getMessage(masterSecret, messageId);

        try
        {
            LogFB.w(TAG, "Sending message: " + messageId);

            deliver(record);
            database.markAsSent(messageId, true);

            if (record.getExpiresIn() > 0)
            {
                database.markExpireStarted(messageId);
                expirationManager.scheduleDeletion(record.getId(), record.isMms(), record.getExpiresIn());
            }

        }
        catch (InsecureFallbackApprovalException e)
        {
            LogFB.w(TAG, e);
            database.markAsPendingInsecureSmsFallback(record.getId());
            MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipients(), record.getThreadId());
            ApplicationContext.getInstance(context).getJobManager().add(new DirectoryRefreshJob(context));
        }
        catch (UntrustedIdentityException e)
        {
            LogFB.w(TAG, e);
            Recipients recipients = RecipientFactory.getRecipientsFromString(context, e.getE164Number(), false);
            long recipientId = recipients.getPrimaryRecipient().getRecipientId();

            database.addMismatchedIdentity(record.getId(), recipientId, e.getIdentityKey());
            database.markAsSentFailed(record.getId());
            database.markAsPush(record.getId());
        }
    }

    @Override
    public boolean onShouldRetryThrowable(Exception exception)
    {
        if (exception instanceof RetryLaterException) return true;

        return false;
    }

    @Override
    public void onCanceled()
    {
        DatabaseFactory.getSmsDatabase(context).markAsSentFailed(messageId);

        long threadId = DatabaseFactory.getSmsDatabase(context).getThreadIdForMessage(messageId);
        Recipients recipients = DatabaseFactory.getThreadDatabase(context).getRecipientsForThreadId(threadId);

        if (threadId != -1 && recipients != null)
        {
            MessageNotifier.notifyMessageDeliveryFailed(context, recipients, threadId);
        }
    }

    private void deliver(SmsMessageRecord message)
            throws UntrustedIdentityException, InsecureFallbackApprovalException, RetryLaterException
    {
        try
        {
            SignalServiceAddress address = getPushAddress(message.getIndividualRecipient().getNumber());
            SignalServiceMessageSender messageSender = messageSenderFactory.create();
            SignalServiceDataMessage textSecureMessage = SignalServiceDataMessage.newBuilder()
                    .withTimestamp(message.getDateSent())
                    .withBody(message.getBody().getBody())
                    .withExpiration((int) (message.getExpiresIn() / 1000))
                    .asEndSessionMessage(message.isEndSession())
                    .build();


            messageSender.sendMessage(address, textSecureMessage);
        }
        catch (InvalidNumberException | UnregisteredUserException e)
        {
            LogFB.w(TAG, e);
            throw new InsecureFallbackApprovalException(e);
        }
        catch (IOException e)
        {
            LogFB.w(TAG, e);
            throw new RetryLaterException(e);
        }
    }
}
