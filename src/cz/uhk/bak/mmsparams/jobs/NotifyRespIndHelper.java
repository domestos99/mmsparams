package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import com.google.android.mms.pdu_alt.NotifyRespInd;
import com.google.android.mms.pdu_alt.PduHeaders;

import cz.uhk.bak.mmsparams.mms.CompatMmsConnection;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class NotifyRespIndHelper
{
    private static final String TAG = NotifyRespIndHelper.class.getSimpleName();

    public enum RetrievedStatus
    {
        RETRIEVED,
        DEFERRED
    }

    public static void sendNotifyRespIndRetrieved(final Context context, final byte[] transactionId, final long messageID,
                                                  final int subscriptionId, final RetrievedStatus retrievedStatus) throws Exception
    {
        int status;

        switch (retrievedStatus)
        {
            case RETRIEVED:
                status = PduHeaders.STATUS_RETRIEVED;
                break;
            case DEFERRED:
                status = PduHeaders.STATUS_DEFERRED;
                break;

            default:
                status = PduHeaders.STATUS_RETRIEVED;
                break;
        }

        NotifyRespInd notifyResponse = new NotifyRespInd(PduHeaders.CURRENT_MMS_VERSION,
                transactionId,
                status);

        boolean allowDeliveryReport = TextSecurePreferences.isMmsAllowSendDeliveryReport(context);
        notifyResponse.setReportAllowed(allowDeliveryReport ? PduHeaders.VALUE_YES : PduHeaders.VALUE_NO);


        byte[] nBytes = PduByteHelper.getPduBytes(context, notifyResponse);

        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
        mmsErrorLogger.setMessageID(messageID);

        final byte[] sendConf = new CompatMmsConnection(context, mmsErrorLogger).send(nBytes, subscriptionId);

        PduByteHelper.storeMessagePdu(context, messageID, PduHeaders.MESSAGE_TYPE_NOTIFYRESP_IND, nBytes);
    }


}
