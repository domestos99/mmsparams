package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;
import org.whispersystems.signalservice.api.util.InvalidNumberException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.NoSuchMessageException;
import cz.uhk.bak.mmsparams.dependencies.InjectableType;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.AttachmentHelper;
import cz.uhk.bak.mmsparams.mms.MediaConstraints;
import cz.uhk.bak.mmsparams.mms.MmsException;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.service.ExpiringMessageManager;
import cz.uhk.bak.mmsparams.transport.InsecureFallbackApprovalException;
import cz.uhk.bak.mmsparams.transport.RetryLaterException;
import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;

import static cz.uhk.bak.mmsparams.dependencies.SignalCommunicationModule.SignalMessageSenderFactory;

public class PushMediaSendJob extends PushSendJob implements InjectableType
{

    private static final long serialVersionUID = 1L;

    private static final String TAG = PushMediaSendJob.class.getSimpleName();

    @Inject
    transient SignalMessageSenderFactory messageSenderFactory;

    private final long messageId;

    public PushMediaSendJob(Context context, long messageId, String destination)
    {
        super(context, constructParameters(context, destination));
        this.messageId = messageId;
    }

    @Override
    public void onAdded()
    {

    }

    @Override
    public void onPushSend(MasterSecret masterSecret)
            throws RetryLaterException, MmsException, NoSuchMessageException,
            UndeliverableMessageException
    {
        ExpiringMessageManager expirationManager = ApplicationContext.getInstance(context).getExpiringMessageManager();
        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
        OutgoingMediaMessage message = database.getOutgoingMessage(masterSecret, messageId);

        try
        {
            deliver(masterSecret, message);
            database.markAsSent(messageId, true);
            markAttachmentsUploaded(messageId, message.getAttachments());

            if (message.getExpiresIn() > 0 && !message.isExpirationUpdate())
            {
                database.markExpireStarted(messageId);
                expirationManager.scheduleDeletion(messageId, true, message.getExpiresIn());
            }

        }
        catch (InsecureFallbackApprovalException ifae)
        {
            LogFB.w(TAG, ifae);
            database.markAsPendingInsecureSmsFallback(messageId);
            notifyMediaMessageDeliveryFailed(context, messageId);
            ApplicationContext.getInstance(context).getJobManager().add(new DirectoryRefreshJob(context));
        }
        catch (UntrustedIdentityException uie)
        {
            LogFB.w(TAG, uie);
            Recipients recipients = RecipientFactory.getRecipientsFromString(context, uie.getE164Number(), false);
            long recipientId = recipients.getPrimaryRecipient().getRecipientId();

            database.addMismatchedIdentity(messageId, recipientId, uie.getIdentityKey());
            database.markAsSentFailed(messageId);
        }
    }

    @Override
    public boolean onShouldRetryThrowable(Exception exception)
    {
        if (exception instanceof RequirementNotMetException) return true;
        if (exception instanceof RetryLaterException) return true;

        return false;
    }

    @Override
    public void onCanceled()
    {
        DatabaseFactory.getMmsDatabase(context).markAsSentFailed(messageId);
        notifyMediaMessageDeliveryFailed(context, messageId);
    }

    private void deliver(MasterSecret masterSecret, OutgoingMediaMessage message)
            throws RetryLaterException, InsecureFallbackApprovalException, UntrustedIdentityException,
            UndeliverableMessageException
    {
        if (message.getRecipients() == null ||
                message.getRecipients().getPrimaryRecipient() == null ||
                message.getRecipients().getPrimaryRecipient().getNumber() == null)
        {
            throw new UndeliverableMessageException("No destination address.");
        }

        SignalServiceMessageSender messageSender = messageSenderFactory.create();

        try
        {
            SignalServiceAddress address = getPushAddress(message.getRecipients().getPrimaryRecipient().getNumber());
            MediaConstraints mediaConstraints = MediaConstraints.getPushMediaConstraints();
            List<Attachment> scaledAttachments = AttachmentHelper.scaleAttachments(context, masterSecret, mediaConstraints, message.getAttachments());
            List<SignalServiceAttachment> attachmentStreams = getAttachmentsFor(masterSecret, scaledAttachments);
            SignalServiceDataMessage mediaMessage = SignalServiceDataMessage.newBuilder()
                    .withBody(message.getBody())
                    .withAttachments(attachmentStreams)
                    .withTimestamp(message.getSentTimeMillis())
                    .withExpiration((int) (message.getExpiresIn() / 1000))
                    .asExpirationUpdate(message.isExpirationUpdate())
                    .build();

            messageSender.sendMessage(address, mediaMessage);
        }
        catch (InvalidNumberException | UnregisteredUserException e)
        {
            LogFB.w(TAG, e);
            throw new InsecureFallbackApprovalException(e);
        }
        catch (FileNotFoundException e)
        {
            LogFB.w(TAG, e);
            throw new UndeliverableMessageException(e);
        }
        catch (IOException e)
        {
            LogFB.w(TAG, e);
            throw new RetryLaterException(e);
        }
    }
}
