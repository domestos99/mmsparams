package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;
import android.support.annotation.NonNull;

import org.whispersystems.jobqueue.JobParameters;

import java.util.List;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.AttachmentDatabase;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;

public abstract class SendJob extends MasterSecretJob
{

    private final static String TAG = SendJob.class.getSimpleName();

    public SendJob(Context context, JobParameters parameters)
    {
        super(context, parameters);
    }

    @Override
    public final void onRun(MasterSecret masterSecret) throws Exception
    {
        onSend(masterSecret);
    }

    protected abstract void onSend(MasterSecret masterSecret) throws Exception;

    protected void markAttachmentsUploaded(long messageId, @NonNull List<Attachment> attachments)
    {
        AttachmentDatabase database = DatabaseFactory.getAttachmentDatabase(context);

        for (Attachment attachment : attachments)
        {
            database.markAttachmentUploaded(messageId, attachment);
        }
    }
}
