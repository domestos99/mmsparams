package cz.uhk.bak.mmsparams.jobs;


import android.content.Context;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.jobqueue.requirements.NetworkRequirement;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup.Type;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.inject.Inject;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.GroupDatabase;
import cz.uhk.bak.mmsparams.database.GroupDatabase.GroupRecord;
import cz.uhk.bak.mmsparams.dependencies.InjectableType;
import cz.uhk.bak.mmsparams.dependencies.SignalCommunicationModule.SignalMessageSenderFactory;
import cz.uhk.bak.mmsparams.log.LogFB;

public class PushGroupUpdateJob extends ContextJob implements InjectableType
{

    private static final String TAG = PushGroupUpdateJob.class.getSimpleName();

    private static final long serialVersionUID = 0L;

    @Inject
    transient SignalMessageSenderFactory messageSenderFactory;

    private final String source;
    private final byte[] groupId;


    public PushGroupUpdateJob(Context context, String source, byte[] groupId)
    {
        super(context, JobParameters.newBuilder()
                .withPersistence()
                .withRequirement(new NetworkRequirement(context))
                .withRetryCount(50)
                .create());

        this.source = source;
        this.groupId = groupId;
    }

    @Override
    public void onAdded()
    {
    }

    @Override
    public void onRun() throws IOException, UntrustedIdentityException
    {
        SignalServiceMessageSender messageSender = messageSenderFactory.create();
        GroupDatabase groupDatabase = DatabaseFactory.getGroupDatabase(context);
        GroupRecord record = groupDatabase.getGroup(groupId);
        SignalServiceAttachment avatar = null;

        if (record == null)
        {
            LogFB.w(TAG, "No information for group record info request: " + new String(groupId));
            return;
        }

        if (record.getAvatar() != null)
        {
            avatar = SignalServiceAttachmentStream.newStreamBuilder()
                    .withContentType("image/jpeg")
                    .withStream(new ByteArrayInputStream(record.getAvatar()))
                    .withLength(record.getAvatar().length)
                    .build();
        }


        SignalServiceGroup groupContext = SignalServiceGroup.newBuilder(Type.UPDATE)
                .withAvatar(avatar)
                .withId(groupId)
                .withMembers(record.getMembers())
                .withName(record.getTitle())
                .build();

        SignalServiceDataMessage message = SignalServiceDataMessage.newBuilder()
                .asGroupMessage(groupContext)
                .withTimestamp(System.currentTimeMillis())
                .build();

        messageSender.sendMessage(new SignalServiceAddress(source), message);
    }

    @Override
    public boolean onShouldRetry(Exception e)
    {
        LogFB.w(TAG, e);
        return e instanceof PushNetworkException;
    }

    @Override
    public void onCanceled()
    {

    }
}
