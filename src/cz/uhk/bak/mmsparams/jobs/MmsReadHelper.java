package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.ReadRecInd;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.database.model.MessageRecord;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.CompatMmsConnection;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class MmsReadHelper
{
    private static final String TAG = MmsReadHelper.class.getSimpleName();


    public static void sendReadReport(final Context context, final MyRetrieveConf retrieveConf, final long messageId, int subscriptionId) throws Exception
    {
        sendReadReport(context, retrieveConf, PduHeaders.READ_STATUS_READ, messageId, subscriptionId);
    }

    public static boolean allowSendReadReportSimple(final Context context)
    {
        return TextSecurePreferences.isMmsAllowSendReadReport(context);
    }

    public static boolean allowSendReadReport(final Context context, final MyRetrieveConf retrieved)
    {
        try
        {
            if (TextSecurePreferences.isMmsAllowSendReadReport(context))
            {
                if (retrieved.getReadReport() == PduHeaders.VALUE_YES || TextSecurePreferences.isMmsSendReadReportEvenIfNotRequired(context))
                {
                    return true;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            LogFB.e(TAG, "allowSendReadReport", ex);
            return false;
        }
        return false;
    }


    public static void sendReadReport(final Context context, final MyRetrieveConf retrieveConf, final int status, final long messageId, int subscriptionId) throws Exception
    {
        byte[] msgID = retrieveConf.getMessageId();
        EncodedStringValue[] to = new EncodedStringValue[]{retrieveConf.getFrom()};

        final ReadRecInd readRec = new ReadRecInd(
                new EncodedStringValue(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR.getBytes()),
                msgID,
                PduHeaders.CURRENT_MMS_VERSION,
                status,
                to);

        readRec.setDate(System.currentTimeMillis() / 1000);


        byte[] nBytes = PduByteHelper.getPduBytes(context, readRec);

        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
        mmsErrorLogger.setMessageID(messageId);

        final byte[] conf = new CompatMmsConnection(context, mmsErrorLogger).send(nBytes, subscriptionId);

        PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_READ_REC_IND, nBytes);

    }

    public static void handleMessageDelete(final Context context, final MasterSecret masterSecret, long messageId)
    {
        List<Long> l = new ArrayList<>();
        l.add(messageId);

        sendReadReportsStat(context, masterSecret, l, PduHeaders.READ_STATUS__DELETED_WITHOUT_BEING_READ);
    }

    public static void sendReadReports(Context context, MasterSecret masterSecret, List<Long> msgIds)
    {
        sendReadReportsStat(context, masterSecret, msgIds, PduHeaders.READ_STATUS_READ);
    }


    private static void sendReadReportsStat(final Context context, final MasterSecret masterSecret, final List<Long> msgIds, final int status)
    {
        if (msgIds.size() == 0)
            return;

        // Is ReadReport Allowed?
        if (!allowSendReadReportSimple(context))
            return;

        final MmsDatabase mmsDb = DatabaseFactory.getMmsDatabase(context);
        final MessagePduDatabase pduDatabase = DatabaseFactory.getmessagePduDatabase(context);

        for (long id : msgIds)
        {
            final MmsDatabase.Reader reader = mmsDb.readerFor(masterSecret, mmsDb.getMessage(id));
            final MessageRecord record = reader.getNext();

            if (record == null)
                continue;

            if (record.isOutgoing())
                continue;

            if (pduDatabase.isPduExists(id, PduHeaders.MESSAGE_TYPE_READ_REC_IND))
            {
                continue;
            }

            MessagePdu retrieveConfPdu = pduDatabase.getByMessageIDType(id, PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF);

            if (retrieveConfPdu == null)
                continue;

            MyRetrieveConf rc = PduByteHelper.getPdu(retrieveConfPdu.getData());

            if (rc == null)
                continue;

            try
            {
                if (allowSendReadReport(context, rc))
                {
                    sendReadReport(context, rc, status, id, record.getSubscriptionId());
                }
            }
            catch (Exception ex)
            {
                LogFB.e(TAG, "handleMessageDelete", ex);
            }
        }
    }


}
