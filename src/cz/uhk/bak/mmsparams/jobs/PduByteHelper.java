package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.android.mms.dom.smil.parser.SmilXmlSerializer;
import com.google.android.mms.ContentType;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.CharacterSets;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MyPduComposer;
import com.google.android.mms.pdu_alt.MyPduParser;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;
import com.google.android.mms.smil.SmilHelper;
import com.klinker.android.send_message.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.AttachmentHelper;
import cz.uhk.bak.mmsparams.mms.MediaConstraints;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.mms.PartAuthority;
import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;
import cz.uhk.bak.mmsparams.util.StringUtil;
import cz.uhk.bak.mmsparams.util.Util;

public class PduByteHelper
{
    public static final String TAG = PduByteHelper.class.getSimpleName();

    public static final byte[] getPduBytes(@NonNull final Context context, @NonNull final GenericPdu pdu)
    {
        return new MyPduComposer(context, pdu).make();
    }

    @Nullable
    public static final <T extends GenericPdu> T getPdu(@NonNull final byte[] bytes)
    {
        if (bytes == null)
            return null;

        final GenericPdu pdu = new MyPduParser(bytes).parse();
        return (T) pdu;
    }

    public static void storeMessagePdu(@NonNull final Context context, @NonNull  final long messageID, @NonNull final int type, @NonNull final byte[] nBytes)
    {
        MessagePduDatabase pduDatabase = DatabaseFactory.getmessagePduDatabase(context);
        pduDatabase.insert(new MessagePdu(1, messageID, type, nBytes));
    }

    public static MySendReq constructSendPdu(final Context context, final MasterSecret masterSecret, final OutgoingMediaMessage message, final MmsProfile settings)
            throws UndeliverableMessageException
    {
        return constructSendPdu(context, masterSecret, message, settings, true);
    }

    public static MySendReq constructSendPdu(final Context context, final MasterSecret masterSecret, final OutgoingMediaMessage message, final MmsProfile settings, final boolean addRecipients)
            throws UndeliverableMessageException
    {
        MySendReq req = new MySendReq();
        String lineNumber = Utils.getMyPhoneNumber(context);
        List<String> numbers = message.getRecipients().toNumberStringList(true);
        MediaConstraints mediaConstraints = MediaConstraints.getMmsMediaConstraints(message.getSubscriptionId());
        List<Attachment> scaledAttachments = AttachmentHelper.scaleAttachments(context, masterSecret, mediaConstraints, message.getAttachments());

        if (!TextUtils.isEmpty(lineNumber))
        {
            if (settings.getSenderVisibility())
                req.setFrom(new EncodedStringValue(lineNumber));
        }
        else
        {
//            req.setFrom(new EncodedStringValue(""));
            req.setFrom(new EncodedStringValue(""));
        }

        if (addRecipients)
        {
            for (String recipient : numbers)
            {
                req.addTo(new EncodedStringValue(StringUtil.removeSpaces(recipient)));
            }
        }

        req.setDate(System.currentTimeMillis() / 1000);

        PduBody body = new PduBody();
        int size = 0;

        if (!TextUtils.isEmpty(message.getBody()))
        {
            PduPart part = new PduPart();
            String name = String.valueOf(System.currentTimeMillis());
            part.setData(Util.toUtf8Bytes(message.getBody()));
            part.setCharset(CharacterSets.UTF_8);
            part.setContentType(ContentType.TEXT_PLAIN.getBytes());
            part.setContentId(name.getBytes());
            part.setContentLocation((name + ".txt").getBytes());
            part.setName((name + ".txt").getBytes());

            body.addPart(part);
            size += getPartSize(part);
        }

        for (Attachment attachment : scaledAttachments)
        {
            try
            {
                if (attachment.getDataUri() == null)
                    throw new IOException("Assertion failed, attachment for outgoing MMS has no data!");

                String fileName = attachment.getFileName();
                PduPart part = new PduPart();

                if (fileName == null)
                {
                    fileName = String.valueOf(Math.abs(Util.getSecureRandom().nextLong()));
                    String fileExtension = MimeTypeMap.getSingleton().getExtensionFromMimeType(attachment.getContentType());

                    if (fileExtension != null) fileName = fileName + "." + fileExtension;
                }

                if (attachment.getContentType().startsWith("text"))
                {
                    part.setCharset(CharacterSets.UTF_8);
                }

                part.setContentType(attachment.getContentType().getBytes());
                part.setContentLocation(fileName.getBytes());
                part.setName(fileName.getBytes());

                int index = fileName.lastIndexOf(".");
                String contentId = (index == -1) ? fileName : fileName.substring(0, index);
                part.setContentId(contentId.getBytes());
                part.setData(Util.readFully(PartAuthority.getAttachmentStream(context, masterSecret, attachment.getDataUri())));

                body.addPart(part);
                size += getPartSize(part);
            }
            catch (IOException e)
            {
                LogFB.w(TAG, e);
            }
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(body), out);
        PduPart smilPart = new PduPart();
        smilPart.setContentId("smil".getBytes());
        smilPart.setContentLocation("smil.xml".getBytes());
        smilPart.setContentType(ContentType.APP_SMIL.getBytes());
        smilPart.setData(out.toByteArray());
        body.addPart(0, smilPart);

        req.setBody(body);
        req.setMessageSize(size);

        req.setMessageClass(settings.getMessage_classVal());
        req.setExpiry(settings.getExpiryTime());
//        req.setMessageClass(PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes());
//        req.setExpiry(7 * 24 * 60 * 60);

        try
        {
            req.setDrmContent(settings.getDrmContentVal());
            req.setAdaptationAllowed(settings.getAdaptationAllowedVal());

            req.setPriority(settings.getPriority());
            // req.setPriority(PduHeaders.PRIORITY_NORMAL);
            req.setDeliveryReport(settings.isDeliveryReportsVal());
            // req.setDeliveryReport(PduHeaders.VALUE_NO);
            req.setReadReport(settings.isReadReportVal());
            // req.setReadReport(PduHeaders.VALUE_NO);

            req.setDeliveryTime(settings.getDeliveryTime());

            req.setSenderIsVisible(settings.getSenderVisibility());
        }
        catch (InvalidHeaderValueException e)
        {
            LogFB.e(TAG, "Set Pr, DR, RR, RR, DT, SiV", e);
        }

        return req;
    }


    private static String getString(final byte[] bytes)
    {
        if (bytes == null || bytes.length == 0)
            return null;
        return new String(bytes);
    }

    public static long getPduBodySize(final PduBody body, final EncodedStringValue subject)
    {
        if (body == null)
        {
            return -1;
        }

        int size = body.getPartsNum();

        long result = 0;
        for (int i = 0; i < size; i++)
        {
            final PduPart part = body.getPart(i);


            byte[] ct = part.getContentType();
            if (ct == null)
                continue;

            if (ContentType.APP_SMIL.equals(getString(ct)))
            {
                continue;
            }

            result += getPartSize(part);
        }

        if (subject != null && !StringUtil.isEmptyOrNull(subject.getString()))
        {
            result += subject.getString().length();
        }

        return result;
    }

    public static long getPartSize(PduPart part)
    {
        return part.getName().length                //    123456789.jpg
                + part.getContentLocation().length   //   123456789.jpg
                + part.getContentType().length   //    image/jpeg
                + part.getData().length          //    pole bytů obsahu
                + part.getContentId().length;   //      <123456789.jpg>
    }

    public static String getSizeString(long size)
    {
        if (size <= 0)
            return String.valueOf(size);


        return Util.getPrettyFileSize(size) + " (" + String.valueOf(size) + ")";
    }


//    public static long getPartSizeKlinker(PduPart part)
//    {
//
//
//        return ((2 * part.getName().length) + part.getContentType().length + part.getData().length + part.getContentId().length);
//
//
//    }


//    public static long getPduBodySizeKlinker(PduBody body)
//    {
//        // assign parts to the pdu body which contains sending data
//        long size = 0;
//        if (parts != null) {
//            for (int i = 0; i < parts.length; i++) {
//                MMSPart part = parts[i];
//                if (part != null) {
//                    try {
//                        PduPart partPdu = new PduPart();
//                        partPdu.setName(part.Name.getBytes());
//                        partPdu.setContentType(part.MimeType.getBytes());
//
//                        if (part.MimeType.startsWith("text")) {
//                            partPdu.setCharset(CharacterSets.UTF_8);
//                        }
//                        // Set Content-Location.
//                        partPdu.setContentLocation(part.Name.getBytes());
//                        int index = part.Name.lastIndexOf(".");
//                        String contentId = (index == -1) ? part.Name
//                                : part.Name.substring(0, index);
//                        partPdu.setContentId(contentId.getBytes());
//                        partPdu.setData(part.Data);
//
//                        pduBody.addPart(partPdu);
//                        size += ((2 * part.Name.getBytes().length) + part.MimeType.getBytes().length + part.Data.length + contentId.getBytes().length);
//                    } catch (Exception e) {
//                    }
//                }
//            }
//        }
//
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(pduBody), out);
//        PduPart smilPart = new PduPart();
//        smilPart.setContentId("smil".getBytes());
//        smilPart.setContentLocation("smil.xml".getBytes());
//        smilPart.setContentType(ContentType.APP_SMIL.getBytes());
//        smilPart.setData(out.toByteArray());
//        pduBody.addPart(0, smilPart);
//
//        sendRequest.setBody(pduBody);
//
//        sendRequest.setMessageSize(size);
//
//    }

//    private static byte[] removeBodyFromData(final Context context, byte[] data)
//    {
//        if (data == null)
//            return data;
//
//        GenericPdu pdu = PduByteHelper.getPdu(data);
//
//        if (pdu == null)
//            return data;
//
//
//        int type = pdu.getMessageType();
//        switch (type)
//        {
//            case PduHeaders.MESSAGE_TYPE_SEND_REQ:
//            case PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF:
//                MultimediaMessagePdu sq = (MultimediaMessagePdu) pdu;
//                sq = removeParts(sq);
//                return PduByteHelper.getPduBytes(context, sq);
//
//            default:
//                return data;
//        }
//    }
//
//    private static MultimediaMessagePdu removeParts(final MultimediaMessagePdu sq)
//    {
//        final PduBody body = sq.getBody();
//        if (body == null)
//            return sq;
//
//
//        int count = body.getPartsNum();
//
//        for (int i = 0; i < count; i++)
//        {
//            PduPart pduPart = body.getPart(i);
//
//            byte[] ct = pduPart.getContentType();
//            if (ct == null)
//                continue;
//
//            String cts = getString(ct);
//
//            if (ContentType.TEXT_PLAIN.equals(cts))
//            {
//                continue;
//            }
//            else if (ContentType.APP_SMIL.equals(cts))
//            {
//                continue;
//            }
//            // Add More here
//            else
//            {
//                if (cts.startsWith("image") || cts.startsWith("audio") || cts.startsWith("video"))
//                {
//                    final PduPart p = pduPart;
//                    p.setData(new byte[0]);
//                    body.removePart(i);
//                    body.addPart(p);
//                }
//            }
//        }
//        sq.setBody(body);
//        return sq;
//    }
//
//    private static String getString(byte[] value)
//    {
//        if (value == null || value.length == 0)
//            return "...";
//
//        return new String(value);
//    }


}
