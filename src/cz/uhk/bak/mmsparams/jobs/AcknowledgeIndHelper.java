package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import com.google.android.mms.pdu_alt.AcknowledgeInd;
import com.google.android.mms.pdu_alt.PduHeaders;

import cz.uhk.bak.mmsparams.mms.CompatMmsConnection;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class AcknowledgeIndHelper
{

    public static void sendAcknowledge(Context context, byte[] transactionId, long messageID, int subscriptionId) throws Exception
    {
        AcknowledgeInd ack = new AcknowledgeInd(PduHeaders.CURRENT_MMS_VERSION, transactionId);

        boolean allowDeliveryReport = TextSecurePreferences.isMmsAllowSendDeliveryReport(context);
        ack.setReportAllowed(allowDeliveryReport ? PduHeaders.VALUE_YES : PduHeaders.VALUE_NO);

        byte[] nBytes = PduByteHelper.getPduBytes(context, ack);

        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
        mmsErrorLogger.setMessageID(messageID);

        final byte[] conf = new CompatMmsConnection(context, mmsErrorLogger).send(nBytes, subscriptionId);

        PduByteHelper.storeMessagePdu(context, messageID, PduHeaders.MESSAGE_TYPE_ACKNOWLEDGE_IND, nBytes);
    }


}
