package cz.uhk.bak.mmsparams.jobs;


import android.content.Context;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.jobqueue.requirements.NetworkRequirement;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

import javax.inject.Inject;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.crypto.IdentityKeyUtil;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.crypto.PreKeyUtil;
import cz.uhk.bak.mmsparams.dependencies.InjectableType;
import cz.uhk.bak.mmsparams.jobs.requirements.MasterSecretRequirement;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class RotateSignedPreKeyJob extends MasterSecretJob implements InjectableType
{

    private static final String TAG = RotateSignedPreKeyJob.class.getName();

    @Inject
    transient SignalServiceAccountManager accountManager;

    public RotateSignedPreKeyJob(Context context)
    {
        super(context, JobParameters.newBuilder()
                .withRequirement(new NetworkRequirement(context))
                .withRequirement(new MasterSecretRequirement(context))
                .withRetryCount(5)
                .create());
    }

    @Override
    public void onAdded()
    {

    }

    @Override
    public void onRun(MasterSecret masterSecret) throws Exception
    {
        LogFB.w(TAG, "Rotating signed prekey...");

        IdentityKeyPair identityKey = IdentityKeyUtil.getIdentityKeyPair(context);
        SignedPreKeyRecord signedPreKeyRecord = PreKeyUtil.generateSignedPreKey(context, identityKey, false);

        accountManager.setSignedPreKey(signedPreKeyRecord);

        PreKeyUtil.setActiveSignedPreKeyId(context, signedPreKeyRecord.getId());
        TextSecurePreferences.setSignedPreKeyRegistered(context, true);
        TextSecurePreferences.setSignedPreKeyFailureCount(context, 0);

        ApplicationContext.getInstance(context)
                .getJobManager()
                .add(new CleanPreKeysJob(context));
    }

    @Override
    public boolean onShouldRetryThrowable(Exception exception)
    {
        return exception instanceof PushNetworkException;
    }

    @Override
    public void onCanceled()
    {
        TextSecurePreferences.setSignedPreKeyFailureCount(context, TextSecurePreferences.getSignedPreKeyFailureCount(context) + 1);
    }
}
