package cz.uhk.bak.mmsparams.jobs;


import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.signalservice.api.SignalServiceMessagePipe;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.SignalServiceProfile;
import org.whispersystems.signalservice.api.util.InvalidNumberException;

import java.io.IOException;

import javax.inject.Inject;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.dependencies.InjectableType;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.recipients.Recipient;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.service.MessageRetrievalService;
import cz.uhk.bak.mmsparams.util.Base64;
import cz.uhk.bak.mmsparams.util.GroupUtil;
import cz.uhk.bak.mmsparams.util.IdentityUtil;
import cz.uhk.bak.mmsparams.util.Util;

public class RetrieveProfileJob extends ContextJob implements InjectableType
{

    private static final String TAG = RetrieveProfileJob.class.getSimpleName();

    @Inject
    transient SignalServiceMessageReceiver receiver;

    private final long[] recipientIds;

    public RetrieveProfileJob(Context context, Recipients recipients)
    {
        super(context, JobParameters.newBuilder()
                .withRetryCount(3)
                .create());

        this.recipientIds = recipients.getIds();
    }

    @Override
    public void onAdded()
    {
    }

    @Override
    public void onRun() throws IOException, InvalidKeyException
    {
        try
        {
            Recipients recipients = RecipientFactory.getRecipientsForIds(context, recipientIds, true);

            for (Recipient recipient : recipients)
            {
                if (recipient.isGroupRecipient()) handleGroupRecipient(recipient);
                else handleIndividualRecipient(recipient);
            }
        }
        catch (InvalidNumberException e)
        {
            LogFB.w(TAG, e);
        }
    }

    @Override
    public boolean onShouldRetry(Exception e)
    {
        return false;
    }

    @Override
    public void onCanceled()
    {
    }

    private void handleIndividualRecipient(Recipient recipient)
            throws IOException, InvalidKeyException, InvalidNumberException
    {
        String number = Util.canonicalizeNumber(context, recipient.getNumber());
        SignalServiceProfile profile = retrieveProfile(number);

        if (TextUtils.isEmpty(profile.getIdentityKey()))
        {
            LogFB.w(TAG, "Identity key is missing on profile!");
            return;
        }

        IdentityKey identityKey = new IdentityKey(Base64.decode(profile.getIdentityKey()), 0);

        if (!DatabaseFactory.getIdentityDatabase(context)
                .getIdentity(recipient.getRecipientId())
                .isPresent())
        {
            LogFB.w(TAG, "Still first use...");
            return;
        }

        IdentityUtil.saveIdentity(context, number, identityKey);
    }

    private void handleGroupRecipient(Recipient group)
            throws IOException, InvalidKeyException, InvalidNumberException
    {
        byte[] groupId = GroupUtil.getDecodedId(group.getNumber());
        Recipients recipients = DatabaseFactory.getGroupDatabase(context).getGroupMembers(groupId, false);

        for (Recipient recipient : recipients)
        {
            handleIndividualRecipient(recipient);
        }
    }

    private SignalServiceProfile retrieveProfile(@NonNull String number) throws IOException
    {
        SignalServiceMessagePipe pipe = MessageRetrievalService.getPipe();

        if (pipe != null)
        {
            try
            {
                return pipe.getProfile(new SignalServiceAddress(number));
            }
            catch (IOException e)
            {
                LogFB.w(TAG, e);
            }
        }

        return receiver.retrieveProfile(new SignalServiceAddress(number));
    }
}
