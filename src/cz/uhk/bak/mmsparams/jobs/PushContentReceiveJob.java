package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.libsignal.InvalidVersionException;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;

import java.io.IOException;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class PushContentReceiveJob extends PushReceivedJob
{

    private static final String TAG = PushContentReceiveJob.class.getSimpleName();

    private final String data;

    public PushContentReceiveJob(Context context)
    {
        super(context, JobParameters.newBuilder().create());
        this.data = null;
    }

    public PushContentReceiveJob(Context context, String data)
    {
        super(context, JobParameters.newBuilder()
                .withPersistence()
                .withWakeLock(true)
                .create());

        this.data = data;
    }

    @Override
    public void onAdded()
    {
    }

    @Override
    public void onRun()
    {
        try
        {
            String sessionKey = TextSecurePreferences.getSignalingKey(context);
            SignalServiceEnvelope envelope = new SignalServiceEnvelope(data, sessionKey);

            handle(envelope, true);
        }
        catch (IOException | InvalidVersionException e)
        {
            LogFB.w(TAG, e);
        }
    }

    @Override
    public void onCanceled()
    {

    }

    @Override
    public boolean onShouldRetry(Exception exception)
    {
        return false;
    }
}
