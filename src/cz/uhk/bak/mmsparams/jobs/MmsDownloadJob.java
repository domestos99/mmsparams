package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;
import android.net.Uri;

import com.google.android.mms.pdu_alt.CharacterSets;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.PduPart;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.jobqueue.requirements.NetworkRequirement;
import org.whispersystems.libsignal.DuplicateMessageException;
import org.whispersystems.libsignal.InvalidMessageException;
import org.whispersystems.libsignal.LegacyMessageException;
import org.whispersystems.libsignal.NoSessionException;
import org.whispersystems.libsignal.util.guava.Optional;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.attachments.UriAttachment;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.crypto.MasterSecretUnion;
import cz.uhk.bak.mmsparams.database.AttachmentDatabase;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
import cz.uhk.bak.mmsparams.database.MessagingDatabase.InsertResult;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.MmsErrorDatabase;
import cz.uhk.bak.mmsparams.jobs.requirements.MasterSecretRequirement;
import cz.uhk.bak.mmsparams.log.DBLog;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.ApnUnavailableException;
import cz.uhk.bak.mmsparams.mms.CompatMmsConnection;
import cz.uhk.bak.mmsparams.mms.IncomingMediaMessage;
import cz.uhk.bak.mmsparams.mms.MmsException;
import cz.uhk.bak.mmsparams.mms.MmsRadioException;
import cz.uhk.bak.mmsparams.mms.PartParser;
import cz.uhk.bak.mmsparams.notifications.MessageNotifier;
import cz.uhk.bak.mmsparams.providers.SingleUseBlobProvider;
import cz.uhk.bak.mmsparams.service.KeyCachingService;
import cz.uhk.bak.mmsparams.util.Util;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class MmsDownloadJob extends MasterSecretJob
{

    private static final String TAG = MmsDownloadJob.class.getSimpleName();

    private final long messageId;
    private final long threadId;
    private final boolean automatic;
    private final boolean downloadNow;
    private final boolean isManualCall;

    public MmsDownloadJob(Context context, long messageId, long threadId, boolean automatic, boolean downloadNow, boolean isManualCall)
    {
        super(context, JobParameters.newBuilder()
                .withPersistence()
                .withRequirement(new MasterSecretRequirement(context))
                .withRequirement(new NetworkRequirement(context))
                .withGroupId("mms-operation")
                .withWakeLock(true, 30, TimeUnit.SECONDS)
                .withRetryCount(1)
                .create());

        this.messageId = messageId;
        this.threadId = threadId;
        this.automatic = automatic;
        this.downloadNow = downloadNow;
        this.isManualCall = isManualCall;
    }

    @Override
    public void onAdded()
    {
        if (automatic && KeyCachingService.getMasterSecret(context) == null)
        {
            DatabaseFactory.getMmsDatabase(context).markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context, null);
        }
    }

    @Override
    public void onRun(MasterSecret masterSecret)
    {
        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);

        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

        if (!downloadNow)
        {
            MessageNotifier.updateNotification(context, masterSecret, threadId);
            return;
        }


        mmsErrorLogger.setMessageID(messageId);
        Optional<MmsDatabase.MmsNotificationInfo> notification = database.getNotification(messageId);

        if (!notification.isPresent())
        {
            LogFB.w(TAG, "No notification for ID: " + messageId);
            return;
        }

        try
        {
            if (notification.get().getContentLocation() == null)
            {
                throw new MmsException("Notification content location was null.");
            }

            database.markDownloadState(messageId, MmsDatabase.Status.DOWNLOAD_CONNECTING);

            String contentLocation = notification.get().getContentLocation();
            byte[] transactionId = new byte[0];

            try
            {
                transactionId = notification.get().getTransactionId().getBytes(CharacterSets.MIMENAME_ISO_8859_1);
            }
            catch (UnsupportedEncodingException e)
            {
                LogFB.w(TAG, e);
            }

            LogFB.w(TAG, "Downloading mms at " + Uri.parse(contentLocation).getHost() + ", subscription ID: " + notification.get().getSubscriptionId());

            mmsErrorLogger.setMessageID(messageId);
            byte[] retrieveBytes = new CompatMmsConnection(context, mmsErrorLogger).retrieve(contentLocation, transactionId, messageId, notification.get().getSubscriptionId());

            if (retrieveBytes == null)
            {
                throw new MmsException("RetrieveConf was null");
            }


            mmsErrorLogger.setMessageID(messageId);
            long newMsgID = storeRetrievedMms(masterSecret, contentLocation, messageId, threadId, retrieveBytes, notification.get().getSubscriptionId());
            mmsErrorLogger.setMessageID(newMsgID);

            if (newMsgID == -2)
            {
                return;
            }

            // Send NotifyRespInd of Acknowledgement
            if (isManualCall)
            {
                // User ordered to download
                // Acknowlege
                sendAcknowledge(transactionId, newMsgID, notification.get().getSubscriptionId());
            }
            else
            {
                // System downloading after receive
                // NotifyRespInd
                sendNotifyRespIndRetrieved(transactionId, newMsgID, notification.get().getSubscriptionId());
            }


//            long msgID = getIntent().getLongExtra(MESSAGE_ID_EXTRA, -1);
//            MmsReceiveHandler receiveHandler = new MmsReceiveHandler(context);
//            receiveHandler.send(retrieveBytes);

        }
        catch (ApnUnavailableException e)
        {
            LogFB.w(TAG, e);
            handleDownloadError(masterSecret, messageId, threadId, MmsDatabase.Status.DOWNLOAD_APN_UNAVAILABLE,
                    automatic);

            mmsErrorLogger.log(messageId, TAG, "DOWNLOAD_APN_UNAVAILABLE", e);
        }
        catch (MmsException e)
        {
            LogFB.w(TAG, e);
            handleDownloadError(masterSecret, messageId, threadId,
                    MmsDatabase.Status.DOWNLOAD_HARD_FAILURE,
                    automatic);

            mmsErrorLogger.log(messageId, TAG, "DOWNLOAD_HARD_FAILURE", e);
        }
        catch (MmsRadioException | IOException e)
        {
            LogFB.w(TAG, e);
            handleDownloadError(masterSecret, messageId, threadId,
                    MmsDatabase.Status.DOWNLOAD_SOFT_FAILURE,
                    automatic);

            mmsErrorLogger.log(messageId, TAG, "DOWNLOAD_SOFT_FAILURE", e);
        }
        catch (DuplicateMessageException e)
        {
            LogFB.w(TAG, e);
            database.markAsDecryptDuplicate(messageId, threadId);


            mmsErrorLogger.log(messageId, TAG, e);
        }
        catch (LegacyMessageException e)
        {
            LogFB.w(TAG, e);
            database.markAsLegacyVersion(messageId, threadId);
            mmsErrorLogger.log(messageId, TAG, e);
        }
        catch (NoSessionException e)
        {
            LogFB.w(TAG, e);
            database.markAsNoSession(messageId, threadId);
            mmsErrorLogger.log(messageId, TAG, e);
        }
        catch (InvalidMessageException e)
        {
            LogFB.w(TAG, e);
            database.markAsDecryptFailed(messageId, threadId);
            mmsErrorLogger.log(messageId, TAG, e);
        }
    }


    private void sendAcknowledge(byte[] transactionId,
                                 long messageID, int subscriptionId)
            throws ApnUnavailableException
    {
        try
        {
            AcknowledgeIndHelper.sendAcknowledge(context, transactionId, messageID, subscriptionId);
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "Send Acknowledge", TAG, ex);

            MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
            mmsErrorLogger.setMessageID(messageID);
            mmsErrorLogger.log(messageId, TAG, "sendAcknowledge", ex);
        }
    }

    private void sendNotifyRespIndRetrieved(byte[] transactionId, long messageID, int subscriptionId)
    {
        try
        {
            NotifyRespIndHelper.sendNotifyRespIndRetrieved(context, transactionId, messageID, subscriptionId, NotifyRespIndHelper.RetrievedStatus.RETRIEVED);
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "Send NotifyRespInd Retrieved", TAG, ex);

            MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
            mmsErrorLogger.setMessageID(messageID);
            mmsErrorLogger.log(messageId, TAG, "sendNotifyRespIndRetrieved", ex);
        }
    }


    @Override
    public void onCanceled()
    {
        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
        database.markDownloadState(messageId, MmsDatabase.Status.DOWNLOAD_SOFT_FAILURE);

        if (automatic)
        {
            database.markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context, null, threadId);
        }
    }

    @Override
    public boolean onShouldRetryThrowable(Exception exception)
    {
        return false;
    }

    private long storeRetrievedMms(final MasterSecret masterSecret, final String contentLocation,
                                   final long messageId, final long threadId, final byte[] retrievedByte,
                                   final int subscriptionId)
            throws MmsException, NoSessionException, DuplicateMessageException, InvalidMessageException,
            LegacyMessageException
    {
        MyRetrieveConf retrieved = (MyRetrieveConf) PduByteHelper.getPdu(retrievedByte); // new MyPduParser(retrievedByte).parse());


        if (retrieved == null)
        {
            DBLog.logToDB(context, "storeRetrievedMms: retrieved is null", TAG, null);
            return -2;
        }

        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
        SingleUseBlobProvider provider = SingleUseBlobProvider.getInstance();
        String from = null;
        List<String> to = new LinkedList<>();
        List<String> cc = new LinkedList<>();
        String body = null;
        List<Attachment> attachments = new LinkedList<>();

        if (retrieved.getFrom() != null)
        {
            from = Util.toIsoString(retrieved.getFrom().getTextString());
        }

        if (retrieved.getTo() != null)
        {
            for (EncodedStringValue toValue : retrieved.getTo())
            {
                to.add(Util.toIsoString(toValue.getTextString()));
            }
        }

        if (retrieved.getCc() != null)
        {
            for (EncodedStringValue ccValue : retrieved.getCc())
            {
                cc.add(Util.toIsoString(ccValue.getTextString()));
            }
        }

        if (retrieved.getBody() != null)
        {
            body = PartParser.getMessageText(retrieved.getBody());
            PduBody media = PartParser.getSupportedMediaParts(retrieved.getBody());

            for (int i = 0; i < media.getPartsNum(); i++)
            {
                PduPart part = media.getPart(i);

                if (part.getData() != null)
                {
                    Uri uri = provider.createUri(part.getData());
                    String name = null;

                    if (part.getName() != null) name = Util.toIsoString(part.getName());

                    attachments.add(new UriAttachment(uri, Util.toIsoString(part.getContentType()),
                            AttachmentDatabase.TRANSFER_PROGRESS_DONE,
                            part.getData().length, name, false));
                }
            }
        }

        IncomingMediaMessage message = new IncomingMediaMessage(from, to, cc, body, retrieved.getDate() * 1000L, attachments, subscriptionId, 0, false);
        Optional<InsertResult> insertResult = database.insertMessageInbox(new MasterSecretUnion(masterSecret),
                message, contentLocation, threadId);


        PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF, retrievedByte);


        if (insertResult.isPresent())
        {
            MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);
            messagePduDatabase.changeMessageID(messageId, insertResult.get().getMessageId());

            MmsErrorDatabase mmsErrorDatabase = DatabaseFactory.getMmsErrorDatabase(context);
            mmsErrorDatabase.changeMessageID(messageId, insertResult.get().getMessageId());

            database.delete(messageId);


            MessageNotifier.updateNotification(context, masterSecret, insertResult.get().getThreadId());

            return insertResult.get().getMessageId();
        }

        return -1;
    }


    private void handleDownloadError(MasterSecret masterSecret, long messageId, long threadId,
                                     int downloadStatus, boolean automatic)
    {
        MmsDatabase db = DatabaseFactory.getMmsDatabase(context);

        db.markDownloadState(messageId, downloadStatus);

        if (automatic)
        {
            db.markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context, masterSecret, threadId);
        }
    }
}
