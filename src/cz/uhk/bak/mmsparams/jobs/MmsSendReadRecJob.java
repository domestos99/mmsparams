//package cz.uhk.bak.mmsparams.jobs;
//
//import android.content.Context;
//import android.text.TextUtils;
//
//import com.google.android.mms.pdu_alt.EncodedStringValue;
//import com.google.android.mms.pdu_alt.MyPduParser;
//import com.google.android.mms.pdu_alt.PduHeaders;
//import com.google.android.mms.pdu_alt.ReadRecInd;
//import com.google.android.mms.pdu_alt.MyRetrieveConf;
//import com.klinker.android.send_message.Utils;
//
//import org.whispersystems.jobqueue.JobParameters;
//
//import cz.uhk.bak.mmsparams.crypto.MasterSecret;
//import cz.uhk.bak.mmsparams.database.DatabaseFactory;
//import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
//import cz.uhk.bak.mmsparams.database.MmsDatabase;
//import cz.uhk.bak.mmsparams.database.model.MessagePdu;
//import cz.uhk.bak.mmsparams.log.DBLog;
//import cz.uhk.bak.mmsparams.mms.CompatMmsConnection;
//
//public class MmsSendReadRecJob extends MasterSecretJob
//{
//    private static final String TAG = MmsSendReadRecJob.class.getSimpleName();
//
//    private final int messageIdDB;
//
//    public MmsSendReadRecJob(Context context, int messageIdDB)
//    {
//        super(context, JobParameters.newBuilder()
//                .withWakeLock(true)
//                .withPersistence().create());
//
//
//        this.messageIdDB = messageIdDB;
//    }
//
//    @Override
//    public void onRun(MasterSecret masterSecret) throws Exception
//    {
//        MmsDatabase mmsDatabase = DatabaseFactory.getMmsDatabase(context);
//        boolean read = mmsDatabase.getIsRead(messageIdDB);
////
////        if (read)
////            return;
//
//        MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);
//        final MessagePdu msgPdu = messagePduDatabase.getByMessageIDType(messageIdDB, PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF);
//
//        if (msgPdu == null)
//            return;
//
//        MyRetrieveConf retrieveConf = (MyRetrieveConf)PduByteHelper.getPdu(msgPdu.getData()); //  new MyPduParser(msgPdu.getData()).parse();
//
//
//        try
//        {
//            // TODO
//            MmsReadHelper.sendReadReport(context, retrieveConf, messageIdDB, -1);
//        }
//        catch (Exception ex)
//        {
//            DBLog.logToDB(context, "Send Acknowledge", TAG, ex);
//        }
//
//
//
//        ReadRecInd readRecInd = constructReadRecInd(retrieveConf);
//        readRecInd.setDate(System.currentTimeMillis() / 1000);
//
//        final byte[] pduBytes = PduByteHelper.getPduBytes(context, readRecInd);
//
//        try
//        {
////            OutgoingLegacyMmsConnection connection = new OutgoingLegacyMmsConnection(context);
//
//            byte[] result = new CompatMmsConnection(context).send(pduBytes, -1);
//
//
////            PduPersister.getPduPersister(context).persist(readRecInd, Telephony.Mms.Outbox.CONTENT_URI, true,
////                    true, null);
////            context.startService(new Intent(context, TransactionService.class));
//
//
//            PduByteHelper.storeMessagePdu(context, messageIdDB, PduHeaders.MESSAGE_TYPE_READ_REC_IND, pduBytes);
//
//        }
//        catch (Exception ex)
//        {
//            int k = 1 + 1;
//        }
//
//    }
//
//    private ReadRecInd constructReadRecInd(MyRetrieveConf retrieveConf) throws Exception
//    {
//        String lineNumber = Utils.getMyPhoneNumber(context);
//        EncodedStringValue from = new EncodedStringValue(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR.getBytes());
//
//        if (!TextUtils.isEmpty(lineNumber))
//        {
//            from = new EncodedStringValue(lineNumber);
//        }
//
//        ReadRecInd readRecInd = new ReadRecInd(from,
//                retrieveConf.getMessageId(),
//                PduHeaders.CURRENT_MMS_VERSION,
//                PduHeaders.READ_STATUS_READ,
//                retrieveConf.getTo()
//        );
//
//        return readRecInd;
//    }
//
//    @Override
//    public boolean onShouldRetryThrowable(Exception exception)
//    {
//        return false;
//    }
//
//    @Override
//    public void onAdded()
//    {
//
//    }
//
//    @Override
//    public void onCanceled()
//    {
//
//    }
//}
