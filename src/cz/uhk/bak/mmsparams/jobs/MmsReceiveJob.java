package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;

import com.google.android.mms.pdu_alt.DeliveryInd;
import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.ReadOrigInd;

import org.whispersystems.jobqueue.JobParameters;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.MmsErrorDatabase;
import cz.uhk.bak.mmsparams.log.DBLog;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.MessageThreadIDPair;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.Util;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class MmsReceiveJob extends ContextJob
{

    private static final long serialVersionUID = 1L;

    private static final String TAG = MmsReceiveJob.class.getSimpleName();

    private final byte[] data;
    private final int subscriptionId;

    public MmsReceiveJob(final Context context, final byte[] data, final int subscriptionId)
    {
        super(context, JobParameters.newBuilder()
                .withWakeLock(true)
                .withPersistence().create());

        this.data = data;
        this.subscriptionId = subscriptionId;
    }

    @Override
    public void onAdded()
    {

    }

    @Override
    public void onRun()
    {
        if (data == null)
        {
            LogFB.w(TAG, "Received NULL pdu, ignoring...");
            return;
        }


        GenericPdu pdu = null;

        try
        {
            pdu = PduByteHelper.getPdu(data);
        }
        catch (RuntimeException e)
        {
            LogFB.w(TAG, e);
        }


        if (isNotification(pdu) && !isBlocked(pdu))
        {
            MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

            final MyNotificationInd pduNotificationInd = (MyNotificationInd) pdu;

            MessageThreadIDPair messageAndThreadId = database.insertMessageInbox(pduNotificationInd, subscriptionId);

            PduByteHelper.storeMessagePdu(context, messageAndThreadId.getMessageId(), PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND, data);


            LogFB.w(TAG, "Inserted received MMS notification...");


            boolean downloadNow = TextSecurePreferences.isAutoDownloadMms(context);

            // Send NotifyRespInd

            // If not download now, send status Deferred
            if (!downloadNow)
            {
                sendNotifyRespIndDeferred(pduNotificationInd.getTransactionId(), messageAndThreadId.getMessageId());
            }

            ApplicationContext.getInstance(context)
                    .getJobManager()
                    .add(new MmsDownloadJob(context,
                            messageAndThreadId.getMessageId(),
                            messageAndThreadId.getThreadID(),
                            true, downloadNow, false));


        }
        else if (isDeliveryInd(pdu))
        {
            // Save Delivery Report
            DeliveryInd deliveryInd = (DeliveryInd) pdu;

// ...
            MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);
            long msgID = messagePduDatabase.getbyMsgPduID(deliveryInd.getMessageId());

            PduByteHelper.storeMessagePdu(context, msgID, PduHeaders.MESSAGE_TYPE_DELIVERY_IND, data);

        }
        else if (isReadOrigInd(pdu))
        {
            ReadOrigInd readOrigInd = (ReadOrigInd) pdu;


            MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);

            long msgID = messagePduDatabase.getbyMsgPduID(readOrigInd.getMessageId());

            PduByteHelper.storeMessagePdu(context, msgID, PduHeaders.MESSAGE_TYPE_READ_ORIG_IND, data);


        }
        else if (isNotification(pdu))
        {
            LogFB.w(TAG, "*** Received blocked MMS, ignoring...");
        }
    }

    private void sendNotifyRespIndDeferred(byte[] transactionId, long messageID)
    {
        try
        {
            NotifyRespIndHelper.sendNotifyRespIndRetrieved(context, transactionId, messageID, subscriptionId, NotifyRespIndHelper.RetrievedStatus.DEFERRED);
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "Send NotifyRespInd Deferred", TAG, ex);


            MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
            mmsErrorLogger.setMessageID(messageID);
            mmsErrorLogger.log(messageID, TAG, "sendNotifyRespIndDeferred", ex);
        }
    }

    @Override
    public void onCanceled()
    {
        // TODO
    }

    @Override
    public boolean onShouldRetry(Exception exception)
    {
        return false;
    }

    private boolean isBlocked(GenericPdu pdu)
    {
        if (pdu.getFrom() != null && pdu.getFrom().getTextString() != null)
        {
            Recipients recipients = RecipientFactory.getRecipientsFromString(context, Util.toIsoString(pdu.getFrom().getTextString()), false);
            return recipients.isBlocked();
        }

        return false;
    }

    private boolean isNotification(GenericPdu pdu)
    {
        return pdu != null && pdu.getMessageType() == PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND;
    }

    private boolean isDeliveryInd(GenericPdu pdu)
    {
        return pdu != null && pdu.getMessageType() == PduHeaders.MESSAGE_TYPE_DELIVERY_IND;
    }

    private boolean isReadOrigInd(GenericPdu pdu)
    {
        return pdu != null && pdu.getMessageType() == PduHeaders.MESSAGE_TYPE_READ_ORIG_IND;
    }
}
