package cz.uhk.bak.mmsparams.jobs;

import android.content.Context;
import android.text.TextUtils;

import org.whispersystems.jobqueue.JobParameters;
import org.whispersystems.libsignal.InvalidMessageException;

import java.io.IOException;

import cz.uhk.bak.mmsparams.crypto.AsymmetricMasterCipher;
import cz.uhk.bak.mmsparams.crypto.AsymmetricMasterSecret;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.crypto.MasterSecretUnion;
import cz.uhk.bak.mmsparams.crypto.MasterSecretUtil;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.EncryptingSmsDatabase;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.SmsDatabase;
import cz.uhk.bak.mmsparams.database.model.MessageRecord;
import cz.uhk.bak.mmsparams.database.model.SmsMessageRecord;
import cz.uhk.bak.mmsparams.jobs.requirements.MasterSecretRequirement;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.notifications.MessageNotifier;

public class MasterSecretDecryptJob extends MasterSecretJob
{

    private static final long serialVersionUID = 1L;
    private static final String TAG = MasterSecretDecryptJob.class.getSimpleName();

    public MasterSecretDecryptJob(Context context)
    {
        super(context, JobParameters.newBuilder()
                .withRequirement(new MasterSecretRequirement(context))
                .create());
    }

    @Override
    public void onRun(MasterSecret masterSecret)
    {
        EncryptingSmsDatabase smsDatabase = DatabaseFactory.getEncryptingSmsDatabase(context);
        SmsDatabase.Reader smsReader = smsDatabase.getDecryptInProgressMessages(masterSecret);

        SmsMessageRecord smsRecord;

        while ((smsRecord = smsReader.getNext()) != null)
        {
            try
            {
                String body = getAsymmetricDecryptedBody(masterSecret, smsRecord.getBody().getBody());
                smsDatabase.updateMessageBody(new MasterSecretUnion(masterSecret), smsRecord.getId(), body);
            }
            catch (InvalidMessageException e)
            {
                LogFB.w(TAG, e);
            }
        }

        MmsDatabase mmsDatabase = DatabaseFactory.getMmsDatabase(context);
        MmsDatabase.Reader mmsReader = mmsDatabase.getDecryptInProgressMessages(masterSecret);

        MessageRecord mmsRecord;

        while ((mmsRecord = mmsReader.getNext()) != null)
        {
            try
            {
                String body = getAsymmetricDecryptedBody(masterSecret, mmsRecord.getBody().getBody());
                mmsDatabase.updateMessageBody(new MasterSecretUnion(masterSecret), mmsRecord.getId(), body);
            }
            catch (InvalidMessageException e)
            {
                LogFB.w(TAG, e);
            }
        }

        smsReader.close();
        mmsReader.close();

        MessageNotifier.updateNotification(context, masterSecret);
    }

    @Override
    public boolean onShouldRetryThrowable(Exception exception)
    {
        return false;
    }

    @Override
    public void onAdded()
    {

    }

    @Override
    public void onCanceled()
    {

    }

    private String getAsymmetricDecryptedBody(MasterSecret masterSecret, String body)
            throws InvalidMessageException
    {
        try
        {
            AsymmetricMasterSecret asymmetricMasterSecret = MasterSecretUtil.getAsymmetricMasterSecret(context, masterSecret);
            AsymmetricMasterCipher asymmetricMasterCipher = new AsymmetricMasterCipher(asymmetricMasterSecret);

            if (TextUtils.isEmpty(body)) return "";
            else return asymmetricMasterCipher.decryptBody(body);

        }
        catch (IOException e)
        {
            throw new InvalidMessageException(e);
        }
    }


}
