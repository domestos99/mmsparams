package cz.uhk.bak.mmsparams.permissions;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import cz.uhk.bak.mmsparams.util.AndroidVersionUtil;

public class PermissionUtil
{

    public static final int RequirePermissionCode = 111111;

    public static boolean requirePermission(Activity activity, String permission)
    {
        if (AndroidVersionUtil.isHigherThanMarshmallow())
        {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
            {
                // Should we show an explanation?
                if (activity.shouldShowRequestPermissionRationale(
                        permission))
                {
                    // Explain to the user why we need to read the contacts
                }

                activity.requestPermissions(new String[]{permission}, RequirePermissionCode);
                return false;
            }
        }
        return true;
    }

    public static boolean requirePermissions(Activity activity, String... permissions)
    {
        if (AndroidVersionUtil.isHigherThanMarshmallow())
        {
            if (!hasPermissions(activity, permissions))
            {
                // Should we show an explanation?
//                if (activity.shouldShowRequestPermissionRationale(
//                        permissions))
//                {
//                    // Explain to the user why we need to read the contacts
//                }

                activity.requestPermissions(permissions, RequirePermissionCode);
                return false;
            }
        }
        return true;
    }

    public static boolean hasPermissions(@NonNull Context context, @NonNull String... allPermissionNeeded)
    {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && context != null && allPermissionNeeded != null)
            for (String permission : allPermissionNeeded)
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                    return false;
        return true;
    }

    public static final String WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
    public static final String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    //    public static final String CAMERA = "android.permission.CAMERA";
//    public static final String RECORD_AUDIO = "android.permission.RECORD_AUDIO";
    public static final String SEND_SMS = "android.permission.SEND_SMS";
    //    public static final String LOCATION_COARSE = "android.permission.ACCESS_COARSE_LOCATION";
//    public static final String LOCATION_FINE = "LOCATION_COARSE";
    public static final String READ_CONTACTS = "android.permission.READ_CONTACTS";
    public static final String WRITE_CONTACTS = "android.permission.WRITE_CONTACTS";
    public static final String READ_SMS = "android.permission.READ_SMS";

    public static final String READ_PHONE_STATE = "android.permission.READ_PHONE_STATE";


//    public static void checkCameraPerm(Activity activity)
//    {
//        requirePermission(activity, CAMERA);
//    }
//
//    public static void checkRecordAudio(Activity activity)
//    {
//        requirePermission(activity, RECORD_AUDIO);
//    }


    public static boolean checkStoragePerm(Activity activity)
    {
        return requirePermission(activity, WRITE_EXTERNAL_STORAGE) && requirePermission(activity, READ_EXTERNAL_STORAGE);
    }

    public static void checkLocationPerm(Activity activity)
    {

    }

    public static boolean checkSendSMSPerm(Activity activity)
    {
        return requirePermission(activity, SEND_SMS);
    }

    public static boolean checkAllPerm(Activity activity)
    {
        return requirePermissions(activity, READ_EXTERNAL_STORAGE,
                WRITE_EXTERNAL_STORAGE, READ_CONTACTS,
                WRITE_CONTACTS, SEND_SMS, READ_PHONE_STATE);
    }

    public static boolean checkContactPerm(Activity activity)
    {
        return requirePermissions(activity, READ_CONTACTS, WRITE_CONTACTS);
    }

    public static boolean checkReadSMS(Activity activity)
    {
        return requirePermission(activity, READ_SMS);
    }

    public static boolean checkREAD_PHONE_STATE(Activity activity)
    {
        return requirePermission(activity, READ_PHONE_STATE);
    }

    public static boolean checkBasic(Activity activity)
    {
        return requirePermissions(activity, READ_EXTERNAL_STORAGE, READ_CONTACTS, READ_SMS, READ_PHONE_STATE);
    }
}
