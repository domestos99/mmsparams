/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.sms;

import android.content.Context;
import android.util.Pair;

import org.whispersystems.jobqueue.JobManager;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ContactTokenDetails;
import org.whispersystems.signalservice.api.util.InvalidNumberException;

import java.io.IOException;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.crypto.MasterSecretUnion;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.EncryptingSmsDatabase;
import cz.uhk.bak.mmsparams.database.MmsDatabase;
import cz.uhk.bak.mmsparams.database.NotInDirectoryException;
import cz.uhk.bak.mmsparams.database.SmsDatabase;
import cz.uhk.bak.mmsparams.database.TextSecureDirectory;
import cz.uhk.bak.mmsparams.database.ThreadDatabase;
import cz.uhk.bak.mmsparams.database.model.MessageRecord;
import cz.uhk.bak.mmsparams.jobs.MmsSendJob;
import cz.uhk.bak.mmsparams.jobs.MmsTemplateSendJob;
import cz.uhk.bak.mmsparams.jobs.PushGroupSendJob;
import cz.uhk.bak.mmsparams.jobs.PushMediaSendJob;
import cz.uhk.bak.mmsparams.jobs.PushTextSendJob;
import cz.uhk.bak.mmsparams.jobs.SmsSendJob;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.MmsException;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.push.AccountManagerFactory;
import cz.uhk.bak.mmsparams.recipients.Recipient;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.service.ExpiringMessageManager;
import cz.uhk.bak.mmsparams.util.GroupUtil;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.Util;

public class MessageSender
{

    private static final String TAG = MessageSender.class.getSimpleName();

    public static long send(final Context context,
                            final MasterSecret masterSecret,
                            final OutgoingTextMessage message,
                            final long threadId,
                            final boolean forceSms,
                            final SmsDatabase.InsertListener insertListener)
    {
        EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);
        Recipients recipients = message.getRecipients();
        boolean keyExchange = message.isKeyExchange();

        long allocatedThreadId;

        if (threadId == -1)
        {
            allocatedThreadId = DatabaseFactory.getThreadDatabase(context).getThreadIdFor(recipients);
        }
        else
        {
            allocatedThreadId = threadId;
        }

        long messageId = database.insertMessageOutbox(new MasterSecretUnion(masterSecret), allocatedThreadId,
                message, forceSms, System.currentTimeMillis(), insertListener);

        sendTextMessage(context, recipients, forceSms, keyExchange, messageId, message.getExpiresIn());

        return allocatedThreadId;
    }

    public static long send(final Context context,
                            final MasterSecret masterSecret,
                            final OutgoingMediaMessage message,
                            final long threadId,
                            final boolean forceSms,
                            final SmsDatabase.InsertListener insertListener)
    {
        try
        {
            ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(context);
            MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

            long allocatedThreadId;

            if (threadId == -1)
            {
                allocatedThreadId = threadDatabase.getThreadIdFor(message.getRecipients(), message.getDistributionType());
            }
            else
            {
                allocatedThreadId = threadId;
            }

            Recipients recipients = message.getRecipients();
            long messageId = database.insertMessageOutbox(new MasterSecretUnion(masterSecret), message, allocatedThreadId, forceSms, insertListener);

            sendMediaMessage(context, masterSecret, recipients, forceSms, messageId, message.getExpiresIn());

            return allocatedThreadId;
        }
        catch (MmsException e)
        {
            LogFB.w(TAG, e);
            return threadId;
        }
    }


    public static long sendWithMmsTemplate(final Context context,
                                           final MasterSecret masterSecret,
                                           final OutgoingMediaMessage message,
                                           final long threadId,
                                           final int mmsTemplateID,
                                           final SmsDatabase.InsertListener insertListener)
    {
        try
        {
            ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(context);
            MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

            long allocatedThreadId;

            if (threadId == -1)
            {
                allocatedThreadId = threadDatabase.getThreadIdFor(message.getRecipients(), message.getDistributionType());
            }
            else
            {
                allocatedThreadId = threadId;
            }

            Recipients recipients = message.getRecipients();
            long messageId = database.insertMessageOutbox(new MasterSecretUnion(masterSecret), message, allocatedThreadId, false, insertListener);

            JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
            jobManager.add(new MmsTemplateSendJob(context, messageId, mmsTemplateID));

            return allocatedThreadId;
        }
        catch (MmsException e)
        {
            LogFB.w(TAG, e);
            return threadId;
        }
    }


    public static void resendGroupMessage(Context context, MasterSecret masterSecret, MessageRecord messageRecord, long filterRecipientId)
    {
        if (!messageRecord.isMms()) throw new AssertionError("Not Group");

        Recipients recipients = DatabaseFactory.getMmsAddressDatabase(context).getRecipientsForId(messageRecord.getId());
        sendGroupPush(context, recipients, messageRecord.getId(), filterRecipientId);
    }

    public static void resend(Context context, MasterSecret masterSecret, MessageRecord messageRecord)
    {
        try
        {
            long messageId = messageRecord.getId();
            boolean forceSms = messageRecord.isForcedSms();
            boolean keyExchange = messageRecord.isKeyExchange();
            long expiresIn = messageRecord.getExpiresIn();

            if (messageRecord.isMms())
            {
                Recipients recipients = DatabaseFactory.getMmsAddressDatabase(context).getRecipientsForId(messageId);
                sendMediaMessage(context, masterSecret, recipients, forceSms, messageId, expiresIn);
            }
            else
            {
                Recipients recipients = messageRecord.getRecipients();
                sendTextMessage(context, recipients, forceSms, keyExchange, messageId, expiresIn);
            }
        }
        catch (MmsException e)
        {
            LogFB.w(TAG, e);
        }
    }

    private static void sendMediaMessage(Context context, MasterSecret masterSecret,
                                         Recipients recipients, boolean forceSms,
                                         long messageId, long expiresIn)
            throws MmsException
    {
        if (!forceSms && isSelfSend(context, recipients))
        {
            sendMediaSelf(context, masterSecret, messageId, expiresIn);
        }
        else if (isGroupPushSend(recipients))
        {
            sendGroupPush(context, recipients, messageId, -1);
        }
        else if (!forceSms && isPushMediaSend(context, recipients))
        {
            sendMediaPush(context, recipients, messageId);
        }
        else
        {
            sendMms(context, messageId);
        }
    }

    private static void sendTextMessage(Context context, Recipients recipients,
                                        boolean forceSms, boolean keyExchange,
                                        long messageId, long expiresIn)
    {
        if (!forceSms && isSelfSend(context, recipients))
        {
            sendTextSelf(context, messageId, expiresIn);
        }
        else if (!forceSms && isPushTextSend(context, recipients, keyExchange))
        {
            sendTextPush(context, recipients, messageId);
        }
        else
        {
            sendSms(context, recipients, messageId);
        }
    }

    private static void sendTextSelf(Context context, long messageId, long expiresIn)
    {
        EncryptingSmsDatabase database = DatabaseFactory.getEncryptingSmsDatabase(context);

        database.markAsSent(messageId, true);

        Pair<Long, Long> messageAndThreadId = database.copyMessageInbox(messageId);
        database.markAsPush(messageAndThreadId.first);

        if (expiresIn > 0)
        {
            ExpiringMessageManager expiringMessageManager = ApplicationContext.getInstance(context).getExpiringMessageManager();

            database.markExpireStarted(messageId);
            expiringMessageManager.scheduleDeletion(messageId, false, expiresIn);
        }
    }

    private static void sendMediaSelf(Context context, MasterSecret masterSecret,
                                      long messageId, long expiresIn)
            throws MmsException
    {
        ExpiringMessageManager expiringMessageManager = ApplicationContext.getInstance(context).getExpiringMessageManager();
        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

        database.markAsSent(messageId, true);
        database.copyMessageInbox(masterSecret, messageId);

        if (expiresIn > 0)
        {
            database.markExpireStarted(messageId);
            expiringMessageManager.scheduleDeletion(messageId, true, expiresIn);
        }
    }

    private static void sendTextPush(Context context, Recipients recipients, long messageId)
    {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new PushTextSendJob(context, messageId, recipients.getPrimaryRecipient().getNumber()));
    }

    private static void sendMediaPush(Context context, Recipients recipients, long messageId)
    {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new PushMediaSendJob(context, messageId, recipients.getPrimaryRecipient().getNumber()));
    }

    private static void sendGroupPush(Context context, Recipients recipients, long messageId, long filterRecipientId)
    {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new PushGroupSendJob(context, messageId, recipients.getPrimaryRecipient().getNumber(), filterRecipientId));
    }

    private static void sendSms(Context context, Recipients recipients, long messageId)
    {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new SmsSendJob(context, messageId, recipients.getPrimaryRecipient().getName()));
    }

    private static void sendMms(Context context, long messageId)
    {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        jobManager.add(new MmsSendJob(context, messageId));
    }

    private static boolean isPushTextSend(Context context, Recipients recipients, boolean keyExchange)
    {
        try
        {
            if (!TextSecurePreferences.isPushRegistered(context))
            {
                return false;
            }

            if (keyExchange)
            {
                return false;
            }

            Recipient recipient = recipients.getPrimaryRecipient();
            String destination = Util.canonicalizeNumber(context, recipient.getNumber());

            return isPushDestination(context, destination);
        }
        catch (InvalidNumberException e)
        {
            LogFB.w(TAG, e);
            return false;
        }
    }

    private static boolean isPushMediaSend(Context context, Recipients recipients)
    {
        try
        {
            if (!TextSecurePreferences.isPushRegistered(context))
            {
                return false;
            }

            if (recipients.getRecipientsList().size() > 1)
            {
                return false;
            }

            Recipient recipient = recipients.getPrimaryRecipient();
            String destination = Util.canonicalizeNumber(context, recipient.getNumber());

            return isPushDestination(context, destination);
        }
        catch (InvalidNumberException e)
        {
            LogFB.w(TAG, e);
            return false;
        }
    }

    private static boolean isGroupPushSend(Recipients recipients)
    {
        return GroupUtil.isEncodedGroup(recipients.getPrimaryRecipient().getNumber());
    }

    private static boolean isSelfSend(Context context, Recipients recipients)
    {
        if (!TextSecurePreferences.isPushRegistered(context))
        {
            return false;
        }

        if (!recipients.isSingleRecipient())
        {
            return false;
        }

        if (recipients.isGroupRecipient())
        {
            return false;
        }

        return Util.isOwnNumber(context, recipients.getPrimaryRecipient().getNumber());
    }

    private static boolean isPushDestination(Context context, String destination)
    {
        TextSecureDirectory directory = TextSecureDirectory.getInstance(context);

        try
        {
            return directory.isSecureTextSupported(destination);
        }
        catch (NotInDirectoryException e)
        {
            try
            {
                SignalServiceAccountManager accountManager = AccountManagerFactory.createManager(context);
                Optional<ContactTokenDetails> registeredUser = accountManager.getContact(destination);

                if (!registeredUser.isPresent())
                {
                    registeredUser = Optional.of(new ContactTokenDetails());
                    registeredUser.get().setNumber(destination);
                    directory.setNumber(registeredUser.get(), false);
                    return false;
                }
                else
                {
                    registeredUser.get().setNumber(destination);
                    directory.setNumber(registeredUser.get(), true);
                    return true;
                }
            }
            catch (IOException e1)
            {
                LogFB.w(TAG, e1);
                return false;
            }
        }
    }

}
