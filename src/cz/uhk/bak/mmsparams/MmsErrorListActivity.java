package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;

public class MmsErrorListActivity extends MyListPassphraseRequiredActionBarActivity implements OnFragmentInteractionListener
{
    BaseFragment fragment;


    public static Intent newInstance(final Context context, final long messageID)
    {
        Intent intent = new Intent(context, MmsErrorListActivity.class);
        intent.putExtra(MessageDetailsActivity.MESSAGE_ID_EXTRA, messageID);
        return intent;
    }

    @Override
    protected BaseFragment getListFragment()
    {
        fragment = MmsErrorListFragment.newInstance();
        return fragment;
    }

    @Override
    protected Bundle customBundle(Bundle bundle)
    {
        long messageId = getIntent().getLongExtra(MessageDetailsActivity.MESSAGE_ID_EXTRA, -1);
        bundle.putLong(MessageDetailsActivity.MESSAGE_ID_EXTRA, messageId);
        return bundle;
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }

    @Override
    protected boolean beforeActivityFinish()
    {
        boolean close = fragment.handleOnClosing();

        if (close)
        {
            onBackPressed();
        }
        return true;

    }

}