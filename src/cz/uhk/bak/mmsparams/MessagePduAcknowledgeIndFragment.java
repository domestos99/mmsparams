package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.AcknowledgeInd;
import com.google.android.mms.pdu_alt.PduBody;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduAcknowledgeIndFragment extends MessagePduBaseFragment
{
    private int paramID;
    private AcknowledgeInd acknowledgeInd;
    private MessagePdu messagePdu;
    public static final String TAG = MessagePduAcknowledgeIndFragment.class.getSimpleName();


    private TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMmsReportAllowed, tvMmsTransactionID, tvPduCreated;

    public static MessagePduAcknowledgeIndFragment newInstance(int paramID)
    {
        MessagePduAcknowledgeIndFragment fragment = new MessagePduAcknowledgeIndFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        acknowledgeInd = (AcknowledgeInd) messagePdu.getGenericPdu();
    }

    private void fillControls()
    {

        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getNumberString(acknowledgeInd.getFrom()));
            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(acknowledgeInd.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(acknowledgeInd.getMmsVersion()));

            tvMmsReportAllowed.setText(PduHeaderConvertor.getValueString(acknowledgeInd.getReportAllowed()));
            tvMmsTransactionID.setText(getString(acknowledgeInd.getTransactionId()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }

    private String getBody(PduBody body)
    {

        return "";
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_acknowledge_ind_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);

        tvMmsReportAllowed = ViewUtil.findById(view, R.id.tvMmsReportAllowed);
        tvMmsTransactionID = ViewUtil.findById(view, R.id.tvMmsTransactionID);

        fillControls();
    }

    @Override
    protected void setupListeners()
    {

    }

}
