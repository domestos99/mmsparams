package cz.uhk.bak.mmsparams.giph.ui;


import android.os.Bundle;
import android.support.v4.content.Loader;

import java.util.List;

import cz.uhk.bak.mmsparams.giph.model.GiphyImage;
import cz.uhk.bak.mmsparams.giph.net.GiphyStickerLoader;

public class GiphyStickerFragment extends GiphyFragment
{
    @Override
    public Loader<List<GiphyImage>> onCreateLoader(int id, Bundle args)
    {
        return new GiphyStickerLoader(getActivity(), searchString);
    }
}
