package cz.uhk.bak.mmsparams;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.whispersystems.libsignal.InvalidMessageException;

import java.util.List;

import cz.uhk.bak.mmsparams.components.AnimatingToggle;
import cz.uhk.bak.mmsparams.components.AttachmentTypeSelector;
import cz.uhk.bak.mmsparams.components.ComposeText;
import cz.uhk.bak.mmsparams.components.HidingLinearLayout;
import cz.uhk.bak.mmsparams.components.InputAwareLayout;
import cz.uhk.bak.mmsparams.components.InputPanel;
import cz.uhk.bak.mmsparams.components.RecipientPicker.RecipientPickerComponent;
import cz.uhk.bak.mmsparams.components.SendButton;
import cz.uhk.bak.mmsparams.components.camera.QuickAttachmentDrawer;
import cz.uhk.bak.mmsparams.components.emoji.EmojiDrawer;
import cz.uhk.bak.mmsparams.crypto.MasterCipher;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.DraftDatabase;
import cz.uhk.bak.mmsparams.database.MmsTemplateDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.database.model.MmsProfilesFactory;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.database.model.MmsTemplateDetailGroup;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.AttachmentManager;
import cz.uhk.bak.mmsparams.mms.AttachmentManager2;
import cz.uhk.bak.mmsparams.mms.MediaConstraints;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.mms.listeners.AttachmentListener;
import cz.uhk.bak.mmsparams.mms.slides.LocationSlide;
import cz.uhk.bak.mmsparams.permissions.PermissionUtil;
import cz.uhk.bak.mmsparams.providers.PersistentBlobProvider;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.util.ActivityResultHelper;
import cz.uhk.bak.mmsparams.util.CharacterCalculator;
import cz.uhk.bak.mmsparams.util.MediaUtil;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.Util;
import cz.uhk.bak.mmsparams.util.ViewUtil;
import cz.uhk.bak.mmsparams.util.views.Stub;

import static android.app.Activity.RESULT_OK;

public class MmsTemplateDetailControls extends DetailControlsBase<MmsTemplate>
        implements
        InputPanel.Listener,
        InputPanel.MediaListener,
        AttachmentListener, QuickAttachmentDrawer.AttachmentDrawerListener
{
    public static final String TAG = MmsTemplateDetailControls.class.getSimpleName();

    public static final int PICK_GALLERY = 1;
    public static final int PICK_DOCUMENT = 2;
    public static final int PICK_AUDIO = 3;
    public static final int PICK_CONTACT_INFO = 4;
    public static final int GROUP_EDIT = 5;
    public static final int TAKE_PHOTO = 6;
    public static final int ADD_CONTACT = 7;
    public static final int PICK_LOCATION = 8;
    public static final int PICK_GIF = 9;
    public static final int SMS_DEFAULT = 10;

    public static final int ADD_CONTACT_TO = 12;
    public static final int ADD_CONTACT_BCC = 13;
    public static final int ADD_CONTACT_CC = 14;


    private MasterSecret masterSecret;
    EditText tbName, tbSubject;

    private InputPanel inputPanel;

    RecipientPickerComponent rpRecipientTo, rpRecipientBcc, rpRecipientCc;

    private MmsTemplateDetailGroup detailGroup;

    private AttachmentManager2 attachmentManager;

    private Stub<EmojiDrawer> emojiDrawerStub;
    private SendButton sendButton;
    private InputAwareLayout container;
    protected ComposeText composeText;
    private ImageButton attachButton;
    private AttachmentTypeSelector attachmentTypeSelector;
    private QuickAttachmentDrawer quickAttachmentDrawer;
    protected HidingLinearLayout quickAttachmentToggle;
    private AnimatingToggle buttonToggle;
    private long mmsTemplateID;

    private MmsProfile mmsProfile;

    protected MmsTemplateDetailControls(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }


    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();
    }

    @Override
    public void initControls(View view)
    {

        tbName = ViewUtil.findById(view, R.id.tbName);
        tbSubject = ViewUtil.findById(view, R.id.tbSubject);

        rpRecipientTo = ViewUtil.findById(view, R.id.rpRecipientTo);
        rpRecipientBcc = ViewUtil.findById(view, R.id.rpRecipientBcc);
        rpRecipientCc = ViewUtil.findById(view, R.id.rpRecipientCc);


        rpRecipientTo.setTitle("To");
        rpRecipientBcc.setTitle("Bcc");
        rpRecipientCc.setTitle("Cc");

//        emojiDrawerStub = ViewUtil.findStubById(baseFragment.getActivity(), R.id.emoji_drawer_stub);
        emojiDrawerStub = new Stub<>((ViewStub) ViewUtil.findById(view, R.id.emoji_drawer_stub));

        sendButton = ViewUtil.findById(view, R.id.send_button);
        container = ViewUtil.findById(view, R.id.layout_container);
        composeText = ViewUtil.findById(view, R.id.embedded_text_editor);
        charactersLeft = ViewUtil.findById(view, R.id.space_left);
        attachButton = ViewUtil.findById(view, R.id.attach_button);
        attachmentTypeSelector = null;
        attachmentManager = new AttachmentManager2(context, view, this, masterSecret);
        quickAttachmentDrawer = ViewUtil.findById(view, R.id.quick_attachment_drawer);
        quickAttachmentToggle = ViewUtil.findById(view, R.id.quick_attachment_toggle);
        buttonToggle = ViewUtil.findById(view, R.id.button_toggle);

        inputPanel = ViewUtil.findById(view, R.id.bottom_panel);
        inputPanel.setListener(this);
        inputPanel.setMediaListener(this);


    }

    @Override
    public void setupListeners()
    {
        rpRecipientTo.setListeners(new RecipientPickerComponent.IRecipientPickInitListener()
        {
            @Override
            public void OnRecipientPickInit()
            {
                handleAddContacts(ADD_CONTACT_TO);
            }
        });

        rpRecipientBcc.setListeners(new RecipientPickerComponent.IRecipientPickInitListener()
        {
            @Override
            public void OnRecipientPickInit()
            {
                handleAddContacts(ADD_CONTACT_BCC);
            }
        });

        rpRecipientCc.setListeners(new RecipientPickerComponent.IRecipientPickInitListener()
        {
            @Override
            public void OnRecipientPickInit()
            {
                handleAddContacts(ADD_CONTACT_CC);
            }
        });


        attachButton.setOnClickListener(new AttachButtonListener());
        attachButton.setOnLongClickListener(null); // new AttachButtonLongClickListener());


        SendButtonListener sendButtonListener = new SendButtonListener();
        ComposeKeyPressedListener composeKeyPressedListener = new ComposeKeyPressedListener();

        composeText.setOnKeyListener(composeKeyPressedListener);
        composeText.addTextChangedListener(composeKeyPressedListener);
        composeText.setOnEditorActionListener(sendButtonListener);
        composeText.setOnClickListener(composeKeyPressedListener);
        composeText.setOnFocusChangeListener(composeKeyPressedListener);


        if (QuickAttachmentDrawer.isDeviceSupported(context))
        {
            quickAttachmentDrawer.setListener(this);
        }
        else
        {
        }


    }


    private String getMessage()
    {
        try
        {
            String rawText = composeText.getTextTrimmed();

            if (rawText.length() < 1 && !attachmentManager.isAttachmentPresent())
                throw new InvalidMessageException(baseFragment.getString(R.string.ConversationActivity_message_is_empty_exclamation));

            return rawText;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            LogFB.e(TAG, "getMessage", ex);
            return null;
        }
    }

    public void initAttachments()
    {
        attachmentManager.cleanup();

        if (mmsTemplateID < 0)
            return;

        DraftDatabase db = DatabaseFactory.getDraftDatabase(context);
        List<DraftDatabase.Draft> drafts = db.getDrafts(new MasterCipher(masterSecret), MmsTemplateDatabase.getDraftMmsTemplateID(mmsTemplateID));


        for (DraftDatabase.Draft draft : drafts)
        {
            try
            {
                if (draft.getType().equals(DraftDatabase.Draft.TEXT))
                {
                    composeText.setText(draft.getValue());
                }
                else if (draft.getType().equals(DraftDatabase.Draft.LOCATION))
                {

                }
                else if (draft.getType().equals(DraftDatabase.Draft.IMAGE))
                {
                    setMedia(Uri.parse(draft.getValue()), MediaType.IMAGE);
                }
                else if (draft.getType().equals(DraftDatabase.Draft.AUDIO))
                {
                    setMedia(Uri.parse(draft.getValue()), MediaType.AUDIO);
                }
                else if (draft.getType().equals(DraftDatabase.Draft.VIDEO))
                {
                    setMedia(Uri.parse(draft.getValue()), MediaType.VIDEO);
                }
//                else if (draft.getType().equals(DraftDatabase.Draft.MMS_SETTINGS))
//                {
//                    int id = Integer.parseInt(draft.getValue());
//
//                    if (id <= 0)
//                    {
//                        setSettings(MmsSetting.getPrefDefault(context));
//                    }
//                    else
//                    {
//                        MmsSettingsDatabase d = DatabaseFactory.getMmsProfileDatabase(context);
//                        setSettings(d.getById(Integer.parseInt(draft.getValue())));
//                    }
//                }
            }
            catch (Exception e)
            {
                LogFB.w(TAG, e);
            }
        }

        updateToggleButtonState();
    }

    public void onSaveData(@NonNull final long id)
    {
        // after save data
        // I have ID
        // I have to store all attachments

        final DraftDatabase.Drafts drafts = getDraftsForCurrentState();

        DraftDatabase db = DatabaseFactory.getDraftDatabase(context);

        db.clearDrafts(MmsTemplateDatabase.getDraftMmsTemplateID(id));
        db.insertDrafts(new MasterCipher(masterSecret), MmsTemplateDatabase.getDraftMmsTemplateID(id), drafts);

    }

    public DraftDatabase.Drafts getDraftsForCurrentState()
    {
        DraftDatabase.Drafts drafts = new DraftDatabase.Drafts();

        if (!Util.isEmpty(composeText))
        {
            drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.TEXT, composeText.getTextTrimmed()));
        }

        for (Slide slide : attachmentManager.buildSlideDeck().getSlides())
        {
            if (slide.hasAudio() && slide.getUri() != null)
                drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.AUDIO, slide.getUri().toString()));
            else if (slide.hasVideo() && slide.getUri() != null)
                drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.VIDEO, slide.getUri().toString()));
            else if (slide.hasLocation())
                drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.LOCATION, ((LocationSlide) slide).getPlace().serialize()));
            else if (slide.hasImage() && slide.getUri() != null)
                drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.IMAGE, slide.getUri().toString()));

        }

        return drafts;
    }

    public void setID(long ID)
    {
        this.mmsTemplateID = ID;
    }

    public void afterLoadData(int id)
    {
        setID(id);
        initAttachments();
    }

    @Nullable
    public Recipients getRecipients()
    {
        String number = this.detailGroup.getTo();

        if (number == null || number.length() == 0)
            return null;

        Recipients r = RecipientFactory.getRecipientsFromStrings(context, number, true);
        return r;
    }

    private class SendButtonListener implements View.OnClickListener, TextView.OnEditorActionListener
    {
        @Override
        public void onClick(View v)
        {

        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
        {
            if (actionId == EditorInfo.IME_ACTION_SEND)
            {
                sendButton.performClick();
                return true;
            }
            return false;
        }
    }


    public void setMasterSecret(MasterSecret masterSecret)
    {
        this.masterSecret = masterSecret;
    }

    @Override
    protected MmsTemplate updateModelFromControls(MmsTemplate obj)
    {
        MmsTemplate mt = new MmsTemplate(obj);

        mt.setName(tbName.getText().toString());

        MmsTemplateDetailGroup rg = getRecipientGroup();

        mt.setMmsTemplateDetailGroup(rg);
        mt.setMmsSettingJSON(MmsProfilesFactory.getBloJson(this.mmsProfile));


        rg.setTo(rpRecipientTo.getNumberCSV());
        rg.setBcc(rpRecipientBcc.getNumberCSV());
        rg.setCc(rpRecipientCc.getNumberCSV());
        rg.setSubject(tbSubject.getText().toString());
        rg.setBody(getMessage());


        mt.setMmsTemplateDetailGroup(rg);


        return mt;
    }

    @Override
    protected void fillControls(MmsTemplate obj)
    {
        tbName.setText(obj.getName());

        this.mmsProfile = MmsProfilesFactory.getFromJsonOrDefault(context, obj.getMmsSettingJSON());


        this.detailGroup = obj.getMmsTemplateDetailGroup();
        if (detailGroup == null)
            detailGroup = new MmsTemplateDetailGroup();

        tbSubject.setText(detailGroup.getSubject());

        setupContactField();

    }

    private void handleAddContacts(int reqCode)
    {
        baseFragment.startActivityForResult(new Intent(context, MmsTemplatePickContactActivity.class), reqCode);
    }

    public void handleContactPicked(Intent data, int reqCode)
    {
        String number = ActivityResultHelper.handleMmsSettingsPicked(data);

        if (number == null)
            return;

        switch (reqCode)
        {
            case ADD_CONTACT_TO:
                rpRecipientTo.addNumber(number);
                break;
            case ADD_CONTACT_BCC:
                rpRecipientBcc.addNumber(number);
                break;
            case ADD_CONTACT_CC:
                rpRecipientCc.addNumber(number);
                break;
        }
    }

    private void setupContactField()
    {
        rpRecipientTo.addNumberCSV(this.detailGroup.getTo());
        rpRecipientBcc.addNumberCSV(this.detailGroup.getBcc());
        rpRecipientCc.addNumberCSV(this.detailGroup.getCc());
    }

    private MmsTemplateDetailGroup getRecipientGroup()
    {
        MmsTemplateDetailGroup rg = new MmsTemplateDetailGroup();

        return rg;
    }


    @Override
    public void onRecorderStarted()
    {

    }

    @Override
    public void onRecorderFinished()
    {

    }

    @Override
    public void onRecorderCanceled()
    {

    }

    @Override
    public void onEmojiToggle()
    {
        if (!emojiDrawerStub.resolved())
        {
            inputPanel.setEmojiDrawer(emojiDrawerStub.get());
            emojiDrawerStub.get().setEmojiEventListener(inputPanel);
        }

        if (container.getCurrentInput() == emojiDrawerStub.get())
        {
            container.showSoftkey(composeText);
        }
        else
        {
            container.show(composeText, emojiDrawerStub.get());
        }
    }

    @Override
    public void onMediaSelected(@NonNull Uri uri, String contentType)
    {
        if (!TextUtils.isEmpty(contentType) && contentType.trim().equals("image/gif"))
        {
            setMedia(uri, MediaType.GIF);
        }
        else if (MediaUtil.isImageType(contentType))
        {
            setMedia(uri, MediaType.IMAGE);
        }
        else if (MediaUtil.isVideoType(contentType))
        {
            setMedia(uri, MediaType.VIDEO);
        }
        else if (MediaUtil.isAudioType(contentType))
        {
            setMedia(uri, MediaType.AUDIO);
        }
    }

    private void setMedia(@Nullable Uri uri, @NonNull MediaType mediaType)
    {
        if (uri == null)
            return;
        attachmentManager.setMedia(masterSecret, uri, mediaType, getCurrentMediaConstraints());
    }

    private MediaConstraints getCurrentMediaConstraints()
    {
        return sendButton.getSelectedTransport().getType() == TransportOption.Type.TEXTSECURE
                ? MediaConstraints.getPushMediaConstraints()
                : MediaConstraints.getMmsMediaConstraints(sendButton.getSelectedTransport().getSimSubscriptionId().or(-1));
    }

    public void onActivityResult(final int reqCode, int resultCode, Intent data)
    {

        LogFB.w(TAG, "onActivityResult called: " + reqCode + ", " + resultCode + " , " + data);


        if ((data == null && reqCode != TAKE_PHOTO && reqCode != SMS_DEFAULT) ||
                (resultCode != RESULT_OK && reqCode != SMS_DEFAULT))
        {
            return;
        }


        switch (reqCode)
        {
            case PICK_GALLERY:
                MediaType mediaType;

                String mimeType = MediaUtil.getMimeType(context, data.getData());

                if (MediaUtil.isGif(mimeType)) mediaType = MediaType.GIF;
                else if (MediaUtil.isVideo(mimeType)) mediaType = MediaType.VIDEO;
                else mediaType = MediaType.IMAGE;

                setMedia(data.getData(), mediaType);
                break;
            case PICK_DOCUMENT:
                setMedia(data.getData(), MediaType.DOCUMENT);
                break;
            case PICK_AUDIO:
                setMedia(data.getData(), MediaType.AUDIO);
                break;

            case MmsTemplateDetailControls.ADD_CONTACT_TO:
                handleContactPicked(data, MmsTemplateDetailControls.ADD_CONTACT_TO);
                break;
            case MmsTemplateDetailControls.ADD_CONTACT_BCC:
                handleContactPicked(data, MmsTemplateDetailControls.ADD_CONTACT_BCC);
                break;
            case MmsTemplateDetailControls.ADD_CONTACT_CC:
                handleContactPicked(data, MmsTemplateDetailControls.ADD_CONTACT_CC);
                break;

        }
    }

    @Override
    public void onAttachmentChanged()
    {
        // TODO proverit
        //  handleSecurityChange(isSecureText, isDefaultSms);
        updateToggleButtonState();
    }

    private void updateToggleButtonState()
    {
        if (composeText.getTextTrimmed().length() == 0 && !attachmentManager.isAttachmentPresent())
        {
            buttonToggle.display(attachButton);
            quickAttachmentToggle.show();
        }
        else
        {
            // buttonToggle.display(sendButton);
            quickAttachmentToggle.hide();
        }
    }

    @Override
    public void onAttachmentDrawerStateChanged(QuickAttachmentDrawer.DrawerState drawerState)
    {
//        if (drawerState == QuickAttachmentDrawer.DrawerState.FULL_EXPANDED)
//        {
//            getSupportActionBar().hide();
//        }
//        else
//        {
//            getSupportActionBar().show();
//        }

        if (drawerState == QuickAttachmentDrawer.DrawerState.COLLAPSED)
        {
            container.hideAttachedInput(true);
        }
    }

    @Override
    public void onImageCapture(@NonNull byte[] imageBytes)
    {
        setMedia(PersistentBlobProvider.getInstance(context)
                        .create(masterSecret, imageBytes, MediaUtil.IMAGE_JPEG, null),
                MediaType.IMAGE);
        quickAttachmentDrawer.hide(false);
    }

    @Override
    public void onCameraFail()
    {
        Toast.makeText(context, R.string.ConversationActivity_quick_camera_unavailable, Toast.LENGTH_SHORT).show();
        quickAttachmentDrawer.hide(false);
//        quickAttachmentToggle.disable();
    }

    @Override
    public void onCameraStart()
    {

    }

    @Override
    public void onCameraStop()
    {

    }


    private class AttachButtonListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            handleAddAttachment();
        }
    }

    private void handleAddAttachment()
    {
        if (!PermissionUtil.checkStoragePerm(baseFragment.getActivity()))
        {
            Toast.makeText(context, R.string.permission_not_granted, Toast.LENGTH_SHORT).show();
            return;
        }


        if (attachmentTypeSelector == null)
        {
            attachmentTypeSelector = new AttachmentTypeSelector(baseFragment.getActivity(), baseFragment.getLoaderManager(), new AttachmentTypeListener());
        }
        attachmentTypeSelector.show(attachButton);

    }


    private class AttachmentTypeListener implements AttachmentTypeSelector.AttachmentClickedListener
    {
        @Override
        public void onClick(int type)
        {
            addAttachment(type);
        }

        @Override
        public void onQuickAttachment(Uri uri)
        {
            Intent intent = new Intent();
            intent.setData(uri);

            baseFragment.onActivityResult(PICK_GALLERY, RESULT_OK, intent);
        }
    }

    private void addAttachment(int type)
    {
        LogFB.w("ComposeMessageActivity", "Selected: " + type);
        switch (type)
        {
            case AttachmentTypeSelector.ADD_GALLERY:
                AttachmentManager.selectGallery(baseFragment, PICK_GALLERY);

                break;
            case AttachmentTypeSelector.ADD_DOCUMENT:
                AttachmentManager.selectDocument(baseFragment, PICK_DOCUMENT);
                break;
            case AttachmentTypeSelector.ADD_SOUND:
                AttachmentManager.selectAudio(baseFragment, PICK_AUDIO);
                break;
        }
    }


    private class ComposeKeyPressedListener implements View.OnKeyListener, View.OnClickListener, TextWatcher, View.OnFocusChangeListener
    {

        int beforeLength;

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event)
        {
            if (event.getAction() == KeyEvent.ACTION_DOWN)
            {
                if (keyCode == KeyEvent.KEYCODE_ENTER)
                {
                    if (TextSecurePreferences.isEnterSendsEnabled(context))
                    {
                        sendButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                        sendButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public void onClick(View v)
        {
            container.showSoftkey(composeText);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            beforeLength = composeText.getTextTrimmed().length();
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            calculateCharactersRemaining();

            if (composeText.getTextTrimmed().length() == 0 || beforeLength == 0)
            {
                composeText.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        updateToggleButtonState();
                    }
                }, 50);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {
        }
    }

    private TextView charactersLeft;

    private void calculateCharactersRemaining()
    {
        String messageBody = composeText.getTextTrimmed();
        TransportOption transportOption = sendButton.getSelectedTransport();
        CharacterCalculator.CharacterState characterState = transportOption.calculateCharacters(messageBody);

        if (characterState.charactersRemaining <= 15 || characterState.messagesSpent > 1)
        {
            charactersLeft.setText(characterState.charactersRemaining + "/" + characterState.maxMessageSize
                    + " (" + characterState.messagesSpent + ")");
            charactersLeft.setVisibility(View.VISIBLE);
        }
        else
        {
            charactersLeft.setVisibility(View.GONE);
        }
    }


}
