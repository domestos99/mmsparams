package cz.uhk.bak.mmsparams;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import cz.uhk.bak.mmsparams.database.ApnExtDatabase;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.mms.ApnExt;
import cz.uhk.bak.mmsparams.mms.ApnSingleton;
import cz.uhk.bak.mmsparams.util.DialogHelper;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class ApnExtDetailFragment extends DetailFragment<ApnExt>
{

    public static final String TAG = ApnExtDetailFragment.class.getSimpleName();
    ApnExtDetailControl controls;
    Button btnSave, btnDelete;

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.apn_ext_detail_fragment;
    }


    @Override
    protected void initControls(View view)
    {
        controls = new ApnExtDetailControl(getApplicationContext(), view);

        btnSave = ViewUtil.findById(view, R.id.btnSave);
        btnDelete = ViewUtil.findById(view, R.id.btnDelete);


        setHasOptionsMenu(true);
    }

    @Override
    protected void afterLoadData()
    {
        super.afterLoadData();


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        if (!instance.isSystem())
        {
            inflater.inflate(R.menu.menu_delete, menu);
            inflater.inflate(R.menu.menu_save, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    // hangle menu tap
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {
            case R.id.menu_save:
                save(true);
                return true;

            case R.id.menu_delete:
                handleDelete();
                return true;
        }
        return false;
    }

    private void handleDelete()
    {
        createAndShowAlertDialog();
    }

    private void createAndShowAlertDialog()
    {
        DialogHelper.createAndShowDeleteAlertDialog(getActivity(), new DialogHelper.OnOkListener()
        {
            @Override
            public void OnOk()
            {
                delete();
            }
        }, null);
    }

    @Override
    protected void setupListeners()
    {

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                save(true);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                delete();
            }
        });

    }

    @Override
    protected void load()
    {
        if (ID == -1)
        {
            instance = getNew();
        }
        else
        {
            instance = getById(ID);
        }

        fillControls(instance);
    }

    @Override
    protected ApnExt getById(int id)
    {
        return ApnSingleton.getInstance(getApplicationContext()).getById(getApplicationContext(), id);
    }

    @Override
    protected ApnExt getNew()
    {
        return ApnExt.createNew(getApplicationContext());
    }


    @Override
    protected void saveData(ApnExt instance)
    {
        ApnExtDatabase db = DatabaseFactory.getApnExtDatabase(getApplicationContext());
        db.save(instance);
    }

    @Override
    protected void deleteData(int id)
    {
        ApnExtDatabase db = DatabaseFactory.getApnExtDatabase(getApplicationContext());
        db.delete(id);
    }


    @Override
    protected void fillControls(ApnExt obj)
    {
        controls.fillControls(obj);
    }

    @Override
    protected ApnExt updateModel(ApnExt obj)
    {
        return controls.updateModelFromControls(obj);
    }


}
