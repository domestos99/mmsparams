package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsProfilesDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
import cz.uhk.bak.mmsparams.util.DialogHelper;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsProfilesDetailFragment extends DetailFragment<MmsProfile>
{
    private static final String TAG = MmsProfilesDetailFragment.class.getSimpleName();
    public final static int DEFAULT_MMS_SETTINGS_MODE = 2;

    MmsProfilesDetailControls controls;
    Button btnSave, btnDelete;

    ImageView btnExpiryInfo, btnDeliveryTimeInfo, btnInfoDeliveryReport, btnInfoAllowSendDeliveryReport, btnInfoSendRRIfNotRequired;

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_profiles_detail_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        controls = new MmsProfilesDetailControls(this, getFragmentManager(), view);

        btnSave = ViewUtil.findById(view, R.id.btnSave);
        btnDelete = ViewUtil.findById(view, R.id.btnDelete);
        btnExpiryInfo = ViewUtil.findById(view, R.id.btnExpiryInfo);
        btnDeliveryTimeInfo = ViewUtil.findById(view, R.id.btnDeliveryTimeInfo);
        btnInfoDeliveryReport = ViewUtil.findById(view, R.id.btnInfoDeliveryReport);
        btnInfoAllowSendDeliveryReport = ViewUtil.findById(view, R.id.btnInfoAllowSendDeliveryReport);
        btnInfoSendRRIfNotRequired = ViewUtil.findById(view, R.id.btnInfoSendRRIfNotRequired);

        setHasOptionsMenu(true);
    }

    @Override
    public void onResume()
    {
        String title = getResources().getString(R.string.text_menu_mms_settings_browse);
        getActivity().setTitle(title + " Detail");

        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_save, menu);

        if (this.mode == DEFAULT_MMS_SETTINGS_MODE || ID < 0)
        {
            // Cannot delete
        }
        else
        {
            inflater.inflate(R.menu.menu_delete, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    // hangle menu tap
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {
            case R.id.menu_save:
                save(true);
                return true;

            case R.id.menu_delete:
                handleDelete();
                return true;
        }
        return false;
    }


    private void handleDelete()
    {
        createAndShowAlertDialog();
    }

    private void createAndShowAlertDialog()
    {
        DialogHelper.createAndShowDeleteAlertDialog(getActivity(), new DialogHelper.OnOkListener()
        {
            @Override
            public void OnOk()
            {
                updateGlobalBeforeDelete();
                delete();
            }
        }, null);
    }

    private void updateGlobalBeforeDelete()
    {
        TextSecurePreferences.checkGlobalProfileDeletation(getContext(), ID);
    }

    @Override
    protected void setupListeners()
    {
        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                save(true);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                delete();
            }
        });

        btnExpiryInfo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getContext(), "Message Expiry - relative time after sending MMS", Toast.LENGTH_SHORT).show();
            }
        });
        btnDeliveryTimeInfo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getContext(), "Message Earliest Delivery - relative time after sending MMS", Toast.LENGTH_SHORT).show();
            }
        });

        btnInfoDeliveryReport.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getContext(), R.string.MmsProfilesDetail_info_InfoDeliveryReport, Toast.LENGTH_SHORT).show();
            }
        });
        btnInfoAllowSendDeliveryReport.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getContext(), R.string.MmsSettingsDetail_info_InfoAllowSendDeliveryReport, Toast.LENGTH_SHORT).show();
            }
        });
        btnInfoSendRRIfNotRequired.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getContext(), R.string.MmsSettingsDetail_info_SendRRIfNotRequired, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected MmsProfile GetForExtraID(int id, int mode)
    {
        if (mode == DEFAULT_MMS_SETTINGS_MODE)
        {
            // Load From Preferences
            return MmsProfile.getPrefDefault(getApplicationContext());
        }
        else
        {
            return super.GetForExtraID(id, mode);
        }

    }

    @Override
    protected MmsProfile getById(int id)
    {
        MmsProfilesDatabase db = DatabaseFactory.getMmsProfileDatabase(getApplicationContext());
        return db.getById(id);
    }

    @Override
    protected MmsProfile getNew()
    {
        MmsProfile ms = MmsProfile.getPrefDefault(getApplicationContext());
        ms.setName("?");
        return ms;
    }

    @Override
    protected void saveData(MmsProfile instance)
    {
        if (mode == DEFAULT_MMS_SETTINGS_MODE)
        {
            instance.setIsVirtual(false);
            MmsProfile.setPrefDefault(getApplicationContext(), instance);
            TextSecurePreferences.updateGlobalProfileIfChanged(getContext(), -1);
        }
        else
        {
            MmsProfilesDatabase db = DatabaseFactory.getMmsProfileDatabase(getApplicationContext());
            long newId = db.save(instance);
            TextSecurePreferences.updateGlobalProfileIfChanged(getContext(), newId);
        }
    }

    @Override
    protected void deleteData(int id)
    {
        MmsProfilesDatabase db = DatabaseFactory.getMmsProfileDatabase(getApplicationContext());
        db.delete(id);
    }

    @Override
    protected void fillControls(MmsProfile obj)
    {
        controls.fillControls(obj);

        if (this.mode == DEFAULT_MMS_SETTINGS_MODE)
        {
            controls.tbName.setEnabled(false);
        }
    }

    @Override
    protected MmsProfile updateModel(MmsProfile obj)
    {
        return controls.updateModelFromControls(obj);
    }


    @Override
    public boolean handleOnClosing()
    {
        boolean hasChanges = hasChanges();

        if (hasChanges)
        {
            // Promnt for close
            DialogHelper.createAndShowSaveAlertDialog(getActivity(), new DialogHelper.OnOkListener()
                    {
                        @Override
                        public void OnOk()
                        {
                            save(true);
                        }
                    },
                    new DialogHelper.OnCancelListener()
                    {
                        @Override
                        public void OnCancel()
                        {
                            closeFragment();
                        }
                    });

            return false;
        }
        else
        {
            return super.handleOnClosing();
        }

    }

    private boolean hasChanges()
    {
        if (ID < 0 && mode != DEFAULT_MMS_SETTINGS_MODE)
        {
            return true;
        }
        else
        {
            // get instance from GUI
            MmsProfile newMS = controls.updateModelFromControls(instance);
            // compare with old

            boolean equal = MmsProfile.isEqual(newMS, instance);

            if (equal)
                return false;
            else
                return true;
        }
    }


    private OnFragmentInteractionListener mListener;


    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

}
