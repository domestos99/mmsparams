package cz.uhk.bak.mmsparams.log;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import cz.uhk.bak.mmsparams.database.model.ErrorLog;

public class LogFB
{
    public static int i(String tag, String msg)
    {
        return Log.i(tag, msg);
    }

    public static int d(String tag, String msg)
    {
        return Log.d(tag, msg);
    }

    public static int d(String tag, String msg, Throwable tr)
    {
        return Log.d(tag, msg, tr);
    }

    public static int v(String tag, String msg)
    {
        return Log.v(tag, msg);
    }

    public static int w(String tag, String msg)
    {
        Crashlytics.log(2, tag, ErrorLog.convert(null, msg).getInfo());

        return Log.w(tag, msg);
    }

    public static int w(String tag, Throwable tr)
    {
        Crashlytics.log(2, tag, ErrorLog.convert(tr, null).getInfo());

        return Log.w(tag, tr);
    }

    public static int w(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(2, tag, ErrorLog.convert(tr, msg).getInfo());

        return Log.w(tag, msg, tr);
    }

    public static int e(String tag, String msg)
    {
        Crashlytics.log(1, tag, ErrorLog.convert(null, msg).getInfo());

        return Log.e(tag, msg);
    }

    public static int e(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(1, tag, ErrorLog.convert(tr, msg).getInfo());

        return Log.e(tag, msg, tr);
    }


}
