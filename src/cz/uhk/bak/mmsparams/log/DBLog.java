package cz.uhk.bak.mmsparams.log;

import android.content.Context;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.ErrorLogDatabase;
import cz.uhk.bak.mmsparams.database.model.ErrorLog;

public class DBLog
{
    public static void logToDB(Context context, Throwable e)
    {
        logToDB(context, "DBLog", e);
    }

    public static void logToDB(Context context, String tag, Throwable e)
    {
        logToDB(context, "", tag, e);
    }


    public static void logToDB(Context context, String massage, String tag, Throwable e)
    {
        try
        {
            LogFB.e(tag, "logToDB", e);

            ErrorLogDatabase db = DatabaseFactory.getErrorLogDatabase(context);
            db.insert(ErrorLog.convert(e, massage));

        }
        catch (Exception ex)
        {
            LogFB.e(tag, "logToDB", e);
            LogFB.e(tag, "logToDB", ex);
        }
    }

}
