package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.MySendConf;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduSendConfFragment extends MessagePduBaseFragment
{
    private int paramID;
    private MySendConf sendConf;
    private MessagePdu messagePdu;

    TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMmsMessageID, tvMmsResponseStatus, tvMmsTransactionID, tvPduCreated,
            tvMmsResponseText;
    public static final String TAG = MessagePduSendConfFragment.class.getSimpleName();


    public static MessagePduSendConfFragment newInstance(int paramID)
    {
        MessagePduSendConfFragment fragment = new MessagePduSendConfFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void beforeBaseInit()
    {
        if (getArguments() != null)
        {
            paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
            messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
            sendConf = (MySendConf) messagePdu.getGenericPdu();
        }
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_send_conf_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);

        tvMmsMessageID = ViewUtil.findById(view, R.id.tvMmsMessageID);
        tvMmsResponseStatus = ViewUtil.findById(view, R.id.tvMmsResponseStatus);
        tvMmsTransactionID = ViewUtil.findById(view, R.id.tvMmsTransactionID);

        tvMmsResponseText = ViewUtil.findById(view, R.id.tvMmsResponseText);


        fillControls();
    }

    @Override
    protected void setupListeners()
    {

    }

    private void fillControls()
    {
        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getNumberString(sendConf.getFrom()));


            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(sendConf.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(sendConf.getMmsVersion()));
            tvMmsMessageID.setText(getString(sendConf.getMessageId()));
            tvMmsResponseStatus.setText(PduHeaderConvertor.getResponseStatusString(sendConf.getResponseStatus()));
            tvMmsTransactionID.setText(getString(sendConf.getTransactionId()));


            tvMmsResponseText.setText(getString(sendConf.getResponseText()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }


}
