package cz.uhk.bak.mmsparams.preferences;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;

import java.util.Arrays;

import cz.uhk.bak.mmsparams.ApplicationPreferencesActivity;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class AppearancePreferenceFragment extends ListSummaryPreferenceFragment
{

    @Override
    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.preferences_appearance);

        this.findPreference(TextSecurePreferences.THEME_PREF).setOnPreferenceChangeListener(new ListSummaryListener());
        this.findPreference(TextSecurePreferences.LANGUAGE_PREF).setOnPreferenceChangeListener(new ListSummaryListener());
        initializeListSummary((ListPreference) findPreference(TextSecurePreferences.THEME_PREF));
        initializeListSummary((ListPreference) findPreference(TextSecurePreferences.LANGUAGE_PREF));
    }

    @Override
    public void onStart()
    {
        super.onStart();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener((ApplicationPreferencesActivity) getActivity());
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((ApplicationPreferencesActivity) getActivity()).getSupportActionBar().setTitle(R.string.preferences__appearance);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener((ApplicationPreferencesActivity) getActivity());
    }

    public static CharSequence getSummary(Context context)
    {
        String[] languageEntries = context.getResources().getStringArray(R.array.language_entries);
        String[] languageEntryValues = context.getResources().getStringArray(R.array.language_values);
        String[] themeEntries = context.getResources().getStringArray(R.array.pref_theme_entries);
        String[] themeEntryValues = context.getResources().getStringArray(R.array.pref_theme_values);

        int langIndex = Arrays.asList(languageEntryValues).indexOf(TextSecurePreferences.getLanguage(context));
        int themeIndex = Arrays.asList(themeEntryValues).indexOf(TextSecurePreferences.getTheme(context));

        if (langIndex == -1) langIndex = 0;
        if (themeIndex == -1) themeIndex = 0;

        return context.getString(R.string.ApplicationPreferencesActivity_appearance_summary,
                themeEntries[themeIndex],
                languageEntries[langIndex]);
    }
}
