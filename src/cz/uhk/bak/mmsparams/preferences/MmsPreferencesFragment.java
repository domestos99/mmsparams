/**
 * Copyright (C) 2014 Open Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.preferences;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import cz.uhk.bak.mmsparams.PassphraseRequiredActionBarActivity;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.components.CustomDefaultPreference;
import cz.uhk.bak.mmsparams.mms.ApnExt;
import cz.uhk.bak.mmsparams.mms.ApnUtils;
import cz.uhk.bak.mmsparams.mms.LegacyMmsConnection;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;


public class MmsPreferencesFragment extends PreferenceFragment
{

    private static final String TAG = MmsPreferencesFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.preferences_manual_mms);

        ((PassphraseRequiredActionBarActivity) getActivity()).getSupportActionBar()
                .setTitle(R.string.preferences__advanced_mms_access_point_names);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub

        LinearLayout v = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);

        Button btn = new Button(getActivity().getApplicationContext());

        btn.setText("Load System APN");

        v.addView(btn);

        btn.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {


                ApnUtils.initDefaultApns(getContext(), new ApnUtils.OnApnFinishedListener()
                {
                    @Override
                    public void onFinished()
                    {

                    }
                });


            }
        });


        return v;
    }


    @Override
    public void onResume()
    {
        super.onResume();
        new LoadApnDefaultsTask().execute();
    }

    private class LoadApnDefaultsTask extends AsyncTask<Void, Void, ApnExt>
    {

        @Override
        protected ApnExt doInBackground(Void... params)
        {
            Context context = getActivity();

            if (context != null)
            {
//                    return ApnDatabase.getInstance(context)
//                            .getDefaultApnParameters(TelephonyUtil.getMccMnc(context),
//                                    TelephonyUtil.getApn(context));

//                return ApnUtils.loadApnsGetDefault(context);

                ApnExt defaultApn = ApnUtils.loadApnsGetDefault(context);
                return defaultApn;

            }

            return null;
        }

        @Override
        protected void onPostExecute(ApnExt apnDefaults)
        {
            ((CustomDefaultPreference) findPreference(TextSecurePreferences.MMSC_HOST_PREF))
                    .setValidator(new CustomDefaultPreference.UriValidator())
                    .setDefaultValue(apnDefaults.getMmsc());

            ((CustomDefaultPreference) findPreference(TextSecurePreferences.MMSC_PROXY_HOST_PREF))
                    .setValidator(new CustomDefaultPreference.HostnameValidator())
                    .setDefaultValue(apnDefaults.getMmsproxy());

            ((CustomDefaultPreference) findPreference(TextSecurePreferences.MMSC_PROXY_PORT_PREF))
                    .setValidator(new CustomDefaultPreference.PortValidator())
                    .setDefaultValue(apnDefaults.getMmsport());

            ((CustomDefaultPreference) findPreference(TextSecurePreferences.MMSC_USERNAME_PREF))
                    .setDefaultValue(apnDefaults.getUserName());

            ((CustomDefaultPreference) findPreference(TextSecurePreferences.MMSC_PASSWORD_PREF))
                    .setDefaultValue(apnDefaults.getPassword());

            ((CustomDefaultPreference) findPreference(TextSecurePreferences.MMS_USER_AGENT))
                    .setDefaultValue(LegacyMmsConnection.USER_AGENT);
        }
    }

}
