package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.MyNotificationInd;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduNotificationIndFragment extends MessagePduBaseFragment
{
    private int paramID;
    private MyNotificationInd notifInd;
    private MessagePdu messagePdu;

    TextView tvPduCreated, tvMessageFrom, tvMessageType, tvMessageContentClass,
            tvMessageExpiry, tvMessageClass, tvMessageSizePdu, tvMessageSubject, tvMessageTransactionId, tvMessageDeliveryReport, tvMmsVersion, tvMessageContentLocation, tvMessageSizeCalc, tvMessageDrmContent;
    public static final String TAG = MessagePduNotificationIndFragment.class.getSimpleName();


    public MessagePduNotificationIndFragment()
    {
    }


    public static MessagePduNotificationIndFragment newInstance(int paramID)
    {
        MessagePduNotificationIndFragment fragment = new MessagePduNotificationIndFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }


    @Override
    protected void beforeBaseInit()
    {
        if (getArguments() != null)
        {
            paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
            messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
            notifInd = (MyNotificationInd) messagePdu.getGenericPdu();
        }
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_notification_ind_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);
        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMessageContentClass = ViewUtil.findById(view, R.id.tvMessageContentClass);
        tvMessageExpiry = ViewUtil.findById(view, R.id.tvMessageExpiry);
        tvMessageClass = ViewUtil.findById(view, R.id.tvMessageClass);
        tvMessageSizePdu = ViewUtil.findById(view, R.id.tvMessageSizePdu);
        tvMessageSubject = ViewUtil.findById(view, R.id.tvMessageSubject);
        tvMessageTransactionId = ViewUtil.findById(view, R.id.tvMessageTransactionId);
        tvMessageDeliveryReport = ViewUtil.findById(view, R.id.tvMessageDeliveryReport);
        tvMessageContentLocation = ViewUtil.findById(view, R.id.tvMessageContentLocation);
        tvMessageDrmContent = ViewUtil.findById(view, R.id.tvMessageDrmContent);


        fillControls();
    }

    private void fillControls()
    {
        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getNumberString(notifInd.getFrom()));

            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(notifInd.getMmsVersion()));

            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(notifInd.getMessageType()));
            tvMessageContentClass.setText(String.valueOf(notifInd.getContentClass()));
            tvMessageExpiry.setText(PduHeaderConvertor.getDateString(notifInd.getExpiry()));
            tvMessageClass.setText(PduHeaderConvertor.getMessageClassString(notifInd.getMessageClass()));
            tvMessageContentLocation.setText(super.getString(notifInd.getContentLocation()));
            tvMessageSizePdu.setText(getSize(notifInd.getMessageSize()));
            tvMessageSubject.setText(getString(notifInd.getSubject()));
            tvMessageTransactionId.setText(getString(notifInd.getTransactionId()));
            tvMessageDeliveryReport.setText(PduHeaderConvertor.getValueString(notifInd.getDeliveryReport()));
            tvMessageDrmContent.setText(PduHeaderConvertor.getValueString(notifInd.getDrmContent()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }


    @Override
    protected void setupListeners()
    {

    }


}
