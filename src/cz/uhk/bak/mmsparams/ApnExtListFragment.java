package cz.uhk.bak.mmsparams;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import cz.uhk.bak.mmsparams.adapters.ApnExtListAdapter;
import cz.uhk.bak.mmsparams.database.ApnExtDatabase;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.mms.ApnExt;
import cz.uhk.bak.mmsparams.mms.ApnSingleton;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class ApnExtListFragment extends BrowseBaseFragment<ApnExt>
{
    public static final String TAG = ApnExtListFragment.class.getSimpleName();
    ListView listView;
    ApnExtListAdapter adapter;
    private android.support.design.widget.FloatingActionButton fab;
    private android.support.design.widget.FloatingActionButton fabPickAPN;

    @Override
    protected void afterBaseInit()
    {
        adapter = new ApnExtListAdapter(getApplicationContext(), data);
        listView.setAdapter(adapter);


        super.afterBaseInit();
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected ArrayAdapter getAdapter()
    {
        return adapter;
    }

    @Override
    protected ListView getListView()
    {
        return listView;
    }

    @Override
    protected List<ApnExt> loadData()
    {
        ApnExtDatabase db = DatabaseFactory.getApnExtDatabase(getApplicationContext());
        List<ApnExt> data = db.getAll();


        List<ApnExt> systemApn = ApnSingleton.getInstance(getActivity()).getSystemApns(getActivity());

        for (int i = systemApn.size() - 1; i >= 0; i--)
        {
            ApnExt a = systemApn.get(i);
            if (a == null)
                continue;

            data.add(0, a);
        }


        return data;
    }

    @Override
    protected void setSensitive(int mode)
    {
        super.setSensitive(mode);

        if (mode != -1)
        {
            fab.setVisibility(View.INVISIBLE);
            fabPickAPN.setVisibility(View.INVISIBLE);
        }

    }


    @Override
    protected int GetLayoutResource()
    {
        return R.layout.apn_ext_browse_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        listView = ViewUtil.findById(view, R.id.list);
        fab = ViewUtil.findById(view, R.id.fab);
        fabPickAPN = ViewUtil.findById(view, R.id.fabPickAPN);
    }


    @Override
    protected void setupListeners()
    {
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                createNew(ApnExtDetailFragment.class);
            }
        });

        fabPickAPN.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                pickAPN();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ApnExt obj = (ApnExt) parent.getItemAtPosition(position);

                if (allowOpenDetail())
                {
                    // This Activity was called by startActivity
                    openDetail(ApnExtDetailFragment.class, obj.getID());
                }
                else
                {
                    //This Activity was called by startActivityForResult
                    returnResult(obj);
                }

            }
        });

    }

    private void pickAPN()
    {
        Intent intentApnBrowse = new Intent(getActivity(), ApnExtListActivity.class);
        intentApnBrowse.putExtra("mode", 1);
        getActivity().startActivityForResult(intentApnBrowse, ApnExtListActivity.PICK_APN_RES_CODE);
    }

    protected void createNew(Class<? extends DetailFragment> cls)
    {
        openDetail(cls, -1);
    }


    protected void openDetail(Class<? extends DetailFragment> cls, int id)
    {
        try
        {
            DetailFragment bf = cls.newInstance();

            Bundle arg = new Bundle();
            arg.putInt("id", id);

            openFragment(bf, arg);
        }
        catch (java.lang.InstantiationException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }


}
