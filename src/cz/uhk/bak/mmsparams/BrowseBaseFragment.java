package cz.uhk.bak.mmsparams;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.bak.mmsparams.log.LogFB;

public abstract class BrowseBaseFragment<T> extends BaseFragment
{

    protected abstract ArrayAdapter getAdapter();

    protected abstract ListView getListView();

    protected final List<T> data;

    private int mode = -1;

    public BrowseBaseFragment()
    {
        super();

        data = new ArrayList<>();
    }

    @Override
    protected void afterBaseInit()
    {
        loadMode();
        refreshData();
        if (mode != -1)
            setSensitive(mode);
    }

    protected void loadMode()
    {
        Bundle extras = this.getArguments();
        if (extras != null)
        {
            mode = extras.getInt("mode", -1);
        }
        else
            mode = -1;
    }

    protected int getMode()
    {
        return mode;
    }

    protected void setSensitive(int mode)
    {
    }

    protected abstract List<T> loadData();


    protected void refreshData()
    {
        try
        {
            List<T> d = loadData();

            data.clear();
            data.addAll(d);

            updateAdapter();
        }
        catch (Exception e)
        {
            LogFB.e("ListFragment", "refreshData", e);
            throw e;
        }
    }

    protected boolean allowOpenDetail()
    {
        return (getMode() == -1);
    }


    protected void updateAdapter()
    {
        ArrayAdapter adapter = getAdapter();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    protected void returnResult(T result)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", (Serializable) result);
        getActivity().setResult(Activity.RESULT_OK, returnIntent);

        getActivity().finish();
    }


    protected void createNew(Class<? extends DetailFragment> cls)
    {
        openDetail(cls, -1);
    }

    protected void openDetail(Class<? extends DetailFragment> cls, int id)
    {
        openDetail(cls, id, DetailFragment.DEFAULT_MODE);
    }

    protected void openDetail(Class<? extends DetailFragment> cls, int id, int mode)
    {
        if (!beforeOpenDetail())
            return;

        try
        {
            DetailFragment bf = cls.newInstance();

            Bundle arg = new Bundle();
            arg.putInt("id", id);
            arg.putInt("mode", mode);

            openFragment(bf, arg);
        }
        catch (java.lang.InstantiationException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    protected boolean beforeOpenDetail()
    {
        return true;
    }


}
