package cz.uhk.bak.mmsparams;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import cz.uhk.bak.mmsparams.adapters.MmsTemplateListAdapter;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsTemplateDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.permissions.PermissionUtil;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsTemplateListFragment extends BrowseBaseFragment<MmsTemplate>
{
    public static final String TAG = MmsTemplateListFragment.class.getSimpleName();

    ListView listView;
    private android.support.design.widget.FloatingActionButton fab;
    MmsTemplateListAdapter adapter;

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_template_browse_fragment;
    }

    @Override
    protected void afterBaseInit()
    {
        adapter = new MmsTemplateListAdapter(getApplicationContext(), data);
        listView.setAdapter(adapter);

        super.afterBaseInit();
    }

    public static MmsTemplateListFragment newInstance()
    {
        MmsTemplateListFragment mt = new MmsTemplateListFragment();
        return mt;
    }


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected ArrayAdapter getAdapter()
    {
        return adapter;
    }

    @Override
    protected ListView getListView()
    {
        return listView;
    }

    @Override
    protected List<MmsTemplate> loadData()
    {
        MmsTemplateDatabase db = DatabaseFactory.getMmsTemplateDatabase(getApplicationContext());
        return db.getAll();
    }

    @Override
    public void onResume()
    {
        getActivity().setTitle(R.string.text_menu_mms_template_browse);
        super.onResume();
    }

    @Override
    protected void initControls(View view)
    {
        listView = ViewUtil.findById(view, R.id.list);
        fab = ViewUtil.findById(view, R.id.fab);

        setHasOptionsMenu(true);
    }

    @Override
    protected void setupListeners()
    {
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                createNew(MmsTemplateDetailFragment.class);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                MmsTemplate obj = (MmsTemplate) parent.getItemAtPosition(position);

                if (allowOpenDetail())
                {
                    // This Activity was called by startActivity
                    openDetail(MmsTemplateDetailFragment.class, obj.getID());
                }
                else
                {
                    //This Activity was called by startActivityForResult
                    returnResult(obj);
                }

            }
        });
    }

    @Override
    protected void setSensitive(int mode)
    {
        super.setSensitive(mode);

        if (mode != -1)
            fab.setVisibility(View.INVISIBLE);
    }

    @Override
    protected boolean beforeOpenDetail()
    {
        if (!PermissionUtil.checkBasic(getActivity()))
        {
            return false;
        }

        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {

        }
        return false;
    }


    @Override
    public boolean handleOnClosing()
    {
        BaseFragment bf = super.getActiveFragment();

        if (bf == null)
            return super.handleOnClosing();
        else
        {
            return bf.handleOnClosing();
        }
    }

}
