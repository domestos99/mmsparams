package cz.uhk.bak.mmsparams;

public interface MasterSecretListener
{
    void onMasterSecretCleared();
}
