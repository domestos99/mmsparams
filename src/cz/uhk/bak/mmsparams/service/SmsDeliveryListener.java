package cz.uhk.bak.mmsparams.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;

import org.whispersystems.jobqueue.JobManager;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.jobs.SmsSentJob;
import cz.uhk.bak.mmsparams.log.LogFB;

public class SmsDeliveryListener extends BroadcastReceiver
{

    private static final String TAG = SmsDeliveryListener.class.getSimpleName();

    public static final String SENT_SMS_ACTION = "cz.uhk.bak.mmsparams.SendReceiveService.SENT_SMS_ACTION";
    public static final String DELIVERED_SMS_ACTION = "cz.uhk.bak.mmsparams.SendReceiveService.DELIVERED_SMS_ACTION";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        JobManager jobManager = ApplicationContext.getInstance(context).getJobManager();
        long messageId = intent.getLongExtra("message_id", -1);

        switch (intent.getAction())
        {
            case SENT_SMS_ACTION:
                int result = getResultCode();

                jobManager.add(new SmsSentJob(context, messageId, SENT_SMS_ACTION, result));
                break;
            case DELIVERED_SMS_ACTION:
                byte[] pdu = intent.getByteArrayExtra("pdu");

                if (pdu == null)
                {
                    LogFB.w(TAG, "No PDU in delivery receipt!");
                    break;
                }

                SmsMessage message = SmsMessage.createFromPdu(pdu);

                if (message == null)
                {
                    LogFB.w(TAG, "Delivery receipt failed to parse!");
                    break;
                }

                jobManager.add(new SmsSentJob(context, messageId, DELIVERED_SMS_ACTION, message.getStatus()));
                break;
            default:
                LogFB.w(TAG, "Unknown action: " + intent.getAction());
        }
    }
}
