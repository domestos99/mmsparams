package cz.uhk.bak.mmsparams.service;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;

import org.whispersystems.libsignal.util.guava.Optional;

import java.net.URISyntaxException;
import java.net.URLDecoder;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.RecipientPreferenceDatabase.RecipientsPreferences;
import cz.uhk.bak.mmsparams.database.ThreadDatabase;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.mms.SlideDeck;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.sms.MessageSender;
import cz.uhk.bak.mmsparams.sms.OutgoingTextMessage;
import cz.uhk.bak.mmsparams.util.Rfc5724Uri;

public class QuickResponseService extends MasterSecretIntentService
{

    private static final String TAG = QuickResponseService.class.getSimpleName();

    public QuickResponseService()
    {
        super("QuickResponseService");
    }

    @Override
    protected void onHandleIntent(Intent intent, @Nullable MasterSecret masterSecret)
    {
        if (!TelephonyManager.ACTION_RESPOND_VIA_MESSAGE.equals(intent.getAction()))
        {
            LogFB.w(TAG, "Received unknown intent: " + intent.getAction());
            return;
        }

        if (masterSecret == null)
        {
            LogFB.w(TAG, "Got quick response request when locked...");
            Toast.makeText(this, R.string.QuickResponseService_quick_response_unavailable_when_Signal_is_locked, Toast.LENGTH_LONG).show();
            return;
        }

        try
        {
            Rfc5724Uri uri = new Rfc5724Uri(intent.getDataString());
            String content = intent.getStringExtra(Intent.EXTRA_TEXT);
            String numbers = uri.getPath();
            if (numbers.contains("%"))
            {
                numbers = URLDecoder.decode(numbers);
            }

            Recipients recipients = RecipientFactory.getRecipientsFromString(this, numbers, false);
            Optional<RecipientsPreferences> preferences = DatabaseFactory.getRecipientPreferenceDatabase(this).getRecipientsPreferences(recipients.getIds());
            int subscriptionId = preferences.isPresent() ? preferences.get().getDefaultSubscriptionId().or(-1) : -1;
            long expiresIn = preferences.isPresent() ? preferences.get().getExpireMessages() * 1000 : 0;

            if (!TextUtils.isEmpty(content))
            {
                if (recipients.isSingleRecipient())
                {
                    MessageSender.send(this, masterSecret, new OutgoingTextMessage(recipients, content, expiresIn, subscriptionId), -1, false, null);
                }
                else
                {
                    MessageSender.send(this, masterSecret, new OutgoingMediaMessage(recipients, new SlideDeck(), content, System.currentTimeMillis(),
                            subscriptionId, expiresIn, ThreadDatabase.DistributionTypes.DEFAULT), -1, false, null);
                }
            }
        }
        catch (URISyntaxException e)
        {
            Toast.makeText(this, R.string.QuickResponseService_problem_sending_message, Toast.LENGTH_LONG).show();
            LogFB.w(TAG, e);
        }
    }
}
