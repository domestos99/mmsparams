package cz.uhk.bak.mmsparams.service;


import android.content.Context;
import android.content.Intent;

import java.util.concurrent.TimeUnit;

import cz.uhk.bak.mmsparams.ApplicationContext;
import cz.uhk.bak.mmsparams.BuildConfig;
import cz.uhk.bak.mmsparams.jobs.UpdateApkJob;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class UpdateApkRefreshListener extends PersistentAlarmManagerListener
{

    private static final String TAG = UpdateApkRefreshListener.class.getSimpleName();

    private static final long INTERVAL = TimeUnit.HOURS.toMillis(6);

    @Override
    protected long getNextScheduledExecutionTime(Context context)
    {
        return TextSecurePreferences.getUpdateApkRefreshTime(context);
    }

    @Override
    protected long onAlarm(Context context, long scheduledTime)
    {
        LogFB.w(TAG, "onAlarm...");

        if (scheduledTime != 0 && BuildConfig.PLAY_STORE_DISABLED)
        {
            LogFB.w(TAG, "Queueing APK update job...");
            ApplicationContext.getInstance(context)
                    .getJobManager()
                    .add(new UpdateApkJob(context));
        }

        long newTime = System.currentTimeMillis() + INTERVAL;
        TextSecurePreferences.setUpdateApkRefreshTime(context, newTime);

        return newTime;
    }

    public static void schedule(Context context)
    {
        new UpdateApkRefreshListener().onReceive(context, new Intent());
    }

}
