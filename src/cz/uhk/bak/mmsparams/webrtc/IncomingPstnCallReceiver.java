package cz.uhk.bak.mmsparams.webrtc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.telephony.TelephonyManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.service.WebRtcCallService;

/**
 * Listens for incoming PSTN calls and rejects them if a RedPhone call is already in progress.
 * <p>
 * Unstable use of reflection employed to gain access to ITelephony.
 */
public class IncomingPstnCallReceiver extends BroadcastReceiver
{

    private static final String TAG = IncomingPstnCallReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent)
    {
        LogFB.w(TAG, "Checking incoming call...");

        if (intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER) == null)
        {
            LogFB.w(TAG, "Telephony event does not contain number...");
            return;
        }

        if (!intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING))
        {
            LogFB.w(TAG, "Telephony event is not state rining...");
            return;
        }

        InCallListener listener = new InCallListener(context, new Handler());

        WebRtcCallService.isCallActive(context, listener);
    }

    private static class InCallListener extends ResultReceiver
    {

        private final Context context;

        InCallListener(Context context, Handler handler)
        {
            super(handler);
            this.context = context.getApplicationContext();
        }

        protected void onReceiveResult(int resultCode, Bundle resultData)
        {
            if (resultCode == 1)
            {
                LogFB.w(TAG, "Attempting to deny incoming PSTN call.");

                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

                try
                {
                    Method getTelephony = tm.getClass().getDeclaredMethod("getITelephony");
                    getTelephony.setAccessible(true);
                    Object telephonyService = getTelephony.invoke(tm);
                    Method endCall = telephonyService.getClass().getDeclaredMethod("endCall");
                    endCall.invoke(telephonyService);
                    LogFB.w(TAG, "Denied Incoming Call.");
                }
                catch (NoSuchMethodException e)
                {
                    LogFB.w(TAG, "Unable to access ITelephony API", e);
                }
                catch (IllegalAccessException e)
                {
                    LogFB.w(TAG, "Unable to access ITelephony API", e);
                }
                catch (InvocationTargetException e)
                {
                    LogFB.w(TAG, "Unable to access ITelephony API", e);
                }
            }
        }
    }
}
