package cz.uhk.bak.mmsparams.contacts;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import java.io.IOException;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.service.KeyCachingService;
import cz.uhk.bak.mmsparams.util.DirectoryHelper;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class ContactsSyncAdapter extends AbstractThreadedSyncAdapter
{

    private static final String TAG = ContactsSyncAdapter.class.getSimpleName();

    public ContactsSyncAdapter(Context context, boolean autoInitialize)
    {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult)
    {
        LogFB.w(TAG, "onPerformSync(" + authority + ")");

        if (TextSecurePreferences.isPushRegistered(getContext()))
        {
            try
            {
                DirectoryHelper.refreshDirectory(getContext(), KeyCachingService.getMasterSecret(getContext()));
            }
            catch (IOException e)
            {
                LogFB.w(TAG, e);
            }
        }
    }

}
