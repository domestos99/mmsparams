package cz.uhk.bak.mmsparams.transport;

public class InsecureFallbackApprovalException extends Exception
{
    public InsecureFallbackApprovalException(String detailMessage)
    {
        super(detailMessage);
    }

    public InsecureFallbackApprovalException(Throwable e)
    {
        super(e);
    }
}
