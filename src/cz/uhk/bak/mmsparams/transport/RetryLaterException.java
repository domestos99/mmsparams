package cz.uhk.bak.mmsparams.transport;

public class RetryLaterException extends Exception
{
    public RetryLaterException(Exception e)
    {
        super(e);
    }
}
