package cz.uhk.bak.mmsparams;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.ApnExt;

public class ApnExtListActivity extends MyListPassphraseRequiredActionBarActivity implements OnFragmentInteractionListener
{

    public static String TAG = ApnExtListActivity.class.getSimpleName();

    public static final int PICK_APN_RES_CODE = 147;

    @NonNull
    @Override
    protected BaseFragment getListFragment()
    {
        return new ApnExtListFragment();
    }


    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data)
    {
        LogFB.w(TAG, "onActivityResult called: " + requestCode + ", " + resultCode + " , " + data);
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null || resultCode != RESULT_OK)
        {
            return;
        }

        switch (requestCode)
        {
            case PICK_APN_RES_CODE:
                setApn(data);
                break;
        }
    }

    private void setApn(@Nullable Intent data)
    {
        try
        {
            Object o = data.getExtras().get("result");
            if (o == null || !(o instanceof ApnExt))
                return;

            ApnExt newApn = (ApnExt) o;

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
