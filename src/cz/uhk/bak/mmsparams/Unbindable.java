package cz.uhk.bak.mmsparams;

public interface Unbindable
{
    public void unbind();
}
