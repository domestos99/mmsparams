package cz.uhk.bak.mmsparams;

import android.app.Activity;
import android.content.Intent;

import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;

public class MmsTemplatePickContactActivity extends NewConversationActivity
{

    private static final String TAG = MmsTemplatePickContactActivity.class.getSimpleName();


    @Override
    public void onContactSelected(String number)
    {
        Recipients recipients = RecipientFactory.getRecipientsFromString(this, number, true);


        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", number);
        this.setResult(Activity.RESULT_OK, returnIntent);

        this.finish();

    }
}
