package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;

import cz.uhk.bak.mmsparams.log.DBLog;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.AndroidVersionUtil;

public abstract class BaseActivity extends FragmentActivity
{
    private static final String TAG = BaseActivity.class.getSimpleName();

    public static boolean HANGLE_ERRORS = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        return (keyCode == KeyEvent.KEYCODE_MENU && isMenuWorkaroundRequired()) || super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_MENU && isMenuWorkaroundRequired())
        {
            openOptionsMenu();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public static boolean isMenuWorkaroundRequired()
    {
        return AndroidVersionUtil.isLowerThanKitKat() &&
                VERSION.SDK_INT > VERSION_CODES.GINGERBREAD_MR1 &&
                ("LGE".equalsIgnoreCase(Build.MANUFACTURER) || "E6710".equalsIgnoreCase(Build.DEVICE));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (HANGLE_ERRORS)
        {
            handleErrors();
        }
    }

    private void handleErrors()
    {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread thread, Throwable e)
            {
                handleUncaughtException(thread, e);
            }
        });
    }

    public void handleUncaughtException(Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        LogFB.w("BaseActivity", "After print stack - before open form");

        try
        {
            DBLog.logToDB(getApplicationContext(), "BaseActivity", e);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

//        MyLogger.e("ActivityBase", "error", e);

        openErrorForm(this, e);

        System.exit(1); // kill off the crashed app
    }


    public static void openErrorForm(Context context, Throwable t)
    {
        Intent intent = new Intent(context, ShowErrorActivity.class);
        intent.putExtra("error", t);
        context.startActivity(intent);
    }
}






























