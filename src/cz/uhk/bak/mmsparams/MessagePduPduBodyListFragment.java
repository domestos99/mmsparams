package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.mms.pdu_alt.MultimediaMessagePdu;
import com.google.android.mms.pdu_alt.PduBody;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduPduBodyListFragment extends BaseFragment
{
    private OnFragmentInteractionListener mListener;
    private LinearLayout llContainer;

    private MessagePdu messagePdu;
    private int paramID;
    private int pduBodyPartCount;
    public static final String TAG = MessagePduPduBodyListFragment.class.getSimpleName();

    public static MessagePduPduBodyListFragment newInstance(int paramID)
    {
        MessagePduPduBodyListFragment fragment = new MessagePduPduBodyListFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        if (getArguments() != null)
        {
            paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
            messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
            final PduBody pduBody = ((MultimediaMessagePdu) messagePdu.getGenericPdu()).getBody();

            pduBodyPartCount = pduBody.getPartsNum();
        }
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_detail_list_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        llContainer = ViewUtil.findById(view, R.id.llContainer);

        int fragCount = 0;

        for (int i = 0; i < pduBodyPartCount; i++)
        {
            MessagePduPduPartFragment fragment = MessagePduPduPartFragment.newInstance(paramID, i);

            getFragmentManager().beginTransaction().add(llContainer.getId(), fragment, "fragment" + fragCount).commit();

            fragCount++;
        }
    }

    @Override
    protected void setupListeners()
    {

    }


    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

}
