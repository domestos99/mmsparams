package cz.uhk.bak.mmsparams;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;

public class MmsProfilesListActivity extends MyListPassphraseRequiredActionBarActivity implements OnFragmentInteractionListener
{

    BaseFragment fragment;


    @Override
    protected BaseFragment getListFragment()
    {
        fragment = new MmsProfilesListFragment();
        return fragment;
    }


    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected boolean beforeActivityFinish()
    {
        boolean close = fragment.handleOnClosing();

        if (close)
        {
            onBackPressed();
        }
        return true;

    }


    public static void pickSettings(Activity activity, int requestCode)
    {
        Intent intentApnBrowse = new Intent(activity, MmsProfilesListActivity.class);
        intentApnBrowse.putExtra("mode", 1);
        activity.startActivityForResult(intentApnBrowse, requestCode);
    }
}
