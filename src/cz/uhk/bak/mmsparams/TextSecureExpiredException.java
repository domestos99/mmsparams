package cz.uhk.bak.mmsparams;

public class TextSecureExpiredException extends Exception
{
    public TextSecureExpiredException(String message)
    {
        super(message);
    }
}
