package cz.uhk.bak.mmsparams.adapters;

import java.util.List;

public interface IAdapterDataUpdated<T>
{
    void Update(List<T> list);
}
