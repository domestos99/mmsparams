package cz.uhk.bak.mmsparams.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsTemplateListAdapter extends MyAdapterBase<MmsTemplate>
{
    private static final int row_layout_ID = R.layout.mms_template_browse_item;

    public MmsTemplateListAdapter(Context context, List<MmsTemplate> objects)
    {
        super(context, row_layout_ID, objects);
    }

    @Override
    protected int getResource()
    {
        return row_layout_ID;
    }

    @Override
    protected void setupView(View view, MmsTemplate obj, int position)
    {
        TextView tv = (TextView) view.findViewById(R.id.name);
        tv.setText(obj.getName());

        TextView info = ViewUtil.findById(view, R.id.info);
        info.setText(obj.getInfo());
    }
}
