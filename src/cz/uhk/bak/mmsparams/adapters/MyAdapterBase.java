package cz.uhk.bak.mmsparams.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class MyAdapterBase<T> extends ArrayAdapter<T>
{

    IAdapterDataUpdated<T> adapterDataUpdated;

    public void setOnAdapterDataUpdated(IAdapterDataUpdated<T> adapterDataUpdated)
    {
        this.adapterDataUpdated = adapterDataUpdated;
    }

    protected void InvokeAdapterDataUpdated()
    {
        if (adapterDataUpdated != null)
        {
            List<T> l = new ArrayList<>();

            for (int i = 0; i < this.getCount(); i++)
            {
                l.add(this.getItem(i));
            }

            adapterDataUpdated.Update(l);
        }
    }

    public MyAdapterBase(Context context, int resource, List<T> objects)
    {
        super(context, resource, objects);
    }

    protected abstract int getResource();

    protected View getCustomView(ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        return inflater.inflate(getResource(), parent, false);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        if (convertView == null)
        {
            convertView = getCustomView(parent);
        }

        setupView(convertView, getItem(position), position);
        return convertView;
    }

    protected abstract void setupView(View view, T obj, int position);
}
