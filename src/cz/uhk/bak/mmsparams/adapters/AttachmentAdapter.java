package cz.uhk.bak.mmsparams.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.List;

import cz.uhk.bak.mmsparams.MediaPreviewActivity;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.components.AudioView;
import cz.uhk.bak.mmsparams.components.DocumentView;
import cz.uhk.bak.mmsparams.components.MmsProfilesView;
import cz.uhk.bak.mmsparams.components.RemovableEditableMediaView;
import cz.uhk.bak.mmsparams.components.ThumbnailView;
import cz.uhk.bak.mmsparams.components.location.SignalMapView;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.mms.AudioSlide;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.mms.slides.DocumentSlide;
import cz.uhk.bak.mmsparams.scribbles.ScribbleActivity;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class AttachmentAdapter extends MyAdapterBase<Slide>
{
    private static final int row_layout_ID = R.layout.conversation_activity_attachment_editor_stub;
    private final MasterSecret masterSecret;
    final List<Slide> objects;

    public AttachmentAdapter(Context context, List<Slide> objects, MasterSecret masterSecret)
    {
        super(context, row_layout_ID, objects);
        this.masterSecret = masterSecret;
        this.objects = objects;
    }

    @Override
    protected int getResource()
    {
        return row_layout_ID;
    }

    @Override
    protected void setupView(View view, Slide obj, int position)
    {

        RemovableEditableMediaView removableMediaView;
        ThumbnailView thumbnail;
        AudioView audioView;
        DocumentView documentView;
        SignalMapView mapView;
        MmsProfilesView mmsProfilesView;
        MmsProfile mmsProfile;


        thumbnail = ViewUtil.findById(view, R.id.attachment_thumbnail);
        audioView = ViewUtil.findById(view, R.id.attachment_audio);
        documentView = ViewUtil.findById(view, R.id.attachment_document);
        mapView = ViewUtil.findById(view, R.id.attachment_location);
        removableMediaView = ViewUtil.findById(view, R.id.removable_media_view);
        mmsProfilesView = ViewUtil.findById(view, R.id.attachment_mms_settings);

        removableMediaView.setRemoveClickListener(new RemoveButtonListener(this, obj));
        removableMediaView.setEditClickListener(new EditButtonListener(obj));
        thumbnail.setOnClickListener(new ThumbnailClickListener(obj));

        thumbnail.clear();
        thumbnail.showProgressSpinner();
        view.setVisibility(View.VISIBLE);

        Slide slide = obj;

        switch (slide.getMediaType())
        {

            case IMAGE:
                thumbnail.setImageResource(masterSecret, slide, false, true);
                removableMediaView.display(thumbnail, slide.getMediaType() == MediaType.IMAGE, RemovableEditableMediaView.FrameType.MEDIA);
                break;
            case GIF:
                break;
            case AUDIO:
                audioView.setAudio(masterSecret, (AudioSlide) slide, false);
                removableMediaView.display(audioView, false, RemovableEditableMediaView.FrameType.MEDIA);
                break;
            case VIDEO:
                thumbnail.setImageResource(masterSecret, slide, false, true);
                removableMediaView.display(thumbnail, slide.getMediaType() == MediaType.IMAGE, RemovableEditableMediaView.FrameType.MEDIA);
                break;
            case DOCUMENT:
                documentView.setDocument((DocumentSlide) slide, false);
                removableMediaView.display(documentView, false, RemovableEditableMediaView.FrameType.MEDIA);
                break;

            case LOCATION:

                break;
        }


    }


    public class ThumbnailClickListener implements View.OnClickListener
    {
        private final Slide slide;

        public ThumbnailClickListener(Slide slide)
        {
            this.slide = slide;
        }

        @Override
        public void onClick(View v)
        {
            if (slide != null)
                previewImageDraft(slide);
        }
    }

    private void previewImageDraft(final @NonNull Slide slide)
    {
        if (MediaPreviewActivity.isContentTypeSupported(slide.getContentType()) && slide.getUri() != null)
        {
            Intent intent = new Intent(getContext(), MediaPreviewActivity.class);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(MediaPreviewActivity.SIZE_EXTRA, slide.asAttachment().getSize());
            intent.setDataAndType(slide.getUri(), slide.getContentType());

            getContext().startActivity(intent);
        }
    }

    public class RemoveButtonListener implements View.OnClickListener
    {

        private final Slide slide;

        private final AttachmentAdapter aa;

        public RemoveButtonListener(AttachmentAdapter aa, Slide slide)
        {
            this.slide = slide;
            this.aa = aa;
        }

        @Override
        public void onClick(View v)
        {
            objects.remove(slide);
            aa.notifyDataSetChanged();
        }
    }

    public class EditButtonListener implements RemovableEditableMediaView.OnEditButtonClick
    {

        private final Slide slide;

        public EditButtonListener(Slide slide)
        {
            this.slide = slide;
        }

        @Override
        public void OnClick(View v, RemovableEditableMediaView.FrameType frameType)
        {
            if (frameType == RemovableEditableMediaView.FrameType.MEDIA)
            {
                Intent intent = new Intent(getContext(), ScribbleActivity.class);
                intent.setData(getSlideUri());
                ((Activity) getContext()).startActivityForResult(intent, ScribbleActivity.SCRIBBLE_REQUEST_CODE);
            }

        }

        private
        @Nullable
        Uri getSlideUri()
        {
            return slide == null ? null : slide.getUri();
        }
    }


}
