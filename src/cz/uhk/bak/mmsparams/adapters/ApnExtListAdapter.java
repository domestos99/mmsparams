package cz.uhk.bak.mmsparams.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.mms.ApnExt;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class ApnExtListAdapter extends MyAdapterBase<ApnExt>
{
    private static final int row_layout_ID = R.layout.apn_ext_browse_item;


    public ApnExtListAdapter(Context context, List<ApnExt> objects)
    {
        super(context, row_layout_ID, objects);
    }

    @Override
    protected int getResource()
    {
        return row_layout_ID;
    }

    @Override
    protected void setupView(View view, ApnExt obj, int position)
    {

        TextView tv = (TextView) view.findViewById(R.id.name);
        tv.setText(obj.getName());

        TextView info = ViewUtil.findById(view, R.id.info);
        info.setText(obj.toString());


        if (obj.isSystem())
        {
            view.setBackgroundColor(Color.rgb(255, 255, 234));
        }


    }
}
