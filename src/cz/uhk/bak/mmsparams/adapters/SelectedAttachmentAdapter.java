package cz.uhk.bak.mmsparams.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cz.uhk.bak.mmsparams.MediaPreviewActivity;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.components.AudioView;
import cz.uhk.bak.mmsparams.components.DocumentView;
import cz.uhk.bak.mmsparams.components.MmsProfilesView;
import cz.uhk.bak.mmsparams.components.RemovableEditableMediaView;
import cz.uhk.bak.mmsparams.components.ThumbnailView;
import cz.uhk.bak.mmsparams.components.location.SignalMapView;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.interfaces.INotifyDataSetChanged;
import cz.uhk.bak.mmsparams.interfaces.IOnDataDelete;
import cz.uhk.bak.mmsparams.mms.AudioSlide;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.mms.slides.DocumentSlide;
import cz.uhk.bak.mmsparams.scribbles.ScribbleActivity;
import cz.uhk.bak.mmsparams.util.ViewUtil;


public class SelectedAttachmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private static final int row_layout_ID = R.layout.conversation_activity_attachment_editor_stub;

    List<Slide> data;

    private final MasterSecret masterSecret;
    private final Context context;

    SelectedAttachmentAdapter self;

    public SelectedAttachmentAdapter(Context context, MasterSecret masterSecret, List<Slide> data)
    {
        this.data = data;
        this.masterSecret = masterSecret;
        this.context = context;
        self = this;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(row_layout_ID, parent, false);

        return new SelectedAttachmentAdapter.RecentPhotoViewHolder(context, masterSecret, itemView,
                new INotifyDataSetChanged()
                {
                    @Override
                    public void notifyDataSetChanged()
                    {
                        self.notifyDataSetChanged();
                    }
                },
                new IOnDataDelete<Slide>()
                {
                    @Override
                    public void onDataDelete(Slide slide)
                    {
                        data.remove(slide);
                    }
                });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        RecentPhotoViewHolder hld = (RecentPhotoViewHolder) holder;

        Slide d = data.get(position);

        hld.setupData(d, isReadOnly);

    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }

    private boolean isReadOnly = false;

    public void setReadOnly()
    {
        isReadOnly = true;
    }

    static class RecentPhotoViewHolder extends RecyclerView.ViewHolder
    {
        // Toto je radek v adapteru
        RemovableEditableMediaView removableMediaView;
        ThumbnailView thumbnail;
        AudioView audioView;
        DocumentView documentView;
        SignalMapView mapView;
        MmsProfilesView mmsProfilesView;
        MmsProfile mmsProfile;

        private final MasterSecret masterSecret;
        private final Context context;
        INotifyDataSetChanged iNotifyDataSetChanged;
        IOnDataDelete<Slide> iOnDataDelete;
        View view;

        public RecentPhotoViewHolder(Context context, MasterSecret masterSecret, View view,
                                     INotifyDataSetChanged iNotifyDataSetChanged, IOnDataDelete<Slide> iOnDataDelete)
        {
            super(view);

            this.view = view;

            this.masterSecret = masterSecret;
            this.context = context;
            this.iNotifyDataSetChanged = iNotifyDataSetChanged;
            this.iOnDataDelete = iOnDataDelete;

            thumbnail = ViewUtil.findById(view, R.id.attachment_thumbnail);
            audioView = ViewUtil.findById(view, R.id.attachment_audio);
            documentView = ViewUtil.findById(view, R.id.attachment_document);
            mapView = ViewUtil.findById(view, R.id.attachment_location);
            removableMediaView = ViewUtil.findById(view, R.id.removable_media_view);
            mmsProfilesView = ViewUtil.findById(view, R.id.attachment_mms_settings);
        }

        public void setupData(Slide obj, boolean isReadOnly)
        {
            removableMediaView.setRemoveClickListener(new SelectedAttachmentAdapter.RecentPhotoViewHolder.RemoveButtonListener(this, obj));
            removableMediaView.setEditClickListener(new SelectedAttachmentAdapter.RecentPhotoViewHolder.EditButtonListener(obj));
            thumbnail.setOnClickListener(new SelectedAttachmentAdapter.RecentPhotoViewHolder.ThumbnailClickListener(obj));

            thumbnail.clear();
            thumbnail.showProgressSpinner();
            view.setVisibility(View.VISIBLE);

            Slide slide = obj;

            switch (slide.getMediaType())
            {

                case IMAGE:
                    thumbnail.setImageResource(masterSecret, slide, false, true);
                    removableMediaView.display(thumbnail, slide.getMediaType() == MediaType.IMAGE && !isReadOnly, !isReadOnly, RemovableEditableMediaView.FrameType.MEDIA);
                    break;
                case GIF:
                    break;
                case AUDIO:
                    audioView.setAudio(masterSecret, (AudioSlide) slide, false);
                    removableMediaView.display(audioView, false, !isReadOnly, RemovableEditableMediaView.FrameType.MEDIA);
                    break;
                case VIDEO:
                    thumbnail.setImageResource(masterSecret, slide, false, true);
                    removableMediaView.display(thumbnail, slide.getMediaType() == MediaType.IMAGE && !isReadOnly, !isReadOnly, RemovableEditableMediaView.FrameType.MEDIA);
                    break;
                case DOCUMENT:
                    documentView.setDocument((DocumentSlide) slide, false);
                    removableMediaView.display(documentView, false, !isReadOnly, RemovableEditableMediaView.FrameType.MEDIA);
                    break;
            }
        }

        public Context getContext()
        {
            return context;
        }


        public class ThumbnailClickListener implements View.OnClickListener
        {
            private final Slide slide;

            public ThumbnailClickListener(Slide slide)
            {
                this.slide = slide;
            }

            @Override
            public void onClick(View v)
            {
                if (slide != null)
                    previewImageDraft(slide);
            }
        }

        private void previewImageDraft(final @NonNull Slide slide)
        {
            if (MediaPreviewActivity.isContentTypeSupported(slide.getContentType()) && slide.getUri() != null)
            {
                Intent intent = new Intent(getContext(), MediaPreviewActivity.class);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(MediaPreviewActivity.SIZE_EXTRA, slide.asAttachment().getSize());
                intent.setDataAndType(slide.getUri(), slide.getContentType());

                getContext().startActivity(intent);
            }
        }

        public class RemoveButtonListener implements View.OnClickListener
        {

            private final Slide slide;

            private final SelectedAttachmentAdapter.RecentPhotoViewHolder aa;

            public RemoveButtonListener(SelectedAttachmentAdapter.RecentPhotoViewHolder aa, Slide slide)
            {
                this.slide = slide;
                this.aa = aa;
            }

            @Override
            public void onClick(View v)
            {
                aa.iOnDataDelete.onDataDelete(slide);
                aa.iNotifyDataSetChanged.notifyDataSetChanged();
//                data.remove(slide);
//                aa.notifyDataSetChanged();
            }
        }

        public class EditButtonListener implements RemovableEditableMediaView.OnEditButtonClick
        {

            private final Slide slide;

            public EditButtonListener(Slide slide)
            {
                this.slide = slide;
            }

            @Override
            public void OnClick(View v, RemovableEditableMediaView.FrameType frameType)
            {
                if (frameType == RemovableEditableMediaView.FrameType.MEDIA)
                {
                    Intent intent = new Intent(getContext(), ScribbleActivity.class);
                    intent.setData(getSlideUri());
                    ((Activity) getContext()).startActivityForResult(intent, ScribbleActivity.SCRIBBLE_REQUEST_CODE);
                }
            }

            private
            @Nullable
            Uri getSlideUri()
            {
                return slide == null ? null : slide.getUri();
            }
        }

    }


}