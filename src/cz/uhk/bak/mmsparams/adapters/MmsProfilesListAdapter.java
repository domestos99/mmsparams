package cz.uhk.bak.mmsparams.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsProfilesListAdapter extends MyAdapterBase<MmsProfile>
{
    private static final int row_layout_ID = R.layout.mms_profiles_browse_item;

    private int globalProfileID;

    public MmsProfilesListAdapter(Context context, List<MmsProfile> objects)
    {
        super(context, row_layout_ID, objects);

        loadGlobalProfile(context);

    }

    private void loadGlobalProfile(Context context)
    {
        globalProfileID = TextSecurePreferences.getMmsSettingProfileObj(context).getID();
    }

    @Override
    protected int getResource()
    {
        return row_layout_ID;
    }


    @Override
    protected void setupView(View view, MmsProfile obj, int position)
    {
        if (obj.getID() == globalProfileID)
        {
            view.setBackgroundColor(Color.rgb(220, 237, 200));
        }

        TextView tv = (TextView) view.findViewById(R.id.name);
        tv.setText(obj.getName());

        TextView info = ViewUtil.findById(view, R.id.info);
        info.setText(obj.getInfo());
    }

    public void customRefresh()
    {
        loadGlobalProfile(super.getContext());
    }
}
