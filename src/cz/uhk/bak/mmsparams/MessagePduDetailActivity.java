package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MenuItem;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MessagePduDatabase;
import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
import cz.uhk.bak.mmsparams.util.DynamicLanguage;
import cz.uhk.bak.mmsparams.util.DynamicTheme;

public class MessagePduDetailActivity extends PassphraseRequiredActionBarActivity implements OnFragmentInteractionListener
{
    public final static String MESSAGE_ID_EXTRA = "message_id";
    public final static String ID_EXTRA = "id";
    private static final String TAG = MessagePduDetailActivity.class.getSimpleName();


    private DynamicTheme dynamicTheme = new DynamicTheme();
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();

    @Override
    protected void onPreCreate()
    {
        dynamicTheme.onCreate(this);
    }

    /*
    public static ArrayList<GenericPdu> getPdusByMessageId(Context context, long messageID)
    {
        MessagePduDatabase db = DatabaseFactory.getmessagePduDatabase(context);
        ArrayList<MessagePdu> messagePdu = db.getMessagePdu(messageID);


        if (messagePdu == null)
        {
            // error
            return null;
        }

        ArrayList<GenericPdu> pdus = new ArrayList<>();

        for (MessagePdu msgPdu : messagePdu)
        {
            pdus.add(new MyPduParser(msgPdu.getData()).parse());
        }

        return pdus;
    }
*/
    public static MessagePdu getGenericPdu(Context context, int id)
    {
        MessagePduDatabase db = DatabaseFactory.getmessagePduDatabase(context);
        MessagePdu pdu = db.getById(id);


        if (pdu == null)
            return null;

        return pdu;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState, @NonNull MasterSecret masterSecret)
    {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        long messageId = getIntent().getLongExtra(MESSAGE_ID_EXTRA, -1);
        Bundle bundle = new Bundle();
        bundle.putLong(MESSAGE_ID_EXTRA, messageId);
        initFragment(android.R.id.content, new MessagePduDetailListFragment(), masterSecret, dynamicLanguage.getCurrentLocale(), bundle);
    }

    @Override
    public void onResume()
    {
        dynamicTheme.onResume(this);
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }


}
