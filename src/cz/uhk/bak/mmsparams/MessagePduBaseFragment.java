package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.net.Uri;

import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.PduBody;

import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
import cz.uhk.bak.mmsparams.jobs.PduByteHelper;
import cz.uhk.bak.mmsparams.log.DBLog;
import cz.uhk.bak.mmsparams.recipients.Recipient;
import cz.uhk.bak.mmsparams.recipients.RecipientFactory;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.StringUtil;

public abstract class MessagePduBaseFragment extends BaseFragment
{
    public static final String TAG = MessagePduBaseFragment.class.getSimpleName();
    public static final String DEFAULT_EMPTY_VALUE = "...";

    protected String getDateTime(long seconds)
    {
        return PduHeaderConvertor.getDateString(seconds);
    }

    protected String getString(boolean value)
    {
        return String.valueOf(value);
    }

    protected String getString(String value)
    {
        if (value == null)
            return DEFAULT_EMPTY_VALUE;

        return value;
    }


    protected String getString(long value)
    {
        return String.valueOf(value);
    }

    protected String getString(EncodedStringValue value)
    {
        if (value == null)
            return DEFAULT_EMPTY_VALUE;
        return value.getString();
    }

//    protected String getString(EncodedStringValue[] values)
//    {
//        if (values == null)
//            return DEFAULT_EMPTY_VALUE;
//
//        String s = "";
//
//        for (EncodedStringValue v : values)
//        {
//            s += v + ", ";
//        }
//
//        if (s.endsWith(","))
//            s = s.substring(0, s.length() - 2); // Remove comma
//
//        return s;
//    }


    protected String getNumberString(EncodedStringValue from)
    {
        final String s = getString(from);
        if (StringUtil.isEmptyOrNull(s) || DEFAULT_EMPTY_VALUE.equals(s))
            return DEFAULT_EMPTY_VALUE;

        try
        {
            Recipients r = RecipientFactory.getRecipientsFromStrings(getContext(), s, true);

            if (r == null || r.getPrimaryRecipient() == null)
                return DEFAULT_EMPTY_VALUE;

            String res = s;
            Recipient rr = r.getPrimaryRecipient();

            if (!StringUtil.isEmptyOrNull(rr.getName()))
            {
                res += " (" + rr.getName() + ")";
            }
            return res;
        }
        catch (Exception ex)
        {
            DBLog.logToDB(getContext(), "getNumberString", TAG, ex);
            return s;
        }
    }

    protected String getToAsString(EncodedStringValue[] toList)
    {
        if (toList == null)
            return DEFAULT_EMPTY_VALUE;

        String to = "";

        for (EncodedStringValue s : toList)
        {
            to += getNumberString(s) + ", ";
        }

        to = to.trim();
        to = StringUtil.trim(to, ',');

        return to;
    }


    protected String getString(byte[] value)
    {
        if (value == null || value.length == 0)
            return "...";

        return new String(value);
    }

    protected String getStringInt(int value)
    {
        return String.valueOf(value);
    }

    protected String getStringLong(long value)
    {
        return String.valueOf(value);
    }


    protected String getSizeCalc(final PduBody body, final EncodedStringValue subject)
    {
        long size = PduByteHelper.getPduBodySize(body, subject);

//        if (subject != null)
//        {
//            size += subject.getString().length();
//        }

        return getSize(size);
    }


    protected String getSize(long size)
    {
        return PduByteHelper.getSizeString(size);
    }

    protected String getCreatedString(long dtCreated)
    {
        if (dtCreated == 0)
            return DEFAULT_EMPTY_VALUE;
        return PduHeaderConvertor.getDateStringMilis(dtCreated);
    }


    protected String getUrlString(Uri uri)
    {
        if (uri == null)
            return DEFAULT_EMPTY_VALUE;


        // TODO
        return uri.toString();

    }


    private OnFragmentInteractionListener mListener;

    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


}
