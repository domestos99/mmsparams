//package cz.uhk.bak.mmsparams;
//
//import android.content.Context;
//import android.net.Uri;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.TextView;
//
//import cz.uhk.bak.mmsparams.components.relativeTimePicker.DateTimePair;
//import cz.uhk.bak.mmsparams.database.DatabaseFactory;
//import cz.uhk.bak.mmsparams.database.MmsSettingsDatabase;
//import cz.uhk.bak.mmsparams.database.model.MmsSetting;
//import cz.uhk.bak.mmsparams.interfaces.OnFragmentInteractionListener;
//import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
//import cz.uhk.bak.mmsparams.util.ViewUtil;
//
//public class MessagePduSettingsFragment extends MessagePduBaseFragment
//{
//    private long messageID;
//    private MmsSetting setting;
//    public static final String TAG = MessagePduSettingsFragment.class.getSimpleName();
//
//
//    private TextView tvMessageDeliveryReport, tvMmsReadReport, tvMmsClass, tvMmsPriority, tvMmsExpiry, tvMmsDeliveryTime, tvMmsSetFrom, tvMessageType, tvMessageIdDb, tvMmsSettingName;
//
//    public static MessagePduSettingsFragment newInstance(long messageID)
//    {
//        MessagePduSettingsFragment fragment = new MessagePduSettingsFragment();
//        Bundle args = new Bundle();
//        args.putLong(MessagePduDetailActivity.ID_EXTRA, messageID);
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//
//    @Override
//    protected String getMyTag()
//    {
//        return TAG;
//    }
//
//    @Override
//    protected void beforeBaseInit()
//    {
//        super.beforeBaseInit();
//
//        messageID = getArguments().getLong(MessagePduDetailActivity.ID_EXTRA);
//
//        MmsSettingsDatabase db = DatabaseFactory.getMmsProfileDatabase(getApplicationContext());
//        setting = db.getMmsSettingByMessageId(messageID);
//    }
//
//    @Override
//    protected int GetLayoutResource()
//    {
//        return R.layout.message_pdu_settings_fragment;
//    }
//
//    @Override
//    protected void initControls(View view)
//    {
//        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
//        tvMessageDeliveryReport = ViewUtil.findById(view, R.id.tvMessageDeliveryReport);
//        tvMmsReadReport = ViewUtil.findById(view, R.id.tvMmsReadReport);
//        tvMmsClass = ViewUtil.findById(view, R.id.tvMmsClass);
//        tvMmsPriority = ViewUtil.findById(view, R.id.tvMmsPriority);
//        tvMmsExpiry = ViewUtil.findById(view, R.id.tvMmsExpiry);
//        tvMmsDeliveryTime = ViewUtil.findById(view, R.id.tvMmsDeliveryTime);
//        tvMmsSetFrom = ViewUtil.findById(view, R.id.tvMmsSetFrom);
//        tvMessageIdDb = ViewUtil.findById(view, R.id.tvMessageIdDb);
//        tvMmsSettingName = ViewUtil.findById(view, R.id.tvMmsSettingName);
//
//        fillControls();
//    }
//
//    private void fillControls()
//    {
//
//        try
//        {
//            tvMessageType.setText("Mms Send Settings");
//
//            tvMmsSettingName.setText(setting.getName());
//            tvMessageIdDb.setText(getString(messageID));
//
//            tvMessageDeliveryReport.setText(getString(setting.isDeliveryReport()));
//            tvMmsReadReport.setText(getString(setting.isReadReport()));
//            tvMmsClass.setText(PduHeaderConvertor.getMessageClassString(setting.getMessage_class()));
//            tvMmsPriority.setText(PduHeaderConvertor.getPriorityString(setting.getPriority()));
//
//            DateTimePair dtExpiry = new DateTimePair(setting.getExpiryTime());
//            tvMmsExpiry.setText(dtExpiry.getString());
//
//            DateTimePair dtDelTime = new DateTimePair(setting.getDeliveryTime());
//            tvMmsDeliveryTime.setText(dtDelTime.getString());
//
//
//            tvMmsSetFrom.setText(getString(setting.getSenderVisibility()));
//
//        } catch (Exception e)
//        {
//            int k = 0;
//        }
//    }
//
//
//
//    @Override
//    protected void setupListeners()
//    {
//
//    }
//}
