package cz.uhk.bak.mmsparams;

import android.support.annotation.NonNull;

import java.util.Locale;
import java.util.Set;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.model.MessageRecord;
import cz.uhk.bak.mmsparams.recipients.Recipients;

public interface BindableConversationItem extends Unbindable
{
    void bind(@NonNull MasterSecret masterSecret,
              @NonNull MessageRecord messageRecord,
              @NonNull Locale locale,
              @NonNull Set<MessageRecord> batchSelected,
              @NonNull Recipients recipients);

    MessageRecord getMessageRecord();
}
