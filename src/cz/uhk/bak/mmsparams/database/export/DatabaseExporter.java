package cz.uhk.bak.mmsparams.database.export;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.NoExternalStorageException;
import cz.uhk.bak.mmsparams.util.StorageUtil;

public class DatabaseExporter
{
    public static final String DB_BACK_UP_PATH = "MMSParamsDatabaseBackUp.db";

    public static void exportDatabaseToSd(Context context, MasterSecret masterSecret)
            throws NoExternalStorageException, IOException
    {
        exportDatabase(context, masterSecret);
    }

    public static File getPlaintextExportFile() throws NoExternalStorageException
    {
        return new File(StorageUtil.getBackupDir(), DB_BACK_UP_PATH);
    }


    private static void exportDatabase(Context context, MasterSecret masterSecret)
            throws NoExternalStorageException, IOException
    {
        SQLiteDatabase db = DatabaseFactory.getInstance(context).getReadableDatabase();

        String path = db.getPath();


        File dbFile = new File(path);
        FileInputStream fis = new FileInputStream(dbFile);

        OutputStream output = new FileOutputStream(getPlaintextExportFile());


        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0)
        {
            output.write(buffer, 0, length);
        }
        output.flush();
        output.close();
        fis.close();
    }


    public static void importDatabaseFromSd(Context context, MasterSecret masterSecret)
    {
        try
        {
            SQLiteDatabase db = DatabaseFactory.getInstance(context).getWritableDatabase();

            InputStream mInput = new FileInputStream(getPlaintextExportFile());
            String outFileName = db.getPath();
            OutputStream mOutput = new FileOutputStream(outFileName);
            byte[] mBuffer = new byte[1024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0)
            {
                mOutput.write(mBuffer, 0, mLength);
            }
            mOutput.flush();
            mOutput.close();
            mInput.close();

            DatabaseFactory.getInstance(context).reset(context);

        }
        catch (Exception ex)
        {

        }
        finally
        {

        }
    }
}
