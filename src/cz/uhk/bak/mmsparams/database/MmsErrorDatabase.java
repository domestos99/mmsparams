package cz.uhk.bak.mmsparams.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import cz.uhk.bak.mmsparams.database.model.MmsError;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.scribbles.StickerSelectActivity;

public class MmsErrorDatabase extends DatabaseBase<MmsError>
{
    public static final String TABLE_NAME = "mmserror";
    private static final String TAG = MmsErrorDatabase.class.getSimpleName();


    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + MmsError.MetaData.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MmsError.MetaData.MSG_ID + " INTEGER, " +
                    MmsError.MetaData.ERROR + " TEXT, " +
                    MmsError.MetaData.DATE_CREATED + " BIGINT " +
                    ");";


    public MmsErrorDatabase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public String getIDColumnName()
    {
        return MmsError.MetaData.ID;
    }

    @Override
    public MmsError getFromCursor(Cursor cursor)
    {
        return MmsError.getFromCursor(cursor);
    }


    public ArrayList<MmsError> getMessageError(long messageId)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        int id = -1, type = -1;
        byte[] data = null;
        ArrayList<MmsError> list = new ArrayList<>();

        try
        {
            cursor = database.query(TABLE_NAME, null, MmsError.MetaData.MSG_ID + " = ? ", new String[]{messageId + ""}, null, null, MmsError.MetaData.DATE_CREATED);
            if (cursor != null && cursor.getCount() > 0)
            {
                while (cursor != null && cursor.moveToNext())
                {
                    try
                    {
                        list.add(MmsError.getFromCursor(cursor));
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        return list;
    }


    public void changeMessageID(long oldMsgID, long newMsgID)
    {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(MmsError.MetaData.MSG_ID, newMsgID);

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.update(TABLE_NAME, contentValues, MmsError.MetaData.MSG_ID + " = ? ", new String[]{oldMsgID + ""});
        notifyConversationListListeners();
    }

    public void deleteByMessageID(long messageId)
    {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        database.delete(TABLE_NAME, MmsError.MetaData.MSG_ID + " = ?", new String[]{messageId + ""});
    }


    public void insertError(long messageId, String tag, Throwable e)
    {
        insertError(messageId, tag, e.getClass().getSimpleName(), e);
    }

    public void insertError(long messageId, String tag, String extraInfo, Throwable e)
    {
        MmsError er = MmsError.createMmsError(messageId, tag, extraInfo, e);

        this.insert(er);
    }


    public MmsError getById(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, MmsError.MetaData.ID + " = ?", new String[]{id + ""}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return MmsError.getFromCursor(cursor);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        LogFB.e(TAG, "MmsSetting ID not found: " + id);


        return null;
    }
}
