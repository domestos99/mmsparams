package cz.uhk.bak.mmsparams.database.identity;

import org.whispersystems.libsignal.IdentityKey;

import cz.uhk.bak.mmsparams.database.IdentityDatabase;

public class IdentityRecord
{

    private final long recipientId;
    private final IdentityKey identitykey;
    private final IdentityDatabase.VerifiedStatus verifiedStatus;
    private final boolean firstUse;
    private final long timestamp;
    private final boolean nonblockingApproval;

    public IdentityRecord(long recipientId,
                          IdentityKey identitykey, IdentityDatabase.VerifiedStatus verifiedStatus,
                          boolean firstUse, long timestamp, boolean nonblockingApproval)
    {
        this.recipientId = recipientId;
        this.identitykey = identitykey;
        this.verifiedStatus = verifiedStatus;
        this.firstUse = firstUse;
        this.timestamp = timestamp;
        this.nonblockingApproval = nonblockingApproval;
    }

    public long getRecipientId()
    {
        return recipientId;
    }

    public IdentityKey getIdentityKey()
    {
        return identitykey;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public IdentityDatabase.VerifiedStatus getVerifiedStatus()
    {
        return verifiedStatus;
    }

    public boolean isApprovedNonBlocking()
    {
        return nonblockingApproval;
    }

    public boolean isFirstUse()
    {
        return firstUse;
    }

    @Override
    public String toString()
    {
        return "{recipientId: " + recipientId + ", identityKey: " + identitykey + ", verifiedStatus: " + verifiedStatus + ", firstUse: " + firstUse + "}";
    }

}

