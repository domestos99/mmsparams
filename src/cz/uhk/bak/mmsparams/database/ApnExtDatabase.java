package cz.uhk.bak.mmsparams.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.ApnExt;

public class ApnExtDatabase extends DatabaseBase<ApnExt>
{
    private static final String TAG = ApnExtDatabase.class.getSimpleName();


    private static final String TABLE_NAME = "apnext";


    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + MmsProfile.MetaData.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ApnExt.MetaData.NAME + " TEXT, " +
                    ApnExt.MetaData.CARRIER + " TEXT, " +
                    ApnExt.MetaData.APN + " TEXT, " +
                    ApnExt.MetaData.MMSC + " TEXT, " +
                    ApnExt.MetaData.MMS_PROXY + " TEXT, " +
                    ApnExt.MetaData.MMS_PORT + " TEXT, " +
                    ApnExt.MetaData.USERNAME + " TEXT, " +
                    ApnExt.MetaData.PASSWORD + " TEXT );";


    public static final String[] CREATE_INDEXS = {
            "CREATE INDEX IF NOT EXISTS draft_thread_index ON " + TABLE_NAME + " (" + ApnExt.MetaData.NAME + ");",
    };

    public ApnExtDatabase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public ApnExt getFromCursor(Cursor cursor)
    {
        return ApnExt.getFromCursor(cursor, false);
    }


    @Override
    public String getIDColumnName()
    {
        return ApnExt.MetaData.ID;
    }


    public ApnExt getById(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, ApnExt.MetaData.ID + " = ?", new String[]{id + ""}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return ApnExt.getFromCursor(cursor, false);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        LogFB.e(TAG, "MmsSetting ID not found: " + id);


        // TODO
        return null;
    }
}
