/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.log.LogFB;

public class MmsProfilesDatabase extends DatabaseBase<MmsProfile>
{
    private static final String TAG = "MmsSettingsDatabase";

    public static final String TABLE_NAME = "mmssettings";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + MmsProfile.MetaData.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MmsProfile.MetaData.NAME + " TEXT, " +
                    MmsProfile.MetaData.DELIVERY_REPORT + " SMALLINT, " +
                    MmsProfile.MetaData.READ_REPORT + " SMALLINT, " +
                    MmsProfile.MetaData.EXPIRY_TIME + " BIGINT, " +
                    MmsProfile.MetaData.DELIVERY_TIME + " BIGINT, " +
                    MmsProfile.MetaData.PRIORITY + " INTEGER, " +
                    MmsProfile.MetaData.MESSAGE_CLASS + " INTEGER, " +
                    MmsProfile.MetaData.ALLOW_SET_FROM + " INTEGER DEFAULT 1, " +
                    MmsProfile.MetaData.IS_VIRTUAL + " INTEGER DEFAULT 0, " +

                    MmsProfile.MetaData.AUTO_DOWNLOAD + " INTEGER DEFAULT 1, " +
                    MmsProfile.MetaData.ALLOW_DR + " INTEGER DEFAULT 1, " +
                    MmsProfile.MetaData.ALLOW_RR + " INTEGER DEFAULT 0, " +
                    MmsProfile.MetaData.DRM_CONTENT + " INTEGER DEFAULT 1, " +
                    MmsProfile.MetaData.ADAPTATION_ALLOWED + " INTEGER DEFAULT 1, " +
                    MmsProfile.MetaData.SEND_RR_IF_NOT_REQUIRED + " INTEGER DEFAULT 0, " +
                    MmsProfile.MetaData.MAX_ATTACH_SIZE + " TEXT " +

                    ");";


    public static final String[] CREATE_INDEXS = {
            "CREATE INDEX IF NOT EXISTS draft_thread_index ON " + TABLE_NAME + " (" + MmsProfile.MetaData.NAME + ");",
    };

    public MmsProfilesDatabase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public MmsProfile getFromCursor(Cursor cursor)
    {
        return MmsProfile.getFromCursor(cursor);
    }

    @Override
    public String getIDColumnName()
    {
        return MmsProfile.MetaData.ID;
    }


    public MmsProfile getById(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, MmsProfile.MetaData.ID + " = ?", new String[]{id + ""}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return MmsProfile.getFromCursor(cursor);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        LogFB.e(TAG, "MmsSetting ID not found: " + id);
        return MmsProfile.getPrefDefault(context);
    }

//    @Override
//    public void delete(int id)
//    {
//        MmsSetting mmsSetting =  getById(id);
//        if (mmsSetting == null)
//            return;
//
//        mmsSetting.setIsVirtual(true);
//
//        save(mmsSetting);
//    }
}
