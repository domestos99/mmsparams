package cz.uhk.bak.mmsparams.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;

import cz.uhk.bak.mmsparams.database.IDBModel;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.JsonUtils;
import cz.uhk.bak.mmsparams.util.JsonUtilsSafe;
import cz.uhk.bak.mmsparams.util.StringUtil;

public class MmsTemplate implements Serializable, IDBModel<MmsTemplate>
{
    public static final String TAG = MmsTemplate.class.getSimpleName();

    private int id;
    private String name;

    private MmsTemplateDetailGroup mmsTemplateDetailGroup;
    private MmsTemplateSendConfig mmsTemplateSendConfig;

    private String mmsSettingJSON;

    public long dateCreated;
    public long dateUpdated;


    public MmsTemplate()
    {
        this.id = -1;
    }

    public MmsTemplate(final MmsTemplate mt)
    {
        this(mt.getID(),
                mt.getName(),
                mt.getMmsTemplateDetailGroup(),
                mt.getMmsSettingJSON(),
                mt.getDateCreated(),
                mt.getDateUpdated());
    }

    public MmsTemplate(int id, String name, MmsTemplateDetailGroup mmsTemplateDetailGroup, String mmsSettingJSON, long dateCreated, long dateUpdated)
    {
        this.id = id;
        this.name = name;
        this.mmsTemplateDetailGroup = mmsTemplateDetailGroup;
        this.mmsSettingJSON = mmsSettingJSON;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }

    @Override
    public ContentValues getContentValues(MmsTemplate instance)
    {
        ContentValues contentValues = new ContentValues();

        // add id ???
        contentValues.put(MmsTemplate.MetaData.NAME, instance.getName());
        contentValues.put(MetaData.DETAIL_GROUP, instance.getMmsTemplateDetailGroupJSON());
        contentValues.put(MmsTemplate.MetaData.MMS_SETTINGS_JSON, instance.getMmsSettingJSON());
        contentValues.put(MmsTemplate.MetaData.DATE_CREATED, instance.getDateCreated());
        contentValues.put(MmsTemplate.MetaData.DATE_UPDATED, instance.getDateUpdated());

        return contentValues;
    }

    public static MmsTemplate getFromCursor(Cursor cursor)
    {
        MmsTemplate mt = new MmsTemplate();

        mt.id = cursor.getInt(cursor.getColumnIndexOrThrow(MetaData.ID));
        mt.name = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.NAME));

        mt.setMmsTemplateDetailGroup(cursor.getString(cursor.getColumnIndexOrThrow(MetaData.DETAIL_GROUP)));
        mt.setMmsTemplateSendConfig(cursor.getString(cursor.getColumnIndexOrThrow(MetaData.SEND_CONFIG)));

        mt.mmsSettingJSON = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MMS_SETTINGS_JSON));
        mt.dateCreated = cursor.getLong(cursor.getColumnIndexOrThrow(MetaData.DATE_CREATED));
        mt.dateUpdated = cursor.getLong(cursor.getColumnIndexOrThrow(MetaData.DATE_UPDATED));

        return mt;
    }

    public static MmsTemplate createNew()
    {
        MmsTemplate mt = new MmsTemplate();
        mt.name = "?";

        return mt;
    }

    public String getInfo()
    {
        return "" + mmsTemplateDetailGroup;
    }

    @Override
    public String toString()
    {
        return this.getInfo();
    }

    public static boolean isEqual(MmsTemplate newMT, MmsTemplate instance)
    {
        if (newMT == null || instance == null)
            return false;

        try
        {

            String json1 = JsonUtils.toJson(MmsTemplateBLO.getMmsBlo(newMT));
            String json2 = JsonUtils.toJson(MmsTemplateBLO.getMmsBlo(instance));

            return json1.equals(json2);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            LogFB.e(TAG, "isEqual", e);
            return false;
        }
    }

    public static class MetaData
    {
        public static final String ID = "_id";
        public static final String NAME = "name";

        public static final String DETAIL_GROUP = "detail_group";
        public static final String SEND_CONFIG = "send_config";


        public static final String MMS_SETTINGS_JSON = "mmsSettingJSON";
        public static final String DATE_CREATED = "date_created";
        public static final String DATE_UPDATED = "date_updated";
    }

    @Override
    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {
        this.dateCreated = dt;
    }

    @Override
    public void setUpdatedDT(long dt)
    {
        this.dateUpdated = dt;
    }

    @Override
    public int getID()
    {
        return getId();
    }


    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getMmsSettingJSON()
    {
        return mmsSettingJSON;
    }

    public long getDateCreated()
    {
        return dateCreated;
    }

    public long getDateUpdated()
    {
        return dateUpdated;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setMmsSettingJSON(String mmsSettingJSON)
    {
        this.mmsSettingJSON = mmsSettingJSON;
    }

    public void setDateCreated(long dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public void setDateUpdated(long dateUpdated)
    {
        this.dateUpdated = dateUpdated;
    }

    public MmsTemplateDetailGroup getMmsTemplateDetailGroup()
    {
        return mmsTemplateDetailGroup;
    }

    public String getMmsTemplateDetailGroupJSON()
    {
        return JsonUtilsSafe.toJson(this.mmsTemplateDetailGroup);
    }

    public void setMmsTemplateDetailGroup(MmsTemplateDetailGroup mmsTemplateDetailGroup)
    {
        this.mmsTemplateDetailGroup = mmsTemplateDetailGroup;
    }

    public void setMmsTemplateDetailGroup(String recipientsGroupJson)
    {
        if (StringUtil.isEmptyOrNull(recipientsGroupJson))
            setMmsTemplateDetailGroup((MmsTemplateDetailGroup) null);
        else
            setMmsTemplateDetailGroup(JsonUtilsSafe.fromJson(recipientsGroupJson, MmsTemplateDetailGroup.class));
    }

    public MmsTemplateSendConfig getMmsTemplateSendConfig()
    {
        return mmsTemplateSendConfig;
    }

    public String getMmsTemplateSendConfigJson()
    {
        return JsonUtilsSafe.toJson(getMmsTemplateSendConfig());
    }

    public void setMmsTemplateSendConfig(MmsTemplateSendConfig mmsTemplateSendConfig)
    {
        this.mmsTemplateSendConfig = mmsTemplateSendConfig;
    }

    public void setMmsTemplateSendConfig(String mmsTemplateSendConfigJson)
    {
        if (StringUtil.isEmptyOrNull(mmsTemplateSendConfigJson))
            setMmsTemplateSendConfig((MmsTemplateSendConfig) null);
        else
            setMmsTemplateSendConfig(JsonUtilsSafe.fromJson(mmsTemplateSendConfigJson, MmsTemplateSendConfig.class));
    }


    public MmsProfile getMmsSettingsObj()
    {
        return MmsProfile.getFromJson(getMmsSettingJSON());
    }


}
