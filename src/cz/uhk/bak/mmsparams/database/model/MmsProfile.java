package cz.uhk.bak.mmsparams.database.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.android.mms.pdu_alt.PduHeaders;

import java.io.Serializable;

import cz.uhk.bak.mmsparams.components.relativeTimePicker.DateTimePair;
import cz.uhk.bak.mmsparams.database.IDBModel;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;

public class MmsProfile implements Serializable, IDBModel<MmsProfile>
{
    private static final String TAG = MmsProfile.class.getSimpleName();

    private int id;
    private String name;
    private boolean deliveryReport;
    private boolean readReport;
    private long expiryTime;
    private long deliveryTime;
    private int priority;
    private int message_class;
    private boolean senderVisibility;
    private boolean isVirtual;

    private boolean autoDownload;
    private boolean allowSendDR;
    private boolean allowSendRR;
    private boolean sendRrIfNotRequired;
    private String maxAttachSize;

    private boolean drmContent;
    private boolean adaptationAllowed;


    public static final long DEFAULT_EXPIRY_TIME = MmsProfilesFactory.DEFAULT_EXPIRY_TIME;
    public static final long DEFAULT_DELIVERY_TIME = MmsProfilesFactory.DEFAULT_DELIVERY_TIME;
    public static final String DEFAULT_MAX_ATTACH_SIZE = MmsProfilesFactory.DEFAULT_MAX_ATTACH_SIZE;

    public static MmsProfile getFromCursor(Cursor cursor)
    {
        MmsProfile mms = new MmsProfile();

        mms.id = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.ID));
        mms.name = cursor.getString(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.NAME));
        mms.deliveryReport = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.DELIVERY_REPORT)) == PduHeaders.VALUE_NO ? false : true;
        mms.readReport = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.READ_REPORT)) == PduHeaders.VALUE_NO ? false : true;
        mms.expiryTime = cursor.getLong(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.EXPIRY_TIME));
        mms.deliveryTime = cursor.getLong(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.DELIVERY_TIME));
        mms.priority = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.PRIORITY));
        mms.message_class = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.MESSAGE_CLASS));
        mms.senderVisibility = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.ALLOW_SET_FROM)) == 0 ? false : true;
        mms.isVirtual = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.IS_VIRTUAL)) == 0 ? false : true;

        mms.autoDownload = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.AUTO_DOWNLOAD)) == 0 ? false : true;
        mms.allowSendDR = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.ALLOW_DR)) == 0 ? false : true;
        mms.allowSendRR = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.ALLOW_RR)) == 0 ? false : true;
        mms.maxAttachSize = cursor.getString(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.MAX_ATTACH_SIZE));
        mms.sendRrIfNotRequired = cursor.getInt(cursor.getColumnIndexOrThrow(MmsProfile.MetaData.SEND_RR_IF_NOT_REQUIRED)) == 0 ? false : true;

        mms.drmContent = cursor.getInt(cursor.getColumnIndexOrThrow(MetaData.DRM_CONTENT)) == 0 ? false : true;
        mms.adaptationAllowed = cursor.getInt(cursor.getColumnIndexOrThrow(MetaData.ADAPTATION_ALLOWED)) == 0 ? false : true;


        return mms;
    }

    @Override
    public ContentValues getContentValues(MmsProfile instance)
    {
        ContentValues contentValues = new ContentValues();

//        if (instance.getID() != -1)
//            contentValues.put(MmsSetting.MetaData.ID, instance.id);
        contentValues.put(MmsProfile.MetaData.NAME, instance.getName());
        contentValues.put(MmsProfile.MetaData.DELIVERY_REPORT, instance.isDeliveryReportsVal());
        contentValues.put(MmsProfile.MetaData.READ_REPORT, instance.isReadReportVal());
        contentValues.put(MmsProfile.MetaData.EXPIRY_TIME, instance.getExpiryTime());
        contentValues.put(MetaData.DELIVERY_TIME, instance.getDeliveryTime());
        contentValues.put(MmsProfile.MetaData.PRIORITY, instance.getPriority());
        contentValues.put(MmsProfile.MetaData.MESSAGE_CLASS, instance.getMessage_class());
        contentValues.put(MetaData.ALLOW_SET_FROM, instance.getAllowSetFromVal());
        contentValues.put(MetaData.IS_VIRTUAL, instance.getIsVirtualVal());

        contentValues.put(MetaData.AUTO_DOWNLOAD, instance.isAutoDownload());
        contentValues.put(MetaData.ALLOW_DR, instance.isAllowSendDR());
        contentValues.put(MetaData.ALLOW_RR, instance.isAllowSendRR());
        contentValues.put(MetaData.MAX_ATTACH_SIZE, instance.getMaxAttachSize());

        contentValues.put(MetaData.SEND_RR_IF_NOT_REQUIRED, instance.isSendRRIfNotRequired());

        contentValues.put(MetaData.DRM_CONTENT, instance.isDrmContent());
        contentValues.put(MetaData.ADAPTATION_ALLOWED, instance.isAdaptationAllowed());


        return contentValues;
    }

    @Override
    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {

    }

    @Override
    public void setUpdatedDT(long dt)
    {

    }

    public void setIsVirtual(boolean isVirtual)
    {
        this.isVirtual = isVirtual;
    }

    public String getInfo()
    {
        String line1 = "DR: " + deliveryReport + "/" + allowSendDR + " RR: " + readReport + "/" + allowSendRR;
        String line2 = "Class: " + PduHeaderConvertor.getMessageClassString(getMessage_class());
        String line3 = "Prio: " + PduHeaderConvertor.getPriorityString(getPriority());
        String line33 = "Attach: " + maxAttachSize;
        String line4 = "Exp: " + formatDT(getExpiryTime());
        String line5 = "EarlDel: " + formatDT(getDeliveryTime());
        String line6 = "SenderVis: " + String.valueOf(getSenderVisibility());


        return line1 + "\n" + line2 + "\n" + line3 + "\n"  + line33 + "\n"+ line4 + "\n" + line5 + "\n" + line6;
    }

    private String formatDT(long dt)
    {
        DateTimePair dateTimePair = new DateTimePair(dt);
        return dateTimePair.getString();
    }


    public static MmsProfile getPrefDefault(Context context)
    {
        return MmsProfilesFactory.getFromPref(context);
    }

    public static void setPrefDefault(Context context, MmsProfile instance)
    {
        MmsProfilesFactory.setToPref(context, instance);

//        String json = null;
//        try
//        {
//            json = JsonUtils.toJson(instance);
//        }
//        catch (IOException e)
//        {
//            DBLog.logToDB(getApplicationContext(), TAG, e);
//        }
//        TextSecurePreferences.setDefaultMmsSettingJSON(getApplicationContext(), json);
    }

    public String getBloJson()
    {
        return MmsProfilesFactory.getBloJson(this);
    }

    public static MmsProfile getFromJson(String json)
    {
        return MmsProfilesFactory.getFromJson(json);
    }

    public static boolean isEqual(MmsProfile newMS, MmsProfile instance)
    {
        if (newMS == null || instance == null)
            return false;


        MmsProfileBLO blo = MmsProfileBLO.getMmsBlo(newMS);
        String json = MmsProfileBLO.getJson(blo);


        MmsProfileBLO blo2 = MmsProfileBLO.getMmsBlo(instance);
        String json2 = MmsProfileBLO.getJson(blo2);


        return json.equals(json2);
    }

    public int getDrmContentVal()
    {
        return PduHeaderConvertor.convertValue(isDrmContent());
    }

    public int getAdaptationAllowedVal()
    {
        return PduHeaderConvertor.convertValue(isAdaptationAllowed());
    }


    public static class MetaData
    {
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String DELIVERY_REPORT = "delivery_report";
        public static final String READ_REPORT = "read_report";
        public static final String EXPIRY_TIME = "expiry_time";
        public static final String DELIVERY_TIME = "delivery_time";
        public static final String PRIORITY = "priority";
        public static final String MESSAGE_CLASS = "message_class";
        public static final String ALLOW_SET_FROM = "allow_set_from"; // sender visibility
        public static final String IS_VIRTUAL = "is_virtual";

        public static final String AUTO_DOWNLOAD = "auto_download";
        public static final String ALLOW_DR = "allow_dr";
        public static final String ALLOW_RR = "allow_rr";
        public static final String MAX_ATTACH_SIZE = "max_attach_size";
        public static final String SEND_RR_IF_NOT_REQUIRED = "send_rr_ifnot_required";

        public static final String DRM_CONTENT = "drm_content";
        public static final String ADAPTATION_ALLOWED = "adaptation_allowed";

    }

    public MmsProfile()
    {
        this("", true, false, DEFAULT_EXPIRY_TIME, DEFAULT_DELIVERY_TIME, PduHeaders.PRIORITY_LOW, PduHeaders.MESSAGE_CLASS_PERSONAL, true,
                false, true, true, false, DEFAULT_MAX_ATTACH_SIZE, true, true, false);
    }


    public MmsProfile(final MmsProfile mmsProfile)
    {
        this.id = mmsProfile.id;
        this.name = mmsProfile.name;
        this.deliveryReport = mmsProfile.deliveryReport;
        this.readReport = mmsProfile.readReport;
        this.expiryTime = mmsProfile.expiryTime;
        this.deliveryTime = mmsProfile.deliveryTime;
        this.priority = mmsProfile.priority;
        this.message_class = mmsProfile.message_class;
        this.senderVisibility = mmsProfile.senderVisibility;
        this.isVirtual = mmsProfile.isVirtual;

        this.autoDownload = mmsProfile.autoDownload;
        this.allowSendDR = mmsProfile.allowSendDR;
        this.allowSendRR = mmsProfile.allowSendRR;
        this.maxAttachSize = mmsProfile.maxAttachSize;

        this.drmContent = mmsProfile.drmContent;
        this.adaptationAllowed = mmsProfile.adaptationAllowed;
        this.sendRrIfNotRequired = mmsProfile.sendRrIfNotRequired;
    }

    public MmsProfile(String name, boolean deliveryReport, boolean readReport, long expiryTime,
                      long deliveryTime, int priority, int message_class, boolean senderVisibility,
                      boolean isVirtual, boolean autoDownload, boolean allowSendDR, boolean allowSendRR,
                      String maxAttachSize, boolean drmContent, boolean adaptationAllowed, boolean sendRrIfNotRequired)
    {
        this.id = -1;
        this.name = name;
        this.deliveryReport = deliveryReport;
        this.readReport = readReport;
        this.expiryTime = expiryTime;
        this.deliveryTime = deliveryTime;
        this.priority = priority;
        this.message_class = message_class;
        this.senderVisibility = senderVisibility;
        this.isVirtual = isVirtual;
        this.autoDownload = autoDownload;
        this.allowSendDR = allowSendDR;
        this.allowSendRR = allowSendRR;
        this.maxAttachSize = maxAttachSize;
        this.drmContent = drmContent;
        this.adaptationAllowed = adaptationAllowed;
        this.sendRrIfNotRequired = sendRrIfNotRequired;
    }

    public MmsProfile(int id, String name, boolean deliveryReport, boolean readReport,
                      long expiryTime, long deliveryTime, int priority,
                      int message_class, boolean senderVisibility, boolean isVirtual,
                      boolean autoDownload, boolean allowSendDR, boolean allowSendRR,
                      String maxAttachSize, boolean drmContent, boolean adaptationAllowed, boolean sendRrIfNotRequired)
    {
        this.id = id;
        this.name = name;
        this.deliveryReport = deliveryReport;
        this.readReport = readReport;
        this.expiryTime = expiryTime;
        this.deliveryTime = deliveryTime;
        this.priority = priority;
        this.message_class = message_class;
        this.senderVisibility = senderVisibility;
        this.isVirtual = isVirtual;
        this.autoDownload = autoDownload;
        this.allowSendDR = allowSendDR;
        this.allowSendRR = allowSendRR;
        this.maxAttachSize = maxAttachSize;
        this.drmContent = drmContent;
        this.adaptationAllowed = adaptationAllowed;
        this.sendRrIfNotRequired = sendRrIfNotRequired;
    }

    public int getID()
    {
        return id;
    }


    public void setID(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public boolean isDeliveryReport()
    {
        return deliveryReport;
    }

    public int isDeliveryReportsVal()
    {
        return PduHeaderConvertor.convertValue(isDeliveryReport());
    }

    public boolean isReadReport()
    {
        return readReport;
    }

    public int isReadReportVal()
    {
        return PduHeaderConvertor.convertValue(isReadReport());
    }

    public long getExpiryTime()
    {
        return expiryTime;
    }

    public int getPriority()
    {
        return priority;
    }

    public int getMessage_class()
    {
        return message_class;
    }

    public byte[] getMessage_classVal()
    {
        return PduHeaderConvertor.convertMessageClass(getMessage_class());
    }


    public void setName(String name)
    {
        this.name = name;
    }

    public void setDeliveryReport(boolean deliveryReport)
    {
        this.deliveryReport = deliveryReport;
    }

    public void setReadReport(boolean readReport)
    {
        this.readReport = readReport;
    }

    public void setExpiryTime(long expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public void setMessage_class(int message_class)
    {
        this.message_class = message_class;
    }


    public boolean getSenderVisibility()
    {
        return senderVisibility;
    }

    public int getAllowSetFromVal()
    {
        return senderVisibility ? 1 : 0;
    }

    public boolean getIsVirtual()
    {
        return isVirtual;
    }

    public int getIsVirtualVal()
    {
        return isVirtual ? 1 : 0;
    }

    public void setSenderVisibility(boolean senderVisibility)
    {
        this.senderVisibility = senderVisibility;
    }

    public void setDeliveryTime(long deliveryTime)
    {
        this.deliveryTime = deliveryTime;
    }

    public boolean isAutoDownload()
    {
        return autoDownload;
    }

    public void setAutoDownload(boolean autoDownload)
    {
        this.autoDownload = autoDownload;
    }

    public boolean isAllowSendDR()
    {
        return allowSendDR;
    }

    public void setAllowSendDR(boolean allowSendDR)
    {
        this.allowSendDR = allowSendDR;
    }

    public boolean isAllowSendRR()
    {
        return allowSendRR;
    }

    public boolean isSendRRIfNotRequired()
    {
        return sendRrIfNotRequired;
    }

    public void setAllowSendRR(boolean allowSendRR)
    {
        this.allowSendRR = allowSendRR;
    }

    public String getMaxAttachSize()
    {
        return maxAttachSize;
    }

    public void setMaxAttachSize(String maxAttachSize)
    {
        this.maxAttachSize = maxAttachSize;
    }

    public long getDeliveryTime()
    {
        return deliveryTime;
    }

    public boolean isDrmContent()
    {
        return drmContent;
    }

    public void setDrmContent(boolean drmContent)
    {
        this.drmContent = drmContent;
    }

    public boolean isAdaptationAllowed()
    {
        return adaptationAllowed;
    }

    public void setAdaptationAllowed(boolean adaptationAllowed)
    {
        this.adaptationAllowed = adaptationAllowed;
    }

    public void setSendRrIfNotRequired(boolean sendRrIfNotRequired)
    {
        this.sendRrIfNotRequired = sendRrIfNotRequired;
    }
}
