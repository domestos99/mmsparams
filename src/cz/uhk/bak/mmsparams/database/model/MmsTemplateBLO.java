package cz.uhk.bak.mmsparams.database.model;

import java.io.Serializable;

public class MmsTemplateBLO implements Serializable
{

    public int id;
    public String name;
    public MmsTemplateDetailGroup mmsTemplateDetailGroup;
    public MmsTemplateSendConfig mmsTemplateSendConfig;
    public String mmsSettingJSON;

    public long dateCreated;
    public long dateUpdated;


    public MmsTemplateBLO()
    {
    }

    public MmsTemplateBLO(int id, String name, MmsTemplateDetailGroup mmsTemplateDetailGroup, MmsTemplateSendConfig mmsTemplateSendConfig, String mmsSettingJSON, long dateCreated, long dateUpdated)
    {
        this.id = id;
        this.name = name;
        this.mmsTemplateDetailGroup = mmsTemplateDetailGroup;
        this.mmsTemplateSendConfig = mmsTemplateSendConfig;
        this.mmsSettingJSON = mmsSettingJSON;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }


    public static MmsTemplate getMmsSetting(MmsTemplateBLO blo)
    {
        MmsTemplate o = new MmsTemplate(
                blo.id,
                blo.name,
                blo.mmsTemplateDetailGroup,
                blo.mmsSettingJSON,
                blo.dateCreated,
                blo.dateUpdated
        );
        return o;
    }


    public static MmsTemplateBLO getMmsBlo(MmsTemplate setting)
    {
        MmsTemplateBLO blo = new MmsTemplateBLO(
                setting.getID(),
                setting.getName(),
                setting.getMmsTemplateDetailGroup(),
                setting.getMmsTemplateSendConfig(),
                setting.getMmsSettingJSON(),
                setting.getDateCreated(),
                setting.getDateUpdated()
        );

        return blo;
    }
}
