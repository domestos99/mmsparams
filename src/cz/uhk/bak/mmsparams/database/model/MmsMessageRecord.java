package cz.uhk.bak.mmsparams.database.model;


import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import cz.uhk.bak.mmsparams.database.documents.IdentityKeyMismatch;
import cz.uhk.bak.mmsparams.database.documents.NetworkFailure;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.mms.SlideDeck;
import cz.uhk.bak.mmsparams.recipients.Recipient;
import cz.uhk.bak.mmsparams.recipients.Recipients;

public abstract class MmsMessageRecord extends MessageRecord
{

    private final
    @NonNull
    SlideDeck slideDeck;

    MmsMessageRecord(Context context, long id, Body body, Recipients recipients,
                     Recipient individualRecipient, int recipientDeviceId, long dateSent,
                     long dateReceived, long threadId, int deliveryStatus, int receiptCount,
                     long type, List<IdentityKeyMismatch> mismatches,
                     List<NetworkFailure> networkFailures, int subscriptionId, long expiresIn,
                     long expireStarted, @NonNull SlideDeck slideDeck)
    {
        super(context, id, body, recipients, individualRecipient, recipientDeviceId, dateSent, dateReceived, threadId,
                deliveryStatus, receiptCount, type, mismatches, networkFailures, subscriptionId,
                expiresIn, expireStarted);
        this.slideDeck = slideDeck;
    }

    @Override
    public boolean isMms()
    {
        return true;
    }

    @NonNull
    public SlideDeck getSlideDeck()
    {
        return slideDeck;
    }

    @Override
    public boolean isMediaPending()
    {
        for (Slide slide : getSlideDeck().getSlides())
        {
            if (slide.isInProgress() || slide.isPendingDownload())
            {
                return true;
            }
        }

        return false;
    }

    public boolean containsMediaSlide()
    {
        return slideDeck.containsMediaSlide();
    }


}
