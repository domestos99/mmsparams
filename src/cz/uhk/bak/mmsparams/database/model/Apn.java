//package cz.uhk.bak.mmsparams.database.model;
//
//import android.database.Cursor;
//import android.support.annotation.NonNull;
//import android.text.TextUtils;
//
//import cz.uhk.bak.mmsparams.database.ApnDatabase;
//
//public class Apn
//{
//    public static Apn EMPTY = new Apn("", "", "", "", "");
//
//    private String carrier;
//    private final String mmsc;
//    private final String proxy;
//    private final String port;
//    private final String username;
//    private final String password;
//
//    public Apn(String carrier, String mmsc, String proxy, String port, String username, String password)
//    {
//        this.carrier = carrier;
//        this.mmsc = mmsc;
//        this.proxy = proxy;
//        this.port = port;
//        this.username = username;
//        this.password = password;
//    }
//
//    public Apn(String mmsc, String proxy, String port, String username, String password)
//    {
//        this(null, mmsc, proxy, port, username, password);
//    }
//
//    public Apn(Apn customApn, Apn defaultApn,
//               boolean useCustomMmsc,
//               boolean useCustomProxy,
//               boolean useCustomProxyPort,
//               boolean useCustomUsername,
//               boolean useCustomPassword)
//    {
//        this.mmsc = useCustomMmsc ? customApn.mmsc : defaultApn.mmsc;
//        this.proxy = useCustomProxy ? customApn.proxy : defaultApn.proxy;
//        this.port = useCustomProxyPort ? customApn.port : defaultApn.port;
//        this.username = useCustomUsername ? customApn.username : defaultApn.username;
//        this.password = useCustomPassword ? customApn.password : defaultApn.password;
//    }
//
//    public String getCarrier()
//    {
//        return carrier;
//    }
//
//    public boolean hasProxy()
//    {
//        return !TextUtils.isEmpty(proxy);
//    }
//
//    public String getMmsc()
//    {
//        return mmsc;
//    }
//
//    public String getProxy()
//    {
//        return hasProxy() ? proxy : null;
//    }
//
//    public int getPort()
//    {
//        return TextUtils.isEmpty(port) ? 80 : Integer.parseInt(port);
//    }
//
//    public boolean hasAuthentication()
//    {
//        return !TextUtils.isEmpty(username);
//    }
//
//    public String getUsername()
//    {
//        return username;
//    }
//
//    public String getPassword()
//    {
//        return password;
//    }
//
//    @Override
//    public String toString()
//    {
//        return Apn.class.getSimpleName() +
//                "{ mmsc: \"" + mmsc + "\"" +
//                ", proxy: " + (proxy == null ? "none" : '"' + proxy + '"') +
//                ", port: " + (port == null ? "(none)" : port) +
//                ", user: " + (username == null ? "none" : '"' + username + '"') +
//                ", pass: " + (password == null ? "none" : '"' + password + '"') + " }";
//    }
//
//
//    public static Apn getFromCursor(@NonNull Cursor cursor)
//    {
//        return new Apn(cursor.getString(cursor.getColumnIndexOrThrow(ApnDatabase.MMSC_COLUMN)),
//                cursor.getString(cursor.getColumnIndexOrThrow(ApnDatabase.MMS_PROXY_COLUMN)),
//                cursor.getString(cursor.getColumnIndexOrThrow(ApnDatabase.MMS_PORT_COLUMN)),
//                cursor.getString(cursor.getColumnIndexOrThrow(ApnDatabase.USER_COLUMN)),
//                cursor.getString(cursor.getColumnIndexOrThrow(ApnDatabase.PASSWORD_COLUMN)));
//
//    }
//
//
//    public boolean isEqual(Apn apn)
//    {
//        return this.mmsc.equals(apn.mmsc) && this.port.equals(apn.port) && this.proxy.equals(apn.proxy);
//    }
//
//    public boolean isEqual(String mmsc, String proxy, String port)
//    {
//        return this.mmsc.equals(mmsc) && this.proxy.equals(proxy) && this.port.equals(port);
//    }
//
//}
