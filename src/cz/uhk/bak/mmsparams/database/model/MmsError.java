package cz.uhk.bak.mmsparams.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;

import cz.uhk.bak.mmsparams.common.EmptyException;
import cz.uhk.bak.mmsparams.database.IDBModel;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.JsonUtils;

public class MmsError implements Serializable, IDBModel<MmsError>
{
    public static final String TAG = MmsError.class.getSimpleName();

    private int id;
    private long mmsId;
    private String error;
    public long dateCreated;

    public MmsError()
    {
        this.id = -1;
    }

    public MmsError(long mmsId, String error)
    {
        this(mmsId, error, System.currentTimeMillis());
    }

    public MmsError(long mmsId, String error, long dateCreated)
    {
        this(-1, mmsId, error, dateCreated);
    }

    public MmsError(int id, long mmsId, String error, long dateCreated)
    {
        this.id = id;
        this.mmsId = mmsId;
        this.error = error;
        this.dateCreated = dateCreated;
    }

    public static MmsError getFromCursor(Cursor cursor)
    {
        MmsError er = new MmsError();

        er.id = cursor.getInt(cursor.getColumnIndexOrThrow(MmsError.MetaData.ID));
        er.mmsId = cursor.getLong(cursor.getColumnIndexOrThrow(MetaData.MSG_ID));
        er.error = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.ERROR));
        er.dateCreated = cursor.getLong(cursor.getColumnIndexOrThrow(MetaData.DATE_CREATED));

        return er;
    }

    public static class MetaData
    {
        public static final String ID = "_id";
        public static final String MSG_ID = "msg_id";
        public static final String ERROR = "error";
        public static final String DATE_CREATED = "date_created";
    }


    @Override
    public int getID()
    {
        return id;
    }

    @Override
    public ContentValues getContentValues(MmsError instance)
    {
        ContentValues contentValues = new ContentValues();

        // add id ???
        contentValues.put(MmsError.MetaData.MSG_ID, instance.getMmsId());
        contentValues.put(MmsError.MetaData.ERROR, instance.getError());
        contentValues.put(MmsError.MetaData.DATE_CREATED, instance.getDateCreated());

        return contentValues;
    }

    public static MmsError createMmsError(final long messageID, final String tag, final String extraInfo, final Throwable e)
    {
        MmsError er = new MmsError();

        er.mmsId = messageID;
        er.dateCreated = System.currentTimeMillis();

        er.error = createErrorText(tag, extraInfo, e);

        return er;
    }

    private static String createErrorText(final String tag, final String extraInfo, final Throwable e)
    {
        try
        {
            final ErrorBLO blo = new ErrorBLO();

            blo.tag = tag;
            blo.info = extraInfo;

            Throwable a = e;
            if (a == null)
            {
                a = new EmptyException();
            }

            blo.message = a.getMessage();
            blo.stackTrace = ErrorLog.getStackTrace(a);

            return JsonUtils.toJson(blo);
        }
        catch (Exception ex)
        {
            LogFB.e(TAG, "createErrorMessage", ex);
            return null;
        }
    }


    @Override
    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {
        dateCreated = dt;
    }

    @Override
    public void setUpdatedDT(long dt)
    {

    }

    public int getId()
    {
        return id;
    }

    public long getMmsId()
    {
        return mmsId;
    }

    public String getError()
    {
        return error;
    }

    public long getDateCreated()
    {
        return dateCreated;
    }

}
