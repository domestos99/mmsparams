package cz.uhk.bak.mmsparams.database.model;

import java.io.IOException;
import java.io.Serializable;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.JsonUtils;

public class MmsProfileBLO implements Serializable
{
    public int id;
    public String name;
    public boolean deliveryReport;
    public boolean readReport;
    public long expiryTime;
    public long deliveryTime;
    public int priority;
    public int message_class;
    public boolean senderVisibility;
    public boolean isVirtual;
    public boolean autoDownload;
    public boolean allowSendDR;
    public boolean allowSendRR;
    public String maxAttachSize;
    public boolean drmContent;
    public boolean adaptationAllowed;
    public boolean sendRrIfNotRequired;


    public MmsProfileBLO()
    {
    }

    public MmsProfileBLO(int id, String name, boolean deliveryReport, boolean readReport, long expiryTime, long deliveryTime, int priority, int message_class,
                         boolean senderVisibility, boolean isVirtual, boolean autoDownload,
                         boolean allowSendDR, boolean allowSendRR, String maxAttachSize, boolean drmContent, boolean adaptationAllowed, boolean sendRrIfNotRequired)
    {
        this.id = id;
        this.name = name;
        this.deliveryReport = deliveryReport;
        this.readReport = readReport;
        this.expiryTime = expiryTime;
        this.deliveryTime = deliveryTime;
        this.priority = priority;
        this.message_class = message_class;
        this.senderVisibility = senderVisibility;
        this.isVirtual = isVirtual;
        this.autoDownload = autoDownload;
        this.allowSendDR = allowSendDR;
        this.allowSendRR = allowSendRR;
        this.maxAttachSize = maxAttachSize;
        this.drmContent = drmContent;
        this.adaptationAllowed = adaptationAllowed;
        this.sendRrIfNotRequired = sendRrIfNotRequired;
    }


    public static MmsProfile getMmsSetting(MmsProfileBLO blo)
    {
        MmsProfile o = new MmsProfile(
                blo.id,
                blo.name,
                blo.deliveryReport,
                blo.readReport,
                blo.expiryTime,
                blo.deliveryTime,
                blo.priority,
                blo.message_class,
                blo.senderVisibility,
                blo.isVirtual,
                blo.autoDownload,
                blo.allowSendDR,
                blo.allowSendRR,
                blo.maxAttachSize,
                blo.drmContent,
                blo.adaptationAllowed,
                blo.sendRrIfNotRequired
        );
        return o;
    }


    public static MmsProfileBLO getMmsBlo(MmsProfile setting)
    {
        MmsProfileBLO blo = new MmsProfileBLO(
                setting.getID(),
                setting.getName(),
                setting.isDeliveryReport(),
                setting.isReadReport(),
                setting.getExpiryTime(),
                setting.getDeliveryTime(),
                setting.getPriority(),
                setting.getMessage_class(),
                setting.getSenderVisibility(),
                setting.getIsVirtual(),
                setting.isAutoDownload(),
                setting.isAllowSendDR(),
                setting.isAllowSendRR(),
                setting.getMaxAttachSize(),
                setting.isDrmContent(),
                setting.isAdaptationAllowed(),
                setting.isSendRRIfNotRequired()
        );

        return blo;
    }


    public static String getJson(MmsProfileBLO blo)
    {
        try
        {
            return JsonUtils.toJson(blo);
        }
        catch (IOException e)
        {
            LogFB.e(MmsProfileBLO.class.getSimpleName(), "getJson", e);
            return null;
        }
    }

    public static MmsProfileBLO getFromJson(String json)
    {
        if (json == null || json.length() == 0)
            return null;
        try
        {
            return JsonUtils.fromJson(json, MmsProfileBLO.class);
        }
        catch (IOException e)
        {
            LogFB.e(MmsProfileBLO.class.getSimpleName(), "getFromJson", e);
            return null;
        }
    }


}
