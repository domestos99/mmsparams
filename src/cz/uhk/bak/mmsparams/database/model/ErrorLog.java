package cz.uhk.bak.mmsparams.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.uhk.bak.mmsparams.common.EmptyException;
import cz.uhk.bak.mmsparams.database.IDBModel;

public class ErrorLog implements IDBModel<ErrorLog>, Serializable
{
    private int id;
    private String message;
    private String date;
    private String stackTrace;
    private boolean isSubmited;

    public ErrorLog()
    {
        this.id = -1;
    }

    public ErrorLog(String message, Date date, String stackTrace, boolean isSubmited)
    {
        this(-1, message, date, stackTrace, isSubmited);
    }

    public ErrorLog(int id, String message, Date date, String stackTrace, boolean isSubmited)
    {
        this.id = id;
        this.message = message;
        this.date = getDT(date);
        this.stackTrace = stackTrace;
        this.isSubmited = isSubmited;
    }

    public static String getDT(Date dt)
    {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dt);
    }

    @Override
    public int getID()
    {
        return id;
    }

    @Override
    public ContentValues getContentValues(ErrorLog obj)
    {
        ContentValues contentValues = new ContentValues();

//        if (instance.getID() != -1)
//            contentValues.put(MmsSetting.MetaData.ID, instance.id);
        contentValues.put(ErrorLog.MetaData.MESSAGE, obj.getMessage());
        contentValues.put(ErrorLog.MetaData.DATE, obj.getDate());
        contentValues.put(ErrorLog.MetaData.STACK_TRACE, obj.getStackTrace());
        contentValues.put(ErrorLog.MetaData.SUBMITED, obj.isSubmited() ? 1 : 0);

        return contentValues;
    }

    public static ErrorLog getFromCursor(Cursor cursor)
    {
        ErrorLog el = new ErrorLog();

        el.id = cursor.getInt(cursor.getColumnIndexOrThrow(ErrorLog.MetaData.ID));
        el.message = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MESSAGE));
        el.date = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.DATE));
        el.stackTrace = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.STACK_TRACE));
        el.isSubmited = cursor.getInt(cursor.getColumnIndexOrThrow(MetaData.SUBMITED)) == 1 ? true : false;

        return el;
    }


    public static ErrorLog convert(Throwable e, String message)
    {
        ErrorLog el = new ErrorLog();

        el.date = getDT(new Date());

        if (e == null)
            e = new EmptyException();

        if (message == null)
            message = "";

        el.message = message + "  -  " + e.getMessage();


        String sStackTrace = getStackTrace(e);

        el.stackTrace = sStackTrace;

        el.isSubmited = false;

        return el;
    }

    public static String getStackTrace(final Throwable e)
    {
        Throwable a = e;
        if (a == null)
            a = new EmptyException();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        a.printStackTrace(pw);
        return sw.toString();
    }

    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {

    }

    @Override
    public void setUpdatedDT(long dt)
    {

    }


    public static class MetaData
    {
        public static final String ID = "_id";
        public static final String MESSAGE = "message";
        public static final String DATE = "date_cr";
        public static final String STACK_TRACE = "stacktrace";
        public static final String SUBMITED = "is_submited";
    }


    public String getMessage()
    {
        return message;
    }

    public String getDate()
    {
        return date;
    }

    public String getStackTrace()
    {
        return stackTrace;
    }

    public boolean isSubmited()
    {
        return isSubmited;
    }

    public String getInfo()
    {
        return this.toString();
    }


    @Override
    public String toString()
    {
        return "ErrorLog{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", date='" + date + '\'' +
                ", stackTrace='" + stackTrace + '\'' +
                ", isSubmited=" + isSubmited +
                '}';
    }
}
