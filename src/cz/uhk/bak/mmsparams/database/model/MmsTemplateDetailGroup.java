package cz.uhk.bak.mmsparams.database.model;

import java.io.Serializable;

public class MmsTemplateDetailGroup implements Serializable
{
    public String To;
    public String Bcc;
    public String Cc;
    public String Subject;
    public String Body;

    public MmsTemplateDetailGroup()
    {
    }


    public MmsTemplateDetailGroup(MmsTemplateDetailGroup gr)
    {
        this(gr.getTo(), gr.getBcc(), gr.getCc(), gr.getSubject(), gr.getBody());
    }

    public MmsTemplateDetailGroup(String to, String bcc, String cc, String subject, String body)
    {
        To = to;
        Bcc = bcc;
        Cc = cc;
        Subject = subject;
        Body = body;
    }

    public String getTo()
    {
        return To;
    }

    public void setTo(String to)
    {
        To = to;
    }

    public String getBcc()
    {
        return Bcc;
    }

    public void setBcc(String bcc)
    {
        Bcc = bcc;
    }

    public String getCc()
    {
        return Cc;
    }

    public void setCc(String cc)
    {
        Cc = cc;
    }

    public String getSubject()
    {
        return Subject;
    }

    public void setSubject(String subject)
    {
        Subject = subject;
    }

    public String getBody()
    {
        return Body;
    }

    public void setBody(String body)
    {
        Body = body;
    }

    @Override
    public String toString()
    {
        return "To: '" + To + "'\r\n" +
                "Bcc: '" + Bcc + "'\r\n" +
                "Cc: '" + Cc + "'\r\n" +
                "Subject: " + Subject + "\r\n" +
                "Body: " + Body + "\r\n";

    }
}
