package cz.uhk.bak.mmsparams.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.log.LogFB;

public class MmsTemplateDatabase extends DatabaseBase<MmsTemplate>
{
    public static final String TAG = MmsTemplateDatabase.class.getSimpleName();
    public static final String TABLE_NAME = "mmstemplate";


    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + MmsTemplate.MetaData.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MmsTemplate.MetaData.NAME + " TEXT, " +
                    MmsTemplate.MetaData.DETAIL_GROUP + " TEXT, " +
                    MmsTemplate.MetaData.SEND_CONFIG + " TEXT, " +
                    MmsTemplate.MetaData.MMS_SETTINGS_JSON + " TEXT, " +
                    MmsTemplate.MetaData.DATE_CREATED + " BIGINT, " +
                    MmsTemplate.MetaData.DATE_UPDATED + " BIGINT " +
                    ");";


    public MmsTemplateDatabase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    public static long getDraftMmsTemplateID(long id)
    {
        return Integer.parseInt("999888" + String.valueOf(id));
    }

    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public String getIDColumnName()
    {
        return MmsTemplate.MetaData.ID;
    }

    @Override
    public MmsTemplate getFromCursor(Cursor cursor)
    {
        return MmsTemplate.getFromCursor(cursor);
    }


    public MmsTemplate getById(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, MmsTemplate.MetaData.ID + " = ?", new String[]{id + ""}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return MmsTemplate.getFromCursor(cursor);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        LogFB.e(TAG, "MmsSetting ID not found: " + id);


        return null;
    }

    public void deleteWithDrafts(int id)
    {
        delete(id);
        DraftDatabase draftDatabase = DatabaseFactory.getDraftDatabase(context);
        draftDatabase.clearDrafts(getDraftMmsTemplateID(id));
    }
}
