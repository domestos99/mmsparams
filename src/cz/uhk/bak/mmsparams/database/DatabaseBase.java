package cz.uhk.bak.mmsparams.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public abstract class DatabaseBase<T extends IDBModel> extends Database
{
    public DatabaseBase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    public abstract String getTableName();

    public abstract String getIDColumnName();

    public abstract T getFromCursor(Cursor cursor);


    public int getCount()
    {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = db.query(getTableName(), new String[]{"COUNT(*)"}, null, null, null, null, null);

            if (cursor != null && cursor.moveToFirst()) return cursor.getInt(0);
            else return 0;
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
    }

    public ArrayList<T> getAll()
    {
        return getAll("");
    }

    public ArrayList<T> getAll(String where)
    {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + getTableName() + " " + where, null);

        ArrayList<T> list = new ArrayList<>();


        if (cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {

                T setting = getFromCursor(cursor);

                list.add(setting);

                cursor.moveToNext();
            }
        }
        return list;
    }


    public void delete(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        database.delete(getTableName(), getIDColumnName() + " = ?", new String[]{id + ""});
    }


    public long save(T instance)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        long dt = System.currentTimeMillis();
        if (instance.getID() < 0)
            instance.setCreatedDT(dt);
        instance.setUpdatedDT(dt);

        ContentValues cv = instance.getContentValues(instance);

        if (instance.getID() < 0)
        {
            return database.insert(getTableName(), null, cv);
        }
        else
        {
            database.update(getTableName(), cv, getIDColumnName() + " = ?", new String[]{instance.getID() + ""});
            return instance.getID();
        }
    }

    public void insert(T instance)
    {
        instance.setIDNegative();
        save(instance);
    }


}
