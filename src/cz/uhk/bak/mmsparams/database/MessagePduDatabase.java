package cz.uhk.bak.mmsparams.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;

public class MessagePduDatabase extends DatabaseBase<MessagePdu>
{
    public MessagePduDatabase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public String getIDColumnName()
    {
        return MessagePdu.MetaData.ID;
    }

    @Override
    public MessagePdu getFromCursor(Cursor cursor)
    {
        return MessagePdu.createFromCursor(cursor);
    }

    public static final String TAG = MessagePduDatabase.class.getSimpleName();
    public static final String TABLE_NAME = "messagepdu";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + MessagePdu.MetaData.ID + " INTEGER PRIMARY KEY, " +
                    MessagePdu.MetaData.MESSAGE_ID + " INTEGER, " +
                    MessagePdu.MetaData.MSG_PDU_ID + " TEXT, " +
                    MessagePdu.MetaData.TYPE + " INTEGER, " +
                    MessagePdu.MetaData.DATA + " BLOB, " +
                    MessagePdu.MetaData.DATE_CREATED + " BIGINT " +
                    ");";

    public ArrayList<MessagePdu> getMessagePdu(long messageId)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        int id = -1, type = -1;
        byte[] data = null;
        ArrayList<MessagePdu> list = new ArrayList<>();

        try
        {
            cursor = database.query(TABLE_NAME, null, MessagePdu.MetaData.MESSAGE_ID + " = ? ", new String[]{messageId + ""}, null, null, null);
            if (cursor != null && cursor.getCount() > 0)
            {
                while (cursor != null && cursor.moveToNext())
                {
                    try
                    {
                        list.add(MessagePdu.createFromCursor(cursor));
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        return list;
    }

    public void changeMessageID(long oldMsgID, long newMsgID)
    {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(MessagePdu.MetaData.MESSAGE_ID, newMsgID);

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.update(TABLE_NAME, contentValues, MessagePdu.MetaData.MESSAGE_ID + " = ? ", new String[]{oldMsgID + ""});
        notifyConversationListListeners();
    }

    public MessagePdu getById(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, MessagePdu.MetaData.ID + " = ?", new String[]{id + ""}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return MessagePdu.createFromCursor(cursor);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public long getbyMsgPduID(String messagePduId)
    {
        if (messagePduId == null || messagePduId.length() == 0)
            return -1;

        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, MessagePdu.MetaData.MSG_PDU_ID + " = ?", new String[]{messagePduId}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return MessagePdu.createFromCursor(cursor).getMessageID();
            }
        }
        catch (Exception ex)
        {
            return -1;
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        return -1;
    }

    public long getbyMsgPduID(byte[] messagePduId)
    {
        return getbyMsgPduID(new String(messagePduId));
    }

    public void deletePduFromMessageID(long messageId)
    {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        database.delete(TABLE_NAME, MessagePdu.MetaData.MESSAGE_ID + " = ?", new String[]{messageId + ""});
    }


    public MessagePdu getByMessageIDType(long messageIdDB, int pduType)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            String where = MessagePdu.MetaData.MESSAGE_ID + " = ? AND " + MessagePdu.MetaData.TYPE + " = ?";

            cursor = database.query(TABLE_NAME, null, where, new String[]{messageIdDB + "", pduType + ""}, null, null, MessagePdu.MetaData.DATE_CREATED + " DESC");

            while (cursor != null && cursor.moveToFirst())
            {
                return MessagePdu.createFromCursor(cursor);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        return null;


    }

    public boolean isPduExists(long messageId, int pduType)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            String where = MessagePdu.MetaData.MESSAGE_ID + " = ? AND " + MessagePdu.MetaData.TYPE + " = ?";

            cursor = database.query(TABLE_NAME, new String[]{"COUNT(*)"}, where, null, null, null, MessagePdu.MetaData.DATE_CREATED + " DESC");

            if (cursor != null && cursor.moveToFirst())
            {
                int count = cursor.getInt(0);
                return count != 0;
            }

            while (cursor != null && cursor.moveToFirst())
            {
                return true;
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        return false;
    }
}
