package cz.uhk.bak.mmsparams.database.loaders;

import android.content.Context;

import java.util.ArrayList;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsProfilesDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;

public class MmsProfilesListLoader extends android.support.v4.content.AsyncTaskLoader<ArrayList<MmsProfile>>
{

    public MmsProfilesListLoader(Context context)
    {
        super(context);
    }

    @Override
    public ArrayList<MmsProfile> loadInBackground()
    {
        MmsProfilesDatabase db = DatabaseFactory.getMmsProfileDatabase(getContext());

        return db.getAll();
    }

}
