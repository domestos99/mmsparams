package cz.uhk.bak.mmsparams.database.loaders;

import android.content.Context;
import android.database.Cursor;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.util.AbstractCursorLoader;

public class BlockedContactsLoader extends AbstractCursorLoader
{

    public BlockedContactsLoader(Context context)
    {
        super(context);
    }

    @Override
    public Cursor getCursor()
    {
        return DatabaseFactory.getRecipientPreferenceDatabase(getContext())
                .getBlocked();
    }

}
