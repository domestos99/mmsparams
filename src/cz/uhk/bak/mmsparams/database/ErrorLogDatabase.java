package cz.uhk.bak.mmsparams.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import java.util.List;

import cz.uhk.bak.mmsparams.database.model.ErrorLog;

public class ErrorLogDatabase extends DatabaseBase<ErrorLog>
{
    public ErrorLogDatabase(Context context, SQLiteOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    public static final String TABLE_NAME = "errorlog";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + ErrorLog.MetaData.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ErrorLog.MetaData.MESSAGE + " TEXT, " +
                    ErrorLog.MetaData.DATE + " TEXT, " +
                    ErrorLog.MetaData.STACK_TRACE + " TEXT, " +
                    ErrorLog.MetaData.SUBMITED + " SMALLINT )";


    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public String getIDColumnName()
    {
        return ErrorLog.MetaData.ID;
    }

    @Override
    public ErrorLog getFromCursor(Cursor cursor)
    {
        return ErrorLog.getFromCursor(cursor);
    }

    public void insert(ErrorLog errorLog)
    {
        errorLog.setIDNegative();
        save(errorLog);
    }

    public String getAllSerialized(Context context)
    {
        List<ErrorLog> l = getAll();

        StringBuilder sb = new StringBuilder();


        sb.append(Build.ID);      // API Level
        sb.append("\n");
        sb.append(android.os.Build.VERSION.SDK + " (" + Build.VERSION.RELEASE + ")");      // API Level
        sb.append("\n");
        sb.append(android.os.Build.DEVICE);        // Device
        sb.append("\n");
        sb.append(android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);        // Model
        sb.append("\n");
        sb.append(android.os.Build.PRODUCT);     // Product
        sb.append("\n");
        sb.append("\n");


        for (ErrorLog el : l)
        {
            sb.append(el.toString());
            sb.append("\n");
            sb.append("------------------------");
            sb.append("\n");
        }

        return sb.toString();
    }
}





























