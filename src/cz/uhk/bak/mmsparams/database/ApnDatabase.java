package cz.uhk.bak.mmsparams.database;


import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import org.whispersystems.libsignal.util.guava.Optional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.ApnExt;
import cz.uhk.bak.mmsparams.util.RandomIDSingleton;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import cz.uhk.bak.mmsparams.util.Util;

/**
 * Database to query APN and MMSC information
 */
public class ApnDatabase
{
    private static final String TAG = ApnDatabase.class.getSimpleName();

    private final SQLiteDatabase db;
    private final Context context;

    private static final String DATABASE_NAME = "apns.db";
    private static final String ASSET_PATH = "databases" + File.separator + DATABASE_NAME;

    private static final String TABLE_NAME = "apns";
    private static final String ID_COLUMN = "_id";
    private static final String MCC_MNC_COLUMN = "mccmnc";
    private static final String MCC_COLUMN = "mcc";
    private static final String MNC_COLUMN = "mnc";
    private static final String CARRIER_COLUMN = "carrier";
    private static final String APN_COLUMN = "apn";
    private static final String MMSC_COLUMN = "mmsc";
    private static final String PORT_COLUMN = "port";
    private static final String TYPE_COLUMN = "type";
    private static final String PROTOCOL_COLUMN = "protocol";
    private static final String BEARER_COLUMN = "bearer";
    private static final String ROAMING_PROTOCOL_COLUMN = "roaming_protocol";
    private static final String CARRIER_ENABLED_COLUMN = "carrier_enabled";
    private static final String MMS_PROXY_COLUMN = "mmsproxy";
    private static final String MMS_PORT_COLUMN = "mmsport";
    private static final String PROXY_COLUMN = "proxy";
    private static final String MVNO_MATCH_DATA_COLUMN = "mvno_match_data";
    private static final String MVNO_TYPE_COLUMN = "mvno";
    private static final String AUTH_TYPE_COLUMN = "authtype";
    private static final String USER_COLUMN = "user";
    private static final String PASSWORD_COLUMN = "password";
    private static final String SERVER_COLUMN = "server";

    private static final String BASE_SELECTION = MCC_MNC_COLUMN + " = ?";

    private static ApnDatabase instance = null;

    public synchronized static ApnDatabase getInstance(Context context) throws IOException
    {
        if (instance == null) instance = new ApnDatabase(context.getApplicationContext());
        return instance;
    }

    private ApnDatabase(final Context context) throws IOException
    {
        this.context = context;

        File dbFile = context.getDatabasePath(DATABASE_NAME);

        if (!dbFile.getParentFile().exists() && !dbFile.getParentFile().mkdir())
        {
            throw new IOException("couldn't make databases directory");
        }

        Util.copy(context.getAssets().open(ASSET_PATH, AssetManager.ACCESS_STREAMING),
                new FileOutputStream(dbFile));

        try
        {
            this.db = SQLiteDatabase.openDatabase(context.getDatabasePath(DATABASE_NAME).getPath(),
                    null,
                    SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        }
        catch (SQLiteException e)
        {
            throw new IOException(e);
        }
    }

    private Apn getCustomApnParameters()
    {
        String mmsc = TextSecurePreferences.getMmscUrl(context).trim();

        if (!TextUtils.isEmpty(mmsc) && !mmsc.startsWith("http"))
            mmsc = "http://" + mmsc;

        String proxy = TextSecurePreferences.getMmscProxy(context);
        String port = TextSecurePreferences.getMmscProxyPort(context);
        String user = TextSecurePreferences.getMmscUsername(context);
        String pass = TextSecurePreferences.getMmscPassword(context);

        return new Apn(mmsc, proxy, port, user, pass);
    }

    public ApnExt getDefaultApnParameters(String mccmnc, String apn)
    {
        if (mccmnc == null)
        {
            LogFB.w(TAG, "mccmnc was null, returning null");
            return null;
        }

        Cursor cursor = null;

        try
        {
            if (apn != null)
            {
                LogFB.w(TAG, "Querying table for MCC+MNC " + mccmnc + " and APN name " + apn);

                cursor = db.query(TABLE_NAME, null,
                        BASE_SELECTION + " AND " + APN_COLUMN + " = ?",
                        new String[]{mccmnc, apn},
                        null, null, null);
            }

            if (cursor == null || !cursor.moveToFirst())
            {
                if (cursor != null)
                    cursor.close();

                LogFB.w(TAG, "Querying table for MCC+MNC " + mccmnc + " without APN name");

//                cursor = db.query(TABLE_NAME, null,
//                        BASE_SELECTION,
//                        new String[]{mccmnc},
//                        null, null, null);

                cursor = db.query(TABLE_NAME, null,
                        BASE_SELECTION + " and type like '%mms%' ",
                        new String[]{mccmnc},
                        null, null, null);
            }

            if (cursor != null && cursor.moveToFirst())
            {
//                Apn params = new Apn(cursor.getString(cursor.getColumnIndexOrThrow(MMSC_COLUMN)),
//                        cursor.getString(cursor.getColumnIndexOrThrow(MMS_PROXY_COLUMN)),
//                        cursor.getString(cursor.getColumnIndexOrThrow(MMS_PORT_COLUMN)),
//                        cursor.getString(cursor.getColumnIndexOrThrow(USER_COLUMN)),
//                        cursor.getString(cursor.getColumnIndexOrThrow(PASSWORD_COLUMN)));

                ApnExt params = getFromCursorDB(cursor);

                LogFB.w(TAG, "Returning preferred APN " + params);
                return params;
            }

            LogFB.w(TAG, "No matching APNs found, returning null");

            return null;
        }
        finally
        {
            if (cursor != null) cursor.close();
        }

    }

    private ApnExt getFromCursorDB(Cursor cursor)
    {
        ApnExt params = new ApnExt(RandomIDSingleton.getInstance().getNextMinusID(), "apndb",
                cursor.getString(cursor.getColumnIndexOrThrow(CARRIER_COLUMN)),
                cursor.getString(cursor.getColumnIndexOrThrow(APN_COLUMN)),
                Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(MCC_COLUMN))),
                Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(MNC_COLUMN))),
                cursor.getString(cursor.getColumnIndexOrThrow(MMSC_COLUMN)),
                cursor.getString(cursor.getColumnIndexOrThrow(MMS_PROXY_COLUMN)),
                String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(MMS_PORT_COLUMN))),
                cursor.getString(cursor.getColumnIndexOrThrow(USER_COLUMN)),
                cursor.getString(cursor.getColumnIndexOrThrow(PASSWORD_COLUMN)),
                cursor.getString(cursor.getColumnIndexOrThrow(TYPE_COLUMN)),
                String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(AUTH_TYPE_COLUMN))),
                true);

        return params;
    }

    public Optional<ApnExt> getMmsConnectionParameters(String mccmnc, String apn)
    {
        // Apn customApn = getCustomApnParameters();
        ApnExt defaultApn = getDefaultApnParameters(mccmnc, apn);
//        Apn result = new Apn(customApn, defaultApn,
//                TextSecurePreferences.getUseCustomMmsc(context),
//                TextSecurePreferences.getUseCustomMmscProxy(context),
//                TextSecurePreferences.getUseCustomMmscProxyPort(context),
//                TextSecurePreferences.getUseCustomMmscUsername(context),
//                TextSecurePreferences.getUseCustomMmscPassword(context));

        if (defaultApn == null)
            return Optional.absent();

        if (TextUtils.isEmpty(defaultApn.getMmsc()))
            return Optional.absent();
        else return Optional.of(defaultApn);
    }


    public static class Apn
    {

        public static Apn EMPTY = new Apn("", "", "", "", "");

        private final String mmsc;
        private final String proxy;
        private final String port;
        private final String username;
        private final String password;

        public Apn(String mmsc, String proxy, String port, String username, String password)
        {
            this.mmsc = mmsc;
            this.proxy = proxy;
            this.port = port;
            this.username = username;
            this.password = password;
        }

        public Apn(Apn customApn, Apn defaultApn,
                   boolean useCustomMmsc,
                   boolean useCustomProxy,
                   boolean useCustomProxyPort,
                   boolean useCustomUsername,
                   boolean useCustomPassword)
        {
            this.mmsc = useCustomMmsc ? customApn.mmsc : defaultApn.mmsc;
            this.proxy = useCustomProxy ? customApn.proxy : defaultApn.proxy;
            this.port = useCustomProxyPort ? customApn.port : defaultApn.port;
            this.username = useCustomUsername ? customApn.username : defaultApn.username;
            this.password = useCustomPassword ? customApn.password : defaultApn.password;
        }

        public boolean hasProxy()
        {
            return !TextUtils.isEmpty(proxy);
        }

        public String getMmsc()
        {
            return mmsc;
        }

        public String getProxy()
        {
            return hasProxy() ? proxy : null;
        }

        public int getPort()
        {
            return TextUtils.isEmpty(port) ? 80 : Integer.parseInt(port);
        }

        public boolean hasAuthentication()
        {
            return !TextUtils.isEmpty(username);
        }

        public String getUsername()
        {
            return username;
        }

        public String getPassword()
        {
            return password;
        }

        @Override
        public String toString()
        {
            return Apn.class.getSimpleName() +
                    "{ mmsc: \"" + mmsc + "\"" +
                    ", proxy: " + (proxy == null ? "none" : '"' + proxy + '"') +
                    ", port: " + (port == null ? "(none)" : port) +
                    ", user: " + (username == null ? "none" : '"' + username + '"') +
                    ", pass: " + (password == null ? "none" : '"' + password + '"') + " }";
        }
    }
}