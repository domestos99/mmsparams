package cz.uhk.bak.mmsparams.dependencies;

import android.content.Context;

import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.util.CredentialsProvider;

import cz.uhk.bak.mmsparams.BuildConfig;
import cz.uhk.bak.mmsparams.crypto.storage.SignalProtocolStoreImpl;
import cz.uhk.bak.mmsparams.jobs.AttachmentDownloadJob;
import cz.uhk.bak.mmsparams.jobs.AvatarDownloadJob;
import cz.uhk.bak.mmsparams.jobs.CleanPreKeysJob;
import cz.uhk.bak.mmsparams.jobs.CreateSignedPreKeyJob;
import cz.uhk.bak.mmsparams.jobs.DeliveryReceiptJob;
import cz.uhk.bak.mmsparams.jobs.GcmRefreshJob;
import cz.uhk.bak.mmsparams.jobs.PushGroupSendJob;
import cz.uhk.bak.mmsparams.jobs.PushGroupUpdateJob;
import cz.uhk.bak.mmsparams.jobs.PushMediaSendJob;
import cz.uhk.bak.mmsparams.jobs.PushNotificationReceiveJob;
import cz.uhk.bak.mmsparams.jobs.PushTextSendJob;
import cz.uhk.bak.mmsparams.jobs.RefreshAttributesJob;
import cz.uhk.bak.mmsparams.jobs.RefreshPreKeysJob;
import cz.uhk.bak.mmsparams.jobs.RequestGroupInfoJob;
import cz.uhk.bak.mmsparams.jobs.RetrieveProfileJob;
import cz.uhk.bak.mmsparams.jobs.RotateSignedPreKeyJob;
import cz.uhk.bak.mmsparams.push.SecurityEventListener;
import cz.uhk.bak.mmsparams.push.SignalServiceNetworkAccess;
import cz.uhk.bak.mmsparams.service.MessageRetrievalService;
import cz.uhk.bak.mmsparams.service.WebRtcCallService;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;
import dagger.Module;
import dagger.Provides;

@Module(complete = false, injects = {CleanPreKeysJob.class,
        CreateSignedPreKeyJob.class,
        DeliveryReceiptJob.class,
        PushGroupSendJob.class,
        PushTextSendJob.class,
        PushMediaSendJob.class,
        AttachmentDownloadJob.class,
        RefreshPreKeysJob.class,
        MessageRetrievalService.class,
        PushNotificationReceiveJob.class,

        RefreshAttributesJob.class,
        GcmRefreshJob.class,
        RequestGroupInfoJob.class,
        PushGroupUpdateJob.class,
        AvatarDownloadJob.class,
        RotateSignedPreKeyJob.class,
        WebRtcCallService.class,
        RetrieveProfileJob.class})
public class SignalCommunicationModule
{

    private final Context context;
    private final SignalServiceNetworkAccess networkAccess;

    public SignalCommunicationModule(Context context, SignalServiceNetworkAccess networkAccess)
    {
        this.context = context;
        this.networkAccess = networkAccess;
    }

    @Provides
    SignalServiceAccountManager provideSignalAccountManager()
    {
        return new SignalServiceAccountManager(networkAccess.getConfiguration(context),
                TextSecurePreferences.getLocalNumber(context),
                TextSecurePreferences.getPushServerPassword(context),
                BuildConfig.USER_AGENT);
    }

    @Provides
    SignalMessageSenderFactory provideSignalMessageSenderFactory()
    {
        return new SignalMessageSenderFactory()
        {
            @Override
            public SignalServiceMessageSender create()
            {
                return new SignalServiceMessageSender(networkAccess.getConfiguration(context),
                        TextSecurePreferences.getLocalNumber(context),
                        TextSecurePreferences.getPushServerPassword(context),
                        new SignalProtocolStoreImpl(context),
                        BuildConfig.USER_AGENT,
                        Optional.fromNullable(MessageRetrievalService.getPipe()),
                        Optional.<SignalServiceMessageSender.EventListener>of(new SecurityEventListener(context)));
            }
        };
    }

    @Provides
    SignalServiceMessageReceiver provideSignalMessageReceiver()
    {
        return new SignalServiceMessageReceiver(networkAccess.getConfiguration(context),
                new DynamicCredentialsProvider(context),
                BuildConfig.USER_AGENT);
    }

    public static interface SignalMessageSenderFactory
    {
        public SignalServiceMessageSender create();
    }

    private static class DynamicCredentialsProvider implements CredentialsProvider
    {

        private final Context context;

        private DynamicCredentialsProvider(Context context)
        {
            this.context = context.getApplicationContext();
        }

        @Override
        public String getUser()
        {
            return TextSecurePreferences.getLocalNumber(context);
        }

        @Override
        public String getPassword()
        {
            return TextSecurePreferences.getPushServerPassword(context);
        }

        @Override
        public String getSignalingKey()
        {
            return TextSecurePreferences.getSignalingKey(context);
        }
    }

}
