package cz.uhk.bak.mmsparams.dependencies;

import android.content.Context;

import org.whispersystems.libsignal.state.SignedPreKeyStore;

import cz.uhk.bak.mmsparams.crypto.storage.SignalProtocolStoreImpl;
import cz.uhk.bak.mmsparams.jobs.CleanPreKeysJob;
import dagger.Module;
import dagger.Provides;

@Module(complete = false, injects = {CleanPreKeysJob.class})
public class AxolotlStorageModule
{

    private final Context context;

    public AxolotlStorageModule(Context context)
    {
        this.context = context;
    }

    @Provides
    SignedPreKeyStoreFactory provideSignedPreKeyStoreFactory()
    {
        return new SignedPreKeyStoreFactory()
        {
            @Override
            public SignedPreKeyStore create()
            {
                return new SignalProtocolStoreImpl(context);
            }
        };
    }

    public static interface SignedPreKeyStoreFactory
    {
        public SignedPreKeyStore create();
    }
}
