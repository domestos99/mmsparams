package cz.uhk.bak.mmsparams;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.uhk.bak.mmsparams.log.DBLog;

public class ShowErrorActivity extends BaseActivity
{
    TextView tbErrorMessage;
    Button btnGoHome;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_error);

        initControls();
        setupListeners();


        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.get("error") != null)
        {
            Throwable t = (Throwable) bundle.get("error");

            DBLog.logToDB(getApplicationContext(), t);

            tbErrorMessage.setText(t.getMessage() + "\r\n\r\n" + t.toString());
        }

    }

    protected void initControls()
    {
        tbErrorMessage = (TextView) findViewById(R.id.tbErrorMessage);
        tbErrorMessage.setEnabled(false);
        btnGoHome = (Button) findViewById(R.id.btnGoHome);
    }

    protected void setupListeners()
    {
        btnGoHome.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), ConversationActivity.class);
                startActivity(intent);
            }
        });
    }


}
