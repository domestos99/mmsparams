package cz.uhk.bak.mmsparams.mms;


import android.content.Context;
import android.support.annotation.NonNull;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.mms.slides.ImageSlide;

public class MmsSlide extends ImageSlide
{

    public MmsSlide(@NonNull Context context, @NonNull Attachment attachment)
    {
        super(context, attachment);
    }

    @NonNull
    @Override
    public String getContentDescription()
    {
        return "MMS";
    }

}
