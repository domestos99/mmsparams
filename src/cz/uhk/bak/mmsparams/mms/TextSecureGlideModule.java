package cz.uhk.bak.mmsparams.mms;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.DiskCacheAdapter;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;

import java.io.InputStream;

import cz.uhk.bak.mmsparams.glide.OkHttpUrlLoader;
import cz.uhk.bak.mmsparams.mms.AttachmentStreamUriLoader.AttachmentModel;
import cz.uhk.bak.mmsparams.mms.ContactPhotoUriLoader.ContactPhotoUri;
import cz.uhk.bak.mmsparams.mms.DecryptableStreamUriLoader.DecryptableUri;

public class TextSecureGlideModule implements GlideModule
{
    @Override
    public void applyOptions(Context context, GlideBuilder builder)
    {
//    builder.setDiskCache(new NoopDiskCacheFactory());
    }

    @Override
    public void registerComponents(Context context, Glide glide)
    {
        glide.register(DecryptableUri.class, InputStream.class, new DecryptableStreamUriLoader.Factory());
        glide.register(ContactPhotoUri.class, InputStream.class, new ContactPhotoUriLoader.Factory());
        glide.register(AttachmentModel.class, InputStream.class, new AttachmentStreamUriLoader.Factory());
        glide.register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory());
    }

    public static class NoopDiskCacheFactory implements DiskCache.Factory
    {
        @Override
        public DiskCache build()
        {
            return new DiskCacheAdapter();
        }
    }
}
