package cz.uhk.bak.mmsparams.mms;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.whispersystems.libsignal.util.guava.Optional;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cz.uhk.bak.mmsparams.MediaPreviewActivity;
import cz.uhk.bak.mmsparams.MmsProfilesListActivity;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.components.AudioView;
import cz.uhk.bak.mmsparams.components.DocumentView;
import cz.uhk.bak.mmsparams.components.MmsProfilesView;
import cz.uhk.bak.mmsparams.components.RemovableEditableMediaView;
import cz.uhk.bak.mmsparams.components.ThumbnailView;
import cz.uhk.bak.mmsparams.components.location.SignalMapView;
import cz.uhk.bak.mmsparams.components.location.SignalPlace;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.giph.ui.GiphyActivity;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.listeners.AttachmentListener;
import cz.uhk.bak.mmsparams.mms.slides.DocumentSlide;
import cz.uhk.bak.mmsparams.mms.slides.LocationSlide;
import cz.uhk.bak.mmsparams.permissions.PermissionUtil;
import cz.uhk.bak.mmsparams.providers.PersistentBlobProvider;
import cz.uhk.bak.mmsparams.scribbles.ScribbleActivity;
import cz.uhk.bak.mmsparams.util.BitmapUtil;
import cz.uhk.bak.mmsparams.util.MediaUtil;
import cz.uhk.bak.mmsparams.util.ViewUtil;
import cz.uhk.bak.mmsparams.util.concurrent.AssertedSuccessListener;
import cz.uhk.bak.mmsparams.util.concurrent.ListenableFuture;
import cz.uhk.bak.mmsparams.util.views.Stub;

public class AttachmentManagerDisplayGroup
{

    private final static String TAG = AttachmentManagerDisplayGroup.class.getSimpleName();
    @NonNull
    private final Context context;

    @NonNull
    private final Stub<View> attachmentViewStub;

    @NonNull
    private final AttachmentListener attachmentListener;

    private RemovableEditableMediaView removableMediaView;
    private ThumbnailView thumbnail;
    private AudioView audioView;
    private DocumentView documentView;
    private SignalMapView mapView;
    private MmsProfilesView mmsProfilesView;
    // private MmsSetting mmsSetting;

    private
    @NonNull
    List<Uri> garbage = new LinkedList<>();
    private
    @NonNull
    Optional<Slide> slide = Optional.absent();
    private
    @Nullable
    Uri captureUri;

    public AttachmentManagerDisplayGroup(@NonNull Activity activity, Stub<View> attachmentViewStub, @NonNull AttachmentListener listener)
    {
        this.context = activity;
        this.attachmentListener = listener;
        this.attachmentViewStub = attachmentViewStub;

        // mmsSetting = MmsSetting.getPrefDefault(context);
    }

    private void inflateStub()
    {
        if (!attachmentViewStub.resolved())
        {
            View root = attachmentViewStub.get();

            this.thumbnail = ViewUtil.findById(root, R.id.attachment_thumbnail);
            this.audioView = ViewUtil.findById(root, R.id.attachment_audio);
            this.documentView = ViewUtil.findById(root, R.id.attachment_document);
            this.mapView = ViewUtil.findById(root, R.id.attachment_location);
            this.removableMediaView = ViewUtil.findById(root, R.id.removable_media_view);
            this.mmsProfilesView = ViewUtil.findById(root, R.id.attachment_mms_settings);

            removableMediaView.setRemoveClickListener(new AttachmentManagerDisplayGroup.RemoveButtonListener());
            removableMediaView.setEditClickListener(new AttachmentManagerDisplayGroup.EditButtonListener());
            thumbnail.setOnClickListener(new AttachmentManagerDisplayGroup.ThumbnailClickListener());
        }

    }

    public void clear(boolean animate)
    {
        if (attachmentViewStub.resolved())
        {

            if (animate)
            {
                ViewUtil.fadeOut(attachmentViewStub.get(), 200).addListener(new ListenableFuture.Listener<Boolean>()
                {
                    @Override
                    public void onSuccess(Boolean result)
                    {
                        thumbnail.clear();
                        attachmentViewStub.get().setVisibility(View.GONE);
                        attachmentListener.onAttachmentChanged();
                    }

                    @Override
                    public void onFailure(ExecutionException e)
                    {
                    }
                });
            }
            else
            {
                thumbnail.clear();
                attachmentViewStub.get().setVisibility(View.GONE);
                attachmentListener.onAttachmentChanged();
            }

            markGarbage(getSlideUri());
            slide = Optional.absent();

            audioView.cleanup();

//            mmsSettingView.cleanUp();
        }
    }

    public void cleanup()
    {
        cleanup(captureUri);
        cleanup(getSlideUri());

        captureUri = null;
        slide = Optional.absent();

        Iterator<Uri> iterator = garbage.listIterator();

        while (iterator.hasNext())
        {
            cleanup(iterator.next());
            iterator.remove();
        }
    }

    private void cleanup(final @Nullable Uri uri)
    {
        if (uri != null && PersistentBlobProvider.isAuthority(context, uri))
        {
            LogFB.w(TAG, "cleaning up " + uri);
            PersistentBlobProvider.getInstance(context).delete(uri);
        }
    }

    private void markGarbage(@Nullable Uri uri)
    {
        if (uri != null && PersistentBlobProvider.isAuthority(context, uri))
        {
            LogFB.w(TAG, "Marking garbage that needs cleaning: " + uri);
            garbage.add(uri);
        }
    }

    private void setSlide(@NonNull Slide slide)
    {
        if (getSlideUri() != null) cleanup(getSlideUri());
        if (captureUri != null && !captureUri.equals(slide.getUri())) cleanup(captureUri);

        this.captureUri = null;
        this.slide = Optional.of(slide);
    }

    public void setLocation(@NonNull final MasterSecret masterSecret,
                            @NonNull final SignalPlace place,
                            @NonNull final MediaConstraints constraints)
    {
        inflateStub();

        ListenableFuture<Bitmap> future = mapView.display(place);

        attachmentViewStub.get().setVisibility(View.VISIBLE);
        removableMediaView.display(mapView, false, RemovableEditableMediaView.FrameType.MEDIA);

        future.addListener(new AssertedSuccessListener<Bitmap>()
        {
            @Override
            public void onSuccess(@NonNull Bitmap result)
            {
                byte[] blob = BitmapUtil.toByteArray(result);
                Uri uri = PersistentBlobProvider.getInstance(context)
                        .create(masterSecret, blob, MediaUtil.IMAGE_PNG, null);
                LocationSlide locationSlide = new LocationSlide(context, uri, blob.length, place);

                setSlide(locationSlide);
                attachmentListener.onAttachmentChanged();
            }
        });
    }

    public void setMedia(@NonNull final MasterSecret masterSecret,
                         @NonNull final Uri uri,
                         @NonNull final MediaType mediaType,
                         @NonNull final MediaConstraints constraints)
    {
        inflateStub();

        new AsyncTask<Void, Void, Slide>()
        {
            @Override
            protected void onPreExecute()
            {
                thumbnail.clear();
                thumbnail.showProgressSpinner();
                attachmentViewStub.get().setVisibility(View.VISIBLE);
            }

            @Override
            protected
            @Nullable
            Slide doInBackground(Void... params)
            {
                try
                {
                    if (PartAuthority.isLocalUri(uri))
                    {
                        return getManuallyCalculatedSlideInfo(uri);
                    }
                    else
                    {
                        Slide result = getContentResolverSlideInfo(uri);

                        if (result == null) return getManuallyCalculatedSlideInfo(uri);
                        else return result;
                    }
                }
                catch (IOException e)
                {
                    LogFB.w(TAG, e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(@Nullable final Slide slide)
            {
                if (slide == null)
                {
                    attachmentViewStub.get().setVisibility(View.GONE);
                    Toast.makeText(context,
                            R.string.ConversationActivity_sorry_there_was_an_error_setting_your_attachment,
                            Toast.LENGTH_SHORT).show();
                }
                else if (!areConstraintsSatisfied(context, masterSecret, slide, constraints))
                {
                    attachmentViewStub.get().setVisibility(View.GONE);
                    Toast.makeText(context,
                            R.string.ConversationActivity_attachment_exceeds_size_limits,
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    setSlide(slide);
                    attachmentViewStub.get().setVisibility(View.VISIBLE);

                    if (slide.hasAudio())
                    {
                        audioView.setAudio(masterSecret, (AudioSlide) slide, false);
                        removableMediaView.display(audioView, false, RemovableEditableMediaView.FrameType.MEDIA);
                    }
                    else if (slide.hasDocument())
                    {
                        documentView.setDocument((DocumentSlide) slide, false);
                        removableMediaView.display(documentView, false, RemovableEditableMediaView.FrameType.MEDIA);
                    }
                    else
                    {
                        thumbnail.setImageResource(masterSecret, slide, false, true);
                        removableMediaView.display(thumbnail, mediaType == MediaType.IMAGE, RemovableEditableMediaView.FrameType.MEDIA);
                    }

                    attachmentListener.onAttachmentChanged();
                }
            }

            private
            @Nullable
            Slide getContentResolverSlideInfo(Uri uri)
            {
                Cursor cursor = null;
                long start = System.currentTimeMillis();

                try
                {
                    cursor = context.getContentResolver().query(uri, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst())
                    {
                        String fileName = cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));
                        long fileSize = cursor.getLong(cursor.getColumnIndexOrThrow(OpenableColumns.SIZE));
                        String mimeType = context.getContentResolver().getType(uri);

                        LogFB.w(TAG, "remote slide with size " + fileSize + " took " + (System.currentTimeMillis() - start) + "ms");
                        return mediaType.createSlide(context, uri, fileName, mimeType, fileSize);
                    }
                }
                finally
                {
                    if (cursor != null) cursor.close();
                }

                return null;
            }

            private
            @NonNull
            Slide getManuallyCalculatedSlideInfo(Uri uri) throws IOException
            {
                long start = System.currentTimeMillis();
                Long mediaSize = null;
                String fileName = null;
                String mimeType = null;

                if (PartAuthority.isLocalUri(uri))
                {
                    mediaSize = PartAuthority.getAttachmentSize(context, masterSecret, uri);
                    fileName = PartAuthority.getAttachmentFileName(context, masterSecret, uri);
                    mimeType = PartAuthority.getAttachmentContentType(context, masterSecret, uri);
                }

                if (mediaSize == null)
                {
                    mediaSize = MediaUtil.getMediaSize(context, masterSecret, uri);
                }

                LogFB.w(TAG, "local slide with size " + mediaSize + " took " + (System.currentTimeMillis() - start) + "ms");
                return mediaType.createSlide(context, uri, fileName, mimeType, mediaSize);
            }
        }.execute();
    }

    public boolean isAttachmentPresent()
    {
        return attachmentViewStub.resolved() && attachmentViewStub.get().getVisibility() == View.VISIBLE;
    }

    public
    @NonNull
    SlideDeck buildSlideDeck()
    {
        SlideDeck deck = new SlideDeck();
        if (slide.isPresent())
            deck.addSlide(slide.get());
        return deck;
    }

    public static void selectDocument(Activity activity, int requestCode)
    {
        selectMediaType(activity, "*/*", null, requestCode);
    }

    public static void selectGallery(Activity activity, int requestCode)
    {
        selectMediaType(activity, "image/*", new String[]{"image/*", "video/*"}, requestCode);
    }

    public static void selectAudio(Activity activity, int requestCode)
    {
        selectMediaType(activity, "audio/*", null, requestCode);
    }

    public static void selectContactInfo(Activity activity, int requestCode)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void selectLocation(Activity activity, int requestCode)
    {
        try
        {
            PermissionUtil.checkLocationPerm(activity);
            activity.startActivityForResult(new PlacePicker.IntentBuilder().build(activity), requestCode);
        }
        catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e)
        {
            LogFB.w(TAG, e);
        }
    }

    public static void selectGif(Activity activity, int requestCode, boolean isForMms)
    {
        Intent intent = new Intent(activity, GiphyActivity.class);
        intent.putExtra(GiphyActivity.EXTRA_IS_MMS, isForMms);
        activity.startActivityForResult(intent, requestCode);
    }


    public static void pickSettings(Activity activity, int requestCode)
    {
        Intent intentApnBrowse = new Intent(activity, MmsProfilesListActivity.class);
        intentApnBrowse.putExtra("mode", 1);
        activity.startActivityForResult(intentApnBrowse, requestCode);
    }

    private
    @Nullable
    Uri getSlideUri()
    {
        return slide.isPresent() ? slide.get().getUri() : null;
    }

    public
    @Nullable
    Uri getCaptureUri()
    {
        return captureUri;
    }

    //        PermissionUtil.requirePermission(this, "android.permission.CAMERA");
//        PermissionUtil.requirePermission(this, "android.permission.RECORD_AUDIO");

    public void capturePhoto(Activity activity, int requestCode)
    {
        try
        {
            PermissionUtil.checkStoragePerm(activity);
//            PermissionUtil.checkCameraPerm(activity);

            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (captureIntent.resolveActivity(activity.getPackageManager()) != null)
            {
                if (captureUri == null)
                {
                    captureUri = PersistentBlobProvider.getInstance(context)
                            .createForExternal(MediaUtil.IMAGE_JPEG);
                }
                LogFB.w(TAG, "captureUri path is " + captureUri.getPath());
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureUri);
                activity.startActivityForResult(captureIntent, requestCode);
            }
        }
        catch (IOException ioe)
        {
            LogFB.w(TAG, ioe);
        }
    }

    private static void selectMediaType(Activity activity, @NonNull String type, @Nullable String[] extraMimeType, int requestCode)
    {
        PermissionUtil.checkStoragePerm(activity);

        final Intent intent = new Intent();
        intent.setType(type);

        if (extraMimeType != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeType);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            try
            {
                activity.startActivityForResult(intent, requestCode);
                return;
            }
            catch (ActivityNotFoundException anfe)
            {
                LogFB.w(TAG, "couldn't complete ACTION_OPEN_DOCUMENT, no activity found. falling back.");
            }
        }

        intent.setAction(Intent.ACTION_GET_CONTENT);

        try
        {
            activity.startActivityForResult(intent, requestCode);
        }
        catch (ActivityNotFoundException anfe)
        {
            LogFB.w(TAG, "couldn't complete ACTION_GET_CONTENT intent, no activity found. falling back.");
            Toast.makeText(activity, R.string.AttachmentManager_cant_open_media_selection, Toast.LENGTH_LONG).show();
        }
    }

    private boolean areConstraintsSatisfied(final @NonNull Context context,
                                            final @NonNull MasterSecret masterSecret,
                                            final @Nullable Slide slide,
                                            final @NonNull MediaConstraints constraints)
    {
        return slide == null ||
                constraints.isSatisfied(context, masterSecret, slide.asAttachment()) ||
                constraints.canResize(slide.asAttachment());
    }

    private void previewImageDraft(final @NonNull Slide slide)
    {
        if (MediaPreviewActivity.isContentTypeSupported(slide.getContentType()) && slide.getUri() != null)
        {
            Intent intent = new Intent(context, MediaPreviewActivity.class);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(MediaPreviewActivity.SIZE_EXTRA, slide.asAttachment().getSize());
            intent.setDataAndType(slide.getUri(), slide.getContentType());

            context.startActivity(intent);
        }
    }

    public View getView()
    {
        return attachmentViewStub.get();
    }

    public Slide buildSlide()
    {
        if (slide != null && slide.isPresent())
            return slide.get();
        return null;
    }


    private class ThumbnailClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            if (slide.isPresent()) previewImageDraft(slide.get());
        }
    }

    private class RemoveButtonListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            cleanup();
            clear(true);
        }
    }

    private class EditButtonListener implements RemovableEditableMediaView.OnEditButtonClick
    {
        @Override
        public void OnClick(View v, RemovableEditableMediaView.FrameType frameType)
        {
            if (frameType == RemovableEditableMediaView.FrameType.MEDIA
                    )
            {
                Intent intent = new Intent(context, ScribbleActivity.class);
                intent.setData(getSlideUri());
                ((Activity) context).startActivityForResult(intent, ScribbleActivity.SCRIBBLE_REQUEST_CODE);
            }
        }
    }
}
