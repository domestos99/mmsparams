/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.mms;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import cz.uhk.bak.mmsparams.BaseFragment;
import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.permissions.PermissionUtil;


public class AttachmentManager
{
    private final static String TAG = AttachmentManager.class.getSimpleName();

    public static void selectDocument(Activity activity, int requestCode)
    {
        selectMediaType(activity, "*/*", null, requestCode);
    }

    public static void selectGallery(Activity activity, int requestCode)
    {
        selectMediaType(activity, "image/*", new String[]{"image/*", "video/*"}, requestCode);
    }

    public static void selectAudio(Activity activity, int requestCode)
    {
        selectMediaType(activity, "audio/*", null, requestCode);
    }

    private static void selectMediaType(Activity activity, @NonNull String type, @Nullable String[] extraMimeType, int requestCode)
    {
        PermissionUtil.checkStoragePerm(activity);

        final Intent intent = new Intent();
        intent.setType(type);

        if (extraMimeType != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeType);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            try
            {
                activity.startActivityForResult(intent, requestCode);
                return;
            }
            catch (ActivityNotFoundException anfe)
            {
                LogFB.w(TAG, "couldn't complete ACTION_OPEN_DOCUMENT, no activity found. falling back.");
            }
        }

        intent.setAction(Intent.ACTION_GET_CONTENT);

        try
        {
            activity.startActivityForResult(intent, requestCode);
        }
        catch (ActivityNotFoundException anfe)
        {
            LogFB.w(TAG, "couldn't complete ACTION_GET_CONTENT intent, no activity found. falling back.");
            Toast.makeText(activity, R.string.AttachmentManager_cant_open_media_selection, Toast.LENGTH_LONG).show();
        }
    }


    public static void selectDocument(BaseFragment activity, int requestCode)
    {
        selectMediaType(activity, "*/*", null, requestCode);
    }

    public static void selectGallery(BaseFragment activity, int requestCode)
    {
        selectMediaType(activity, "image/*", new String[]{"image/*", "video/*"}, requestCode);
    }

    public static void selectAudio(BaseFragment activity, int requestCode)
    {
        selectMediaType(activity, "audio/*", null, requestCode);
    }

    private static void selectMediaType(BaseFragment activity, @NonNull String type, @Nullable String[] extraMimeType, int requestCode)
    {
        PermissionUtil.checkStoragePerm(activity.getActivity());

        final Intent intent = new Intent();
        intent.setType(type);

        if (extraMimeType != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeType);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            try
            {
                activity.startActivityForResult(intent, requestCode);
                return;
            }
            catch (ActivityNotFoundException anfe)
            {
                LogFB.w(TAG, "couldn't complete ACTION_OPEN_DOCUMENT, no activity found. falling back.");
            }
        }

        intent.setAction(Intent.ACTION_GET_CONTENT);

        try
        {
            activity.startActivityForResult(intent, requestCode);
        }
        catch (ActivityNotFoundException anfe)
        {
            LogFB.w(TAG, "couldn't complete ACTION_GET_CONTENT intent, no activity found. falling back.");
            Toast.makeText(activity.getContext(), R.string.AttachmentManager_cant_open_media_selection, Toast.LENGTH_LONG).show();
        }
    }


}
