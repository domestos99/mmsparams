package cz.uhk.bak.mmsparams.mms;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.bak.mmsparams.database.ApnExtDatabase;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;

public class ApnSingleton
{
    public static final String TAG = ApnSingleton.class.getSimpleName();
    private static ApnSingleton instance;


    private List<ApnExt> systemApns;

    private ApnExt apnExt;

    public static ApnSingleton getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new ApnSingleton(context);
        }
        return instance;
    }

    private ApnSingleton(Context context)
    {
        loadApn(context);
    }

    private void loadApn(Context context)
    {
        systemApns = ApnUtils.getSystemApns(context);

        apnExt = systemApns.get(0);


//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
//        {
//
//            /*
//             APN: test-mms
//             wapgw(proxy): 160.218.160.231:8080
//             mmsc url: http://mms-test.o2active.cz
//
//             */
//
//            LogFB.e(TAG, "Changing: " + apnExt.toString());
//
//            apnExt.setApn("test-apn");
//            apnExt.setMmsc("http://mms-test.o2active.cz");
//            apnExt.setMmsproxy("160.218.160.231");
//
//            LogFB.e(TAG, "Result: " + apnExt.toString());
//
//
//        }

//        Optional<ApnExt> apn = ApnUtils.loadApnsGD(context);
//
//        if (apn == null || !apn.isPresent())
//        {
//            LogFB.e(TAG, "No parameters available from ApnDefaults.");
//
//            try
//            {
//                Optional<ApnExt> params = ApnDatabase.getInstance(context)
//                        .getMmsConnectionParameters(TelephonyUtil.getMccMnc(context),
//                                TelephonyUtil.getApn(context));
//
//                if (params == null || !params.isPresent())
//                {
//                    LogFB.e(TAG, "APN not available in DB");
//                    apnExt = null;
//                }
//                else
//                {
//                    apnExt = params.get();
//                }
//            }
//            catch (Exception ex)
//            {
//                DBLog.logToDB(context, TAG, ex);
//                apnExt = null;
//            }
//        }
//        else
//        {
//            apnExt = apn.get();
//        }
    }

    public ApnExt getApnExt(Context context)
    {
        if (apnExt == null)
        {
            loadApn(context);
        }
        return apnExt;
    }

    public List<ApnExt> getSystemApns(Context context)
    {
        if (systemApns == null || systemApns.size() == 0)
        {
            loadApn(context);
        }

        return new ArrayList<>(systemApns);
    }

    public ApnExt getById(Context context, int id)
    {
        if (id < 0)
        {
            for (ApnExt apn : systemApns)
            {
                if (apn.getID() == id)
                    return apn;
            }
            return null;
        }

        ApnExtDatabase db = DatabaseFactory.getApnExtDatabase(context);
        return db.getById(id);
    }
}
