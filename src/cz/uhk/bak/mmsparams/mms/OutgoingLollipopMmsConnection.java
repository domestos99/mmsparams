/**
 * Copyright (C) 2015 Open Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.mms;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;

import com.android.mms.service_alt.MmsConfig;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cz.uhk.bak.mmsparams.log.DBLog;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.providers.MmsBodyProvider;
import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;
import cz.uhk.bak.mmsparams.util.AndroidVersionUtil;
import cz.uhk.bak.mmsparams.util.Util;

public class OutgoingLollipopMmsConnection extends LollipopMmsConnection implements OutgoingMmsConnection
{
    private static final String TAG = OutgoingLollipopMmsConnection.class.getSimpleName();
    private static final String ACTION = OutgoingLollipopMmsConnection.class.getCanonicalName() + "MMS_SENT_ACTION";

    private byte[] response;

    public OutgoingLollipopMmsConnection(Context context)
    {
        super(context, ACTION);
    }

    @TargetApi(VERSION_CODES.LOLLIPOP_MR1)
    @Override
    public synchronized void onResult(Context context, Intent intent)
    {
        if (AndroidVersionUtil.isHigherThanLollipopMR1())
        {
            LogFB.w(TAG, "HTTP status: " + intent.getIntExtra(SmsManager.EXTRA_MMS_HTTP_STATUS, -1));
        }

        response = intent.getByteArrayExtra(SmsManager.EXTRA_MMS_DATA);
    }

    @Override
    @TargetApi(VERSION_CODES.LOLLIPOP)
    public
    @Nullable
    synchronized byte[] send(@NonNull byte[] pduBytes, int subscriptionId)
            throws UndeliverableMessageException
    {
        beginTransaction();
        try
        {
            MmsBodyProvider.Pointer pointer = MmsBodyProvider.makeTemporaryPointer(getContext());
            Util.copy(new ByteArrayInputStream(pduBytes), pointer.getOutputStream());

            SmsManager smsManager;

            if (AndroidVersionUtil.isHigherThanLollipopMR1() && subscriptionId != -1)
            {
                smsManager = SmsManager.getSmsManagerForSubscriptionId(subscriptionId);
            }
            else
            {
                smsManager = SmsManager.getDefault();
            }

            Bundle configOverrides = new Bundle();
            configOverrides.putBoolean(SmsManager.MMS_CONFIG_GROUP_MMS_ENABLED, true);


            configOverrides.putBoolean(SmsManager.MMS_CONFIG_MMS_DELIVERY_REPORT_ENABLED, true);
            configOverrides.putBoolean(SmsManager.MMS_CONFIG_MMS_READ_REPORT_ENABLED, true);

            configOverrides.putBoolean(SmsManager.MMS_CONFIG_SMS_DELIVERY_REPORT_ENABLED, true);


            if (MessageSizeHelper.allowLargeMms(context))
            {
                configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_MESSAGE_SIZE, 3072000); // 307 200 default
                configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_IMAGE_HEIGHT, 2000);
                configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_IMAGE_WIDTH, 3000);

            }


            MmsConfig mmsConfig = MmsConfigManager.getMmsConfig(getContext(), subscriptionId);

            if (mmsConfig != null)
            {
                MmsConfig.Overridden overridden = new MmsConfig.Overridden(mmsConfig, new Bundle());
                configOverrides.putString(SmsManager.MMS_CONFIG_HTTP_PARAMS, overridden.getHttpParams());
                configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_MESSAGE_SIZE, overridden.getMaxMessageSize());

                configOverrides.putBoolean(SmsManager.MMS_CONFIG_MMS_DELIVERY_REPORT_ENABLED, true);
                configOverrides.putBoolean(SmsManager.MMS_CONFIG_MMS_READ_REPORT_ENABLED, true);


                if (MessageSizeHelper.allowLargeMms(context))
                {
                    configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_MESSAGE_SIZE, 3072000); // 307 200 default
                    configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_IMAGE_HEIGHT, 2000);
                    configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_IMAGE_WIDTH, 3000);
                }
            }


//            Bundle configOverrides = new Bundle();
//            configOverrides.putBoolean(SmsManager.MMS_CONFIG_GROUP_MMS_ENABLED, true);
//
//            MmsConfig mmsConfig = MmsConfigManager.getMmsConfig(getContext(), subscriptionId);
//
//            if (mmsConfig != null)
//            {
//                MmsConfig.Overridden overridden = new MmsConfig.Overridden(mmsConfig, new Bundle());
//                configOverrides.putString(SmsManager.MMS_CONFIG_HTTP_PARAMS, overridden.getHttpParams());
//                configOverrides.putInt(SmsManager.MMS_CONFIG_MAX_MESSAGE_SIZE, overridden.getMaxMessageSize());
//            }

            try
            {
                smsManager.sendMultimediaMessage(getContext(),
                        pointer.getUri(),
                        null,
                        configOverrides,
                        getPendingIntent());

                waitForResult();
            }
            catch (SecurityException secEx)
            {
                DBLog.logToDB(getContext(), "SMSManager.sendMultimediaMessage - SecurityException", TAG, secEx);
                return null;
            }
            catch (Exception ex)
            {
                DBLog.logToDB(getContext(), "SMSManager.sendMultimediaMessage", TAG, ex);
                throw ex;
            }

            LogFB.w(TAG, "MMS broadcast received and processed.");
            pointer.close();

            if (response == null)
            {
                throw new UndeliverableMessageException("Null response.");
            }

            return response;
        }
        catch (IOException | TimeoutException e)
        {
            throw new UndeliverableMessageException(e);
        }
        finally
        {
            endTransaction();
        }
    }

}

