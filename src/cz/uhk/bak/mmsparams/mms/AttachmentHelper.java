package cz.uhk.bak.mmsparams.mms;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.AttachmentDatabase;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;

public class AttachmentHelper
{


    public static List<Attachment> scaleAttachments(final @NonNull Context context, final @NonNull MasterSecret masterSecret,
                                                    final @NonNull MediaConstraints constraints,
                                                    final @NonNull List<Attachment> attachments)
            throws UndeliverableMessageException
    {
        AttachmentDatabase attachmentDatabase = DatabaseFactory.getAttachmentDatabase(context);
        List<Attachment> results = new LinkedList<>();

        for (Attachment attachment : attachments)
        {
            try
            {
                if (constraints.isSatisfied(context, masterSecret, attachment) || MessageSizeHelper.allowLargeMms(context))
                {
                    results.add(attachment);
                }
                else if (constraints.canResize(attachment))
                {
                    MediaStream resized = constraints.getResizedMedia(context, masterSecret, attachment);
                    results.add(attachmentDatabase.updateAttachmentData(masterSecret, attachment, resized));
                }
                else
                {
                    throw new UndeliverableMessageException("Size constraints could not be met!");
                }
            }
            catch (IOException | MmsException e)
            {
                throw new UndeliverableMessageException(e);
            }
        }

        return results;
    }


}
