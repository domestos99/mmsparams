/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.mms;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.components.SelectedAttachmentsViewRail;
import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.mms.listeners.AttachmentListener;
import cz.uhk.bak.mmsparams.util.SlideUtils;
import cz.uhk.bak.mmsparams.util.ViewUtil;
import cz.uhk.bak.mmsparams.util.views.Stub;


public class AttachmentManager2
{

    private final static String TAG = AttachmentManager2.class.getSimpleName();
    @NonNull
    private final Context context;
    @NonNull
    private final Stub<View> attachmentViewStub;


    @NonNull
    private final AttachmentListener attachmentListener;


    //    ListView list;
    SelectedAttachmentsViewRail attachmentSelected;


    //  AttachmentAdapter adapter;
    List<Slide> data;
    private final MasterSecret masterSecret;


    public AttachmentManager2(@NonNull Activity activity, @NonNull AttachmentListener listener, MasterSecret masterSecret)
    {
        this.context = activity;
        this.attachmentListener = listener;
        this.attachmentViewStub = ViewUtil.findStubById(activity, R.id.attachment_editor_stub_list);


        data = new ArrayList<>();
        this.masterSecret = masterSecret;
    }

    public AttachmentManager2(@NonNull Context context, View view, @NonNull AttachmentListener listener, MasterSecret masterSecret)
    {
        this.context = context;
        this.attachmentListener = listener;
        this.attachmentViewStub = ViewUtil.findStubById(view, R.id.attachment_editor_stub_list);

        data = new ArrayList<>();
        this.masterSecret = masterSecret;
    }

    private void inflateStub()
    {
        if (!attachmentViewStub.resolved())
        {
            View root = attachmentViewStub.get();

            //list = ViewUtil.findById(root, R.id.list);

            //  attachment_list = ViewUtil.findById(root, R.id.attachment_list);


            attachmentSelected = ViewUtil.findById(root, R.id.attachment_selected);


            // adapter = new AttachmentAdapter(context, data, masterSecret);

            attachmentSelected.setData(masterSecret, data);
//            list.setAdapter(adapter);


            //  this.attachment_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            //  this.attachment_list.setItemAnimator(new DefaultItemAnimator());
            //  attachment_list.setAdapter(adapter);
        }
    }


    public void setMedia(@NonNull final MasterSecret masterSecret,
                         @NonNull final Uri uri,
                         @NonNull final MediaType mediaType,
                         @NonNull final MediaConstraints constraints)
    {
        inflateStub();

        new AsyncTask<Void, Void, Slide>()
        {
            @Override
            protected void onPreExecute()
            {
//                thumbnail.clear();
//                thumbnail.showProgressSpinner();
                attachmentViewStub.get().setVisibility(View.VISIBLE);
            }

            @Override
            protected
            @Nullable
            Slide doInBackground(Void... params)
            {
                try
                {
                    if (PartAuthority.isLocalUri(uri))
                    {
                        return SlideUtils.getManuallyCalculatedSlideInfo(context, masterSecret, uri, mediaType);
                    }
                    else
                    {
                        Slide result = SlideUtils.getContentResolverSlideInfo(context, uri, mediaType);

                        if (result == null)
                            return SlideUtils.getManuallyCalculatedSlideInfo(context, masterSecret, uri, mediaType);
                        else return result;
                    }
                }
                catch (IOException e)
                {
                    LogFB.w(TAG, e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(@Nullable final Slide slide)
            {
                if (slide == null)
                {
                    attachmentViewStub.get().setVisibility(View.GONE);
                    Toast.makeText(context,
                            R.string.ConversationActivity_sorry_there_was_an_error_setting_your_attachment,
                            Toast.LENGTH_SHORT).show();
                }
                else if (!SlideUtils.areConstraintsSatisfied(context, masterSecret, slide, constraints))
                {
                    attachmentViewStub.get().setVisibility(View.GONE);
                    Toast.makeText(context,
                            R.string.ConversationActivity_attachment_exceeds_size_limits,
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
//                    setSlide(slide);

                    attachmentViewStub.get().setVisibility(View.VISIBLE);

                    data.add(slide);


//                    if (slide.hasAudio())
//                    {
//                        audioView.setAudio(masterSecret, (AudioSlide) slide, false);
//                        removableMediaView.display(audioView, false, RemovableEditableMediaView.FrameType.MEDIA);
//                    }
//                    else if (slide.hasDocument())
//                    {
//                        documentView.setDocument((DocumentSlide) slide, false);
//                        removableMediaView.display(documentView, false, RemovableEditableMediaView.FrameType.MEDIA);
//                    }
//                    else
//                    {
//                        thumbnail.setImageResource(masterSecret, slide, false, true);
//                        removableMediaView.display(thumbnail, mediaType == MediaType.IMAGE, RemovableEditableMediaView.FrameType.MEDIA);
//                    }

                    attachmentListener.onAttachmentChanged();

                    if (attachmentSelected != null)
                        attachmentSelected.notifyDataSetChanged();
                }
            }


        }.execute();
    }

    public
    @NonNull
    SlideDeck buildSlideDeck()
    {
        SlideDeck deck = new SlideDeck();

        for (Slide s : data)
        {
            if (s != null)
                deck.addSlide(s);
        }
        return deck;
    }


    public void clear(boolean animate)
    {
        data.clear();
        if (attachmentSelected != null)
            attachmentSelected.notifyDataSetChanged();


        if (attachmentViewStub.resolved())
        {
            attachmentViewStub.get().setVisibility(View.GONE);
            attachmentListener.onAttachmentChanged();
        }

    }

    public void cleanup()
    {
        data.clear();
        if (attachmentSelected != null)
            attachmentSelected.notifyDataSetChanged();

        if (attachmentViewStub.resolved())
        {
            attachmentViewStub.get().setVisibility(View.GONE);
            attachmentListener.onAttachmentChanged();
        }

    }

    public boolean isAttachmentPresent()
    {
        return attachmentViewStub.resolved() && attachmentViewStub.get().getVisibility() == View.VISIBLE;
    }


    public Uri getCaptureUri()
    {
        return null;
    }


}
