package cz.uhk.bak.mmsparams.mms;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;

public interface IncomingMmsConnection
{
    @Nullable
    byte[] retrieve(@NonNull String contentLocation, byte[] transactionId, long messageID, int subscriptionId) throws MmsException, MmsRadioException, ApnUnavailableException, IOException;
}
