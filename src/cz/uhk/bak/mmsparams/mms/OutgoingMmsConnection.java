package cz.uhk.bak.mmsparams.mms;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;


public interface OutgoingMmsConnection
{
    @Nullable
    byte[] send(@NonNull byte[] pduBytes, int subscriptionId) throws UndeliverableMessageException;
}
