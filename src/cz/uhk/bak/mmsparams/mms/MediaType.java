package cz.uhk.bak.mmsparams.mms;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import cz.uhk.bak.mmsparams.mms.slides.DocumentSlide;
import cz.uhk.bak.mmsparams.mms.slides.ImageSlide;
import cz.uhk.bak.mmsparams.util.MediaUtil;

public enum MediaType
{
    IMAGE, LOCATION, GIF, AUDIO, VIDEO, DOCUMENT;

    public
    @NonNull
    Slide createSlide(@NonNull Context context,
                      @NonNull Uri uri,
                      @Nullable String fileName,
                      @Nullable String mimeType,
                      long dataSize)
    {
        if (mimeType == null)
        {
            mimeType = "application/octet-stream";
        }

        switch (this)
        {
            case IMAGE:
                return new ImageSlide(context, uri, dataSize);
            case GIF:
                return new GifSlide(context, uri, dataSize);
            case AUDIO:
                return new AudioSlide(context, uri, dataSize, false);
            case VIDEO:
                return new VideoSlide(context, uri, dataSize);
            case DOCUMENT:
                return new DocumentSlide(context, uri, mimeType, dataSize, fileName);
            default:
                throw new AssertionError("unrecognized enum");
        }
    }

    public static
    @Nullable
    MediaType from(final @Nullable String mimeType)
    {
        if (TextUtils.isEmpty(mimeType)) return null;
        if (MediaUtil.isGif(mimeType)) return GIF;
        if (MediaUtil.isImageType(mimeType)) return IMAGE;
        if (MediaUtil.isAudioType(mimeType)) return AUDIO;
        if (MediaUtil.isVideoType(mimeType)) return VIDEO;

        return DOCUMENT;
    }

}
