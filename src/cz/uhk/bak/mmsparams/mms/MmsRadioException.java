package cz.uhk.bak.mmsparams.mms;

public class MmsRadioException extends Throwable
{
    public MmsRadioException(String s)
    {
        super(s);
    }
}
