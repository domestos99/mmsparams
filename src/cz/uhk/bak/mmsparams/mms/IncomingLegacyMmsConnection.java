/**
 * Copyright (C) 2015 Open Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.mms;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.mms.pdu_alt.MyRetrieveConf;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGetHC4;
import org.apache.http.client.methods.HttpUriRequest;

import java.io.IOException;
import java.util.Arrays;

import cz.uhk.bak.mmsparams.jobs.PduByteHelper;
import cz.uhk.bak.mmsparams.log.LogFB;


@SuppressWarnings("deprecation")
public class IncomingLegacyMmsConnection extends LegacyMmsConnection implements IncomingMmsConnection
{
    private static final String TAG = IncomingLegacyMmsConnection.class.getSimpleName();

    public IncomingLegacyMmsConnection(Context context) throws ApnUnavailableException
    {
        super(context);
    }

    private HttpUriRequest constructRequest(ApnExt contentApn, boolean useProxy) throws IOException
    {
        HttpGetHC4 request = new HttpGetHC4(contentApn.getMmsc());
        for (Header header : getBaseHeaders())
        {
            request.addHeader(header);
        }
        if (useProxy)
        {
            HttpHost proxy = new HttpHost(contentApn.getMmsproxy(), contentApn.getMmsport());
            request.setConfig(RequestConfig.custom().setProxy(proxy).build());
        }
        return request;
    }

    @Override
    public
    @Nullable
    byte[] retrieve(@NonNull String contentLocation,
                    byte[] transactionId, long messageID, int subscriptionId)
            throws MmsRadioException, ApnUnavailableException, IOException
    {
        MmsRadio radio = MmsRadio.getInstance(context);
//        Apn contentApn = new Apn(contentLocation, apn.getMmsproxy(), Integer.toString(apn.getMmsport()), apn.getUserName(), apn.getPassword());


        ApnExt contentApn = new ApnExt(contentLocation, apn);


        if (isDirectConnect())
        {
            LogFB.w(TAG, "Connecting directly...");
            try
            {
                return retrieve(contentApn, transactionId, messageID, false, false);
            }
            catch (IOException | ApnUnavailableException e)
            {
                LogFB.w(TAG, e);
            }
        }

        LogFB.w(TAG, "Changing radio to MMS mode..");
        radio.connect();

        try
        {
            LogFB.w(TAG, "Downloading in MMS mode with proxy...");

            try
            {
                return retrieve(contentApn, transactionId, messageID, true, true);
            }
            catch (IOException | ApnUnavailableException e)
            {
                LogFB.w(TAG, e);
            }

            LogFB.w(TAG, "Downloading in MMS mode without proxy...");

            return retrieve(contentApn, transactionId, messageID, true, false);

        }
        finally
        {
            if (radio != null)
                radio.disconnect();
        }
    }

    public byte[] retrieve(ApnExt contentApn, byte[] transactionId, long messageID, boolean usingMmsRadio, boolean useProxyIfAvailable)
            throws IOException, ApnUnavailableException
    {
        byte[] pdu = null;

        final boolean useProxy = useProxyIfAvailable && contentApn.hasProxy();
        final String targetHost = useProxy
                ? contentApn.getMmsproxy()
                : Uri.parse(contentApn.getMmsc()).getHost();
        if (checkRouteToHost(context, targetHost, usingMmsRadio))
        {
            LogFB.w(TAG, "got successful route to host " + targetHost);
            pdu = execute(constructRequest(contentApn, useProxy));
        }

        if (pdu == null)
        {
            throw new IOException("Connection manager could not obtain route to host.");
        }

        MyRetrieveConf retrieved = (MyRetrieveConf) PduByteHelper.getPdu(pdu); // new MyPduParser(pdu).parse();

        if (retrieved == null)
        {
            LogFB.w(TAG, "Couldn't parse PDU, byte response: " + Arrays.toString(pdu));
            LogFB.w(TAG, "Couldn't parse PDU, ASCII:         " + new String(pdu));
            throw new IOException("Bad retrieved PDU");
        }

//      sendRetrievedAcknowledgement(transactionId,messageID,  usingMmsRadio,useProxy);

        //  sendRetrievedAcknowledgement(transactionId, messageID, usingMmsRadio, useProxy);
        //  sendAcknowledge(transactionId, messageID, usingMmsRadio, useProxy);
        // sendReadReport(retrieved, transactionId, messageID, usingMmsRadio, useProxy);

        return pdu;
    }

}
