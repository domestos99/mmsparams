package cz.uhk.bak.mmsparams.mms.receive;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;

import com.android.mms.transaction.HttpUtils;
import com.android.mms.transaction.TransactionSettings;
import com.android.mms.util.SendingProgressTokenManager;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu_alt.AcknowledgeInd;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.klinker.android.send_message.Utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import cz.uhk.bak.mmsparams.jobs.PduByteHelper;
import cz.uhk.bak.mmsparams.log.LogFB;

public class MmsReceiveHandler
{
    private static final String TAG = MmsReceiveHandler.class.getSimpleName();

    private final Context context;


    public MmsReceiveHandler(Context context)
    {
        this.context = context;
    }

    public void send(byte[] retrieveBytes)
    {
        MyRetrieveConf retrieveConf = (MyRetrieveConf) PduByteHelper.getPdu(retrieveBytes); // new MyPduParser(retrieveBytes).parse();

        byte[] tranId = retrieveConf.getTransactionId();
        if (tranId != null)
        {
            // Create M-Acknowledge.ind
            AcknowledgeInd acknowledgeInd = null;
            try
            {
                acknowledgeInd = new AcknowledgeInd(PduHeaders.CURRENT_MMS_VERSION, tranId);

                // insert the 'from' address per spec
                String lineNumber = Utils.getMyPhoneNumber(context);
                acknowledgeInd.setFrom(new EncodedStringValue(lineNumber));

                // Pack M-Acknowledge.ind and send it
                if (com.android.mms.MmsConfig.getNotifyWapMMSC())
                {
                    sendPdu(context, PduByteHelper.getPduBytes(context, acknowledgeInd) /*, mContentLocation*/);
                }
                else
                {
                    sendPdu(context, PduByteHelper.getPduBytes(context, acknowledgeInd));
                }

                int k = 1;
            }
            catch (InvalidHeaderValueException e)
            {
                LogFB.e(TAG, "error", e);
            }
            catch (MmsException e)
            {
                LogFB.e(TAG, "error", e);
            }
            catch (IOException e)
            {
                LogFB.e(TAG, "error", e);
            }
        }
    }


    byte[] sendPdu(Context context, byte[] pdu, TransactionSettings transactionSettings) throws IOException, MmsException
    {
        return sendPdu(context, SendingProgressTokenManager.NO_TOKEN, pdu, transactionSettings);
    }

    byte[] sendPdu(Context context, byte[] pdu) throws IOException, MmsException
    {
        TransactionSettings transactionSettings = new TransactionSettings(context, null);
        return sendPdu(context, SendingProgressTokenManager.NO_TOKEN, pdu, transactionSettings);
    }


    private byte[] sendPdu(Context context, long token, byte[] pdu, TransactionSettings transactionSettings) throws IOException, MmsException
    {

        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final int result = connMgr.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "enableMMS");


        if (pdu == null)
        {
            throw new MmsException();
        }


        String mmscUrl = getMmsc(context);

        if (mmscUrl == null)
        {
            throw new IOException("Cannot establish route: mmscUrl is null");
        }

        if (com.android.mms.transaction.Transaction.useWifi(context))
        {
            return HttpUtils.httpConnection(
                    context, token,
                    mmscUrl,
                    pdu, HttpUtils.HTTP_POST_METHOD,
                    false, null, 0);
        }

        transactionSettings = new TransactionSettings("http://mms.o2active.cz:8002", "160.218.160.218", 8080);
        mmscUrl = transactionSettings.getMmscUrl();


        final TransactionSettings finalTransactionSettings = transactionSettings;
        final long finalToken = token;
        final Context finalContext = context;
        final String finalMmscUrl = mmscUrl;
        final byte[] finalPdu = pdu;

        ensureRouteToHost(mmscUrl, transactionSettings);
        return HttpUtils.httpConnection(
                context, token,
                mmscUrl,
                pdu, HttpUtils.HTTP_POST_METHOD,
                transactionSettings.isProxySet(),
                transactionSettings.getProxyAddress(),
                transactionSettings.getProxyPort());
    }

    private void ensureRouteToHost(String url, TransactionSettings settings) throws IOException
    {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        InetAddress inetAddr;
        if (settings.isProxySet())
        {
            String proxyAddr = settings.getProxyAddress();
            try
            {
                inetAddr = InetAddress.getByName(proxyAddr);
            }
            catch (UnknownHostException e)
            {
                throw new IOException("Cannot establish route for " + url +
                        ": Unknown proxy " + proxyAddr);
            }
//            if (!connMgr.requestRouteToHostAddress(ConnectivityManager.TYPE_MOBILE_MMS, inetAddr))
//            {
//                throw new IOException("Cannot establish route to proxy " + inetAddr);
//            }
        }
        else
        {
            Uri uri = Uri.parse(url);
            try
            {
                inetAddr = InetAddress.getByName(uri.getHost());
            }
            catch (UnknownHostException e)
            {
                throw new IOException("Cannot establish route for " + url + ": Unknown host");
            }
//            if (!connMgr.requestRouteToHostAddress(ConnectivityManager.TYPE_MOBILE_MMS, inetAddr))
//            {
//                throw new IOException("Cannot establish route to " + inetAddr + " for " + url);
//            }
        }
    }

    private String getMmsc(Context context)
    {

//        TransactionBundle args = (TransactionBundle) msg.obj;
//        TransactionSettings transactionSettings;
//
//        if (Log.isLoggable("TRANSACTION", Log.VERBOSE)) {
//            LogFB.v(TAG, "EVENT_TRANSACTION_REQUEST MmscUrl=" +
//                    args.getMmscUrl() + " proxy port: " + args.getProxyAddress());
//        }
//
//        // Set the connection settings for this transaction.
//        // If these have not been set in args, load the default settings.
//        String mmsc = args.getMmscUrl();
//        if (mmsc != null) {
//            transactionSettings = new TransactionSettings(
//                    mmsc, args.getProxyAddress(), args.getProxyPort());
//        } else {
//            transactionSettings = new TransactionSettings(
//                    context, null);
//        }
//
//
//
//        return "";

        return "http://mms";
    }


}
