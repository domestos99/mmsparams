package cz.uhk.bak.mmsparams.mms;

import android.content.Context;
import android.support.annotation.NonNull;

import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class MessageSizeHelper
{
    public static boolean allowLargeMms(@NonNull Context context)
    {
        String value = TextSecurePreferences.getMaxAttachSize(context);

        if (PduHeaderConvertor.ATTACH_SIZE_UNLIMITED.equals(value))
        {
            return true;
        }

        return false;
    }
}
