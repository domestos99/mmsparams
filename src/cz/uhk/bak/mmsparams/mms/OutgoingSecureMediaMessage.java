package cz.uhk.bak.mmsparams.mms;

import java.util.List;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.recipients.Recipients;

public class OutgoingSecureMediaMessage extends OutgoingMediaMessage
{

    public OutgoingSecureMediaMessage(Recipients recipients, String body,
                                      List<Attachment> attachments,

                                      long sentTimeMillis,
                                      int distributionType,
                                      long expiresIn)
    {
        super(recipients, body, attachments, sentTimeMillis, -1, expiresIn, distributionType);
    }

    public OutgoingSecureMediaMessage(OutgoingMediaMessage base)
    {
        super(base);
    }

    @Override
    public boolean isSecure()
    {
        return true;
    }
}
