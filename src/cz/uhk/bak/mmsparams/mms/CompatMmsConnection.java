package cz.uhk.bak.mmsparams.mms;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.PduHeaders;

import java.io.IOException;

import cz.uhk.bak.mmsparams.common.EmptyException;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsErrorDatabase;
import cz.uhk.bak.mmsparams.jobs.PduByteHelper;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.transport.UndeliverableMessageException;
import cz.uhk.bak.mmsparams.util.AndroidVersionUtil;
import cz.uhk.bak.mmsparams.util.mms.MmsErrorLogger;

public class CompatMmsConnection implements OutgoingMmsConnection, IncomingMmsConnection
{
    private static final String TAG = CompatMmsConnection.class.getSimpleName();

    private final Context context;
    private final MmsErrorLogger mmsErrorLogger;

    public CompatMmsConnection(final Context context, final MmsErrorLogger mmsErrorLogger)
    {
        this.context = context;
        this.mmsErrorLogger = mmsErrorLogger;
    }

    @Nullable
    @Override
    public byte[] send(@NonNull byte[] pduBytes, int subscriptionId)
            throws UndeliverableMessageException
    {
        if (AndroidVersionUtil.isHigherThanLollipopMR1())
        {
            try
            {
                LogFB.w(TAG, "Sending via Lollipop API");
                return new OutgoingLollipopMmsConnection(context).send(pduBytes, subscriptionId);
            }
            catch (UndeliverableMessageException e)
            {
                LogFB.w(TAG, e);
                mmsErrorLogger.log(TAG, e);
            }

            LogFB.w(TAG, "Falling back to legacy connection...");
        }

        if (!AndroidVersionUtil.isHigherThanLollipopMR1() && subscriptionId == -1)
        {
            LogFB.w(TAG, "Sending via legacy connection");
            try
            {
                byte[] result = new OutgoingLegacyMmsConnection(context).send(pduBytes, subscriptionId);

                GenericPdu pduResult = PduByteHelper.getPdu(result); // new MyPduParser(result).parse();

                if (pduResult == null || pduResult.getMessageType() != PduHeaders.MESSAGE_TYPE_SEND_CONF)
                {
                    return null;
                }


                MySendConf resultObj = (MySendConf) pduResult;
                if (result != null && resultObj.getResponseStatus() == PduHeaders.RESPONSE_STATUS_OK)
                {
                    return result;
                }
                else
                {
                    String msg = "Got bad legacy response: " + (result != null ? resultObj.getResponseStatus() : null);
                    LogFB.w(TAG, msg);
                    mmsErrorLogger.log(TAG, new EmptyException(msg));
                }
            }
            catch (UndeliverableMessageException | ApnUnavailableException e)
            {
                LogFB.w(TAG, e);
                mmsErrorLogger.log(TAG, e);
            }
        }

        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP && VERSION.SDK_INT < VERSION_CODES.LOLLIPOP_MR1)
        {
            LogFB.w(TAG, "Falling back to sending via Lollipop API");
            return new OutgoingLollipopMmsConnection(context).send(pduBytes, subscriptionId);
        }

        throw new UndeliverableMessageException("Both lollipop and legacy connections failed...");
    }

    @Nullable
    @Override
    public byte[] retrieve(@NonNull String contentLocation,
                           byte[] transactionId,
                           long messageID,
                           int subscriptionId)
            throws MmsException, MmsRadioException, ApnUnavailableException, IOException
    {
        if (AndroidVersionUtil.isHigherThanLollipopMR1())
        {
            LogFB.w(TAG, "Receiving via Lollipop API");
            try
            {
                return new IncomingLollipopMmsConnection(context).retrieve(contentLocation, transactionId, messageID, subscriptionId);
            }
            catch (MmsException e)
            {
                LogFB.w(TAG, e);
            }

            LogFB.w(TAG, "Falling back to receiving via legacy connection");
        }

        if (VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1 || subscriptionId == -1)
        {
            LogFB.w(TAG, "Receiving via legacy API");
            try
            {
                return new IncomingLegacyMmsConnection(context).retrieve(contentLocation, transactionId, messageID, subscriptionId);
            }
            catch (MmsRadioException | ApnUnavailableException | IOException e)
            {
                LogFB.w(TAG, e);
            }
        }

        if (AndroidVersionUtil.isHigherThanLollipop() && VERSION.SDK_INT < VERSION_CODES.LOLLIPOP_MR1)
        {
            LogFB.w(TAG, "Falling back to receiving via Lollipop API");
            return new IncomingLollipopMmsConnection(context).retrieve(contentLocation, transactionId, messageID, subscriptionId);
        }

        throw new IOException("Both lollipop and fallback APIs failed...");
    }
}
