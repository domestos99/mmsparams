package cz.uhk.bak.mmsparams.mms;

import java.util.LinkedList;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.database.ThreadDatabase;
import cz.uhk.bak.mmsparams.recipients.Recipients;

public class OutgoingExpirationUpdateMessage extends OutgoingSecureMediaMessage
{

    public OutgoingExpirationUpdateMessage(Recipients recipients, long sentTimeMillis, long expiresIn)
    {
        super(recipients, "", new LinkedList<Attachment>(), sentTimeMillis,
                ThreadDatabase.DistributionTypes.CONVERSATION, expiresIn);
    }

    @Override
    public boolean isExpirationUpdate()
    {
        return true;
    }

}
