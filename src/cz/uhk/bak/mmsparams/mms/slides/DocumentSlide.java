package cz.uhk.bak.mmsparams.mms.slides;


import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.Slide;

public class DocumentSlide extends Slide
{

    public DocumentSlide(@NonNull Context context, @NonNull Attachment attachment)
    {
        super(context, attachment, MediaType.DOCUMENT);
    }

    public DocumentSlide(@NonNull Context context, @NonNull Uri uri,
                         @NonNull String contentType, long size,
                         @Nullable String fileName)
    {
        super(context, constructAttachmentFromUri(context, uri, contentType, size, true, fileName, false), MediaType.DOCUMENT);
    }

    @Override
    public boolean hasDocument()
    {
        return true;
    }

}
