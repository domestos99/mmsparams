//package cz.uhk.bak.mmsparams.mms.slides;
//
//import android.content.Context;
//import android.net.Uri;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//
//import cz.uhk.bak.mmsparams.attachments.Attachment;
//import cz.uhk.bak.mmsparams.database.AttachmentDatabase;
//import cz.uhk.bak.mmsparams.database.model.MmsSetting;
//import cz.uhk.bak.mmsparams.mms.ApnExt;
//import cz.uhk.bak.mmsparams.mms.MediaType;
//import cz.uhk.bak.mmsparams.mms.Slide;
//
//public class MmsSettingSlide extends Slide
//{
//
//    public static final String TAG = MmsSettingSlide.class.getSimpleName();
//
//    private final MmsSetting mmsSetting;
//    public MmsSettingSlide(@NonNull Context context, @NonNull MmsSetting mmsSetting)
//    {
//        super(context, null, MediaType.MMS_SETTING);
//        this.mmsSetting = mmsSetting;
//    }
//
//    public MmsSetting getMmsSetting()
//    {
//        return mmsSetting;
//    }
//
//
//
//    @Override
//    public String getContentType()
//    {
//        return TAG;
//    }
//
//    @Override
//    public long getTransferState()
//    {
//        return AttachmentDatabase.TRANSFER_PROGRESS_DONE;
//    }
//
//    @Nullable
//    public Uri getUri()
//    {
//        return null;
//    }
//
//    @Nullable
//    public Uri getThumbnailUri()
//    {
//        return null;
//    }
//}
