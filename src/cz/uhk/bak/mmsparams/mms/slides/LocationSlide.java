package cz.uhk.bak.mmsparams.mms.slides;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import org.whispersystems.libsignal.util.guava.Optional;

import cz.uhk.bak.mmsparams.components.location.SignalPlace;
import cz.uhk.bak.mmsparams.mms.MediaType;

public class LocationSlide extends ImageSlide
{

    @NonNull
    private final SignalPlace place;

    public LocationSlide(@NonNull Context context, @NonNull Uri uri, long size, @NonNull SignalPlace place)
    {
        super(context, uri, size, MediaType.LOCATION);
        this.place = place;
    }

    @Override
    @NonNull
    public Optional<String> getBody()
    {
        return Optional.of(place.getDescription());
    }

    @NonNull
    public SignalPlace getPlace()
    {
        return place;
    }

    @Override
    public boolean hasLocation()
    {
        return true;
    }

}
