/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.mms.slides;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import cz.uhk.bak.mmsparams.R;
import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.mms.MediaType;
import cz.uhk.bak.mmsparams.mms.Slide;
import cz.uhk.bak.mmsparams.util.MediaUtil;

public class ImageSlide extends Slide
{

    private static final String TAG = ImageSlide.class.getSimpleName();

    public ImageSlide(@NonNull Context context, @NonNull Attachment attachment)
    {
        super(context, attachment, MediaType.IMAGE);
    }

    public ImageSlide(Context context, Uri uri, long size)
    {
        super(context, constructAttachmentFromUri(context, uri, MediaUtil.IMAGE_JPEG, size, true, null, false), MediaType.IMAGE);
    }


    protected ImageSlide(Context context, Uri uri, long size, MediaType mediaType)
    {
        super(context, constructAttachmentFromUri(context, uri, MediaUtil.IMAGE_JPEG, size, true, null, false), mediaType);
    }

    @Override
    public
    @DrawableRes
    int getPlaceholderRes(Theme theme)
    {
        return 0;
    }

    @Override
    public boolean hasImage()
    {
        return true;
    }

    @NonNull
    @Override
    public String getContentDescription()
    {
        return context.getString(R.string.Slide_image);
    }
}
