package cz.uhk.bak.mmsparams.mms;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.mms.slides.ImageSlide;
import cz.uhk.bak.mmsparams.util.MediaUtil;

public class GifSlide extends ImageSlide
{

    public GifSlide(Context context, Attachment attachment)
    {
        super(context, attachment);
    }

    public GifSlide(Context context, Uri uri, long size)
    {
        super(context, constructAttachmentFromUri(context, uri, MediaUtil.IMAGE_GIF, size, true, null, false));
    }

    @Override
    @Nullable
    public Uri getThumbnailUri()
    {
        return getUri();
    }
}
