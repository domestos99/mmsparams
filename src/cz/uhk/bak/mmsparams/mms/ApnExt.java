package cz.uhk.bak.mmsparams.mms;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import java.io.Serializable;

import cz.uhk.bak.mmsparams.database.IDBModel;

public class ApnExt implements IDBModel<ApnExt>, Serializable
{
//    public static ApnExt EMPTY = new ApnExt();

    private int id;
    private String name;
    private String carrier;
    private String apn;

    private String mmsc;
    private String mmsproxy;
    private String mmsport;

    private String username;
    private String password;


    private int mcc;
    private int mnc;
    private String type;

    private boolean isSystem;

    // 0 is for no authentication
// 1 is for PAP authentication
// 2 is for CHAP authentication
// 3 is for CHAP or PAP, whatever is available
    private String authtype;

    private ApnExt()
    {
        id = -1;
    }

    public ApnExt(int id, String name, String carrier, String apn, int mcc, int mnc, String mmsc, String mmsproxy,
                  String mmsport, String username, String password, String type, String authtype, boolean isSystem)
    {
        this.id = id;
        this.name = name;
        this.carrier = carrier;
        this.apn = apn;
        this.mcc = mcc;
        this.mnc = mnc;
        this.mmsc = mmsc;
        this.mmsproxy = mmsproxy;
        this.mmsport = mmsport;
        this.username = username;
        this.password = password;
        this.type = type;
        this.authtype = authtype;
        this.isSystem = isSystem;
    }

    public ApnExt(String contentLocation, ApnExt apn)
    {
//        Apn contentApn = new Apn(contentLocation, apn.getMmsproxy(), Integer.toString(apn.getMmsport()), apn.getUserName(), apn.getPassword());
        this(apn.id, apn.name, apn.carrier, apn.apn, apn.mcc, apn.mnc,
                contentLocation, // mmsc
                apn.mmsproxy, apn.mmsport, apn.username, apn.password, apn.type, apn.authtype, apn.isSystem);


    }


    public ApnExt(ApnExt apn)
    {
//        Apn contentApn = new Apn(contentLocation, apn.getMmsproxy(), Integer.toString(apn.getMmsport()), apn.getUserName(), apn.getPassword());
        this(apn.id, apn.name, apn.carrier, apn.apn, apn.mcc, apn.mnc,
                apn.mmsc, // mmsc
                apn.mmsproxy, apn.mmsport, apn.username, apn.password, apn.type, apn.authtype, apn.isSystem);


    }


    @Override
    public int getID()
    {
        return this.id;
    }

    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {

    }

    @Override
    public void setUpdatedDT(long dt)
    {

    }

    public String getName()
    {
        return this.name;
    }

    public static ApnExt createNew(Context context)
    {
        ApnExt apnNew = ApnUtils.loadApnsGetDefault(context);

        if (apnNew == null)
        {
            apnNew = new ApnExt();
        }

        apnNew.name = "?";
        apnNew.isSystem = false;

        return apnNew;
    }

    public String getInfo()
    {

//        String line1 = "DR: " + deliveryReport + " RR: " + readReport;
//        String line2 = "Msg Class: " + PduHeaderConvertor.getMessageClassString(getMessage_class());
//        String line3 = "Msg Prio: " + PduHeaderConvertor.getPriorityString(getPriority());
//        String line4 = "Exp: " + getExpiryTime();
//
//
//        return line1 + "\n" + line2 + "\n" + line3 + "\n" + line4;
        return this.toString();
    }

    public String getBloJson()
    {
        return null;
    }


//    public ApnExt(ApnExt customApn, ApnExt defaultApn,
//                  boolean useCustomMmsc,
//                  boolean useCustomProxy,
//                  boolean useCustomProxyPort,
//                  boolean useCustomUsername,
//                  boolean useCustomPassword)
//    {
//        this.mmsc = useCustomMmsc ? customApn.mmsc : defaultApn.mmsc;
//        this.mmsproxy = useCustomProxy ? customApn.mmsproxy : defaultApn.mmsproxy;
//        this.mmsport = useCustomProxyPort ? customApn.mmsport : defaultApn.mmsport;
//        this.username = useCustomUsername ? customApn.username : defaultApn.username;
//        this.password = useCustomPassword ? customApn.password : defaultApn.password;
//    }


    public static class MetaData
    {
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String CARRIER = "carrier";
        public static final String APN = "apn";
        //        public static final String MCC = "mcc";
//        public static final String MNC = "mnc";
        public static final String MMSC = "mmsc";
        public static final String MMS_PROXY = "mmsproxy";
        public static final String MMS_PORT = "mmsport";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
//        public static final String TYPE = "type";
//        public static final String AUTHTYPE = "authtype";

    }


    public static ApnExt getFromCursor(Cursor cursor, boolean isSystem)
    {
        ApnExt apn = new ApnExt();

        apn.id = cursor.getInt(cursor.getColumnIndexOrThrow(ApnExt.MetaData.ID));

        apn.name = cursor.getString(cursor.getColumnIndexOrThrow(ApnExt.MetaData.NAME));
        apn.carrier = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.CARRIER));
        apn.apn = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.APN));
//        apn.mcc = cursor.getInt(cursor.getColumnIndexOrThrow(MetaData.MCC));
//        apn.mnc = cursor.getInt(cursor.getColumnIndexOrThrow(MetaData.MNC));
        apn.mmsc = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MMSC));
        apn.mmsproxy = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MMS_PROXY));
        apn.mmsport = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MMS_PORT));
        apn.username = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.USERNAME));
        apn.password = cursor.getString(cursor.getColumnIndexOrThrow(ApnExt.MetaData.PASSWORD));
//        apn.type = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.TYPE));
//        apn.authtype = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.AUTHTYPE));
        apn.isSystem = isSystem;


        return apn;
    }

    @Override
    public ContentValues getContentValues(ApnExt instance)
    {
        ContentValues contentValues = new ContentValues();

//        if (instance.getID() != -1)
//            contentValues.put(MmsSetting.MetaData.ID, instance.id);
        contentValues.put(ApnExt.MetaData.NAME, instance.getName());
        contentValues.put(ApnExt.MetaData.CARRIER, instance.getCarrier());
        contentValues.put(ApnExt.MetaData.APN, instance.getApn());
        contentValues.put(ApnExt.MetaData.MMSC, instance.getMmsc());
        contentValues.put(ApnExt.MetaData.MMS_PROXY, instance.getMmsproxy());
        contentValues.put(ApnExt.MetaData.MMS_PORT, instance.getMmsport());
        contentValues.put(ApnExt.MetaData.USERNAME, instance.getUserName());
        contentValues.put(ApnExt.MetaData.PASSWORD, instance.getPassword());

        return contentValues;
    }

    public String getCarrier()
    {
        return carrier;
    }

    public String getApn()
    {
        return apn;
    }

    public int getMcc()
    {
        return mcc;
    }

    public int getMnc()
    {
        return mnc;
    }

    public String getMmsc()
    {
        return mmsc;
    }

    public String getMmsproxy()
    {
        return mmsproxy;
    }


    public int getMmsport()
    {
        return TextUtils.isEmpty(mmsport) ? 80 : Integer.parseInt(mmsport);
    }

    public String getUserName()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public String getType()
    {
        return type;
    }

    public String getAuthtype()
    {
        return authtype;
    }


    @Override
    public String toString()
    {
        return "ApnExt{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", carrier='" + carrier + '\'' +
                ", apn='" + apn + '\'' +
                ", mmsc='" + mmsc + '\'' +
                ", mmsproxy='" + mmsproxy + '\'' +
                ", mmsport='" + mmsport + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", mcc=" + mcc +
                ", mnc=" + mnc +
                ", type='" + type + '\'' +
                ", isSystem=" + isSystem +
                ", authtype='" + authtype + '\'' +
                '}';
    }

    public boolean isEqual(ApnExt apn)
    {
        return this.mmsc.equals(apn.mmsc) && this.mmsport.equals(apn.mmsport) && this.mmsproxy.equals(apn.mmsproxy);
    }

    public boolean isEqual(String mmsc, String proxy, String port)
    {
        return this.mmsc.equals(mmsc) && this.mmsproxy.equals(proxy) && this.mmsport.equals(port);
    }

    public boolean hasAuthentication()
    {
        return !TextUtils.isEmpty(username);
    }

    public boolean hasProxy()
    {
        return !TextUtils.isEmpty(mmsproxy);
    }

//    public static ApnExt getCustomApn(Context context)
//    {
//
//        Optional<ApnExt> optApn = ApnUtils.loadApnsGetDefault(context);
//
//        ApnExt defaultApn = optApn.get();
//
//
//        String mmsc = TextSecurePreferences.getMmscUrl(context).trim();
//
//        if (!TextUtils.isEmpty(mmsc) && !mmsc.startsWith("http"))
//            mmsc = "http://" + mmsc;
//
//        String proxy = TextSecurePreferences.getMmscProxy(context);
//        String port = TextSecurePreferences.getMmscProxyPort(context);
//        String user = TextSecurePreferences.getMmscUsername(context);
//        String pass = TextSecurePreferences.getMmscPassword(context);
//
//
//        defaultApn.mmsc = mmsc;
//        defaultApn.mmsproxy = proxy;
//        defaultApn.mmsport = port;
//        defaultApn.username = user;
//        defaultApn.password = pass;
//
//        return defaultApn;
//
//    }


    public void setName(String name)
    {
        this.name = name;
    }

    public void setCarrier(String carrier)
    {
        this.carrier = carrier;
    }

    public void setApn(String apn)
    {
        this.apn = apn;
    }

    public void setMmsc(String mmsc)
    {
        this.mmsc = mmsc;
    }

    public void setMmsproxy(String mmsproxy)
    {
        this.mmsproxy = mmsproxy;
    }

    public void setMmsport(String mmsport)
    {
        this.mmsport = mmsport;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isSystem()
    {
        return isSystem;
    }
}
