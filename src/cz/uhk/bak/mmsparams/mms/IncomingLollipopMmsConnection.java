/**
 * Copyright (C) 2015 Open Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.uhk.bak.mmsparams.mms;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;

import com.android.mms.service_alt.MmsConfig;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.providers.MmsBodyProvider;
import cz.uhk.bak.mmsparams.util.AndroidVersionUtil;
import cz.uhk.bak.mmsparams.util.Util;

public class IncomingLollipopMmsConnection extends LollipopMmsConnection implements IncomingMmsConnection
{

    public static final String ACTION = IncomingLollipopMmsConnection.class.getCanonicalName() + "MMS_DOWNLOADED_ACTION";
    private static final String TAG = IncomingLollipopMmsConnection.class.getSimpleName();

    public IncomingLollipopMmsConnection(Context context)
    {
        super(context, ACTION);
    }

    @TargetApi(VERSION_CODES.LOLLIPOP)
    @Override
    public synchronized void onResult(Context context, Intent intent)
    {
        if (AndroidVersionUtil.isHigherThanLollipopMR1())
        {
            LogFB.w(TAG, "HTTP status: " + intent.getIntExtra(SmsManager.EXTRA_MMS_HTTP_STATUS, -1));
        }
        LogFB.w(TAG, "code: " + getResultCode() + ", result string: " + getResultData());
    }

    @Override
    @TargetApi(VERSION_CODES.LOLLIPOP)
    public synchronized
    @Nullable
    byte[] retrieve(@NonNull String contentLocation,
                    byte[] transactionId, long messageID,
                    int subscriptionId) throws MmsException
    {
        beginTransaction();

        try
        {
            MmsBodyProvider.Pointer pointer = MmsBodyProvider.makeTemporaryPointer(getContext());

            LogFB.w(TAG, "downloading multimedia from " + contentLocation + " to " + pointer.getUri());

            SmsManager smsManager;

            if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP_MR1 && subscriptionId != -1)
            {
                smsManager = SmsManager.getSmsManagerForSubscriptionId(subscriptionId);
            }
            else
            {
                smsManager = SmsManager.getDefault();
            }


            Bundle configOverrides = new Bundle();
            configOverrides.putBoolean(SmsManager.MMS_CONFIG_MMS_DELIVERY_REPORT_ENABLED, true);

            configOverrides.putBoolean(SmsManager.MMS_CONFIG_MMS_READ_REPORT_ENABLED, true);

            MmsConfig mmsConfig = MmsConfigManager.getMmsConfig(getContext(), subscriptionId);

            MmsConfig.Overridden overridden = new MmsConfig.Overridden(mmsConfig, configOverrides);

            boolean dlr = overridden.isMmsDeliveryReportsEnabled();
            boolean dlrr = overridden.isMmsReadReportsEnabled();


//            MmsConfig.Overridden overridden = new MmsConfig.Overridden(mmsConfig, new Bundle());

            smsManager.downloadMultimediaMessage(getContext(),
                    contentLocation,
                    pointer.getUri(),
                    configOverrides,
                    getPendingIntent());


            waitForResult();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Util.copy(pointer.getInputStream(), baos);
            pointer.close();


            LogFB.w(TAG, baos.size() + "-byte response: ");// + Hex.dump(baos.toByteArray()));

            return baos.toByteArray();
        }
        catch (IOException | TimeoutException e)
        {
            LogFB.w(TAG, e);
            throw new MmsException(e);
        }
        finally
        {
            endTransaction();
        }
    }
}
