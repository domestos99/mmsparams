package cz.uhk.bak.mmsparams.mms;

import android.view.View;

public interface SlideClickListener
{
    void onClick(View v, Slide slide);
}
