package cz.uhk.bak.mmsparams.mms.listeners;

public interface AttachmentListener
{
    void onAttachmentChanged();
}