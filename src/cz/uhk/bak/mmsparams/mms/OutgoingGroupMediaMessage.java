package cz.uhk.bak.mmsparams.mms;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.whispersystems.signalservice.internal.push.SignalServiceProtos.GroupContext;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import cz.uhk.bak.mmsparams.attachments.Attachment;
import cz.uhk.bak.mmsparams.database.ThreadDatabase;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.util.Base64;

public class OutgoingGroupMediaMessage extends OutgoingSecureMediaMessage
{

    private final GroupContext group;

    public OutgoingGroupMediaMessage(@NonNull Recipients recipients,
                                     @NonNull String encodedGroupContext,
                                     @NonNull List<Attachment> avatar,
                                     long sentTimeMillis,
                                     long expiresIn)
            throws IOException
    {
        super(recipients, encodedGroupContext, avatar, sentTimeMillis,
                ThreadDatabase.DistributionTypes.CONVERSATION, expiresIn);

        this.group = GroupContext.parseFrom(Base64.decode(encodedGroupContext));
    }

    public OutgoingGroupMediaMessage(@NonNull Recipients recipients,
                                     @NonNull GroupContext group,
                                     @Nullable final Attachment avatar,
                                     long sentTimeMillis,
                                     long expireIn)
    {
        super(recipients, Base64.encodeBytes(group.toByteArray()),
                new LinkedList<Attachment>()
                {{
                    if (avatar != null) add(avatar);
                }},
                System.currentTimeMillis(),
                ThreadDatabase.DistributionTypes.CONVERSATION, expireIn);

        this.group = group;
    }

    @Override
    public boolean isGroup()
    {
        return true;
    }

    public boolean isGroupUpdate()
    {
        return group.getType().getNumber() == GroupContext.Type.UPDATE_VALUE;
    }

    public boolean isGroupQuit()
    {
        return group.getType().getNumber() == GroupContext.Type.QUIT_VALUE;
    }

    public GroupContext getGroupContext()
    {
        return group;
    }
}
