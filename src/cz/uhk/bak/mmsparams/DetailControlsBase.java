package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;

public abstract class DetailControlsBase<T>
{
    protected final BaseFragment baseFragment;
    protected final Context context;
    protected final FragmentManager fragmentManager;
    protected final View view;

    protected DetailControlsBase(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        this.baseFragment = baseFragment;
        this.context = baseFragment.getApplicationContext();
        this.fragmentManager = fragmentManager;
        this.view = view;

        beforeBaseInit();
        baseInit(view);
        afterBaseInit();
    }


    protected abstract T updateModelFromControls(T obj);

    protected abstract void fillControls(T obj);


    protected void beforeBaseInit()
    {
    }

    protected void afterBaseInit()
    {
    }


    protected final void baseInit(final View view)
    {
        initControls(view);
        setupListeners();
    }

    protected abstract void initControls(final View view);

    protected abstract void setupListeners();

}
