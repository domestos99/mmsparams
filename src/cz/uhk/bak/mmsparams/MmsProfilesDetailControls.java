package cz.uhk.bak.mmsparams;


import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cz.uhk.bak.mmsparams.components.relativeTimePicker.DateTimePair;
import cz.uhk.bak.mmsparams.components.relativeTimePicker.SlideRelativeTimePicker;
import cz.uhk.bak.mmsparams.components.relativeTimePicker.SlideRelativeTimePickerListener;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsProfilesDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsProfile;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsProfilesDetailControls extends DetailControlsBase<MmsProfile>
{

    EditText tbName;
    CheckBox chbDeliveryReport, chbReadReport, chbAllowSetFrom, chbDrmContent, chbAdaptationAllowd;
    RadioButton rbMsgClsPersonal, rbMsgClsAdvert, rbMsgClsInforma, rbMsgClsAuto, rbPrioLow, rbPrioNormal, rbPrioHigh;
    Button btnPickDeliveryTime, btnPickExpiry;
    EditText tvExpiryPicked, tvDeliveryTimePicked;
    long expiry, deliveryTime;

    private CheckBox chbAllowSendDeliveryReport, chbAllowSendReadReport, chbMmsAutoDownload, chbSendRRIfNotRequired;
    private RadioButton rbMaxAttachSize300, rbMaxAttachSizeUnlimited;

    final SimpleDateFormat mFormatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");


    public MmsProfilesDetailControls(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }

    @Override
    protected void initControls(View view)
    {
        tbName = ViewUtil.findById(view, R.id.tbName);
        chbDeliveryReport = ViewUtil.findById(view, R.id.chbDeliveryReport);
        chbReadReport = ViewUtil.findById(view, R.id.chbReadReport);
        rbMsgClsPersonal = ViewUtil.findById(view, R.id.rbMsgClsPersonal);
        rbMsgClsAdvert = ViewUtil.findById(view, R.id.rbMsgClsAdvert);
        rbMsgClsInforma = ViewUtil.findById(view, R.id.rbMsgClsInforma);
        rbMsgClsAuto = ViewUtil.findById(view, R.id.rbMsgClsAuto);
        rbPrioLow = ViewUtil.findById(view, R.id.rbPrioLow);
        rbPrioNormal = ViewUtil.findById(view, R.id.rbPrioNormal);
        rbPrioHigh = ViewUtil.findById(view, R.id.rbPrioHigh);
        chbAllowSetFrom = ViewUtil.findById(view, R.id.chbAllowSetFrom);

        tvExpiryPicked = ViewUtil.findById(view, R.id.tvExpiryPicked);
        tvDeliveryTimePicked = ViewUtil.findById(view, R.id.tvDeliveryTimePicked);

        btnPickDeliveryTime = ViewUtil.findById(view, R.id.btnPickDeliveryTime);
        btnPickExpiry = ViewUtil.findById(view, R.id.btnPickExpiry);


        chbAllowSendDeliveryReport = ViewUtil.findById(view, R.id.chbAllowSendDeliveryReport);
        chbAllowSendReadReport = ViewUtil.findById(view, R.id.chbAllowSendReadReport);
        chbMmsAutoDownload = ViewUtil.findById(view, R.id.chbMmsAutoDownload);
        rbMaxAttachSize300 = ViewUtil.findById(view, R.id.rbMaxAttachSize300);
        rbMaxAttachSizeUnlimited = ViewUtil.findById(view, R.id.rbMaxAttachSizeUnlimited);
        chbDrmContent = ViewUtil.findById(view, R.id.chbDrmContent);
        chbAdaptationAllowd = ViewUtil.findById(view, R.id.chbAdaptationAllowd);
        chbSendRRIfNotRequired = ViewUtil.findById(view, R.id.chbSendRRIfNotRequired);
    }

    @Override
    protected void setupListeners()
    {
        chbAllowSendReadReport.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                setSensitive();
            }
        });


    }

    private void setSensitive()
    {
        if (chbAllowSendReadReport.isChecked())
        {
            chbSendRRIfNotRequired.setEnabled(true);
        }
        else
        {
            chbSendRRIfNotRequired.setEnabled(false);
        }
    }

    private Date getDatePlusDays()
    {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 7);

        return c.getTime();
    }


    public MmsProfile updateModelFromControls(MmsProfile obj)
    {
        MmsProfile settings = new MmsProfile(obj);


        settings.setName(tbName.getText().toString());

        settings.setDeliveryReport(chbDeliveryReport.isChecked());
        settings.setReadReport(chbReadReport.isChecked());

        settings.setSenderVisibility(chbAllowSetFrom.isChecked());

        settings.setExpiryTime(MmsProfile.DEFAULT_EXPIRY_TIME);

        settings.setPriority(PduHeaderConvertor.resolvePriority(rbPrioLow, rbPrioNormal, rbPrioHigh));
        settings.setMessage_class(PduHeaderConvertor.resolveMessageCls(rbMsgClsPersonal, rbMsgClsAdvert, rbMsgClsInforma, rbMsgClsAuto));

        settings.setExpiryTime(this.expiry);
        settings.setDeliveryTime(this.deliveryTime);

        settings.setAllowSendDR(chbAllowSendDeliveryReport.isChecked());
        settings.setAllowSendRR(chbAllowSendReadReport.isChecked());
        settings.setAutoDownload(chbMmsAutoDownload.isChecked());
        settings.setDrmContent(chbDrmContent.isChecked());
        settings.setAdaptationAllowed(chbAdaptationAllowd.isChecked());

        settings.setSendRrIfNotRequired(chbSendRRIfNotRequired.isChecked());

        settings.setMaxAttachSize(PduHeaderConvertor.resolveMaxAttachSize(rbMaxAttachSize300, rbMaxAttachSizeUnlimited));

        return settings;
    }


    public void fillControls(MmsProfile obj)
    {
        tbName.setText(obj.getName());

        chbDeliveryReport.setChecked(obj.isDeliveryReport());
        chbReadReport.setChecked(obj.isReadReport());

        chbAllowSetFrom.setChecked(obj.getSenderVisibility());


        PduHeaderConvertor.setPriority(obj.getPriority(), rbPrioLow, rbPrioNormal, rbPrioHigh);
        PduHeaderConvertor.setMessageCls(obj.getMessage_class(), rbMsgClsPersonal, rbMsgClsAdvert, rbMsgClsInforma, rbMsgClsAuto);

        this.expiry = obj.getExpiryTime();
        this.deliveryTime = obj.getDeliveryTime();


        this.chbAllowSendDeliveryReport.setChecked(obj.isAllowSendDR());
        this.chbAllowSendReadReport.setChecked(obj.isAllowSendRR());
        this.chbMmsAutoDownload.setChecked(obj.isAutoDownload());

        this.chbSendRRIfNotRequired.setChecked(obj.isSendRRIfNotRequired());

        this.chbDrmContent.setChecked(obj.isDrmContent());
        this.chbAdaptationAllowd.setChecked(obj.isAdaptationAllowed());

        setMaxAttachSize(obj.getMaxAttachSize());


        setPickedExpiry(new DateTimePair(this.expiry));
        setPickedDeliveryTime(new DateTimePair(this.deliveryTime));


        final SlideRelativeTimePickerListener listenerDeliveryTime = new SlideRelativeTimePickerListener()
        {

            @Override
            public void onDateTimeSet(DateTimePair dateTimePair)
            {
                setPickedDeliveryTime(dateTimePair);
            }
        };

        btnPickDeliveryTime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new SlideRelativeTimePicker.Builder(fragmentManager)
                        .setListener(listenerDeliveryTime)
                        .setInitialDate(new DateTimePair(deliveryTime))
                        .setDefaultDate(new DateTimePair(0, 0, 0, 0))
                        .build().show();
            }
        });


        final SlideRelativeTimePickerListener listenerExpiry = new SlideRelativeTimePickerListener()
        {

            @Override
            public void onDateTimeSet(DateTimePair dateTimePair)
            {
                setPickedExpiry(dateTimePair);
            }
        };

        btnPickExpiry.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new SlideRelativeTimePicker.Builder(fragmentManager)
                        .setListener(listenerExpiry)
                        .setInitialDate(new DateTimePair(expiry))
                        .setDefaultDate(new DateTimePair(7, 0, 0, 0))
                        .build().show();
            }
        });


        setSensitive();
    }


    public boolean isControlsEdited(MmsProfile obj)
    {
        if (!tbName.getText().toString().equals(obj.getName()))
            return true;

        if (chbDeliveryReport.isChecked() != obj.isDeliveryReport())
            return true;

        if (chbReadReport.isChecked() != obj.isReadReport())
            return true;

        if (chbAllowSetFrom.isChecked() != obj.getSenderVisibility())
            return true;

        if (expiry != obj.getExpiryTime())
            return true;

        if (deliveryTime != obj.getDeliveryTime())
            return true;


        if (!PduHeaderConvertor.isSelectPriorityEqual(obj.getPriority(), rbPrioLow, rbPrioNormal, rbPrioHigh))
            return true;

        if (!PduHeaderConvertor.isSelectMsgClassEqual(obj.getMessage_class(), rbMsgClsPersonal, rbMsgClsAdvert, rbMsgClsInforma, rbMsgClsAuto))
            return true;


        return false;
    }

    public MmsProfile handleChangeAndSave(MmsProfile mmsProfile)
    {
        if (isControlsEdited(mmsProfile))
        {
            MmsProfile aaa = updateModelFromControls(mmsProfile);

            aaa.setIsVirtual(true);

            MmsProfilesDatabase db = DatabaseFactory.getMmsProfileDatabase(context);

            db.insert(aaa);
            return aaa;
        }
        else
        {
            return new MmsProfile(mmsProfile);
        }

    }


    public void setPickedDeliveryTime(DateTimePair dtp)
    {
        this.deliveryTime = dtp.getTotalSeconds();
        tvDeliveryTimePicked.setText(dtp.getString());
    }

    public void setPickedExpiry(DateTimePair dtp)
    {
        this.expiry = dtp.getTotalSeconds();
        tvExpiryPicked.setText(dtp.getString());
    }

    public void setMaxAttachSize(String maxAttachSize)
    {
        if (PduHeaderConvertor.ATTACH_SIZE_300.equals(maxAttachSize))
        {
            rbMaxAttachSize300.setChecked(true);
        }
        else if (PduHeaderConvertor.ATTACH_SIZE_UNLIMITED.equals(maxAttachSize))
        {
            rbMaxAttachSizeUnlimited.setChecked(true);
        }


    }


}

