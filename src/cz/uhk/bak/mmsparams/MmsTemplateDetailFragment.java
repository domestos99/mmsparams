package cz.uhk.bak.mmsparams;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.klinker.android.send_message.Utils;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsTemplateDatabase;
import cz.uhk.bak.mmsparams.database.SmsDatabase;
import cz.uhk.bak.mmsparams.database.ThreadDatabase;
import cz.uhk.bak.mmsparams.database.model.MessageRecord;
import cz.uhk.bak.mmsparams.database.model.MmsTemplate;
import cz.uhk.bak.mmsparams.mms.OutgoingMediaMessage;
import cz.uhk.bak.mmsparams.recipients.Recipients;
import cz.uhk.bak.mmsparams.service.KeyCachingService;
import cz.uhk.bak.mmsparams.sms.MessageSender;
import cz.uhk.bak.mmsparams.util.AppUtils;
import cz.uhk.bak.mmsparams.util.DialogHelper;
import cz.uhk.bak.mmsparams.util.MmsTemplateUtils;
import cz.uhk.bak.mmsparams.util.Util;

public class MmsTemplateDetailFragment extends DetailFragment<MmsTemplate>
{
    public static final String TAG = MmsTemplateDetailFragment.class.getSimpleName();

    MmsTemplateDetailControls controls;
    private MasterSecret masterSecret;

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_template_detail_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        this.masterSecret = KeyCachingService.getMasterSecret(getApplicationContext());

        controls = new MmsTemplateDetailControls(this, getFragmentManager(), view);

        controls.setMasterSecret(masterSecret);
        setHasOptionsMenu(true);
    }

    public static MmsTemplateDetailFragment newInstance()
    {
        MmsTemplateDetailFragment tm = new MmsTemplateDetailFragment();
        return tm;
    }

    @Override
    protected void setupListeners()
    {

    }

    @Override
    public void onResume()
    {
        String title = getResources().getString(R.string.text_menu_mms_template_browse);
        getActivity().setTitle(title + " Detail");

        super.onResume();
    }

    @Override
    protected void afterLoadData()
    {
        super.afterLoadData();
        controls.afterLoadData(ID);
    }

    @Override
    public void onActivityResult(final int reqCode, int resultCode, Intent data)
    {
        super.onActivityResult(reqCode, resultCode, data);
        controls.onActivityResult(reqCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
//        if (ID >= 0)
//        {
            inflater.inflate(R.menu.menu_send, menu);
//        }

        inflater.inflate(R.menu.menu_save, menu);

        if (ID >= 0)
        {
            inflater.inflate(R.menu.menu_delete, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {
            case R.id.menu_save:
                save(true);
                return true;

            case R.id.menu_delete:
                handleDelete();
                return true;

            case R.id.menu_send:
                handleSend();
                return true;
        }
        return false;
    }


    private void handleSend()
    {
        save(false);


        // Is Default SMS/MMS App?

        if (!Util.isDefaultSmsProvider(getContext()))
        {
            AppUtils.makeDefaultSmsApp(getActivity());
            return;
        }


        if (!MmsTemplateUtils.hasRecipients(instance.getMmsTemplateDetailGroup()))
        {

            Toast.makeText(getContext(), "No Recipients set!", Toast.LENGTH_SHORT).show();

            return;
        }


        OutgoingMediaMessage outgoingMessage = MmsTemplateUtils.createMessage(getContext(), masterSecret, instance);


        ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(getContext());

        final long threadId = threadDatabase.getThreadIdFor(outgoingMessage.getRecipients(), outgoingMessage.getDistributionType());

        final long msgID = stageOutgoingMessage(outgoingMessage, threadId);


        new AsyncTask<OutgoingMediaMessage, Void, Long>()
        {
            @Override
            protected Long doInBackground(OutgoingMediaMessage... messages)
            {
                return MessageSender.sendWithMmsTemplate(getContext(), masterSecret, messages[0], threadId, ID, new SmsDatabase.InsertListener()
                {
                    @Override
                    public void onComplete()
                    {
                        // releaseOutgoingMessage(id);
                    }
                });
            }

            @Override
            protected void onPostExecute(Long result)
            {
                // sendComplete(result);
                // future.set(null);

                getActivity().finish();
            }
        }.execute(outgoingMessage);

    }

    private Recipients getRecipients()
    {
        return controls.getRecipients();
    }

    public long stageOutgoingMessage(OutgoingMediaMessage message, final long threadID)
    {
        MessageRecord messageRecord = DatabaseFactory.getMmsDatabase(getContext()).readerFor(message, threadID).getCurrent();
        return messageRecord.getId();
    }


    private void handleDelete()
    {
        createAndShowAlertDialog();
    }

    private void createAndShowAlertDialog()
    {
        DialogHelper.createAndShowDeleteAlertDialog(getActivity(), new DialogHelper.OnOkListener()
        {
            @Override
            public void OnOk()
            {
                delete();
            }
        }, null);
    }

    private MmsTemplateDatabase getDB()
    {
        return DatabaseFactory.getMmsTemplateDatabase(getApplicationContext());
    }

    @Override
    protected MmsTemplate getById(int id)
    {
        return getDB().getById(id);
    }

    @Override
    protected MmsTemplate getNew()
    {
        MmsTemplate ms = MmsTemplate.createNew();
        return ms;
    }

    @Override
    protected void saveData(MmsTemplate instance)
    {
        long id = getDB().save(instance);

        instance.setId((int) id);

        controls.setID(id);
        controls.onSaveData(id);
        ID = (int) id;
    }

    @Override
    protected void deleteData(int id)
    {
        getDB().deleteWithDrafts(id);
    }

    @Override
    protected void fillControls(MmsTemplate obj)
    {
        controls.fillControls(obj);
    }

    public void setMasterSecret(MasterSecret masterSecret)
    {
        this.masterSecret = masterSecret;
    }

    @Override
    protected MmsTemplate updateModel(MmsTemplate obj)
    {
        return controls.updateModelFromControls(obj);
    }


    @Override
    public boolean handleOnClosing()
    {
        boolean hasChanges = hasChanges();

        if (hasChanges)
        {
            // Promnt for close
            DialogHelper.createAndShowSaveAlertDialog(getActivity(), new DialogHelper.OnOkListener()
                    {
                        @Override
                        public void OnOk()
                        {
                            save(true);
                        }
                    },
                    new DialogHelper.OnCancelListener()
                    {
                        @Override
                        public void OnCancel()
                        {
                            closeFragment();
                        }
                    });

            return false;
        }
        else
        {
            return super.handleOnClosing();
        }

    }

    private boolean hasChanges()
    {
        if (ID < 0)
        {
            return true;
        }
        else
        {
            // get instance from GUI
            MmsTemplate newMT = controls.updateModelFromControls(instance);
            // compare with old

            boolean equal = MmsTemplate.isEqual(newMT, instance);

            if (equal)
                return false;
            else
                return true;
        }
    }


}
