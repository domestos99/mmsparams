package cz.uhk.bak.mmsparams;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.uhk.bak.mmsparams.database.model.ErrorBLO;
import cz.uhk.bak.mmsparams.database.model.MmsError;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.JsonUtils;
import cz.uhk.bak.mmsparams.util.StringUtil;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MmsErrorDetailControls extends DetailControlsBase<MmsError>
{
    public static final String TAG = MmsErrorDetailControls.class.getSimpleName();


    TextView tvTag, tvDateTime, tvInfoMsg, tvExMessage, tvStackTrace;


    protected MmsErrorDetailControls(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }


    @Override
    protected void initControls(View view)
    {
        tvTag = ViewUtil.findById(view, R.id.tvTag);
        tvDateTime = ViewUtil.findById(view, R.id.tvDateTime);
        tvInfoMsg = ViewUtil.findById(view, R.id.tvInfoMsg);
        tvExMessage = ViewUtil.findById(view, R.id.tvExMessage);
        tvStackTrace = ViewUtil.findById(view, R.id.tvStackTrace);
    }

    @Override
    protected void setupListeners()
    {

    }

    @Override
    protected MmsError updateModelFromControls(MmsError obj)
    {
        return obj;
    }

    @Override
    protected void fillControls(MmsError obj)
    {
        tvTag.setText("my tag");


        long ms = obj.getDateCreated();
        if (ms < 0)
            tvDateTime.setText(String.valueOf(ms));

        Date dt = new Date(ms);
        String dateTimeS = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(dt);
        tvDateTime.setText(dateTimeS);


        String json = obj.getError();
        if (StringUtil.isEmptyOrNull(json))
            return;

        try
        {
            ErrorBLO blo = JsonUtils.fromJson(json, ErrorBLO.class);

            tvTag.setText(blo.tag);
            tvExMessage.setText(blo.message);
            tvInfoMsg.setText(blo.info);
            tvStackTrace.setText(blo.stackTrace);
        }
        catch (Exception e)
        {
            LogFB.e(TAG, "setupView", e);
        }


    }

}
