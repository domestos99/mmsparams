package cz.uhk.bak.mmsparams;

import android.os.Bundle;

public abstract class DetailFragment<T> extends BaseFragment
{
    protected T instance;
    protected int ID;
    protected int mode;
    protected static final int DEFAULT_MODE = -1;

    public DetailFragment()
    {
        super();
    }

    @Override
    protected void afterBaseInit()
    {
        loadID();
        load();
        afterLoadData();
        fillControls(instance);
    }

    protected void loadID()
    {
        Bundle extras = this.getArguments();
        ID = extras.getInt("id", -1);
        mode = extras.getInt("mode", DEFAULT_MODE);
    }

    protected void load()
    {
        if (ID == -1 && mode == DEFAULT_MODE)
        {
            instance = getNew();
        }
        else if (ID > -1 && mode == DEFAULT_MODE)
        {
            instance = getById(ID);
        }
        else
        {
            instance = GetForExtraID(ID, mode);
        }

        fillControls(instance);
    }

    protected void afterLoadData()
    {

    }


    protected abstract T getById(int id);

    protected T GetForExtraID(int id, int mode)
    {
        return getNew();
    }


    protected abstract T getNew();

    protected void save(boolean close)
    {
        instance = updateModel(instance);

        saveData(instance);

        if (close)
            super.closeFragment();


    }

    protected void delete()
    {
        deleteData(ID);
        super.closeFragment();
    }

    protected abstract void saveData(T instance);

    protected abstract void deleteData(int id);


    protected abstract void fillControls(T obj);

    /**
     * Update current instance with new values from controls. Should be immutable.
     *
     * @param obj instance of model to update
     * @return update new instance of model
     */
    protected abstract T updateModel(T obj);


}
