package cz.uhk.bak.mmsparams;

import android.support.annotation.NonNull;

import java.util.Locale;
import java.util.Set;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.database.model.ThreadRecord;

public interface BindableConversationListItem extends Unbindable
{

    public void bind(@NonNull MasterSecret masterSecret, @NonNull ThreadRecord thread,
                     @NonNull Locale locale, @NonNull Set<Long> selectedThreads, boolean batchMode);
}
