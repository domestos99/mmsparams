package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MenuItem;

import cz.uhk.bak.mmsparams.crypto.MasterSecret;
import cz.uhk.bak.mmsparams.util.DynamicLanguage;
import cz.uhk.bak.mmsparams.util.DynamicTheme;

public abstract class MyListPassphraseRequiredActionBarActivity extends PassphraseRequiredActionBarActivity
{

    private DynamicTheme dynamicTheme = new DynamicTheme();
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();

    @Override
    protected void onPreCreate()
    {
        dynamicTheme.onCreate(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState, @NonNull MasterSecret masterSecret)
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int mode = getIntent().getIntExtra("mode", -1);
        Bundle bundle = new Bundle();
        bundle.putInt("mode", mode);

        bundle = customBundle(bundle);

        initFragment(android.R.id.content, getListFragment(), masterSecret, dynamicLanguage.getCurrentLocale(), bundle);
    }

    protected Bundle customBundle(Bundle bundle)
    {
        return bundle;
    }


    @NonNull
    protected abstract BaseFragment getListFragment();

    @Override
    public void onResume()
    {
        dynamicTheme.onResume(this);
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        switch (item.getItemId())
        {
            case android.R.id.home:

                boolean handled = beforeActivityFinish();

                if (!handled)
                    finish();
                return true;
        }

        return false;
    }

    protected boolean beforeActivityFinish()
    {
        return false;
    }


}
