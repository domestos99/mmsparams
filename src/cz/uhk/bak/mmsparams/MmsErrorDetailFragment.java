package cz.uhk.bak.mmsparams;

import android.view.View;

import cz.uhk.bak.mmsparams.database.DatabaseFactory;
import cz.uhk.bak.mmsparams.database.MmsErrorDatabase;
import cz.uhk.bak.mmsparams.database.model.MmsError;

public class MmsErrorDetailFragment extends DetailFragment<MmsError>
{
    public static final String TAG = MmsErrorDetailFragment.class.getSimpleName();

    MmsErrorDetailControls controls;


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_error_detail_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        controls = new MmsErrorDetailControls(this, getFragmentManager(), view);
    }

    @Override
    protected void setupListeners()
    {

    }


    @Override
    protected MmsError getById(int id)
    {
        MmsErrorDatabase db = DatabaseFactory.getMmsErrorDatabase(getContext());
        return db.getById(id);
    }

    @Override
    protected MmsError getNew()
    {
       return null;
    }

    @Override
    protected void saveData(MmsError instance)
    {

    }

    @Override
    protected void deleteData(int id)
    {

    }

    @Override
    protected void fillControls(MmsError obj)
    {
        controls.fillControls(obj);
    }

    @Override
    protected MmsError updateModel(MmsError obj)
    {
        return controls.updateModelFromControls(obj);
    }
}
