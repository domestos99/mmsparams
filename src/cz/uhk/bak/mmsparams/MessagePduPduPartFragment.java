package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.mms.ContentType;
import com.google.android.mms.pdu_alt.MultimediaMessagePdu;
import com.google.android.mms.pdu_alt.PduPart;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.jobs.PduByteHelper;
import cz.uhk.bak.mmsparams.util.CharSetHelper;
import cz.uhk.bak.mmsparams.util.ViewUtil;


public class MessagePduPduPartFragment extends MessagePduBaseFragment
{
    private int paramID;
    private int pduPartIndex;
    private PduPart pduPart;

    private MessagePdu messagePdu;

    private static final String PDU_PART_INDEX = "pdu_part_index";

    private TextView tvPduPartCharset, tvPduPartContentType, tvPduPartContentID, tvPduPartContentLocation, tvPduPartName,
            tvPduPartDataUri, tvPduPartContentDisposition, tvPduPartContentTransferEncoding, tvPduPartFilename, tvPduPartDataLength, tvPduPartGenerateLocation, tvPduPartPduPartSize;

    private EditText tvPduPartData;
    public static final String TAG = MessagePduPduPartFragment.class.getSimpleName();

    public static MessagePduPduPartFragment newInstance(int paramID, int pduPartIndex)
    {
        MessagePduPduPartFragment fragment = new MessagePduPduPartFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        args.putInt(PDU_PART_INDEX, pduPartIndex);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_pdu_body_fragment;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        pduPartIndex = getArguments().getInt(PDU_PART_INDEX);

        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        pduPart = ((MultimediaMessagePdu) messagePdu.getGenericPdu()).getBody().getPart(pduPartIndex);
    }

    @Override
    protected void initControls(View view)
    {
        tvPduPartData = ViewUtil.findById(view, R.id.tvPduPartData);
        tvPduPartCharset = ViewUtil.findById(view, R.id.tvPduPartCharset);
        tvPduPartContentType = ViewUtil.findById(view, R.id.tvPduPartContentType);
        tvPduPartContentID = ViewUtil.findById(view, R.id.tvPduPartContentID);
        tvPduPartContentLocation = ViewUtil.findById(view, R.id.tvPduPartContentLocation);
        tvPduPartName = ViewUtil.findById(view, R.id.tvPduPartName);
        tvPduPartDataUri = ViewUtil.findById(view, R.id.tvPduPartDataUri);
        tvPduPartContentDisposition = ViewUtil.findById(view, R.id.tvPduPartContentDisposition);
        tvPduPartContentTransferEncoding = ViewUtil.findById(view, R.id.tvPduPartContentTransferEncoding);
        tvPduPartFilename = ViewUtil.findById(view, R.id.tvPduPartFilename);
        tvPduPartDataLength = ViewUtil.findById(view, R.id.tvPduPartDataLength);
        tvPduPartGenerateLocation = ViewUtil.findById(view, R.id.tvPduPartGenerateLocation);
        tvPduPartPduPartSize = ViewUtil.findById(view, R.id.tvPduPartPduPartSize);

        fillControls();
    }

    private void fillControls()
    {
        tvPduPartCharset.setText(CharSetHelper.getCharSetName(pduPart.getCharset()));
        tvPduPartContentType.setText(getString(pduPart.getContentType()));
        tvPduPartContentID.setText(getString(pduPart.getContentId()));
        tvPduPartContentLocation.setText(getString(pduPart.getContentLocation()));
        tvPduPartName.setText(getString(pduPart.getName()));
        tvPduPartDataUri.setText(getUrlString(pduPart.getDataUri()));
        tvPduPartContentDisposition.setText(getString(pduPart.getContentDisposition()));
        tvPduPartContentTransferEncoding.setText(getString(pduPart.getContentTransferEncoding()));
        tvPduPartFilename.setText(getString(pduPart.getFilename()));
        tvPduPartDataLength.setText(getStringInt(pduPart.getDataLength()));
        tvPduPartGenerateLocation.setText(getString(pduPart.generateLocation()));

        tvPduPartPduPartSize.setText(getStringLong(PduByteHelper.getPartSize(pduPart)));

        resolveData();
    }

    private void resolveData()
    {
        byte[] ct = pduPart.getContentType();
        if (ct == null)
            return;

        String cts = getString(ct);

        if (ContentType.TEXT_PLAIN.equals(cts))
        {
            tvPduPartData.setText(getString(pduPart.getData()));
        }
        else if (ContentType.APP_SMIL.equals(cts))
        {
            String xml = getString(pduPart.getData());

            xml = xml.replace(">", ">\r\n");

            tvPduPartData.setText(xml);
        }
        // Add More here

        else
        {
            tvPduPartData.setText(cts + " byte[]");
        }
    }


//    private void handleText()
//    {
//        if (!TextUtils.isEmpty(message.getBody()))
//        {
//            PduPart part = new PduPart();
//            String name = String.valueOf(System.currentTimeMillis());
//            part.setData(Util.toUtf8Bytes(message.getBody()));
//            part.setCharset(CharacterSets.UTF_8);
//            part.setContentType(ContentType.TEXT_PLAIN.getBytes());
//            part.setContentId(name.getBytes());
//            part.setContentLocation((name + ".txt").getBytes());
//            part.setName((name + ".txt").getBytes());
//
//            body.addPart(part);
//            size += getPartSize(part);
//        }
//    }
//
//
//    private void handleSmil()
//    {
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(body), out);
//        PduPart smilPart = new PduPart();
//        smilPart.setContentId("smil".getBytes());
//        smilPart.setContentLocation("smil.xml".getBytes());
//        smilPart.setContentType(ContentType.APP_SMIL.getBytes());
//        smilPart.setData(out.toByteArray());
//        body.addPart(0, smilPart);
//    }
//
//    private void handlAttachment()
//    {
//        for (Attachment attachment : scaledAttachments)
//        {
//            try
//            {
//                if (attachment.getDataUri() == null)
//                    throw new IOException("Assertion failed, attachment for outgoing MMS has no data!");
//
//                String fileName = attachment.getFileName();
//                PduPart part = new PduPart();
//
//                if (fileName == null)
//                {
//                    fileName = String.valueOf(Math.abs(Util.getSecureRandom().nextLong()));
//                    String fileExtension = MimeTypeMap.getSingleton().getExtensionFromMimeType(attachment.getContentType());
//
//                    if (fileExtension != null) fileName = fileName + "." + fileExtension;
//                }
//
//                if (attachment.getContentType().startsWith("text"))
//                {
//                    part.setCharset(CharacterSets.UTF_8);
//                }
//
//                part.setContentType(attachment.getContentType().getBytes());
//                part.setContentLocation(fileName.getBytes());
//                part.setName(fileName.getBytes());
//
//                int index = fileName.lastIndexOf(".");
//                String contentId = (index == -1) ? fileName : fileName.substring(0, index);
//                part.setContentId(contentId.getBytes());
//                part.setData(Util.readFully(PartAuthority.getAttachmentStream(context, masterSecret, attachment.getDataUri())));
//
//                body.addPart(part);
//                size += getPartSize(part);
//            }
//            catch (IOException e)
//            {
//                LogFB.w(TAG, e);
//            }
//        }
//    }


    @Override
    protected void setupListeners()
    {

    }


}
