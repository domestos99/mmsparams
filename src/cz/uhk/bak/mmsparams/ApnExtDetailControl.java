package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.view.View;
import android.widget.EditText;

import cz.uhk.bak.mmsparams.mms.ApnExt;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class ApnExtDetailControl
{
    private final Context context;

    EditText tbName, tbCarrier, tbApn, tbMmsc, tbProxy, tbPort, tbUserName, tbPassword;


    public ApnExtDetailControl(Context context, View view)
    {
        this.context = context;


        tbName = ViewUtil.findById(view, R.id.tbName);
        tbCarrier = ViewUtil.findById(view, R.id.tbCarrier);
        tbApn = ViewUtil.findById(view, R.id.tbApn);
        tbMmsc = ViewUtil.findById(view, R.id.tbMMSC);
        tbProxy = ViewUtil.findById(view, R.id.tbProxy);
        tbPort = ViewUtil.findById(view, R.id.tbPort);
        tbUserName = ViewUtil.findById(view, R.id.tbUsername);
        tbPassword = ViewUtil.findById(view, R.id.tbPassword);
    }

    public void fillControls(ApnExt obj)
    {

        tbName.setText(obj.getName());
        tbCarrier.setText(obj.getCarrier());
        tbApn.setText(obj.getApn());
        tbMmsc.setText(obj.getMmsc());
        tbProxy.setText(obj.getMmsproxy());
        tbPort.setText(String.valueOf(obj.getMmsport()));
        tbUserName.setText(obj.getUserName());
        tbPassword.setText(obj.getPassword());


        if (obj.isSystem())
            setReadOnly();

    }

    private void setReadOnly()
    {
        tbName.setEnabled(false);
        tbCarrier.setEnabled(false);
        tbApn.setEnabled(false);
        tbMmsc.setEnabled(false);
        tbProxy.setEnabled(false);
        tbPort.setEnabled(false);
        tbUserName.setEnabled(false);
        tbPassword.setEnabled(false);
    }

    public ApnExt updateModelFromControls(ApnExt obj)
    {
        ApnExt apn = new ApnExt(obj);


        apn.setName(tbName.getText().toString());
        apn.setCarrier(tbCarrier.getText().toString());
        apn.setApn(tbApn.getText().toString());
        apn.setMmsc(tbMmsc.getText().toString());
        apn.setMmsproxy(tbProxy.getText().toString());
        apn.setMmsport(tbPort.getText().toString());
        apn.setUsername(tbUserName.getText().toString());
        apn.setPassword(tbPassword.getText().toString());

        return apn;
    }
}
