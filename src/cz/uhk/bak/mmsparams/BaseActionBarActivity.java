package cz.uhk.bak.mmsparams;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;

import java.lang.reflect.Field;

import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.AndroidVersionUtil;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;


public abstract class BaseActionBarActivity extends AppCompatActivity
{
    private static final String TAG = BaseActionBarActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        if (BaseActivity.isMenuWorkaroundRequired())
        {
            forceOverflowMenu();
        }
        super.onCreate(savedInstanceState);

        if (BaseActivity.HANGLE_ERRORS)
        {
            handleErrors();
        }
    }

    protected void handleErrors()
    {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread thread, Throwable e)
            {
                handleUncaughtException(thread, e);
            }
        });
    }

    public void handleUncaughtException(Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        LogFB.e("ActivityBase", "error", e);

        openErrorForm(this, e);

        System.exit(1); // kill off the crashed app
    }

    public static void openErrorForm(Context context, Throwable t)
    {
        Intent intent = new Intent(context, ShowErrorActivity.class);
        intent.putExtra("error", t);
        context.startActivity(intent);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        initializeScreenshotSecurity();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        return (keyCode == KeyEvent.KEYCODE_MENU && BaseActivity.isMenuWorkaroundRequired()) || super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_MENU && BaseActivity.isMenuWorkaroundRequired())
        {
            openOptionsMenu();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void initializeScreenshotSecurity()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH &&
                TextSecurePreferences.isScreenSecurityEnabled(this))
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
        else
        {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    /**
     * Modified from: http://stackoverflow.com/a/13098824
     */
    private void forceOverflowMenu()
    {
        try
        {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null)
            {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (IllegalAccessException e)
        {
            LogFB.w(TAG, "Failed to force overflow menu.");
        }
        catch (NoSuchFieldException e)
        {
            LogFB.w(TAG, "Failed to force overflow menu.");
        }
    }

    protected void startActivitySceneTransition(Intent intent, View sharedView, String transitionName)
    {
        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(this, sharedView, transitionName)
                .toBundle();
        ActivityCompat.startActivity(this, intent, bundle);
    }

    @TargetApi(VERSION_CODES.LOLLIPOP)
    protected void setStatusBarColor(int color)
    {
        if (AndroidVersionUtil.isHigherThanLollipop())
        {
            getWindow().setStatusBarColor(color);
        }
    }

}
