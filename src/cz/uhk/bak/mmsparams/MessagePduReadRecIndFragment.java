package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.ReadRecInd;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduReadRecIndFragment extends MessagePduBaseFragment
{
    private int paramID;
    private ReadRecInd readRecInd;
    private MessagePdu messagePdu;

    private TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMmsTo, tvMmsDate, tvMmsMessageID, tvMmsReadStatus, tvPduCreated;
    public static final String TAG = MessagePduReadRecIndFragment.class.getSimpleName();

    public static MessagePduReadRecIndFragment newInstance(int paramID)
    {
        MessagePduReadRecIndFragment fragment = new MessagePduReadRecIndFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        readRecInd = (ReadRecInd) messagePdu.getGenericPdu();
    }

    private void fillControls()
    {

        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getString(readRecInd.getFrom()));
            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(readRecInd.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(readRecInd.getMmsVersion()));
            tvMmsTo.setText(getToAsString(readRecInd.getTo()));
            tvMmsDate.setText(getDateTime(readRecInd.getDate()));
            tvMmsMessageID.setText(getString(readRecInd.getMessageId()));
            tvMmsReadStatus.setText(PduHeaderConvertor.getReadStatusString(readRecInd.getReadStatus()));

        }
        catch (Exception e)
        {
            int k = 0;
        }
    }

    private String getBody(PduBody body)
    {

        return "";
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_read_rec_ind_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);


        tvMmsTo = ViewUtil.findById(view, R.id.tvMmsTo);
        tvMmsDate = ViewUtil.findById(view, R.id.tvMmsDate);
        tvMmsMessageID = ViewUtil.findById(view, R.id.tvMmsMessageID);
        tvMmsReadStatus = ViewUtil.findById(view, R.id.tvMmsReadStatus);


        fillControls();
    }

    @Override
    protected void setupListeners()
    {

    }

}
