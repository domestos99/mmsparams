package cz.uhk.bak.mmsparams.push;

import android.content.Context;

import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

import cz.uhk.bak.mmsparams.crypto.SecurityEvent;

public class SecurityEventListener implements SignalServiceMessageSender.EventListener
{

    private static final String TAG = SecurityEventListener.class.getSimpleName();

    private final Context context;

    public SecurityEventListener(Context context)
    {
        this.context = context.getApplicationContext();
    }

    @Override
    public void onSecurityEvent(SignalServiceAddress textSecureAddress)
    {
        SecurityEvent.broadcastSecurityUpdateEvent(context);
    }
}
