package cz.uhk.bak.mmsparams.push;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.security.ProviderInstaller;

import org.whispersystems.signalservice.api.SignalServiceAccountManager;

import cz.uhk.bak.mmsparams.BuildConfig;
import cz.uhk.bak.mmsparams.log.LogFB;
import cz.uhk.bak.mmsparams.util.TextSecurePreferences;

public class AccountManagerFactory
{

    private static final String TAG = AccountManagerFactory.class.getName();

    public static SignalServiceAccountManager createManager(Context context)
    {
        return new SignalServiceAccountManager(new SignalServiceNetworkAccess(context).getConfiguration(context),
                TextSecurePreferences.getLocalNumber(context),
                TextSecurePreferences.getPushServerPassword(context),
                BuildConfig.USER_AGENT);
    }

    public static SignalServiceAccountManager createManager(final Context context, String number, String password)
    {
        if (new SignalServiceNetworkAccess(context).isCensored(number))
        {
            new AsyncTask<Void, Void, Void>()
            {
                @Override
                protected Void doInBackground(Void... params)
                {
                    try
                    {
                        ProviderInstaller.installIfNeeded(context);
                    }
                    catch (Throwable t)
                    {
                        LogFB.w(TAG, t);
                    }
                    return null;
                }
            }.execute();
        }

        return new SignalServiceAccountManager(new SignalServiceNetworkAccess(context).getConfiguration(number),
                number, password, BuildConfig.USER_AGENT);
    }

}
