package cz.uhk.bak.mmsparams.push;


import android.content.Context;

import org.whispersystems.signalservice.api.push.TrustStore;

import java.io.InputStream;

import cz.uhk.bak.mmsparams.R;

public class GoogleFrontingTrustStore implements TrustStore
{

    private final Context context;

    public GoogleFrontingTrustStore(Context context)
    {
        this.context = context.getApplicationContext();
    }

    @Override
    public InputStream getKeyStoreInputStream()
    {
        return context.getResources().openRawResource(R.raw.censorship_fronting);
    }

    @Override
    public String getKeyStorePassword()
    {
        return "whisper";
    }

}
