package cz.uhk.bak.mmsparams;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import cz.uhk.bak.mmsparams.log.LogFB;

public abstract class BaseFragmentLight extends Fragment
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (BaseActivity.HANGLE_ERRORS)
        {
            handleErrors();
        }
    }

    protected Context getApplicationContext()
    {
        return getContext().getApplicationContext();
    }


    protected void openFragment(BaseFragment fragment, Bundle args)
    {
        if (args != null)
            fragment.setArguments(args);

        this.getFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, fragment.getMyTag())
                .addToBackStack(fragment.getMyTag())
                .commit();

    }

    protected BaseFragment getActiveFragment()
    {
        final FragmentManager fm = this.getFragmentManager();

        if (fm == null || fm.getBackStackEntryCount() == 0)
        {
            return null;
        }
        String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
        return (BaseFragment) fm.findFragmentByTag(tag);
    }


    protected void closeFragment()
    {
        getActivity().onBackPressed();
    }


    private void handleErrors()
    {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread thread, Throwable e)
            {
                handleUncaughtException(thread, e);
            }
        });
    }

    public void handleUncaughtException(Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        LogFB.e("FragmentBase", "error", e);

        //   ErrorUtils.openErrorForm(getApplicationContext(), e);

        Intent intent = new Intent(getApplicationContext(), ShowErrorActivity.class);
        intent.putExtra("error", e);
        startActivity(intent);


        System.exit(1); // kill off the crashed app
    }
}
