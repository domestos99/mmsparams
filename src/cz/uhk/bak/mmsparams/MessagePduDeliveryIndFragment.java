package cz.uhk.bak.mmsparams;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.DeliveryInd;

import cz.uhk.bak.mmsparams.database.model.MessagePdu;
import cz.uhk.bak.mmsparams.util.PduHeaderConvertor;
import cz.uhk.bak.mmsparams.util.ViewUtil;

public class MessagePduDeliveryIndFragment extends MessagePduBaseFragment
{
    private int paramID;
    private DeliveryInd deliveryInd;
    private MessagePdu messagePdu;

    TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMmsTo, tvMmsDate, tvMmsMessageID, tvMmsStatus, tvPduCreated;
    public static final String TAG = MessagePduDeliveryIndFragment.class.getSimpleName();

    public static MessagePduDeliveryIndFragment newInstance(int paramID)
    {
        MessagePduDeliveryIndFragment fragment = new MessagePduDeliveryIndFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }


    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        deliveryInd = (DeliveryInd) messagePdu.getGenericPdu();
    }

    private void fillControls()
    {
        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getString(deliveryInd.getFrom()));

            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(deliveryInd.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(deliveryInd.getMmsVersion()));

            tvMmsMessageID.setText(getString(deliveryInd.getMessageId()));

            tvMmsDate.setText(getDateTime(deliveryInd.getDate()));
            tvMmsStatus.setText(PduHeaderConvertor.getStatusString(deliveryInd.getStatus()));
            tvMmsTo.setText(getToAsString(deliveryInd.getTo()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_delivery_ind_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);

        tvMmsTo = ViewUtil.findById(view, R.id.tvMmsTo);
        tvMmsDate = ViewUtil.findById(view, R.id.tvMmsDate);
        tvMmsMessageID = ViewUtil.findById(view, R.id.tvMmsMessageID);
        tvMmsStatus = ViewUtil.findById(view, R.id.tvMmsStatus);


        fillControls();
    }

    @Override
    protected void setupListeners()
    {

    }


}
