package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MySendReq extends SendReq
{
    public MySendReq()
    {
        super();
    }

    public MySendReq(byte[] contentType, EncodedStringValue from, int mmsVersion, byte[] transactionId) throws InvalidHeaderValueException
    {
        super(contentType, from, mmsVersion, transactionId);
    }

    public MySendReq(PduHeaders headers)
    {
        super(headers);
    }

    public MySendReq(PduHeaders headers, PduBody body)
    {
        super(headers, body);
    }


    public void setDeliveryTime(long value)
    {
        mPduHeaders.setLongInteger(value, PduHeaders.DELIVERY_TIME);
    }

    public long getDeliveryTime()
    {
        return mPduHeaders.getLongInteger(PduHeaders.DELIVERY_TIME);
    }

    public void setSenderIsVisible(boolean visible) throws InvalidHeaderValueException
    {
        // zrejme je to krizem - proverit
        if (visible)
        {
            mPduHeaders.setOctet(PduHeaders.SENDER_VISIBILITY_SHOW, PduHeaders.SENDER_VISIBILITY);
        }
        else
        {
            mPduHeaders.setOctet(PduHeaders.SENDER_VISIBILITY_HIDE, PduHeaders.SENDER_VISIBILITY);
        }
    }

    public int getSenderIsVisible()
    {
        int value = mPduHeaders.getOctet(PduHeaders.SENDER_VISIBILITY);

        if (value == PduHeaders.SENDER_VISIBILITY_SHOW)
            return 1;
        else if (value == PduHeaders.SENDER_VISIBILITY_HIDE)
            return 0;
        else
            return -1;
    }

    public int getSenderIsVisible2()
    {
        return mPduHeaders.getOctet(PduHeaders.SENDER_VISIBILITY);
    }


    public int getAdaptationAllowed()
    {
        return mPduHeaders.getOctet(PduHeaders.ADAPTATION_ALLOWED);
    }

    public void setAdaptationAllowed(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.ADAPTATION_ALLOWED);
    }


    public int getDrmContent()
    {
        return mPduHeaders.getOctet(PduHeaders.DRM_CONTENT);
    }

    public void setDrmContent(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.DRM_CONTENT);
    }


}
