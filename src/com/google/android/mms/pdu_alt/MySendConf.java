package com.google.android.mms.pdu_alt;


import com.google.android.mms.InvalidHeaderValueException;

public class MySendConf extends SendConf
{
    public MySendConf() throws InvalidHeaderValueException
    {
        super();
    }

    public MySendConf(PduHeaders headers)
    {
        super(headers);
    }


    public EncodedStringValue getResponseText()
    {
        return mPduHeaders.getEncodedStringValue(PduHeaders.RESPONSE_TEXT);
    }


     /*
     * Optional, not supported header fields:
     *
     *    public byte[] getContentLocation() {return null;}
     *    public void setContentLocation(byte[] value) {}
     *
     *    public EncodedStringValue getResponseText() {return null;}
     *    public void setResponseText(EncodedStringValue value) {}
     *
     *    public byte getStoreStatus() {return 0x00;}
     *    public void setStoreStatus(byte value) {}
     *
     *    public byte[] getStoreStatusText() {return null;}
     *    public void setStoreStatusText(byte[] value) {}
     */
}
