package com.google.android.mms.pdu_alt;


import com.google.android.mms.InvalidHeaderValueException;

public class MyRetrieveConf extends RetrieveConf
{
    public MyRetrieveConf() throws InvalidHeaderValueException
    {
        super();
    }

    public MyRetrieveConf(PduHeaders headers)
    {
        super(headers);
    }

    public MyRetrieveConf(PduHeaders headers, PduBody body)
    {
        super(headers, body);
    }

    public int getDrmContent()
    {
        return mPduHeaders.getOctet(PduHeaders.DRM_CONTENT);
    }

     /*
     * Optional, not supported header fields:
     *
     *     public byte[] getApplicId() {return null;}
     *     public void setApplicId(byte[] value) {}
     *
     *     public byte[] getAuxApplicId() {return null;}
     *     public void getAuxApplicId(byte[] value) {}
     *
     *     public byte getContentClass() {return 0x00;}
     *     public void setApplicId(byte value) {}
     *
     *     public byte getDrmContent() {return 0x00;}
     *     public void setDrmContent(byte value) {}
     *
     *     public byte getDistributionIndicator() {return 0x00;}
     *     public void setDistributionIndicator(byte value) {}
     *
     *     public PreviouslySentByValue getPreviouslySentBy() {return null;}
     *     public void setPreviouslySentBy(PreviouslySentByValue value) {}
     *
     *     public PreviouslySentDateValue getPreviouslySentDate() {}
     *     public void setPreviouslySentDate(PreviouslySentDateValue value) {}
     *
     *     public MmFlagsValue getMmFlags() {return null;}
     *     public void setMmFlags(MmFlagsValue value) {}
     *
     *     public MmStateValue getMmState() {return null;}
     *     public void getMmState(MmStateValue value) {}
     *
     *     public byte[] getReplaceId() {return 0x00;}
     *     public void setReplaceId(byte[] value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     *
     *     public byte getReplyCharging() {return 0x00;}
     *     public void setReplyCharging(byte value) {}
     *
     *     public byte getReplyChargingDeadline() {return 0x00;}
     *     public void setReplyChargingDeadline(byte value) {}
     *
     *     public byte[] getReplyChargingId() {return 0x00;}
     *     public void setReplyChargingId(byte[] value) {}
     *
     *     public long getReplyChargingSize() {return 0;}
     *     public void setReplyChargingSize(long value) {}
     */
}
