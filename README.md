# README #

[Google Play](https://play.google.com/store/apps/details?id=cz.uhk.bak.mmsparams)


## License

This project has been taken from https://github.com/WhisperSystems/Signal-Android and modified.

Licensed under the GPLv3: http://www.gnu.org/licenses/gpl-3.0.html

Google Play and the Google Play logo are trademarks of Google Inc.
